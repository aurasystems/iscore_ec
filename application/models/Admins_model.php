<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admins_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function get_admins(){
        $this->db->where("admin",1);
        $query=$this->db->get("users");
        return $query->result();
    }
	
	function get_user_id($user){
		$this->db->order_by('id','DESC');
		$user_row=$this->db->get_where('users',array('user'=>$user,'admin'=>1))->row();
		if($user_row){
			return $user_row->id;
		}
		return false;
	}
}