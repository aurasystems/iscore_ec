<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Back_settings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->model('Back_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index($status = NULL)
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $data = array('title' => 'Back Settings');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
            $data['label_att'] = array ('class' => 'col-sm-4 control-label');
            $data['back_settings'] = $this->Back_settings_model->get_back_settings();
            $this->load->view("pages/settings/back_settings", $data);
        }
        else
        {
            redirect('Dashboard');
        }
    }
    public function update()
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('system_name', 'System Name', 'trim|required');
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            { 
                $data = array(
                    'system_name' => $this->input->post('system_name')
                );
                if($_FILES['userfile']['name'] != '')
                {
                    $config['upload_path'] = './uploads/settings/back';
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
                    $config['max_size'] = '10000';
                    $config['max_width'] = '5000';
                    $config['max_height'] = '5000';
                    $this->lang->load('upload');
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload())
                    {
                        $upload_data = $this->upload->data();
                        $data['system_logo'] = $upload_data['file_name'];
                    }
                }
                $this->Back_settings_model->update_settings($data);
            }
        }
        else
        {
            redirect('Dashboard');
        } 
    }
}