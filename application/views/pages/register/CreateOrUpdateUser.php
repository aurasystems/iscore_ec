<!DOCTYPE html>
<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/icheck/skins/all.css" rel="stylesheet"/>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account separate-inputs boxed <?= $this->lang->line('system_dir') ?>" data-page="signup">
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3">
                    <div class="account-wall">
                        <div class="text-center" style="margin-bottom: 20px">
                            <img src="<?= base_url() ?>uploads/content/Untitled-1-1.png" style="width:40%" />
                        </div>
                        <?= flash_success() ?>
                        <?= flash_error() ?>
                        <?php $attributes = array('id' => 'submit_form', 'class' => 'form-signup', 'role' => 'form'); ?>
                        <form method="POST" action="" class="form-horizontal" role="form">
                            <div class="row">
                                <?= csrf() ?>
                                <?php
                                $fields = ["user" => "", "clientName" => "", "address" => "", "phoneNumber" => "", "password" => "", "confirm_password" => "", "email" => ""];
                                foreach ($fields as $field => $field_label) {
                                    $name = $field;
                                    $label = $name;
                                    $type = "text";
                                    if ($field_label) {
                                        $label = $field_label;
                                    }
                                    if ($field == "password" || $field == "confirm_password") {
                                        $type = "password";
                                    }
                                    echo " <div class='col-sm-6'>
                                            <div class='append-icon'>
                                                <input type='$type' id='$name' name='$name' " . ($field == "password" || $field == "confirm_password" ? 'autocomplete="off"' : '') . " placeholder='{$this->lang->line($label)}' class='form-control form-white' value='{$this->input->post($name)}'/>
                                                 " . form_error($name) . "
                                            </div>
                                        </div>";
                                    if ($field == "confirm_password") {
                                        echo ' <div class="row">
                                        <div class="col-sm-6 progress" style="display: none;width: 42%;
    margin: 10px 31px;">
                                            <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            </div>
                                        </div>
                                    </div>';
                                    }
                                }
                                ?>


                            </div>

                            <div class="text-center">
                                <?php echo form_submit(array('name' => 'submit', 'id' => 'submit', 'class' => 'btn btn-lg btn-dark m-t-20', 'data-style' => 'expand-left', 'value' => $this->lang->line('lang_register'), 'style' => 'background-color: #1D4B8E')); ?>
                            </div>
                            <div class="text-center">
                                <a href="<?= base_url() ?>Login"><?= $this->lang->line('lang_have_account') ?></a>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOCKSCREEN BOX -->
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/js/plugins.js"></script>
        <?php $this->load->view('private/scripts/login_js'); ?>
        <script>
            $(document).ready(function () {
                //            BEGIN PASSWORD STRENGTH
<?php if (!empty($password_management) && $password_management->complex) { ?>
                    $('#password').keyup(function () {
                        checkStrength($('#password').val());
                    });
                    function checkStrength(password) {

                        var strength = 0;
    <?php
    if (!empty($password_management) && $password_management->min_length) {
        ?>
                            var min_length = "<?= $password_management->min_length ?>";
                            if (password.length < min_length) {
                                progress_bar(25, "progress-bar-danger", "<?= $this->lang->line("too_short") ?>");
                            } else {
                                strength += 1;
                            }
        <?php
    } else {
        ?>
                            strength += 1;
        <?php
    }
    ?>
                        // If password contains both lower and uppercase characters, increase strength value.
                        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                            strength += 1;
                        // If it has numbers and characters, increase strength value.
                        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                            strength += 1;
                        // If it has one special character, increase strength value.
                        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // If it has two special characters, increase strength value.
                        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // Calculated strength value, we can return messages
                        // If value is less than 2
                        console.log(strength);
                        if (strength < 2) {
                            progress_bar(50, "progress-bar-danger", "<?= $this->lang->line("weak") ?>");
                        } else if (strength <= 3) {
                            progress_bar(75, "progress-bar-warning", "<?= $this->lang->line("medium") ?>");
                        } else {
                            progress_bar(100, "progress-bar-success", "<?= $this->lang->line("strong") ?>");
                        }
                    }


                    function progress_bar(per, c, text) {
                        $("#result").css("width", per + "%");
                        $("#result").attr("aria-valuenow", per);
                        $("#result").removeClass("progress-bar-danger");
                        $("#result").removeClass("progress-bar-warning");
                        $("#result").removeClass("progress-bar-success");
                        $("#result").addClass(c);
                        $("#result").html(text);
                        $(".progress").show();
                        if (per == 100) {
                            $("#submit").removeAttr('disabled');
                        } else {
                            $("#submit").attr("disabled", "disabled");
                        }
                    }
<?php } ?>
//            END PASSWORD STRENGTH
            });
        </script>
    </body>
</html>