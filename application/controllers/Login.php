<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(1);
        $this->load->model('Login_model');
        $this->load->library("Ad_lib");
    }

    public function index() {
        if (logged_in()) {
            redirect(base_url("Search/PublicationSearch"));
        } else {
            $data['title'] = 'Sign in';
            $this->load->helper('captcha');
            $config = array(
                'img_url' => base_url() . 'uploads/',
                'img_path' => 'uploads/',
                'colors' => array(
                    'background' => array(255, 255, 255),
                    'border' => array(255, 255, 255),
                    'text' => array(0, 0, 0),
                    //'grid' => array(155, 164, 171),
                    'grid' => array(199, 87, 87)
                )
            );
            $captcha = create_captcha($config);
            $this->session->unset_userdata('valuecaptchaCode');
           // echo $captcha['word'] . '----------';
            $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
            $data['captchaImg'] = base_url() . 'uploads/' . $captcha['filename'];
            $this->load->view("pages/login", $data);
        }
    }

    public function validate_captcha() {
        //return true;
        if ($this->input->post('captcha') != $this->session->userdata['valuecaptchaCode']) {
            $this->form_validation->set_message('validate_captcha', $this->lang->line('captcha_code_is_wrong'));
            return false;
        } else {
            return true;
        }
    }

    public function refresh_captcha() {
        // Captcha configuration
        $config = array(
            'img_url' => base_url() . 'uploads/',
            'img_path' => 'uploads/',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                //'grid' => array(155, 164, 171),
                'grid' => array(199, 87, 87)
            )
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and set new captcha word
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        // Display captcha image link
        echo base_url('uploads/' . $captcha['filename']);
    }

    public function do_login() {
        $this->load->library('Ad_lib');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha');
        if ($this->form_validation->run() == FALSE) {
            $this->index();
        } else {
            $username = $this->input->post('username');
            $password = md5($this->input->post('password'));
            $error = $this->ad_lib->check_login($username, $this->input->post('password'));
           // $error=1;
            $user = $this->Login_model->check_login($username, $password);
//            if ($error == 0) {  // Must reset user password
//                if (!$user) {
//                    $data = array(
//                        "user" => $this->input->post("username"),
//                        "clientName" => $this->input->post("username"),
//                        "subscription" => 0,
//                        "password" => md5($this->input->post("password")),
//                    );
//                    $user_id = $this->add_user($data);
//                }
//                $this->session->set_userdata('first_login', 1);
//                redirect('user/reset_password');
//            } else
            if ($error == 1) {


                if ($user) {

                    $this->check_password_lock($user);
                    $this->check_password_expiry($user);
                    $session_data = $this->set_session($user);

                    $this->insert_session($user);

                    if ($user->admin == 1) {
                        save_audit('Admin logged in with username: ' . $this->input->post('username'));
                        redirect('Dashboard');
                    } else {
                        //$ip=$this->config->item('search_server_ip');
                        //redirect_post("$ip/Search/PublicationSearch",$session_data);
                        $this->check_subscription($user->id);
                        save_audit('User logged in with username: ' . $this->input->post('username'));
                        redirect('Search/PublicationSearch');
                    }
                } else {        // user not exist in my db
                    $data = array(
                        "user" => strtolower($this->input->post("username")),
                        "clientName" => $this->input->post("username"),
                        "subscription" => 0,
                        "password" => md5($this->input->post("password"))
                    );
                    $user_id = $this->add_user($data);

                    $user_row = $this->db->get_where('users', array('id' => $user_id))->row();
                    $this->insert_session($user_row);

                    $data['id'] = $user_id;

                    $session_data = $this->set_session_first_time($data);
                    $this->check_subscription($user_id);
                    save_audit('User logged in');
                    redirect('Search/PublicationSearch');
                }
            } else {
                //$this->session->set_flashdata('msg', $error);
                $this->session->set_flashdata('msg', 'Invalid Username/Password.');
		redirect(current_url());
            }
        }
    }

    function insert_session($user) {
        $this->db->update('SESSIONS', array('LOGOUT' => 1), array('USER_ID' => $user->id));
        $this->db->insert('SESSIONS', array('USER_ID' => $user->id, 'USER_ADMIN' => $user->admin,
            'LOGIN_TIME' => date('Y-m-d H:i:s')));
        $this->db->order_by('ID', 'DESC');
        $session = $this->db->get('SESSIONS')->row();

        $this->session->set_userdata('session_id', $session->ID);
    }

    function add_user($data) {

        $data['admin'] = 0;
        $this->Global_model->global_insert("users", $data);

        $this->load->model('Users_model');
        $user_id = $this->Users_model->get_user_id($data['user']);


        //insert password
        $pass_data = [
            "user_id" => $user_id,
            "password" => $data['password'],
            "timestamp" => date("Y-m-d h:i:s")
        ];
        save_audit('new search user added while login with username: ' . $data['user']);
        $this->Global_model->global_insert("user_passwords", $pass_data);
        return $user_id;
    }

    //TODO
    function ad_authenticate($admin = NULL) {
        if ($admin) {
            //authenticate from admin AD
        } else {
            //authenticate from users AD
        }
        //active directory authentication
//        $username="cn=read-only-admin";
//        $password="password";
//        $bind=  $this->ad_lib->authenticate($username,$password);
//        if($bind['error'])
//        {
//            $this->session->set_flashdata('msg', $bind['error']);
//            redirect(current_url());
//        }
    }

    function Logout() {
        $website_link = $this->config->item('website_link');
        if (($this->session->userdata('user_id') != "")) {
            $this->db->update('SESSIONS', array('LOGOUT' => 1), array('ID' => $this->session->userdata('session_id')));
            $new_data = array(
                'user_id' => '',
                'user_email' => '',
                'user_display_name' => '',
                'logged_in' => FALSE
            );
            $this->session->unset_userdata($new_data);
            $this->session->sess_destroy();

            save_audit('logged out');
            redirect($website_link);
        } else {
            redirect($website_link);
        }
    }

    function authenticate() {
        $username = "admin";
        $password = "12";
        $username = "read-only-admin";
        $username = "uid=tesla,dc=example,dc=com";
        $password = "password";
        $bind = $this->ad_lib->authenticate($username, $password);
        var_dump($bind);
    }

    function check_password_lock($user) {
        $password_management = $this->Login_model->get_password_management();
        if ($password_management) {

            if (!empty($user->lockout_counter) && $user->lockout_counter >= $password_management->lockout_threshold) {
                //calculate lockout duration
                $mins = floor((strtotime("now") - strtotime($user->lockout_timestamp)) / 60);
                if ($mins < $password_management->lockout_duration) {
                    //destroy session and redirect to lockout page
                    $this->session->sess_destroy();
                    redirect(base_url("admin/Login/lockout/$password_management->lockout_duration"));
                }
                //if eligibile for counter reset
                if ($mins >= $password_management->reset_counter) {
                    $reset = array(
                        "lockout_counter" => 0,
                        "lockout_timestamp" => NULL
                    );
                    $this->Global_model->global_update("users", $user->id, $reset);
                }
            }
        }
    }

    function check_password_expiry($check_login_result) {
        $password_management = $this->Login_model->get_password_management();
        if ($password_management) {
            //check if force reset password is applied
//            if ($check_login_result->force_reset) {
//                $this->session->set_userdata(array("temp_id" => $check_login_result->id));
//                //redirect to change password
//                redirect(base_url('Users/change_password'));
//            }
            //check password settings
            $user_id = $check_login_result->id;
            $last_password = $this->Login_model->get_last_password($user_id);

            $days = floor((strtotime("now") - strtotime($last_password->timestamp)) / 86400);
            // check if password age expired
            if ($days >= $password_management->max_age) {
                $update = array('force_reset' => 1);
                $this->Global_model->global_update("users", $user_id, $update);
//                $this->session->set_userdata(array("temp_id" => $user_id));
                //redirect to change password
//                redirect(base_url('Users/change_password'));
            }
            //end check password settings
        }
    }

    function set_session($user) {
        //add user data to session
        $new_data = array(
            'user' => $user->user,
            'user_id' => $user->id,
            'user_email' => $user->email,
            'user_display_name' => $user->user,
            'admin' => $user->admin,
            'logged_in' => TRUE,
            'force_reset' => $user->force_reset,
            'phone_number' => $user->phoneNumber
        );

        $this->session->set_userdata($new_data);
        return $new_data;
    }

    function set_session_first_time($user) {
        $new_data = array(
            'user' => $user['user'],
            'user_id' => $user['id'],
            'admin' => 0,
            'force_reset' => isset($user['force_reset']) ? $user['force_reset'] : 0,
            'user_display_name' => $user['user'],
            'logged_in' => TRUE,
        );
        $this->session->set_userdata($new_data);
        return $new_data;
    }

    function check_subscription($user_id) {
        $this->load->model("Subscription_model");
        $sub = $this->Subscription_model->check_subscription($user_id);
        if (!$sub) {
            redirect(base_url("Subscription/choose_subscription"));
        }
    }

    function show_cols() {

        $this->db->query('ALTER TABLE CREDITOR ADD (LOGIN_CODE VARCHAR2(4))');
        print_r($this->db->query("SELECT column_name
FROM USER_TAB_COLUMNS
WHERE table_name = 'CREDITOR'")->result());
    }

}
