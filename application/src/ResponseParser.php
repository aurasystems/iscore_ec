<?php

class ResponseParser{
    protected $response;
    public function __construct($response) {
        $this->response=$response;
    }
    
    public function ParsePublicationSearch(){
        $this->checkFault();
        if(!empty($this->response["respList"])){
            $list=  $this->response["respList"];
            return $list;
        }
        
        return FALSE;
    }
    
    protected function checkFault(){
        if(isset($this->response['faultstring'])){
            //set flash
            $this->session->set_flashdata("error",  $this->response['faultstring']);
            redirect(current_url());
        }
    }
    
    public function ParseSearchHistory(){
        $this->checkFault();
        if(!empty($this->response["respList"])){
            $list=  $this->response["respList"];
            return $list;
        }
        
        return FALSE;
    }
}

