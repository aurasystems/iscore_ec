<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends CI_Model {

    function get_password_management() {
        $query = $this->db->get("password_management");
        return $query->row();
    }

    function get_active_directory() {
        $query = $this->db->get("active_directory");
        return $query->row();
    }

    function get_settings($table) {
        $query = $this->db->get($table);
        return $query->row();
    }

    function get_audit_trail_settings() {
        $this->db->select('id,audits_expiry,audits_file_path');
        return $this->db->get('back_settings')->row();
    }

}
