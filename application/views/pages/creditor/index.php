<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("creditors") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <table class="table table-dynamic table-tools">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= $this->lang->line('name') ?></th>
                                                <th><?= $this->lang->line('username') ?></th>
                                                <th><?= $this->lang->line('phoneNumber') ?></th>
                                                <th><?= $this->lang->line('email') ?></th>
                                                <th><?= $this->lang->line('date_of_birth') ?></th>
                                                <th><?= $this->lang->line('lang_address') ?></th>
                                                <th><?= $this->lang->line('creditor_type') ?></th>
                                                <th><?= $this->lang->line('nationality') ?></th>
                                                <th><?= $this->lang->line('nationalID') ?></th>
                                                <th><?= $this->lang->line('passportNumber') ?></th>
                                                <th><?= $this->lang->line('issuanceCountry') ?></th>
                                                <th><?= $this->lang->line('mobile_confirmed') ?></th>
                                                <th><?= $this->lang->line('reg_date') ?></th>

<th><?= $this->lang->line('paid') ?></th>

<th><?= $this->lang->line('payment_date') ?></th>
                                                <th><?= $this->lang->line('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($creditors as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->CREDITOR_NAME ?></td>
                                                    <td><?= $one->USERNAME ?></td>
                                                    <td>+<?= $one->MOBILE_NUM ?></td>
                                                    <td><?= $one->EMAIL ?></td>
                                                    <td><?= date('Y-m-d', strtotime($one->DATE_OF_BIRTH)) ?></td>
                                                    <td><?= $one->ADDRESS ?></td>
                                                    <td><?= $this->lang->line($one->CREDITOR_TYPE) ?></td>
                                                    <td><?= $one->NATIONALITY ?></td>
                                                    <td><?= $one->NATIONAL_ID ?></td>
                                                    <td><?= $one->PASSPORT_NUM ?></td>
                                                    <td><?= $one->ISSUANCE_COUNTRY ?></td>
                                                    <td><i class="fa fa-<?= $one->MOBILE_CONFIRMED ? 'check' : 'times-x' ?>"></i></td>
                                                    <td><?= date('Y-m-d H:i:s', strtotime($one->TIMESTAMP)) ?></td>
<td><i class="fa fa-<?= $one->PAYMENT_DATE ? 'check' : 'close' ?>"></i></td>
<td><?= $one->PAYMENT_DATE ?></td>
                                                    <td><?php if (get_p("user_payments", "v")) { ?>
                                                            <a title="<?= $this->lang->line("user_payments") ?>" href="<?= base_url() ?>Users/user_payments/<?= $one->ID ?>/1" class="btn btn-sm btn-primary"><i class="fa fa-money"></i></a>
                                                        <?php } ?></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {
                $(".delete").click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= $this->lang->line("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>Admins/delete/" + id;
                    }
                });
            });
        </script>
    </body>
</html>