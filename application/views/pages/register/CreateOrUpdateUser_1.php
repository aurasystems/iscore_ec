<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $this->lang->line("CreateOrUpdateUser") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="GET" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <?= form_group_get("user") ?>
                                        <?= form_group_get("clientName") ?>
                                        <?= form_group_get("address") ?>
                                        <?= form_group_get("phoneNumber") ?>
                                        <?= form_group_get("email") ?>
                                        <?php
                                        $field = "password";
                                        $value = "";
                                        $form_group = " <div class='form-group'>
                                                <label class='col-sm-4 control-label'>" . $this->lang->line($field) . "</label>
                                                <div class='col-sm-7'>
                                                    <input type='password' name='$field' id='$field' class='form-control' value='" . $value . "' autocomplete='off'>
                                                    <span class='c-red'>" . form_error($field) . "</span>
                                                </div>
                                            </div>";
                                        echo $form_group;

                                        $field = "confirm_password";
                                        $value = "";
                                        $form_group = " <div class='form-group'>
                                                <label class='col-sm-4 control-label'>" . $this->lang->line($field) . "</label>
                                                <div class='col-sm-7'>
                                                    <input type='password' name='$field' id='$field' class='form-control' value='" . $value . "' autocomplete='off'>
                                                    <span class='c-red'>" . form_error($field) . "</span>
                                                </div>
                                            </div>";
                                        echo $form_group;
                                        ?>
                                        <div class="col-sm-12 text-center">
                                            <input name="submit" type="submit" class="btn btn-primary" value="<?= $this->lang->line("search") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
</html>