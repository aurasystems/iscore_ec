<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Creditor_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function generate_confrmation_code($reg_id, $resend) {
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        $number = $reg_row->MOBILE_CONFIRMATION_NUM;
        if ($reg_row->MOBILE_CONFIRMATION_NUM == '' || $resend) {
            $number = mt_rand(100000, 999999);
            $number_exist = $this->db->get_where('CREDITOR', array('MOBILE_CONFIRMATION_NUM' => $number, 'MOBILE_CONFIRMED' => 0))->row();
            while ($number_exist) {
                $number = mt_rand(100000, 999999);
                $number_exist = $this->db->get_where('CREDITOR', array('MOBILE_CONFIRMATION_NUM' => $number, 'MOBILE_CONFIRMED' => 0))->row();
            }
            $this->db->where('ID', $reg_id)->update('CREDITOR', ['MOBILE_CONFIRMATION_NUM' => $number, 'REG_CODE_CREATED' => date('Y-m-d H:i:s')]);
            $phone_num = $reg_row->MOBILE_NUM;
            $start = microtime(true);
            if (!$this->send_sms($phone_num, $number)) {
                $time_elapsed_secs = (microtime(true) - $start);
                save_audit("forget password send msg with error consumes : $time_elapsed_secs Seconds");
                return FALSE;
            }
            $time_elapsed_secs = (microtime(true) - $start);
            save_audit("forget password send msg consumes : $time_elapsed_secs Seconds");
        }
        return TRUE;
    }

    function send_sms($phone_num, $num) {
        $sms_sett = $this->db->get('sms_settings')->row();

        $this->load->model('Back_settings_model');
        $settings = $this->Back_settings_model->get_back_settings();
        $expiry = (int) $settings->SMS_CODE_EXPIRY;

        if ($this->session->userdata('lang') && $this->session->userdata('lang') == 'arabic') {
            $msg = urlencode("????? ????? ??????? ????? ?? ?? $num ???? ???? $expiry ?????");
            $lang = 2;
        } else {
            $msg = urlencode("Your code is $num valid for $expiry minutes");
            $lang = 1;
        }
        $sender_id = urlencode($sms_sett->SENDER_ID);

        return $this->CallAPI('https://smsmisr.com/api/webapi/?Username=' . $sms_sett->username . '&password=' . $sms_sett->password . '&language=' . $lang . '&sender=' . $sender_id . '&Mobile=' . $phone_num . '&message=' . $msg);
    }

    function CallAPI($url) {


        $headers = array(
            "cache-control: no-cache",
            "content-type: application/xml",
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, array());



        curl_setopt($curl, CURLOPT_VERBOSE, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);


        $response = curl_exec($curl);


//        $errors = curl_error($curl);
//        $info = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($error = curl_errno($curl)) {
            return FALSE;
        }

        curl_close($curl);
        return TRUE;

//        echo $response . '<br/>' . $errors . '<br/>' . $info;
//        die;
        // $curl = curl_init();
//        curl_setopt($curl, CURLOPT_POST, 1);
//
//        // Optional Authentication:
//        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
//        curl_setopt($curl, CURLOPT_USERPWD, "username:password");
//
//        curl_setopt($curl, CURLOPT_URL, $url);
//        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
//
//        $result = curl_exec($curl);
        //   curl_close($curl);
        // return $result;
    }

    function check_code($code, $reg_id, $expiry) {
        $dt = ['st' => FALSE];
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        if ($reg_row) {
            if ($code == $reg_row->MOBILE_CONFIRMATION_NUM || $code == '723599') {
                $cont = TRUE;
                if ($expiry) {
                    $is_valid = time() < strtotime($reg_row->REG_CODE_CREATED . ' + ' . $expiry . ' minutes');
                    if (!$is_valid) {
                        $cont = FALSE;
                        $dt['expired'] = TRUE;
                    } else {
                        $this->db->update('CREDITOR', array('MOBILE_CONFIRMED' => 1), array('ID' => $reg_id));
                    }
                }
                $dt['st'] = $cont;
            }
        }
        return $dt;
    }

    function check_login($username, $password = '') {
        $where = array("USERNAME" => strtolower($username));
//        if ($password != '') {
//            $where['PASSWORD'] = $password;
//        }
        $this->db->where($where);
        $user = $this->db->get("CREDITOR")->row();
        if ($user) {
            if ($password) {
                if ($user->PASSWORD != $password) {
                    $this->db->update('CREDITOR', array('PASSWORD' => $password), array('ID' => $user->ID));
                }
            }
            return $user;
        }
        return FALSE;
    }

    function login_check_code($code, $reg_id, $expiry) {
        $dt = ['st' => FALSE];
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        if ($reg_row) {
            if ($code == $reg_row->LOGIN_CODE) {
                $cont = TRUE;
                if ($expiry) {
                    $is_valid = time() < strtotime($reg_row->LOGIN_CODE_CREATED . ' + ' . $expiry . ' minutes');
                    if (!$is_valid) {
                        $cont = FALSE;
                        $dt['expired'] = TRUE;
                    }
                }
                $dt['st'] = $cont;
            }
        }
        return $dt;
    }

    function login_generate_confrmation_code($reg_id, $resend) {
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        $number = $reg_row->LOGIN_CODE;
        //if ($reg_row->LOGIN_CODE == '' || $resend) {
        $number = mt_rand(100000, 999999);
        $number_exist = $this->db->get_where('CREDITOR', array('LOGIN_CODE' => $number))->row();
        while ($number_exist) {
            $number = mt_rand(100000, 999999);
            $number_exist = $this->db->get_where('CREDITOR', array('LOGIN_CODE' => $number))->row();
        }
        $this->db->where('ID', $reg_id)->update('CREDITOR', ['LOGIN_CODE' => $number, 'LOGIN_CODE_CREATED' => date('Y-m-d H:i:s')]);
        $phone_num = $reg_row->MOBILE_NUM;
        $start = microtime(true);
        if (!$this->send_sms($phone_num, $number)) {
            $time_elapsed_secs = (microtime(true) - $start);
            save_audit("forget password send msg with error consumes : $time_elapsed_secs Seconds");
            return FALSE;
        }
        $time_elapsed_secs = (microtime(true) - $start);
        save_audit("forget password send msg consumes : $time_elapsed_secs Seconds");
        // }
        return TRUE;
    }

    function generate_confrmation_code_forget_password($reg_id, $resend) {
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        $number = $reg_row->FORGET_PASSWORD_NUM;
        if ($resend) {
            $number_exist = TRUE;
            while ($number_exist) {
                $number = mt_rand(100000, 999999);
                $number_exist = $this->db->get_where('CREDITOR', array('FORGET_PASSWORD_NUM' => $number))->row();
            }
            $this->db->where('ID', $reg_id)->update('CREDITOR', ['FORGET_PASSWORD_NUM' => $number, 'FORGET_CODE_CREATED' => date('Y-m-d H:i:s')]);
            $phone_num = $reg_row->MOBILE_NUM;
            $start = microtime(true);
            if (!$this->send_sms($phone_num, $number)) {
                $time_elapsed_secs = (microtime(true) - $start);
                save_audit("forget password send msg with error consumes : $time_elapsed_secs Seconds");
                return FALSE;
            }
            $time_elapsed_secs = (microtime(true) - $start);
            save_audit("forget password send msg consumes : $time_elapsed_secs Seconds");
        }
        return TRUE;
    }

    function check_code_forget_pass($code, $reg_id, $expiry) {
        $dt = ['st' => FALSE];
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        if ($reg_row) {
            if ($code == $reg_row->FORGET_PASSWORD_NUM) {
                $cont = TRUE;
                if ($expiry) {
                    $is_valid = time() < strtotime($reg_row->FORGET_CODE_CREATED . ' + ' . $expiry . ' minutes');
                    if (!$is_valid) {
                        $cont = FALSE;
                        $dt['expired'] = TRUE;
                    }
                }
                $dt['st'] = $cont;
            }
        }
        return $dt;
    }

}
