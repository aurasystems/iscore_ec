<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_email_settings') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($status == "success") { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                    <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                </div>
                            <?php } ?>

                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("lang_email_settings") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <div class="box-body big col-md-6">
                                        <?php
                                        echo form_open_multipart('Email_settings/update', $attributes);
                                        ?>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_smtp_host') . ':', 'lang_smtp_host', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($email_settings) {
                                                    $smtp_host = $email_settings->smtp_host;
                                                } else {
                                                    $smtp_host = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'smtp_host', 'name' => 'smtp_host', 'class' => 'form-control', 'placeholder' => 'SMTP HOST', 'value' => $smtp_host)); ?>
                                                <?php echo form_error('smtp_host'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_host_mail') . ':', 'lang_host_mail', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($email_settings) {
                                                    $host_mail = $email_settings->host_mail;
                                                } else {
                                                    $host_mail = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'host_mail', 'name' => 'host_mail', 'class' => 'form-control', 'placeholder' => 'HOST MAIL', 'value' => $host_mail)); ?>
                                                <?php echo form_error('host_mail'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_smtp_port') . ':', 'lang_smtp_port', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($email_settings) {
                                                    $smtp_port = $email_settings->smtp_port;
                                                } else {
                                                    $smtp_port = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'smtp_port', 'name' => 'smtp_port', 'class' => 'form-control', 'placeholder' => 'SMTP PORT', 'value' => $smtp_port)); ?>
                                                <?php echo form_error('smtp_port'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_smtp_user') . ':', 'lang_smtp_user', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($email_settings) {
                                                    $smtp_user = $email_settings->smtp_user;
                                                } else {
                                                    $smtp_user = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'smtp_user', 'name' => 'smtp_user', 'class' => 'form-control', 'placeholder' => 'SMTP USER', 'value' => $smtp_user)); ?>
                                                <?php echo form_error('smtp_user'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('lang_smtp_pass') . ':', 'lang_smtp_pass', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($email_settings) {
                                                    $smtp_pass = $email_settings->smtp_pass;
                                                } else {
                                                    $smtp_pass = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'smtp_pass', 'name' => 'smtp_pass', 'class' => 'form-control', 'placeholder' => 'SMTP PASS', 'value' => $smtp_pass)); ?>
                                                <?php echo form_error('smtp_pass'); ?>
                                            </div>
                                        </div>
                                        <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-primary')); ?>
                                        <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>Dashboard"><?= $this->lang->line('lang_cancel') ?></a></span>
                                            <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>