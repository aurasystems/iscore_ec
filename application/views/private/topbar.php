<!-- BEGIN TOPBAR -->
<div class="topbar">
    <div class="header-left">
        <div class="topnav">
            <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span><?= $this->lang->line('lang_menu') ?></span></span></a>
        </div>
    </div>
    <div class="header-right">
        <a href="<?= $this->config->item('website_link'); ?>" class="btn btn-primary btn-sm pull-left m-t-10"><?= $this->lang->line('back_to_website') ?></a>

        <ul class="header-menu nav navbar-nav">
            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="language-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-globe"></i>
                    <span><?= $this->lang->line('lang_language') ?></span>
                </a>
                <ul class="dropdown-menu language-menu">
                    <li>
                        <a href="<?= base_url() ?>Language" data-lang="en"><img src="<?= base_url() ?>assets/images/flags/usa.png" alt="flag-english"> <span><?= $this->lang->line('lang_english') ?></span></a>
                    </li>
                    <li>
                        <a href="<?= base_url() ?>Language/index/arabic" data-lang="ar"><img src="<?= base_url() ?>assets/images/flags/Egypt.png" alt="flag-english"> <span><?= $this->lang->line('lang_arabic') ?></span></a>
                    </li>
                </ul>
            </li>
            <!-- END USER DROPDOWN -->
            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="user-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <?php
                    if ($this->session->userdata('user_image')) {
                        $image = $this->session->userdata('user_image');
                    } else {
                        $image = 'default_avatar.png';
                    }
                    ?>
                    <img src="<?= base_url() ?>uploads/users/<?= $image ?>" alt="user image">
                    <span class="username">Hi, <?= $this->session->userdata('user_display_name') ? $this->session->userdata('user_display_name') : $this->session->userdata('new_user_username') ?></span>
                </a>
                <ul class="dropdown-menu">
                    <?php if ($this->session->userdata('logged_in')) { ?>
                        <?php //if($this->session->userdata('admin')==1){  ?>
                        <li>
                            <a href="<?= base_url() ?>Users/change_password/<?= $this->session->userdata('user_id') ?>"><i class="icon-settings"></i><span><?= $this->lang->line('change_password') ?></span></a>
                        </li>
                        <?php
                        //}
                        if (!$this->session->userdata("admin")) {
                            ?>
                            <li>
                                <a href="<?= base_url() ?>Profile/subscription"><i class="fa fa-money"></i><span><?= $this->lang->line('subscription') ?></span></a>
                            </li>
                        <?php } ?>
                    <?php } ?>
                    <?php $logout_link = $this->session->userdata('logged_in') ? 'Login/Logout' : 'N_user/logout' ?>
                    <li>
                        <a href="<?= base_url($logout_link) ?>"><i class="icon-logout"></i><span><?= $this->lang->line('lang_logout') ?></span></a>
                    </li>
                </ul>
            </li>
            <!-- END USER DROPDOWN -->
        </ul>

    </div>
    <!-- header-right -->
</div>
<!-- END TOPBAR -->