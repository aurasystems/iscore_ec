<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Register_model');
        construct_init(1);
        $this->load->library("Api_lib");
    }
    public function index()
    {
        if(!logged_in()){
            redirect(base_url("Search/PublicationSearch"));
        }
        else
        {
            $data['title'] = 'Register';
            $this->load->view("pages/register", $data);
        }
    }
    
     public function CreateOrUpdateUser(){
        $this->load->model("Settings_model");
        $password_management=  $this->Settings_model->get_password_management();
        if($this->input->get("submit")){
            $this->form_validation->set_data($this->input->get());
            $this->form_validation->set_rules("user",$this->lang->line("user"),"trim|required");
            $this->form_validation->set_rules("clientName",$this->lang->line("clientName"),"trim|required");
            $this->form_validation->set_rules("address",$this->lang->line("address"),"trim|required");
            $this->form_validation->set_rules("phoneNumber",$this->lang->line("phoneNumber"),"trim|required");
            $this->form_validation->set_rules("email",$this->lang->line("email"),"trim|required");
            $this->form_validation->set_rules("password",$this->lang->line("password"),"trim|required");
            $this->form_validation->set_rules("confirm_password",$this->lang->line("confirm_password"),"trim|required|matches[password]");
    //        $this->form_validation->set_rules("subscription",$this->lang->line("subscription"),"trim|required");
    //        if($this->input->post("subscription")){
    //            $this->form_validation->set_rules("startDateOfSubscription",$this->lang->line("startDateOfSubscription"),"trim|required");
    //        }

            if(!$this->form_validation->run()){
                $data['title']=  $this->lang->line("CreateOrUpdateUser");
                $data['password_management'] = $password_management;
                $this->load->view("pages/register/CreateOrUpdateUser",$data);
            }
            else{
                $params=array(
                    "user"=>"",  $this->input->get("user"),
                    "clientName"=>$this->input->get("clientName"),
                    "address"=>$this->input->get("address"),
                    "phoneNumber"=>$this->input->get("phoneNumber"),
                    "email"=>$this->input->get("email"),
                    "subscription"=>0
            );
                $params['admin']=0;
                //insert in our data base
                //TODO get user id
                $user_id=$this->Global_model->global_insert("users",$params);
                $this->set_session($params);
                //TODO check if user exists in AD
                $this->ad_authenticate();
                $this->check_subscription($user_id);
                //if yes update info
                //if not add him to AD
                //set subscription as default to 0
                //call api
//                $response=$this->api_lib->CreateOrUpdateUser($params);
//                //handle reponse
//                if($response['FaultString']){
//                    //set flash
//                    $this->session->set_flashdata("error",$response['FaultString']);
//                    redirect(current_url());
//                }
//                if($response['userCreateOrUpdateStatus']=="USER_CREATE_SUCCESFUL"){
//                    $params['startDateOfSubscription']=NULL;
//                    $params['endDateOfSubscription']=NULL;
//                }
//                elseif($response['userCreateOrUpdateStatus']=="USER_UPDATE_SUCCESFUL"){
//                    $params['startDateOfSubscription']= $response['startDateOfSubscription'];
//                    $params['endDateOfSubscription']=$response['endDateOfSubscription'];
//                }
//                //insert in our data base
//                $this->Global_model->global_insert("users",$params);
//                $this->set_session($params);
                redirect(base_url("Search/PublicationSearch"));
            }
        }
        else{
            $data['title']=  $this->lang->line("CreateOrUpdateUser");
            $data['password_management'] = $password_management;
            $this->load->view("pages/register/CreateOrUpdateUser",$data);
        }

        
    }
    
    public function set_session($data){
        //TODO
        $session=array(
            "user"=>$data["user"],
            "logged_in"=>1
        );
        $this->session->set_userdata($session);
    }
    
    public function test(){
        $this->load->library("Ad_lib");
        $t=$this->ad_lib->edit_user("read-only-admin",["email"=>"r@r.com"]);
        var_dump($t);
//        $this->ad_lib->delete_user("read-only-admin");
    }
    
//    function set_session($user){
//        //add user data to session
//        $new_data = array(
//            'user' => $user->user,
//            'user_id' => $user->id,
//            'user_email' => $user->email,
//            'user_display_name' => $user->clientName,
//            'logged_in' => TRUE
//        );
//        $this->session->set_userdata($new_data);
//    }
    
    function check_subscription($user_id){
        $this->load->model("Subscription_model");
        $sub= $this->Subscription_model->check_subscription($user_id);
        if(!$sub){
            redirect(base_url("Subscription/choose_subscription"));
        }
    }
    
    //TODO
    function ad_authenticate(){
        //active directory authentication
        $username="cn=read-only-admin";
        $password="password";
        $bind=  $this->ad_lib->authenticate($username,$password);
        if($bind['error'])
        {
            $this->session->set_flashdata('msg', $bind['error']);
            redirect(current_url());
        }
    }
}