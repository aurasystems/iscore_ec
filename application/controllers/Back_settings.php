<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Back_settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->library('session');
        $this->load->model('Back_settings_model');
        if ($this->session->userdata('language') != "") {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        } else {
            $this->lang->load('default');
        }
        $this->session->set_userdata('active', '6');
        $this->session->set_userdata('sub_active', '21');
    }

    public function index($status = NULL) {
        check_p("settings", "c");
        if (($this->session->userdata('user_id') != "")) {
            $data = array('title' => 'Back Settings');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $data['back_settings'] = $this->Back_settings_model->get_back_settings();
            $this->load->view("pages/settings/back_settings", $data);
        } else {
            redirect('Dashboard');
        }
    }

    public function update() {
        check_p("settings", "c");
        if (($this->session->userdata('user_id') != "")) {
            $this->load->library('form_validation');
          //  $this->form_validation->set_rules('system_name', 'System Name', 'trim|required');
            $this->form_validation->set_rules('creditor_currency', $this->lang->line('creditor_currency'), 'trim|required');
            $this->form_validation->set_rules('creditor_amount', $this->lang->line('creditor_amount'), 'is_numeric|trim|required');
            $this->form_validation->set_rules('sms_code_expiry', $this->lang->line('sms_code_expiry'), 'is_numeric|trim|required');
            if ($this->form_validation->run() == FALSE) {
                $this->index();
            } else {
                $data = array(
                  //  'system_name' => $this->input->post('system_name'),
                    'CREDITOR_CURRENCY' => $this->input->post('creditor_currency'),
                    'CREDITOR_AMOUNT' => (int) $this->input->post('creditor_amount'),
                    'SMS_CODE_EXPIRY' => (int) $this->input->post('sms_code_expiry')
                );
//                if ($_FILES['userfile']['name'] != '') {
//                    $config['upload_path'] = './uploads/settings/back';
//                    $config['allowed_types'] = 'gif|jpg|png|jpeg';
//                    $config['max_size'] = '10000';
//                    $config['max_width'] = '5000';
//                    $config['max_height'] = '5000';
//                    $this->lang->load('upload');
//                    $this->load->library('upload', $config);
//                    if ($this->upload->do_upload()) {
//                        $upload_data = $this->upload->data();
//                        $data['system_logo'] = $upload_data['file_name'];
//                    }
//                }
                $this->Back_settings_model->update_settings($data);
                save_audit('Update back settings');
            }
        } else {
            redirect('Dashboard');
        }
    }

}
