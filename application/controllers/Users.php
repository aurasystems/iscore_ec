<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Users_model');
        //  $this->load->library("Api_lib");
        construct_init(1);
        $this->load->library('Ad_lib');
        $this->session->set_userdata('active', '3');
    }

    function index() {
        check_p("users", "v");
        $users = $this->Users_model->get_users();
        $data['title'] = $this->lang->line('users');
        $data['users'] = $users;
        $this->load->view("pages/users/index", $data);
    }

    function construct_ad_data($data_arr) {
        $ldaprecord['cn'] = $data_arr['user'];
        $ldaprecord['givenname'] = $data_arr['user'];
        $ldaprecord['sn'] = $data_arr['user'];
        $ldaprecord['mail'] = $data_arr['email'];
        $ldaprecord['mobile'] = $data_arr['phoneNumber'];
        //$ldaprecord['uid'] = '';
        $ldaprecord['displayname'] = $data_arr['clientName'];
        $ldaprecord['samaccountname'] = $data_arr['user'];
        if (isset($data_arr['password'])) {  // Edit case
            $ldaprecord['password'] = $data_arr['password'];
        }
        return $ldaprecord;
    }

    public function check_blocked($pass) {
        if (check_password_blacklist(md5($pass))) {
            $this->form_validation->set_message('check_blocked', $this->lang->line('password_in_blocked_list'));
            return false;
        }
        return TRUE;
    }

    public function add() {
//        set_active("add_user");
        //check_p("users", "c");
        $this->load->model("Settings_model");
        $password_management = $this->Settings_model->get_password_management();
        $this->form_validation->set_rules("user", $this->lang->line("user"), "trim|required");
        $this->form_validation->set_rules("clientName", $this->lang->line("clientName"), "trim|required");
        $this->form_validation->set_rules("address", $this->lang->line("address"), "trim|required");
        $this->form_validation->set_rules("phoneNumber", $this->lang->line("phoneNumber"), "trim|required");
        $this->form_validation->set_rules("email", $this->lang->line("email"), "trim|required");
        $this->form_validation->set_rules("password", $this->lang->line("password"), "trim|required|callback_check_blocked");
        $this->form_validation->set_rules("confirm_password", $this->lang->line("confirm_password"), "trim|required|matches[password]");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line('add_user');
            $data['password_management'] = $password_management;
            $this->load->view('pages/users/add', $data);
        } else {

            $data = array(
                "user" => strtolower($this->input->post("user")),
                "clientName" => $this->input->post("clientName"),
                //"NameOfTheUser" => $this->input->post("clientName"),
                "address" => $this->input->post("address"),
                "phoneNumber" => $this->input->post("phoneNumber"),
                "email" => $this->input->post("email"),
                "nameOfTheUser" => "",
                "subscription" => 0,
                "password" => $this->input->post("password")
            );
            $error_msg = $this->ad_lib->add_user($this->construct_ad_data($data));
            $data['password'] = md5($this->input->post("password"));
            if ($error_msg == 1) {
                unset($data['password']);

                $this->load->model('Rest_Api_model');
                // Assign call params
                $cp = [
                    'auth' => $this->config->item('api_key'),
                    'url' => $this->config->item('search_server_ip') . '/Rest_Api',
                    'dt' => []
                ];

                $cp['action'] = 'CreateOrUpdateUser';
                $cp['params'] = $data;
                $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

                //   $response = $this->api_lib->CreateOrUpdateUser($data);
                //echo $response['response']['userCreateOrUpdateStatus'];die;
                if (!(isset($response['response']) && isset($response['response']['userCreateOrUpdateStatus']) && strpos($response['response']['userCreateOrUpdateStatus'], "SUCCESSFUL") !== false)) {
                    $this->session->set_flashdata('error', implode(' ', $response));
                    if (!$this->session->userdata("logged_in")) {
                        redirect(base_url("Login"));
                    } else {
                        redirect(base_url("Users"));
                    }
                }
                $data['admin'] = 0;
                unset($data['nameOfTheUser']);
                $this->Global_model->global_insert("users", $data);

                $user_id = $this->Users_model->get_user_id($this->input->post("user"));



                //insert password
                $pass_data = [
                    "user_id" => $user_id,
                    "password" => md5($this->input->post("password")),
                    "timestamp" => date("Y-m-d h:i:s")
                ];
                $this->Global_model->global_insert("user_passwords", $pass_data);

                save_audit('adding new search user with username: ' . $this->input->post("user"));
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                if (!$this->session->userdata("logged_in")) {
                    redirect(base_url("Login"));
                } else {
                    redirect(base_url("Users"));
                }
            } else {
                save_audit('ERROR while adding new search user: ' . $error_msg);
                $this->session->set_flashdata("error", $error_msg);
                if (!$this->session->userdata("logged_in")) {
                    redirect(base_url("Login"));
                } else {
                    redirect(base_url("Users"));
                }
            }
        }
    }

    public function edit($id = NULL) {
        check_param($id);
//        set_active("users");
        check_p("users", "u");
        //  $this->form_validation->set_rules("user", $this->lang->line("user"), "trim|required");
        $this->form_validation->set_rules("clientName", $this->lang->line("clientName"), "trim|required");
        $this->form_validation->set_rules("address", $this->lang->line("address"), "trim|required");
        $this->form_validation->set_rules("phoneNumber", $this->lang->line("phoneNumber"), "trim|required");
        $this->form_validation->set_rules("email", $this->lang->line("email"), "trim|required|valid_email");
        $one = $this->Global_model->get_data_by_id("users", $id);
        check_param($one, "resource");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line('edit_user');
            $data['one'] = $one;
            $this->load->view('pages/users/edit', $data);
        } else {
            $user_row = $this->Global_model->get_data_by_id('users', $id);

            $data = array(
                "user" => strtolower($one->user),
                "clientName" => $this->input->post("clientName"),
                "address" => $this->input->post("address"),
                "phoneNumber" => $this->input->post("phoneNumber"),
                "email" => $this->input->post("email"),
                "nameOfTheUser" => "",
                "subscription" => $user_row->subscription,
            );
            if ($user_row->subscription == 1) {
                $data['startDateOfSubscription'] = api_date($user_row->startDateOfSubscription);
            }
            $error_msg = $this->ad_lib->edit_user($one->user, $this->construct_ad_data($data));
            if ($error_msg == 1) {
                $this->load->model('Rest_Api_model');
                // Assign call params
                $cp = [
                    'auth' => $this->config->item('api_key'),
                    'url' => $this->config->item('search_server_ip') . '/Rest_Api',
                    'dt' => []
                ];

                $cp['action'] = 'CreateOrUpdateUser';
                $cp['params'] = $data;
                $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
                //$response = $this->api_lib->CreateOrUpdateUser($data);
                if (!(isset($response['response']) && isset($response['response']['userCreateOrUpdateStatus']) && strpos($response['response']['userCreateOrUpdateStatus'], "SUCCESSFUL") !== false)) {
                    $this->session->set_flashdata('error', implode(' ', $response));
                    redirect(base_url('Users'));
                }
                unset($data['nameOfTheUser']);
                unset($data['startDateOfSubscription']);
                save_audit('updating search user data with username: ' . $one->user);
                $this->Global_model->global_update("users", $id, $data);
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                redirect(base_url("Users"));
            } else {
                save_audit('ERROR while updating search user data: ' . $error_msg);
                $this->session->set_flashdata("error", $error_msg);
                redirect(base_url("Users"));
            }
        }
    }

    function edit_view($id) {
        $data['title'] = $this->lang->line('edit_user');
        $data['one'] = $this->Global_model->get_data_by_id("users", $id);
        $this->load->view('pages/users/edit', $data);
    }

    public function delete($id = NULL) {
        check_param($id);
        check_p("users", "d");
        //check existing users with access level
        // $exists = $this->Categories_model->check_user_customers($id);
        // if ($exists) {
        //    $this->session->set_flashdata("error", lang("customers_assigned_user"));
        //    redirect(base_url("Categories"));
        // }
        $user_row = $this->Global_model->get_data_by_id('users', $id);
        $this->Global_model->global_delete("users", $id);
        save_audit('Search user deleted username:' . $user_row->user);
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("Users"));
    }

    function user_payments($user_id = NULL, $user_creditor = 0) { // $user_creditor 0 for users
        check_param($user_id);
        check_p("payments", "v");
        $payments = $this->Users_model->get_user_payments($user_id, $user_creditor);
        $data['title'] = $this->lang->line('user_payments');
        $data['payments'] = $payments;
        $this->load->view("pages/users/user_payments", $data);
    }

    function change_password($user_id = NULL) {
        //if($this->session->userdata('admin')==0){
        //redirect('Dashboard');
        //}
        check_param($user_id);
        if ($this->session->userdata("user_id") != $user_id) {
            check_p("users", "u");
        }
        $this->load->model("Login_model");
        $password_management = $this->Login_model->get_password_management();
        //check minimum length
        $length = "";
        if (!empty($password_management) && $password_management->min_length) {
            $length = "min_length[$password_management->min_length]";
        }
        $this->form_validation->set_rules('old_password', $this->lang->line('old_password'), 'xss_clean|trim|required|callback_valid_old_password[' . $user_id . ']');
        $this->form_validation->set_rules('password', $this->lang->line('lang_password'), "xss_clean|trim|required|matches[password2]|callback_password_validation|callback_check_blocked|$length");
        $this->form_validation->set_rules('password2', $this->lang->line('confirm_password'), 'xss_clean|trim|required|matches[password]');
        if (!$this->form_validation->run()) {
            $data = array('title' => $this->lang->line("change_password"));
            $data['system_user_id'] = $user_id;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $data['user_data'] = $this->Global_model->get_data_by_id('users', $user_id);
            $this->load->model("admin/Login_model");
            $password_management = $this->Login_model->get_password_management();
            $data['password_management'] = $password_management;
            $this->load->view('pages/users/change_password', $data);
        } else {
            
            $user = $this->Global_model->get_data_by_id('users', $user_id);
            $this->load->library('Ad_lib');
            $ad_data['password'] = $this->input->post("password");
            //$error_msg = $this->ad_lib->edit_user($user->user, $ad_data);
            $error_msg = $this->ad_lib->edit_user_password($user->user, $this->input->post("password"));
            if ($error_msg == 1) {
                //insert password in database
                $data = array(
                    'user_id' => $user_id,
                    'password' => md5($this->input->post("password")),
                    'timestamp' => date("Y-m-d H:i:s")
                );

                /**/ $this->Global_model->global_insert("user_passwords", $data);
                //end insert password in database
                if ($this->session->userdata("user_id") == $user_id) {
                    save_audit('User changed his password');
                    $update = array('force_reset' => 0);
                    $this->Global_model->global_update("users", $user_id, $update);
$this->session->unset_userdata('force_reset');
                } elseif ($this->input->post("force_reset")) {
                    $user_row = $this->Global_model->get_data_by_id('users', $user_id);
                    save_audit('change password for user:' . $user_row->user);
                    $update = array('force_reset' => $this->input->post("force_reset"));
                    $this->Global_model->global_update("users", $user_id, $update);
                }
                $this->session->set_flashdata('success', $this->lang->line('password_chanhged_success'));
                redirect(current_url());
            } else {
                $this->session->set_flashdata('error', $error_msg);
                redirect(current_url());
            }
        }
    }

    function valid_old_password($value, $user_id) {
        $this->form_validation->set_message("valid_old_password", $this->lang->line("old_password_not_match"));
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $last = $this->Users_model->get_last_password($user_id);
        if (!empty($last)) {
            if ($last->password == md5($value)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    function password_validation() {
        //check password matches any histrory
        $this->load->model("Login_model");
        $this->form_validation->set_message('password_validation', 'The {field} field shouldn\'t match any of your previous passwords');
        $new_password = $this->input->post("password");
        $password_management = $this->Login_model->get_password_management();
        if ($password_management->enforce_history) {
            $exists = $this->Login_model->check_password($this->session->userdata("user_id"), md5($new_password));
            if (!empty($exists)) {
                return FALSE;
            }
        }
        return TRUE;
    }

}
