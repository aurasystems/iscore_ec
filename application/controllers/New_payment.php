<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class New_payment extends CI_Controller {

    protected $api_username;
    protected $api_password;
    protected $merchant_id;

    public function __construct() {
        parent::__construct();

        $payment_sett = $this->db->get('payment_settings')->row();

        if (!$payment_sett) {
            $this->session->set_flashdata('error', $this->lang->line('payment_settings_error'));
            redirect(base_url());
        }

        $this->merchant_id = $payment_sett->MERCHANT_ID;
        $this->api_username = $payment_sett->API_USERNAME;
        $this->api_password = $payment_sett->API_PASSWORD;
    }

    function retrieve_order() {
        $response = $this->CallAPI2('GET', 'https://banquemisr.gateway.mastercard.com/api/rest/version/55/merchant/' . $this->merchant_id
                . '/order/' . $this->session->userdata('payOrderId'));
        if (!$response) {
            return FALSE;
        }
        $res_arr = json_decode($response);
        if (strtolower($res_arr->result) != 'success') {
            return FALSE;
        }
        $success_payment = FALSE;
        for ($i = 0; $i < count($res_arr->transaction); $i++) {
            $transaction_id = $res_arr->transaction[$i]->transaction->id;
            $trans_response = $this->CallAPI2('GET', 'https://banquemisr.gateway.mastercard.com/api/rest/version/57/merchant/' . $this->merchant_id .
                    '/order/' . $this->session->userdata('payOrderId') . '/transaction/' . $transaction_id);
            $trans_res_arr = json_decode($trans_response);
            if ($trans_res_arr->response->gatewayCode == 'APPROVED' || $trans_res_arr->response->gatewayCode == 'APPROVED_AUTO' ||
                    $trans_res_arr->response->gatewayCode == 'APPROVED_PENDING_SETTLEMENT') {
                $success_payment = TRUE;
                break;
            }
        }
        if (!$success_payment) {
            return FALSE;
        }


        return array('amount' => $res_arr->amount,
            'currency' => $res_arr->currency,
            'payment_method' => $res_arr->sourceOfFunds->provided->card->brand);
    }

    function create_api_session() {

        $order_id = $this->get_order_id();

        $fields = array('order' => array('amount' => $this->input->get('amount'), 'currency' => $this->input->get('currency'), 'id' => $order_id),
            'apiOperation' => 'CREATE_CHECKOUT_SESSION',
            'interaction' => array('operation' => 'PURCHASE')
        );
        $response = $this->CallAPI2('POST', 'https://banquemisr.gateway.mastercard.com/api/rest/version/55/merchant/' . $this->merchant_id
                . '/session', $fields);
        if ($response) {
            $res_arr = json_decode($response);
            if (strtolower($res_arr->result) == 'success') {
                $this->session->set_userdata('successIndicator', $res_arr->successIndicator);
                $this->session->set_userdata('payOrderId', $order_id);
                echo json_encode(array(1, $res_arr->session->id));
                return;
            }
        }
        $res_arr = json_decode($response);
        echo json_encode(array(0, $res_arr->explanation));
    }

    function CallAPI2($method, $url, $data = false) {
        $curl = curl_init();

        if ($method == 'POST') {
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        }

        $headers = array(
            "cache-control: no-cache",
            "content-type: application/xml",
        );

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, $this->api_username . ":" . $this->api_password);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        //curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return FALSE;
        }

        return $result;
    }

    function set_flashdata() {
        $this->session->set_flashdata('error', $this->input->get('error_msg'));
    }

    function get_order_id() {
        $order_id = $this->db->get('back_settings')->row()->ORDER_ID;
        $this->db->update('back_settings', array('ORDER_ID' => ++$order_id));
        return $order_id;
    }

    function check_success_payment() {

        if (($this->session->userdata('successIndicator') && ($this->session->userdata('successIndicator') == $this->input->get('resultIndicator'))) &&
                (($this->session->userdata('amount') && $this->session->userdata('currency')))) {
            return TRUE;
        }
        $this->session->set_flashdata('error', $this->lang->line('payment_error'));
        echo FALSE;
    }

    function pay_callback() {
        $order_details = $this->retrieve_order();
        if (!$order_details ||
                ($this->session->userdata('amount') != $order_details['amount']) ||
                ($this->session->userdata('currency') != $order_details['currency'])) {
            $this->session->set_flashdata('error', $this->lang->line('payment_error'));
            redirect(base_url("Subscription/choose_subscription"));
        }
        // $this->load->library("Api_lib");

        $amount = $order_details['amount'];
        $currency = $order_details['currency'];
        $user_id = $this->session->userdata('user_id');
        $user_row = $this->db->get_where('users', array('id' => $user_id))->row();

        if ($this->session->userdata('type') == 'SUBSCRIPTION') {

            $data = array(
                "user" => strtolower($user_row->user),
                "clientName" => $user_row->clientName,
                //"NameOfTheUser" => $this->input->post("clientName"),
                "address" => $user_row->address,
                "phoneNumber" => $user_row->phoneNumber,
                "email" => $user_row->email,
                "nameOfTheUser" => "",
                "subscription" => 1,
                "startDateOfSubscription" => api_date(date("Y-m-d H:i:s"))
            );

            $this->load->model('Rest_Api_model');
            // Assign call params
            $cp = [
                'auth' => $this->config->item('api_key'),
                'url' => $this->config->item('search_server_ip') . '/Rest_Api',
                'dt' => []
            ];

            $cp['action'] = 'CreateOrUpdateUser';
            $cp['params'] = $data;
            $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

            //  $response = $this->api_lib->ChargedFeeAmount($charge_params);
            //  $response = $this->api_lib->CreateOrUpdateUser($data);
            if (!empty($response['faultstring'])) {
                //set flash
                $this->session->set_flashdata("error", $response['faultstring']);
                redirect(base_url("Subscription/choose_subscription"));
            }
//            $response['response']['startDateOfSubscription'] = "2020-03-32T00:00:00";
//            $response['response']['endDateOfSubscription'] = "2021-03-32T00:00:00";

            $data = array(
                "subscription" => 1,
                "startDateOfSubscription" => to_date($response['response']['startDateOfSubscription']),
                "endDateOfSubscription" => to_date($response['response']['endDateOfSubscription'])
            );
        } else if ($this->session->userdata('type') == 'SEARCH') {  // one time search 
            $data = array(
                "ONE_TIME_SEARCH" => 1,
            );
        }
        $this->Global_model->global_update("users", $user_id, $data);
        // SEND EMAIL
        if ($amount != '') {

            $subscription_type = 'SearchOneTime';
            if ($this->session->userdata('type') == 'SUBSCRIPTION') {
                $subscription_type = 'SearchSubscriptionAnnual';
            }
            $this->db->insert('user_payments', array('user_id' => intval($user_id), 'price' => $amount,
                'amount_paid' => $amount, 'payment_method' => $order_details['payment_method'], 'payment_date' => date('Y-m-d'),
                'service_type' => $subscription_type, 'PAYMENT_CODE' => $this->session->userdata('payOrderId')));  //
            // Get all data

            if ($this->session->userdata('user_email') != '') {
                $msg = "Dear " . $this->session->userdata('user_display_name') . ",<br><br><br>"
                        . "Kindly note that your payment with the amount of (EGP $amount) has been successfully paid.<br>"
                        . "<ul>"
                        . "<li>Phone : " . $this->session->userdata('phone_number') . "</li>"
                        . "<li>Fee Type : " . $subscription_type . "</li>"
                        . "<li>Amount :  $amount  ($currency) </li>"
                        . "<li>Payment Date : " . date('Y-m-d') . "</li>";
                if ($this->session->userdata('type') == 'SUBSCRIPTION') {
                    $msg .= "<li>Subscription Start Date: " . to_date($response['response']['startDateOfSubscription']) . "</li>"
                            . "<li>Subscription End Date : " . to_date($response['response']['endDateOfSubscription']) . "</li>";
                }

                $msg .= "</ul> <br><br><br>"
                        //. "<br/>You can now use our search  "
                        . "Best regards";
                $this->Global_model->send_mail($this->session->userdata('user_email'), $msg, 'IScore ECR: (Online Payment Successful)');
            }
        }
        // END SEND EMAIL
        $this->session->set_flashdata('success', $this->lang->line("payment_successful"));
        redirect(base_url("Search/PublicationSearch"));
    }

    function pay_callback_creditor() {
        $reg_id = $this->session->userdata('cre_user_id');
        $order_details = $this->retrieve_order();

        if (!$order_details ||
                ($this->session->userdata('cre_amount') != $order_details['amount']) ||
                ($this->session->userdata('cre_currency') != $order_details['currency'])) {
            //$this->session->set_flashdata('error', $this->lang->line('payment_error'));
            redirect(base_url("Creditor/payment_error_page"));
        }
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        if ($reg_row) {
            //save_audit('Creditor payment ' . $msg . ' for username: ' . $reg_row->USERNAME);
            // BEGIN INSERT PAYMENT.
            $start_date = date('Y-m-d');
            $amount = $order_details['amount'];
            $currency = $order_details['currency'];

            $this->db->where('ID', $reg_id)->update('CREDITOR', ['PAYMENT_DATE' => $start_date]);
            // SEND EMAIL
            if ($amount && $reg_row->EMAIL != '') {
                $this->db->insert('user_payments', array('user_id' => intval($reg_id), 'price' => $amount,
                    'amount_paid' => $amount, 'payment_method' => $order_details['payment_method'], 'payment_date' => $start_date,
                    'service_type' => NULL, 'USER_CREDITOR' => 1, 'PAYMENT_CODE' => $this->session->userdata('payOrderId')));  //
                // Get all data


                $msg = "Dear " . $reg_row->CREDITOR_NAME . ",<br><br><br>"
                        . "Kindly note that your payment with the amount of (EGP $amount) has been successfully paid.<br>"
                        . "<ul>"
                        . "<li>Phone : +" . $reg_row->MOBILE_NUM . "</li>"
                        . "<li>Amount :  $amount  ($currency) </li>"
                        . "<li>Payment Date : " . $start_date . "</li>";
//                $msg .= "<li>Subscription Start Date: " . $start_date . "</li>"
//                        . "<li>Subscription End Date : " . date('Y-m-d', strtotime('+1 years', strtotime($start_date))) . "</li>";

                $msg .= "</ul> <br><br><br>"
                        //. "<br/>You can now use our search  "
                        . "Best regards";
                $this->Global_model->send_mail($reg_row->EMAIL, $msg, 'IScore ECR: (Online Payment Successful)');
            }
            // END SEND EMAIL
            $this->session->set_flashdata('success', $this->lang->line("payment_successful"));

            // redirect($this->config->item('creditor_sub_redirect'));
            // $this->load->helper('cookie');
            // set_cookie('allow_redirect', '1');
            $this->session->set_userdata('allow_frame', 1);
            $this->load->view('pages/private_page');
            return;
//            $this->load->model('Rest_Api_model');
//            // Assign call params
//            $cp = [
//                'auth' => $this->config->item('api_key'),
//                'url' => $this->config->item('search_server_ip') . '/Rest_Api',
//                'dt' => []
//            ];
//            $code = mt_rand(100000, 999999);
//
//            $cp['action'] = 'put_code';
//            $cp['params'] = array('code' => $code);
//            $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
//            if (!isset($response['success'])) {
//                $this->session->set_flashdata('msg', $this->lang->line('error_occured'));
//                redirect(base_url('creditor/login'));
//            }
//            // redirect to 27 , 28
//            redirect($this->config->item('search_server_ip') . '/Creditor_redirect/redirect_to/' . $code);
        }
    }

    function test_ss() {
        $data = array(
            "user" => 'SARA_MAGDY',
            "clientName" => 'sara_magdy',
            //"NameOfTheUser" => $this->input->post("clientName"),
            "address" => 'cairo',
            "phoneNumber" => '201004707161',
            "email" => 'sara.magdi@aura-sys',
            "nameOfTheUser" => "",
            "subscription" => 1,
            "startDateOfSubscription" => api_date(date("Y-m-d H:i:s"))
        );

        $this->load->model('Rest_Api_model');
        // Assign call params
        $cp = [
            'auth' => $this->config->item('api_key'),
            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
            'dt' => []
        ];

        $cp['action'] = 'CreateOrUpdateUser';
        $cp['params'] = $data;
        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

        //  $response = $this->api_lib->ChargedFeeAmount($charge_params);
        //  $response = $this->api_lib->CreateOrUpdateUser($data);
        if (!empty($response['faultstring'])) {
            //set flash
            $this->session->set_flashdata("error", $response['faultstring']);
            redirect(base_url("Subscription/choose_subscription"));
        }
    }

}
