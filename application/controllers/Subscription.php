<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscription extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model("Subscription_model");
       // $this->load->library("Api_lib");
        $this->session->set_userdata('active', '7');
    }

    function choose_subscription($error = '') {
        $subscription = $this->Global_model->get_data_by_id("users", $this->session->userdata("user_id"));
        if (!empty($subscription) && (($subscription->subscription == 1 && date("Y-m-d H:i:s") <= $subscription->endDateOfSubscription && date("Y-m-d H:i:s") >= $subscription->startDateOfSubscription) || ($subscription->ONE_TIME_SEARCH))) {
            redirect(base_url("Profile/subscription"));
        }
        $data['title'] = $this->lang->line("subscribe");
        $data['error'] = $error;
        $this->load->view("pages/subscription/subscribe", $data);
    }

    function subscribe($type = "SEARCH") {

        if (!$this->session->flashdata('from_our_system')) {
            redirect(base_url('New_payment/pay_callback'));
        }

        $this->load->model('Rest_Api_model');
        // Assign call params
        $cp = [
            'auth' => $this->config->item('api_key'),
            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
            'dt' => []
        ];

        $charge_params = array(
            "user" => strtolower($this->session->userdata("user")),
            "feeType" => $type,
        );

        $cp['action'] = 'ChargedFeeAmount';
        $cp['params'] = $charge_params;
        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

      //  $response = $this->api_lib->ChargedFeeAmount($charge_params);
        if (!empty($response['faultstring'])) {
            //set flash
            save_audit('subscribtion(feeType:' . $type . '): ERROR in ChargedFeeAmount API (' . $response['faultstring'] . ') for user: ' . $this->session->userdata('user'));
            $this->session->set_flashdata("error", $response['faultstring']);
            redirect(base_url("Subscription/choose_subscription"));
        }
        $response = $response['response'];

//        $response['serviceFee'] = 1000;
//        $response['currency'] = 'EGP';
        //$params['paidFeeAmount'] = $response['serviceFee'];
        //TODO redirect to online payment gatweway
//        $response['serviceFee'] = 50;
//        $response['currency'] = 'EGP';
        $amount = add_vat($response['serviceFee']);
        if ($amount == 0 || $response['currency'] == '') {
            $this->session->set_flashdata('error', $this->lang->line('payment_settings_error'));
            redirect(base_url("Subscription/choose_subscription"));
        }

        $payment_sett = $this->db->get('payment_settings')->row();

        if (!$payment_sett) {
            $this->session->set_flashdata('error', $this->lang->line('payment_settings_error'));
            redirect(base_url("Subscription/choose_subscription"));
        }

        $payment_type = $type == 'SEARCH' ? 'One Time Search' : 'Yearly Subscription';

        $this->session->set_userdata('amount', $amount);
        $this->session->set_userdata('currency', $response['currency']); //$response['currency']
        $this->session->set_userdata('type', $type);



        $arr = array('amount' => $amount,
            'currency' => $response['currency'],
            'type' => 'subscription',
            'payment_type' => $payment_type,
            'payment_sett' => $payment_sett);
        $this->load->view('payment', $arr);
        // redirect(base_url('Online_payment/migs/' . $amount . '/' . $response['currency'] . '/' . $type));
    }

    function pre_subscribe($type = "SEARCH") {
        $this->session->set_flashdata('from_our_system', 1);
        redirect(base_url("Subscription/subscribe/$type"));
    }

}
