<html>
    <head>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <?php
        if ($type == 'subscription') {
            $success_url = base_url('New_payment/pay_callback');
        } else {
            $success_url = base_url('New_payment/pay_callback_creditor');
        }
        ?>
        <script src="https://banquemisr.gateway.mastercard.com/checkout/version/55/checkout.js"
                data-error="errorCallback"
                data-cancel="cancelCallback"
                data-complete="<?= $success_url ?>" >
//                              data-complete="completeCallback" 
        </script>

        <script type="text/javascript">

            function errorCallback(error) {
                console.log('error');
                $.ajax({
                    url: "<?= base_url('New_payment/set_flashdata') ?>",
                    type: "GET",
                    data: {"error_msg": error.explanation},
                    success: function () {
<?php if ($type == 'subscription') { ?>
                            location.href = '<?= base_url("Subscription/choose_subscription") ?>';
<?php } else { ?>
                            location.href = '<?= base_url("creditor/payment_error_page") ?>';
<?php } ?>
                    }
                });
            }
            function cancelCallback() {
            console.log('cancel');
<?php if ($type == 'subscription') { ?>
                    location.href = '<?= base_url("Subscription/choose_subscription") ?>';
<?php } else { ?>
                    location.href = '<?= base_url("creditor/payment_error_page") ?>';
<?php } ?>
            }

            function go_to_payment() {
                $.ajax({
                    url: "<?= base_url('New_payment/create_api_session') ?>",
                    type: "GET",
                    data: {"amount":<?= $amount ?>, "currency": '<?= $currency ?>'},
                    dataType: "JSON",
                    success: function (response) {
                        if (response[0] != 1) {
                            $.ajax({
                                url: "<?= base_url('New_payment/set_flashdata') ?>",
                                type: "GET",
                                data: {"error_msg": response[1]},
                                success: function () {
<?php if ($type == 'subscription') { ?>
                                        location.href = '<?= base_url("Subscription/choose_subscription") ?>';
<?php } else { ?>
                                        location.href = '<?= base_url("creditor/payment_error_page") ?>';
<?php } ?>
                                }
                            });
                            return;
                        }

                        Checkout.configure({
                            merchant: '<?= $payment_sett->MERCHANT_ID ?>',
                            order: {
                                amount: function () {
                                    //Dynamic calculation of amount
                                    return <?= $amount ?>;
                                },
                                currency: '<?= $currency ?>',
                                description: '<?= $payment_type ?>',
                                id: ''
                            },
                            interaction: {
                                operation: 'PURCHASE', // set this field to 'PURCHASE' for Hosted Checkout to perform a Pay Operation.
                                merchant: {
                                    name: '<?= $payment_sett->MERCHANT_NAME ?>'
//                        address: {
//                            line1: '200 Sample St',
//                            line2: '1234 Example Town'
//                        }
                                }//,
                            },
                            session: {
                                id: response[1]
                            }
                        });
                        Checkout.showPaymentPage();
                    }
                });
            }
            (function () {
                go_to_payment();
            })();
        </script>
    </head>
    <!-- <body>
         ...
         <input type="button" value="Pay with Lightbox" onclick="Checkout.showLightbox();" />
         <input type="button" value="Pay with Payment Page" onclick="Checkout.showPaymentPage();" />
         ...
     </body> -->
</html>