<?php

function construct_init($no_log = NULL) {
    $CI = &get_instance();
    if ($CI->session->userdata("first_login") && (strtolower($CI->router->fetch_class()) == 'login' || strtolower($CI->router->fetch_class()) == 'creditor' || strtolower($CI->router->fetch_class()) == 'n_user')) {
        $CI->session->unset_userdata('first_login');
    }
//    if ($CI->session->userdata("first_login") &&
//            !(strtolower($CI->router->fetch_class()) == 'user' && strtolower($CI->router->fetch_method()) == 'reset_password') &&
//            !(strtolower($CI->router->fetch_class()) == 'user' && strtolower($CI->router->fetch_method()) == 'reset_my_password') &&
//            !(strtolower($CI->router->fetch_class()) == 'user' && strtolower($CI->router->fetch_method()) == 'refresh_captcha') &&
//            !(strtolower($CI->router->fetch_class()) == 'user' && strtolower($CI->router->fetch_method()) == 'do_reset_password') &&
//            !(strtolower($CI->router->fetch_class()) == 'user' && strtolower($CI->router->fetch_method()) == 'add_user_forget_pass') &&
//            !(strtolower($CI->router->fetch_class()) == 'user' && strtolower($CI->router->fetch_method()) == 'password_reset_success')) {
//        redirect(base_url("User/reset_password"));
//    }

    if (!$no_log) {
        if (!$CI->session->userdata("logged_in") || !$CI->session->userdata('session_id')) {
            redirect("Login");
        }

        $session_row = $CI->db->get_where('SESSIONS', array('ID' => $CI->session->userdata('session_id')))->row();
        if ($session_row->LOGOUT) {
            redirect("Login/logout");
        }
    }
    // check if another user logged with the current account
    if ($CI->session->userdata('cre_logged_in')) {
        $CI->db->select('last_login_time');
        $creditor = $CI->db->get_where('CREDITOR', array('ID' => $CI->session->userdata('cre_user_id')))->row();
        if (!$creditor || strtotime($creditor->last_login_time) != strtotime($CI->session->userdata('cre_logged_time'))) {
            $new_data = array(
                'cre_user' => '',
                'cre_user_id' => '',
                'cre_user_email' => '',
                'cre_logged_time' => '',
                'cre_logged_in' => FALSE
            );
            $CI->session->unset_userdata($new_data);
            $CI->session->sess_destroy();

            save_audit('creditor multi session logged out');
            redirect(base_url('Creditor/login'));
        }
    }
    if ($CI->session->userdata('lang') == "arabic") {
        $CI->config->set_item('language', $CI->session->userdata('lang'));
        $CI->lang->load('default', $CI->session->userdata('lang'));
    } else {
        $CI->config->set_item('language', 'default');
        $CI->lang->load('default');
    }
    if ($CI->session->userdata("force_reset") && !(strtolower($CI->router->fetch_class()) == 'users' && strtolower($CI->router->fetch_method()) == 'change_password') && !(strtolower($CI->router->fetch_class()) == 'login' && strtolower($CI->router->fetch_method()) == 'logout')) {
        redirect(base_url("Users/change_password/{$CI->session->userdata("user_id")}"));
    }
}

function logged_in() {
    $CI = &get_instance();
    if ($CI->session->userdata("user_id")) {
        return TRUE;
    }
    return FALSE;
}

function to_date($date, $custom = NULL) {
    $format = "Y-m-d H:i:s";
    if ($custom) {
        $format = $custom;
    }
    return date($format, strtotime($date));
}

function selected($value, $compare) {
    if ($value == $compare) {
        return "selected";
    }
    return "";
}

function valid_mobile($mobile) {
    if (strlen($mobile) == 11 && $mobile[0] === "0") {
        return TRUE;
    }
    return FALSE;
}

function valid_date($date) {
    if (!strtotime($date)) {
        return FALSE;
    }
    return True;
}

function form_group($field, $edit = NULL, $date = "") {
    $CI = &get_instance();
    $value = $CI->input->post($field);
    if (!$CI->input->post()) {
        $value = $CI->input->get($field);
    }
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . $CI->lang->line($field) . "</label>
                        <div class='col-sm-7'>
                            <input type='text' name='$field' id='$field' class='form-control $date' value='" . $value . "'>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

function form_group_select($field, $options, $option_name = "name", $edit = NULL, $mutiple = "") {
    $CI = &get_instance();
    if ($option_name == NULL) {
        $option_name = "name";
    }
    $value = $CI->input->post($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = "<div class='form-group'>
                    <label class='col-sm-4 control-label'>" . $CI->lang->line($field) . "</label>
                    <div class='col-sm-7'>
                        <select $mutiple name='$field' id='$field' data-search='true' class='form-control'>
                            <option value=''>" . $CI->lang->line('select') . "</option>";
    foreach ($options as $one) {
        $form_group .= "<option " . selected($value, $one->id) .
                " value='" . $one->id . "'>" . $one->{$option_name} . "</option>";
    }
    $form_group .= "</select>
                        <span class='c-red'>" . form_error($field) . "</span>
                    </div>
                </div>";
    return $form_group;
}

function form_group_text($field, $edit = NULL) {
    $CI = &get_instance();
    $value = $CI->input->post($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . $CI->lang->line($field) . "</label>
                        <div class='col-sm-7'>
                        <textarea rows='5' name='$field' id='$field' class='form-control'>$value</textarea>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

//for get
function form_group_get($field, $edit = NULL, $date = "") {
    $CI = &get_instance();
    $value = $CI->input->get($field);
    if ($value == '') {
        $value = set_value($field);
    }
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . $CI->lang->line($field) . "</label>
                        <div class='col-sm-7'>
                            <input type='text' name='$field' id='$field' class='form-control $date' value='" . $value . "'>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

function form_group_select_get($field, $options, $option_name = "name", $edit = NULL, $mutiple = "") {
    $CI = &get_instance();
    if ($option_name == NULL) {
        $option_name = "name";
    }
    $value = $CI->input->get($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = "<div class='form-group'>
                    <label class='col-sm-4 control-label'>" . $CI->lang->line($field) . "</label>
                    <div class='col-sm-7'>
                        <select $mutiple name='$field' id='$field' data-search='true' class='form-control'>
                            <option value=''>" . $CI->lang->line('select') . "</option>";
    foreach ($options as $one) {
        $form_group .= "<option " . selected($value, $one->id) .
                " value='" . $one->id . "'>" . $one->{$option_name} . "</option>";
    }
    $form_group .= "</select>
                        <span class='c-red'>" . form_error($field) . "</span>
                    </div>
                </div>";
    return $form_group;
}

function form_group_text_get($field, $edit = NULL) {
    $CI = &get_instance();
    $value = $CI->input->get($field);
    if ($edit) {
        $value = $edit;
    }
    $form_group = " <div class='form-group'>
                        <label class='col-sm-4 control-label'>" . $CI->lang->line($field) . "</label>
                        <div class='col-sm-7'>
                        <textarea rows='5' name='$field' id='$field' class='form-control'>$value</textarea>
                            <span class='c-red'>" . form_error($field) . "</span>
                        </div>
                    </div>";
    return $form_group;
}

function flash_success() {
    $CI = &get_instance();
    $flash = "";
    if ($CI->session->flashdata("success")) {
        $flash .= "<div class='row'>
                <div class='col-md-12'>
                    <div class='alert alert-block alert-success fade in'>
                        <a class='close' data-dismiss='alert' href='#'>×</a>
                        <p><strong> " . $CI->lang->line("success") . " </strong> " . $CI->session->flashdata("success") . "</p>
                    </div>
                </div>
            </div>
    ";
    }
    return $flash;
}

function flash_error() {
    $CI = &get_instance();
    $flash = "";
    if ($CI->session->flashdata("error")) {
        $flash .= "<div class='row'>
                <div class='col-md-12'>
                    <div class='alert alert-block alert-danger fade in'>
                        <a class='close' data-dismiss='alert' href='#'>×</a>
                        <p><strong>" . $CI->lang->line("error") . " </strong> " . $CI->session->flashdata("error") . "</p>
                    </div>
                </div>
            </div>
    ";
    }
    return $flash;
}

function flash_msg() {
    $CI = &get_instance();
    $flash = "";
    if ($CI->session->flashdata("msg")) {
        $flash .= "<div class='row'>
                <div class='col-md-12'>
                    <div class='alert alert-block alert-warning fade in'>
                        <a class='close' data-dismiss='alert' href='#'>×</a>
                        <p><strong></strong> " . $CI->session->flashdata("msg") . "</p>
                    </div>
                </div>
            </div>
    ";
    }
    return $flash;
}

function handle_file($field, $custom_config = NULL) {
//    $data=array(
//        "field"=>"the name of the file field",
//        "custom_config"=>"array of custom config",
//    );
//    $config['upload_path'] = './uploads/settings/back';
//    $config['allowed_types'] = 'gif|jpg|png|jpeg';
//    $config['max_size'] = '10000';
//    $config['max_width'] = '5000';
//    $config['max_height'] = '5000';
    $CI = &get_instance();
    $response['status'] = "no_file";
    if ($_FILES[$field]['name'] != "") {
        $config['upload_path'] = './uploads';
        $config['allowed_types'] = "*";
        if ($custom_config) {
            $config = $custom_config;
        }
        $CI->load->library("upload", $config);
        if ($CI->upload->do_upload($field)) {
            $file_data = $CI->upload->data();
            $response['status'] = "success";
            $response['file_name'] = $file_data['file_name'];
        } else {
            $response['status'] = "fail";
            $response['error'] = $CI->$this->upload->display_errors();
            $CI->session->set_flashdata("error", $response['error']);
            redirect(current_url());
        }
    }
    return $response;
}

function pd($value) {
    echo "<pre>";
    print_r($value);
    echo "</pre>";
    die;
}

function p($value) {
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}

function checked($value) {
    if ($value == 1) {
        return "checked";
    }
    return "";
}

function send_email($data) {
    $CI = &get_instance();
    $email_settings = $CI->db->get("email_settings")->row();
    if (!empty($email_settings)) {
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $email_settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $email_settings->smtp_port, // 465 
            'smtp_user' => $email_settings->smtp_user,
            'smtp_pass' => $email_settings->smtp_pass,
            'mailtype' => 'html'
        );
        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->from($email_settings->host_mail);
        $CI->email->to($data['to']);
        if (!empty($data['subject'])) {
            $CI->email->subject($data['subject']);
        }
        $CI->email->message($data['message']);
        $sent = $CI->email->send();
//                $err = $this->email->print_debugger();
//                $err = json_encode($err) . "\n";
    }
}

function in_content($key, $array) {
    if (!empty($array[$key])) {
        return $array[$key];
    }
    return "";
}

//function check_p($module, $permission) {
////    $CI= &get_instance();
////    $permissions = $CI->session->userdata("permissions");
////    $p = $permissions[$module][$permission];
////    if (!$p) {
////        redirect(base_url("Login/permission_error"));
////    }
//}
//function get_p($module, $permission) {
//    return true;
//    $CI= &get_instance();
//    $permissions = $CI->session->userdata("permissions");
//    $p = $permissions[$module][$permission];
//    return $p;
//}

function or_p($arr) {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = false;
    foreach ($arr as $module => $permission) {
        if ($permissions[$module][$permission]) {
            $p = true;
            break;
        }
    }
    if (!$p) {
        redirect(base_url("Login/permission_error"));
    }
}

function and_p($arr) {
    $CI = &get_instance();
    $permissions = $CI->session->userdata("permissions");
    $p = true;
    foreach ($arr as $module => $permission) {
        if (!$permissions[$module][$permission]) {
            $p = false;
            break;
        }
    }
    if (!$p) {
        redirect(base_url("Login/permission_error"));
    }
}

function csrf() {
    $CI = &get_instance();
    $name = $CI->security->get_csrf_token_name();
    $value = $CI->security->get_csrf_hash();
    return "<input type='hidden' name='$name' value='$value' >";
}

function check_param($param, $error = NULL) {
    if (!$param) {
        redirect(base_url("General/error/$error"));
    }
}

function get_issuance_country() {
    return array(
        "AFGHANISTAN",
        "ALBANIA",
        "ALGERIA",
        "AMERICAN_SAMOA",
        "ANDORRA",
        "ANGOLA",
        "ANGUILLA",
        "ANTARCTICA",
        "ANTIGUA_AND_BARBUDA",
        "ARGENTINA",
        "ARMENIA",
        "ARUBA",
        "AUSTRALIA",
        "AUSTRIA",
        "AZERBAIJAN",
        "BAHAMAS_THE",
        "BAHRAIN",
        "BANGLADESH",
        "BARBADOS",
        "BELARUS",
        "BELGIUM",
        "BELIZE",
        "BENIN",
        "BERMUDA",
        "BHUTAN",
        "BOLIVIA",
        "BOSNIA_AND_HERZEGOVINA",
        "BOTSWANA",
        "BOUVET_ISLAND",
        "BRAZIL",
        "BRITISH_INDIAN_OCEAN_TERRITORY",
        "BRUNEI_DARUSSALAM",
        "BULGARIA",
        "BURKINA_FASO",
        "BURUNDI",
        "CAMBODIA",
        "CAMEROON",
        "CANADA",
        "CAPE_VERDE",
        "CAYMAN_ISLANDS",
        "CENTRAL_AFRICAN_REPUBLIC",
        "CHAD",
        "CHANNEL_ISLANDS",
        "CHILE",
        "CHINA",
        "CHRISTMAS_ISLAND",
        "COCOS_KEELING_ISLANDS",
        "COLOMBIA",
        "COMOROS",
        "CONGO_DEM_REP",
        "CONGO_REP",
        "COOK_ISLANDS",
        "COSTA_RICA",
        "COTE_D_IVOIRE",
        "CROATIA",
        "CUBA",
        "CYPRUS",
        "CZECH_REPUBLIC",
        "DENMARK",
        "DJIBOUTI",
        "DOMINICA",
        "DOMINICAN_REPUBLIC",
        "ECUADOR",
        "EGYPT_ARAB_REP",
        "EL_SALVADOR",
        "EQUATORIAL_GUINEA",
        "MALDIVES",
        "MALI",
        "MALTA",
        "MARSHALL_ISLANDS",
        "MARTINIQUE",
        "MAURITANIA",
        "MAURITIUS",
        "MAYOTTE",
        "MEXICO",
        "MICRONESIA_FED_STS",
        "MOLDOVA",
        "MONACO",
        "MONGOLIA",
        "MONTENEGRO",
        "MONTSERRAT",
        "MOROCCO",
        "MOZAMBIQUE",
        "MYANMAR",
        "NAMIBIA",
        "NAURU",
        "NEPAL",
        "NETHERLANDS",
        "NETHERLANDS_ANTILLES",
        "NEW_CALEDONIA",
        "NEW_ZEALAND",
        "NICARAGUA",
        "NIGER",
        "NIGERIA",
        "NIUE",
        "NORFOLK_ISLAND",
        "NORTHERN_MARIANA_ISLANDS",
        "NORWAY",
        "OMAN",
        "PAKISTAN",
        "PALAU",
        "PANAMA",
        "PAPUA_NEW_GUINEA",
        "PARAGUAY",
        "PERU",
        "PHILIPPINES",
        "PITCAIRN",
        "POLAND",
        "PORTUGAL",
        "PUERTO_RICO",
        "QATAR",
        "REUNION",
        "ROMANIA",
        "RUSSIAN_FEDERATION",
        "RWANDA",
        "SAMOA",
        "SAN_MARINO",
        "SAO_TOME_AND_PRINCIPE",
        "SAUDI_ARABIA",
        "SENEGAL",
        "SERBIA",
        "SEYCHELLES",
        "SIERRA_LEONE",
        "SINGAPORE",
        "SLOVAK_REPUBLIC",
        "SLOVENIA",
        "SOLOMON_ISLANDS",
        "SOMALIA",
        "SOUTH_AFRICA",
        "SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS",
        "SPAIN",
        "SRI_LANKA",
        "ST_HELENA",
        "ST_KITTS_AND_NEVIS",
        "ST_LUCIA",
        "ST_PIERRE_AND_MIQUELON",
        "ST_VINCENT_AND_THE_GRENADINES",
        "SUDAN",
        "SURINAME",
        "SVALBARD_AND_JAN_MAYEN_ISLANDS",
        "SWAZILAND",
        "SWEDEN",
        "SWITZERLAND",
        "SYRIAN_ARAB_REPUBLIC",
        "TAIWAN_PROVINCE_OF_CHINA",
        "TAJIKISTAN",
        "TANZANIA",
        "THAILAND",
        "TIMORLESTE",
        "TOGO",
        "TOKELAU",
        "TONGA",
        "TRINIDAD_AND_TOBAGO",
        "TUNISIA",
        "TURKEY",
        "TURKMENISTAN",
        "TURKS_AND_CAICOS_ISLANDS",
        "TUVALU",
        "UGANDA",
        "UKRAINE",
        "UNITED_ARAB_EMIRATES",
        "UNITED_KINGDOM",
        "UNITED_STATES",
        "UNITED_STATES_MINOR_OUTLYING_ISLANDS",
        "URUGUAY",
        "UZBEKISTAN",
        "VANUATU",
        "VATICAN_CITY_STATE",
        "VENEZUELA_RB",
        "VIETNAM",
        "VIRGIN_ISLANDS_BRITISH",
        "VIRGIN_ISLANDS_US",
        "WALLIS_AND_FUTUNA_ISLANDS",
        "WEST_BANK_AND_GAZA",
        "WESTERN_SAHARA",
        "YEMEN_REP",
        "ZAMBIA",
        "ZIMBABWE",
        "ERITREA",
        "ESTONIA",
        "ETHIOPIA",
        "FAEROE_ISLANDS",
        "FALKLAND_ISLANDS_MALVINAS",
        "FIJI",
        "FINLAND",
        "FRANCE",
        "FRANCE_METROPOLITAN",
        "FRENCH_GUIANA",
        "FRENCH_POLYNESIA",
        "FRENCH_SOUTHERN_TERRITORIES",
        "GABON",
        "GAMBIA_THE",
        "GEORGIA",
        "GERMANY",
        "GHANA",
        "GIBRALTAR",
        "GREECE",
        "GREENLAND",
        "GRENADA",
        "GUADELOUPE",
        "GUAM",
        "GUATEMALA",
        "GUINEA",
        "GUINEABISSAU",
        "GUYANA",
        "HAITI",
        "HEARD_AND_MCDONALD_ISLANDS",
        "HONDURAS",
        "HONG_KONG_CHINA",
        "HUNGARY",
        "ICELAND",
        "INDIA",
        "INDONESIA",
        "IRAN_ISLAMIC_REP",
        "IRAQ",
        "IRELAND",
        "ISLE_OF_MAN",
        "ISRAEL",
        "ITALY",
        "JAMAICA",
        "JAPAN",
        "JORDAN",
        "KAZAKHSTAN",
        "KENYA",
        "KIRIBATI",
        "KOREA_DEM_REP",
        "KOREA_REP",
        "KUWAIT",
        "KYRGYZ_REPUBLIC",
        "LAO_PDR",
        "LATVIA",
        "LEBANON",
        "LESOTHO",
        "LIBERIA",
        "LIBYA",
        "LIECHTENSTEIN",
        "LITHUANIA",
        "LUXEMBOURG",
        "MACAO_CHINA",
        "MACEDONIA_FYR",
        "MADAGASCAR",
        "MALAWI",
        "MALAYSIA"
    );
}

function api_date($date = NULL) {
    if ($date) {
        $date = str_replace('/', '-', $date);
        return date("Y-m-d", strtotime($date)) . "T" . date("H:i:s", strtotime($date));
    }
    return date("Y-m-d") . "T" . date("H:i:s");
}

function get_all_countries() {
    return array(
        "AF" => "Afghanistan",
        "AL" => "Albania",
        "DZ" => "Algeria",
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua and Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AU" => "Australia",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BD" => "Bangladesh",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia and Herzegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "BN" => "Brunei Darussalam",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "CV" => "Cape Verde",
        "KY" => "Cayman Islands",
        "CF" => "Central African Republic",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China",
        "CX" => "Christmas Island",
        "CC" => "Cocos (Keeling) Islands",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CG" => "Congo",
        "CD" => "Congo, the Democratic Republic of the",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Cote D'Ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands (Malvinas)",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French Southern Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GN" => "Guinea",
        "GW" => "Guinea-Bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard Island and Mcdonald Islands",
        "VA" => "Holy See (Vatican City State)",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran, Islamic Republic of",
        "IQ" => "Iraq",
        "IE" => "Ireland",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "KP" => "Korea, Democratic People's Republic of",
        "KR" => "Korea, Republic of",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Lao People's Democratic Republic",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libyan Arab Jamahiriya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macao",
        "MK" => "Macedonia, the Former Yugoslav Republic of",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia, Federated States of",
        "MD" => "Moldova, Republic of",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "AN" => "Netherlands Antilles",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Northern Mariana Islands",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau",
        "PS" => "Palestinian Territory, Occupied",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn",
        "PL" => "Poland",
        "PT" => "Portugal",
        "PR" => "Puerto Rico",
        "QA" => "Qatar",
        "RE" => "Reunion",
        "RO" => "Romania",
        "RU" => "Russian Federation",
        "RW" => "Rwanda",
        "SH" => "Saint Helena",
        "KN" => "Saint Kitts and Nevis",
        "LC" => "Saint Lucia",
        "PM" => "Saint Pierre and Miquelon",
        "VC" => "Saint Vincent and the Grenadines",
        "WS" => "Samoa",
        "SM" => "San Marino",
        "ST" => "Sao Tome and Principe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "CS" => "Serbia and Montenegro",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia and the South Sandwich Islands",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syrian Arab Republic",
        "TW" => "Taiwan, Province of China",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania, United Republic of",
        "TH" => "Thailand",
        "TL" => "Timor-Leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad and Tobago",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks and Caicos Islands",
        "TV" => "Tuvalu",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "US" => "United States",
        "UM" => "United States Minor Outlying Islands",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VE" => "Venezuela",
        "VN" => "Viet Nam",
        "VG" => "Virgin Islands, British",
        "VI" => "Virgin Islands, U.s.",
        "WF" => "Wallis and Futuna",
        "EH" => "Western Sahara",
        "YE" => "Yemen",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe"
    );
}

function get_all_nationals() {
    return array(
        'Afghan',
        'Albanian',
        'Algerian',
        'American',
        'Andorran',
        'Angolan',
        'Antiguans',
        'Argentinean',
        'Armenian',
        'Australian',
        'Austrian',
        'Azerbaijani',
        'Bahamian',
        'Bahraini',
        'Bangladeshi',
        'Barbadian',
        'Barbudans',
        'Batswana',
        'Belarusian',
        'Belgian',
        'Belizean',
        'Beninese',
        'Bhutanese',
        'Bolivian',
        'Bosnian',
        'Brazilian',
        'British',
        'Bruneian',
        'Bulgarian',
        'Burkinabe',
        'Burmese',
        'Burundian',
        'Cambodian',
        'Cameroonian',
        'Canadian',
        'Cape Verdean',
        'Central African',
        'Chadian',
        'Chilean',
        'Chinese',
        'Colombian',
        'Comoran',
        'Congolese',
        'Costa Rican',
        'Croatian',
        'Cuban',
        'Cypriot',
        'Czech',
        'Danish',
        'Djibouti',
        'Dominican',
        'Dutch',
        'East Timorese',
        'Ecuadorean',
        'Egyptian',
        'Emirian',
        'Equatorial Guinean',
        'Eritrean',
        'Estonian',
        'Ethiopian',
        'Fijian',
        'Filipino',
        'Finnish',
        'French',
        'Gabonese',
        'Gambian',
        'Georgian',
        'German',
        'Ghanaian',
        'Greek',
        'Grenadian',
        'Guatemalan',
        'Guinea-Bissauan',
        'Guinean',
        'Guyanese',
        'Haitian',
        'Herzegovinian',
        'Honduran',
        'Hungarian',
        'I-Kiribati',
        'Icelander',
        'Indian',
        'Indonesian',
        'Iranian',
        'Iraqi',
        'Irish',
        'Israeli',
        'Italian',
        'Ivorian',
        'Jamaican',
        'Japanese',
        'Jordanian',
        'Kazakhstani',
        'Kenyan',
        'Kittian and Nevisian',
        'Kuwaiti',
        'Kyrgyz',
        'Laotian',
        'Latvian',
        'Lebanese',
        'Liberian',
        'Libyan',
        'Liechtensteiner',
        'Lithuanian',
        'Luxembourger',
        'Macedonian',
        'Malagasy',
        'Malawian',
        'Malaysian',
        'Maldivan',
        'Malian',
        'Maltese',
        'Marshallese',
        'Mauritanian',
        'Mauritian',
        'Mexican',
        'Micronesian',
        'Moldovan',
        'Monacan',
        'Mongolian',
        'Moroccan',
        'Mosotho',
        'Motswana',
        'Mozambican',
        'Namibian',
        'Nauruan',
        'Nepalese',
        'New Zealander',
        'Nicaraguan',
        'Nigerian',
        'Nigerien',
        'North Korean',
        'Northern Irish',
        'Norwegian',
        'Omani',
        'Pakistani',
        'Palauan',
        'Panamanian',
        'Papua New Guinean',
        'Paraguayan',
        'Peruvian',
        'Polish',
        'Portuguese',
        'Qatari',
        'Romanian',
        'Russian',
        'Rwandan',
        'Saint Lucian',
        'Salvadoran',
        'Samoan',
        'San Marinese',
        'Sao Tomean',
        'Saudi',
        'Scottish',
        'Senegalese',
        'Serbian',
        'Seychellois',
        'Sierra Leonean',
        'Singaporean',
        'Slovakian',
        'Slovenian',
        'Solomon Islander',
        'Somali',
        'South African',
        'South Korean',
        'Spanish',
        'Sri Lankan',
        'Sudanese',
        'Surinamer',
        'Swazi',
        'Swedish',
        'Swiss',
        'Syrian',
        'Taiwanese',
        'Tajik',
        'Tanzanian',
        'Thai',
        'Togolese',
        'Tongan',
        'Trinidadian/Tobagonian',
        'Tunisian',
        'Turkish',
        'Tuvaluan',
        'Ugandan',
        'Ukrainian',
        'Uruguayan',
        'Uzbekistani',
        'Venezuelan',
        'Vietnamese',
        'Welsh',
        'Yemenite',
        'Zambian',
        'Zimbabwean'
    );
}

function mobile_countries_code() {
    return array(
        'AD' => array('name' => 'ANDORRA', 'code' => '376'),
        'AE' => array('name' => 'UNITED ARAB EMIRATES', 'code' => '971'),
        'AF' => array('name' => 'AFGHANISTAN', 'code' => '93'),
        'AG' => array('name' => 'ANTIGUA AND BARBUDA', 'code' => '1268'),
        'AI' => array('name' => 'ANGUILLA', 'code' => '1264'),
        'AL' => array('name' => 'ALBANIA', 'code' => '355'),
        'AM' => array('name' => 'ARMENIA', 'code' => '374'),
        'AN' => array('name' => 'NETHERLANDS ANTILLES', 'code' => '599'),
        'AO' => array('name' => 'ANGOLA', 'code' => '244'),
        'AQ' => array('name' => 'ANTARCTICA', 'code' => '672'),
        'AR' => array('name' => 'ARGENTINA', 'code' => '54'),
        'AS' => array('name' => 'AMERICAN SAMOA', 'code' => '1684'),
        'AT' => array('name' => 'AUSTRIA', 'code' => '43'),
        'AU' => array('name' => 'AUSTRALIA', 'code' => '61'),
        'AW' => array('name' => 'ARUBA', 'code' => '297'),
        'AZ' => array('name' => 'AZERBAIJAN', 'code' => '994'),
        'BA' => array('name' => 'BOSNIA AND HERZEGOVINA', 'code' => '387'),
        'BB' => array('name' => 'BARBADOS', 'code' => '1246'),
        'BD' => array('name' => 'BANGLADESH', 'code' => '880'),
        'BE' => array('name' => 'BELGIUM', 'code' => '32'),
        'BF' => array('name' => 'BURKINA FASO', 'code' => '226'),
        'BG' => array('name' => 'BULGARIA', 'code' => '359'),
        'BH' => array('name' => 'BAHRAIN', 'code' => '973'),
        'BI' => array('name' => 'BURUNDI', 'code' => '257'),
        'BJ' => array('name' => 'BENIN', 'code' => '229'),
        'BL' => array('name' => 'SAINT BARTHELEMY', 'code' => '590'),
        'BM' => array('name' => 'BERMUDA', 'code' => '1441'),
        'BN' => array('name' => 'BRUNEI DARUSSALAM', 'code' => '673'),
        'BO' => array('name' => 'BOLIVIA', 'code' => '591'),
        'BR' => array('name' => 'BRAZIL', 'code' => '55'),
        'BS' => array('name' => 'BAHAMAS', 'code' => '1242'),
        'BT' => array('name' => 'BHUTAN', 'code' => '975'),
        'BW' => array('name' => 'BOTSWANA', 'code' => '267'),
        'BY' => array('name' => 'BELARUS', 'code' => '375'),
        'BZ' => array('name' => 'BELIZE', 'code' => '501'),
        'CA' => array('name' => 'CANADA', 'code' => '1'),
        'CC' => array('name' => 'COCOS (KEELING) ISLANDS', 'code' => '61'),
        'CD' => array('name' => 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'code' => '243'),
        'CF' => array('name' => 'CENTRAL AFRICAN REPUBLIC', 'code' => '236'),
        'CG' => array('name' => 'CONGO', 'code' => '242'),
        'CH' => array('name' => 'SWITZERLAND', 'code' => '41'),
        'CI' => array('name' => 'COTE D IVOIRE', 'code' => '225'),
        'CK' => array('name' => 'COOK ISLANDS', 'code' => '682'),
        'CL' => array('name' => 'CHILE', 'code' => '56'),
        'CM' => array('name' => 'CAMEROON', 'code' => '237'),
        'CN' => array('name' => 'CHINA', 'code' => '86'),
        'CO' => array('name' => 'COLOMBIA', 'code' => '57'),
        'CR' => array('name' => 'COSTA RICA', 'code' => '506'),
        'CU' => array('name' => 'CUBA', 'code' => '53'),
        'CV' => array('name' => 'CAPE VERDE', 'code' => '238'),
        'CX' => array('name' => 'CHRISTMAS ISLAND', 'code' => '61'),
        'CY' => array('name' => 'CYPRUS', 'code' => '357'),
        'CZ' => array('name' => 'CZECH REPUBLIC', 'code' => '420'),
        'DE' => array('name' => 'GERMANY', 'code' => '49'),
        'DJ' => array('name' => 'DJIBOUTI', 'code' => '253'),
        'DK' => array('name' => 'DENMARK', 'code' => '45'),
        'DM' => array('name' => 'DOMINICA', 'code' => '1767'),
        'DO' => array('name' => 'DOMINICAN REPUBLIC', 'code' => '1809'),
        'DZ' => array('name' => 'ALGERIA', 'code' => '213'),
        'EC' => array('name' => 'ECUADOR', 'code' => '593'),
        'EE' => array('name' => 'ESTONIA', 'code' => '372'),
        'EG' => array('name' => 'EGYPT', 'code' => '20'),
        'ER' => array('name' => 'ERITREA', 'code' => '291'),
        'ES' => array('name' => 'SPAIN', 'code' => '34'),
        'ET' => array('name' => 'ETHIOPIA', 'code' => '251'),
        'FI' => array('name' => 'FINLAND', 'code' => '358'),
        'FJ' => array('name' => 'FIJI', 'code' => '679'),
        'FK' => array('name' => 'FALKLAND ISLANDS (MALVINAS)', 'code' => '500'),
        'FM' => array('name' => 'MICRONESIA, FEDERATED STATES OF', 'code' => '691'),
        'FO' => array('name' => 'FAROE ISLANDS', 'code' => '298'),
        'FR' => array('name' => 'FRANCE', 'code' => '33'),
        'GA' => array('name' => 'GABON', 'code' => '241'),
        'GB' => array('name' => 'UNITED KINGDOM', 'code' => '44'),
        'GD' => array('name' => 'GRENADA', 'code' => '1473'),
        'GE' => array('name' => 'GEORGIA', 'code' => '995'),
        'GH' => array('name' => 'GHANA', 'code' => '233'),
        'GI' => array('name' => 'GIBRALTAR', 'code' => '350'),
        'GL' => array('name' => 'GREENLAND', 'code' => '299'),
        'GM' => array('name' => 'GAMBIA', 'code' => '220'),
        'GN' => array('name' => 'GUINEA', 'code' => '224'),
        'GQ' => array('name' => 'EQUATORIAL GUINEA', 'code' => '240'),
        'GR' => array('name' => 'GREECE', 'code' => '30'),
        'GT' => array('name' => 'GUATEMALA', 'code' => '502'),
        'GU' => array('name' => 'GUAM', 'code' => '1671'),
        'GW' => array('name' => 'GUINEA-BISSAU', 'code' => '245'),
        'GY' => array('name' => 'GUYANA', 'code' => '592'),
        'HK' => array('name' => 'HONG KONG', 'code' => '852'),
        'HN' => array('name' => 'HONDURAS', 'code' => '504'),
        'HR' => array('name' => 'CROATIA', 'code' => '385'),
        'HT' => array('name' => 'HAITI', 'code' => '509'),
        'HU' => array('name' => 'HUNGARY', 'code' => '36'),
        'ID' => array('name' => 'INDONESIA', 'code' => '62'),
        'IE' => array('name' => 'IRELAND', 'code' => '353'),
        'IL' => array('name' => 'ISRAEL', 'code' => '972'),
        'IM' => array('name' => 'ISLE OF MAN', 'code' => '44'),
        'IN' => array('name' => 'INDIA', 'code' => '91'),
        'IQ' => array('name' => 'IRAQ', 'code' => '964'),
        'IR' => array('name' => 'IRAN, ISLAMIC REPUBLIC OF', 'code' => '98'),
        'IS' => array('name' => 'ICELAND', 'code' => '354'),
        'IT' => array('name' => 'ITALY', 'code' => '39'),
        'JM' => array('name' => 'JAMAICA', 'code' => '1876'),
        'JO' => array('name' => 'JORDAN', 'code' => '962'),
        'JP' => array('name' => 'JAPAN', 'code' => '81'),
        'KE' => array('name' => 'KENYA', 'code' => '254'),
        'KG' => array('name' => 'KYRGYZSTAN', 'code' => '996'),
        'KH' => array('name' => 'CAMBODIA', 'code' => '855'),
        'KI' => array('name' => 'KIRIBATI', 'code' => '686'),
        'KM' => array('name' => 'COMOROS', 'code' => '269'),
        'KN' => array('name' => 'SAINT KITTS AND NEVIS', 'code' => '1869'),
        'KP' => array('name' => 'KOREA DEMOCRATIC PEOPLES REPUBLIC OF', 'code' => '850'),
        'KR' => array('name' => 'KOREA REPUBLIC OF', 'code' => '82'),
        'KW' => array('name' => 'KUWAIT', 'code' => '965'),
        'KY' => array('name' => 'CAYMAN ISLANDS', 'code' => '1345'),
        'KZ' => array('name' => 'KAZAKSTAN', 'code' => '7'),
        'LA' => array('name' => 'LAO PEOPLES DEMOCRATIC REPUBLIC', 'code' => '856'),
        'LB' => array('name' => 'LEBANON', 'code' => '961'),
        'LC' => array('name' => 'SAINT LUCIA', 'code' => '1758'),
        'LI' => array('name' => 'LIECHTENSTEIN', 'code' => '423'),
        'LK' => array('name' => 'SRI LANKA', 'code' => '94'),
        'LR' => array('name' => 'LIBERIA', 'code' => '231'),
        'LS' => array('name' => 'LESOTHO', 'code' => '266'),
        'LT' => array('name' => 'LITHUANIA', 'code' => '370'),
        'LU' => array('name' => 'LUXEMBOURG', 'code' => '352'),
        'LV' => array('name' => 'LATVIA', 'code' => '371'),
        'LY' => array('name' => 'LIBYAN ARAB JAMAHIRIYA', 'code' => '218'),
        'MA' => array('name' => 'MOROCCO', 'code' => '212'),
        'MC' => array('name' => 'MONACO', 'code' => '377'),
        'MD' => array('name' => 'MOLDOVA, REPUBLIC OF', 'code' => '373'),
        'ME' => array('name' => 'MONTENEGRO', 'code' => '382'),
        'MF' => array('name' => 'SAINT MARTIN', 'code' => '1599'),
        'MG' => array('name' => 'MADAGASCAR', 'code' => '261'),
        'MH' => array('name' => 'MARSHALL ISLANDS', 'code' => '692'),
        'MK' => array('name' => 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'code' => '389'),
        'ML' => array('name' => 'MALI', 'code' => '223'),
        'MM' => array('name' => 'MYANMAR', 'code' => '95'),
        'MN' => array('name' => 'MONGOLIA', 'code' => '976'),
        'MO' => array('name' => 'MACAU', 'code' => '853'),
        'MP' => array('name' => 'NORTHERN MARIANA ISLANDS', 'code' => '1670'),
        'MR' => array('name' => 'MAURITANIA', 'code' => '222'),
        'MS' => array('name' => 'MONTSERRAT', 'code' => '1664'),
        'MT' => array('name' => 'MALTA', 'code' => '356'),
        'MU' => array('name' => 'MAURITIUS', 'code' => '230'),
        'MV' => array('name' => 'MALDIVES', 'code' => '960'),
        'MW' => array('name' => 'MALAWI', 'code' => '265'),
        'MX' => array('name' => 'MEXICO', 'code' => '52'),
        'MY' => array('name' => 'MALAYSIA', 'code' => '60'),
        'MZ' => array('name' => 'MOZAMBIQUE', 'code' => '258'),
        'NA' => array('name' => 'NAMIBIA', 'code' => '264'),
        'NC' => array('name' => 'NEW CALEDONIA', 'code' => '687'),
        'NE' => array('name' => 'NIGER', 'code' => '227'),
        'NG' => array('name' => 'NIGERIA', 'code' => '234'),
        'NI' => array('name' => 'NICARAGUA', 'code' => '505'),
        'NL' => array('name' => 'NETHERLANDS', 'code' => '31'),
        'NO' => array('name' => 'NORWAY', 'code' => '47'),
        'NP' => array('name' => 'NEPAL', 'code' => '977'),
        'NR' => array('name' => 'NAURU', 'code' => '674'),
        'NU' => array('name' => 'NIUE', 'code' => '683'),
        'NZ' => array('name' => 'NEW ZEALAND', 'code' => '64'),
        'OM' => array('name' => 'OMAN', 'code' => '968'),
        'PA' => array('name' => 'PANAMA', 'code' => '507'),
        'PE' => array('name' => 'PERU', 'code' => '51'),
        'PF' => array('name' => 'FRENCH POLYNESIA', 'code' => '689'),
        'PG' => array('name' => 'PAPUA NEW GUINEA', 'code' => '675'),
        'PH' => array('name' => 'PHILIPPINES', 'code' => '63'),
        'PK' => array('name' => 'PAKISTAN', 'code' => '92'),
        'PL' => array('name' => 'POLAND', 'code' => '48'),
        'PM' => array('name' => 'SAINT PIERRE AND MIQUELON', 'code' => '508'),
        'PN' => array('name' => 'PITCAIRN', 'code' => '870'),
        'PR' => array('name' => 'PUERTO RICO', 'code' => '1'),
        'PT' => array('name' => 'PORTUGAL', 'code' => '351'),
        'PW' => array('name' => 'PALAU', 'code' => '680'),
        'PY' => array('name' => 'PARAGUAY', 'code' => '595'),
        'QA' => array('name' => 'QATAR', 'code' => '974'),
        'RO' => array('name' => 'ROMANIA', 'code' => '40'),
        'RS' => array('name' => 'SERBIA', 'code' => '381'),
        'RU' => array('name' => 'RUSSIAN FEDERATION', 'code' => '7'),
        'RW' => array('name' => 'RWANDA', 'code' => '250'),
        'SA' => array('name' => 'SAUDI ARABIA', 'code' => '966'),
        'SB' => array('name' => 'SOLOMON ISLANDS', 'code' => '677'),
        'SC' => array('name' => 'SEYCHELLES', 'code' => '248'),
        'SD' => array('name' => 'SUDAN', 'code' => '249'),
        'SE' => array('name' => 'SWEDEN', 'code' => '46'),
        'SG' => array('name' => 'SINGAPORE', 'code' => '65'),
        'SH' => array('name' => 'SAINT HELENA', 'code' => '290'),
        'SI' => array('name' => 'SLOVENIA', 'code' => '386'),
        'SK' => array('name' => 'SLOVAKIA', 'code' => '421'),
        'SL' => array('name' => 'SIERRA LEONE', 'code' => '232'),
        'SM' => array('name' => 'SAN MARINO', 'code' => '378'),
        'SN' => array('name' => 'SENEGAL', 'code' => '221'),
        'SO' => array('name' => 'SOMALIA', 'code' => '252'),
        'SR' => array('name' => 'SURINAME', 'code' => '597'),
        'ST' => array('name' => 'SAO TOME AND PRINCIPE', 'code' => '239'),
        'SV' => array('name' => 'EL SALVADOR', 'code' => '503'),
        'SY' => array('name' => 'SYRIAN ARAB REPUBLIC', 'code' => '963'),
        'SZ' => array('name' => 'SWAZILAND', 'code' => '268'),
        'TC' => array('name' => 'TURKS AND CAICOS ISLANDS', 'code' => '1649'),
        'TD' => array('name' => 'CHAD', 'code' => '235'),
        'TG' => array('name' => 'TOGO', 'code' => '228'),
        'TH' => array('name' => 'THAILAND', 'code' => '66'),
        'TJ' => array('name' => 'TAJIKISTAN', 'code' => '992'),
        'TK' => array('name' => 'TOKELAU', 'code' => '690'),
        'TL' => array('name' => 'TIMOR-LESTE', 'code' => '670'),
        'TM' => array('name' => 'TURKMENISTAN', 'code' => '993'),
        'TN' => array('name' => 'TUNISIA', 'code' => '216'),
        'TO' => array('name' => 'TONGA', 'code' => '676'),
        'TR' => array('name' => 'TURKEY', 'code' => '90'),
        'TT' => array('name' => 'TRINIDAD AND TOBAGO', 'code' => '1868'),
        'TV' => array('name' => 'TUVALU', 'code' => '688'),
        'TW' => array('name' => 'TAIWAN, PROVINCE OF CHINA', 'code' => '886'),
        'TZ' => array('name' => 'TANZANIA, UNITED REPUBLIC OF', 'code' => '255'),
        'UA' => array('name' => 'UKRAINE', 'code' => '380'),
        'UG' => array('name' => 'UGANDA', 'code' => '256'),
        'US' => array('name' => 'UNITED STATES', 'code' => '1'),
        'UY' => array('name' => 'URUGUAY', 'code' => '598'),
        'UZ' => array('name' => 'UZBEKISTAN', 'code' => '998'),
        'VA' => array('name' => 'HOLY SEE (VATICAN CITY STATE)', 'code' => '39'),
        'VC' => array('name' => 'SAINT VINCENT AND THE GRENADINES', 'code' => '1784'),
        'VE' => array('name' => 'VENEZUELA', 'code' => '58'),
        'VG' => array('name' => 'VIRGIN ISLANDS, BRITISH', 'code' => '1284'),
        'VI' => array('name' => 'VIRGIN ISLANDS, U.S.', 'code' => '1340'),
        'VN' => array('name' => 'VIET NAM', 'code' => '84'),
        'VU' => array('name' => 'VANUATU', 'code' => '678'),
        'WF' => array('name' => 'WALLIS AND FUTUNA', 'code' => '681'),
        'WS' => array('name' => 'SAMOA', 'code' => '685'),
        'XK' => array('name' => 'KOSOVO', 'code' => '381'),
        'YE' => array('name' => 'YEMEN', 'code' => '967'),
        'YT' => array('name' => 'MAYOTTE', 'code' => '262'),
        'ZA' => array('name' => 'SOUTH AFRICA', 'code' => '27'),
        'ZM' => array('name' => 'ZAMBIA', 'code' => '260'),
        'ZW' => array('name' => 'ZIMBABWE', 'code' => '263')
    );
}

function redirect_post($url, array $data, array $headers = null) {

    $html = "<html><body><form id='form' action='$url' method='post'>";
    foreach ($data as $key => $value) {
        $html .= csrf();
        $html .= "<input type='hidden' name='$key' value='$value'>";
    }
    $html .= "</form><script>document.getElementById('form').submit();</script>";
    $html .= "</body></html>";
    print($html);
}

function login_change_lang() {
    $CI = &get_instance();
    $lang = 'EN';
    if ($CI->session->userdata("lang") && $CI->session->userdata("lang") == 'arabic') {
        $lang = 'AR';
    }

    $return = '<ul class="header-menu nav navbar-nav">
            <li class="dropdown" id="language-header">
                <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                    <i class="icon-globe"></i>
                    <span id="lang_text">' . $lang . '</span>
                </a>
                <ul class="dropdown-menu language-menu">';
    if ($lang == 'AR') {
        $return .= '<li>
                            <a href="' . base_url() . 'Language" data-lang="en"><span>English</span></a>
                        </li>';
    } else {
        $return .= '<li>
                            <a href="' . base_url() . 'Language/index/arabic" data-lang="ar"><span>العربية</span></a>
                        </li>';
    }
    $return .= '</ul>
            </li>
        </ul>';
    return $return;
}

function add_vat($amount) {
    $amount += ($amount * 14) / 100;
    return $amount;
}

function is_active($menu_index, $sub_menu = 0) {
    $CI = &get_instance();
    $active_session = $sub_menu == 1 ? $CI->session->userdata('sub_active') : $CI->session->userdata('active');
    if ($active_session == $menu_index) {
        return '-active active';
    }
    return '';
}

function get_password_complex_hint() {
    $CI = &get_instance();
    $CI->load->model("Settings_model");
    $settings = $CI->Settings_model->get_password_management();

    if (!$settings) {
        return '';
    }

    $msg = '';
    if ($settings->min_length) {
        $msg .= sprintf($CI->lang->line('password_length'), $settings->min_length) . '<br/>';
    }

    if ($settings->complex) {
        $msg .= $CI->lang->line('complex_password');
    }

    return $msg;
}

function check_password_blacklist($password) {
    $CI = &get_instance();
    $row = $CI->db->get_where('PASSWORD_BLACKLIST', array('PASSWORD' => $password))->row();
    if ($row) {
        return TRUE;
    }
    return FALSE;
}
