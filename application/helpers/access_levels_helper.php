<?php

function get_level_functions(){
    $functions=[
        "access_levels",
        "users",
        "admins",
        "settings",
        "payments"
    ];
    return $functions;
}

//function check_p($module, $permission) {
//    $CI= &get_instance();
//    $permissions = $CI->session->userdata("permissions");
//    $p = $permissions[$module][$permission];
//    if (!$p) {
//        redirect(base_url("General/permission_error"));
//    }
//}
//
//function get_p($module, $permission) {
//    $CI= &get_instance();
//    $permissions = $CI->session->userdata("permissions");
//    $p = $permissions[$module][$permission];
//    return $p;
//}
//
//function or_p($arr) {
//    $CI= &get_instance();
//    $permissions = $CI->session->userdata("permissions");
//    $p = false;
//    foreach ($arr as $module => $permission) {
//        if ($permissions[$module][$permission]) {
//            $p = true;
//            break;
//        }
//    }
//    if (!$p) {
//        redirect(base_url("General/permission_error"));
//    }
//}
//
//function and_p($arr) {
//    $CI= &get_instance();
//    $permissions = $CI->session->userdata("permissions");
//    $p = true;
//    foreach ($arr as $module => $permission) {
//        if (!$permissions[$module][$permission]) {
//            $p = false;
//            break;
//        }
//    }
//    if (!$p) {
//        redirect(base_url("General/permission_error"));
//    }
//}

function load_permissions($level_id){
    $CI= &get_instance();
    $perms=  $CI->db->where("level_id",$level_id)->get("level_functions")->row();
    $functions=  get_level_functions();
    $permissions=[];
    foreach ($functions as $function){
        $permissions[$function]['v']=substr($perms->$function, 0, 1);
        $permissions[$function]['c']=substr($perms->$function, 1, 1);
        $permissions[$function]['u']=substr($perms->$function, 2, 1);
        $permissions[$function]['d']=substr($perms->$function, 3, 1);
    }
    
    $CI->session->set_userdata("permissions",$permissions);
}

function get_level_id($access_level=NULL){
    if(!$access_level){
        $CI= &get_instance();
        return $CI->session->userdata("user_level_id");
    }
    switch ($access_level){
        //customer_administrator, branch_operator
        case "super_admin": 
            return "1";
            break;
    }
}

function compare_level($access_level,$level_id=NULL){
    return FALSE;
//    $CI= &get_instance();
//    $compare_id=$level_id;
//    if(!$level_id){
//        $compare_id=$CI->session->userdata("user_level_id");
//    }
//    if(get_level_id($access_level)===$compare_id){
//        return TRUE;
//    }
//    return FALSE;
}

function get_p($module,$permission){
    $CI=&get_instance();
    if($CI->session->userdata("admin")==1){
        return TRUE;
    }
    return FALSE;
}

function check_p($module,$permission){
    $CI=&get_instance();
    if($CI->session->userdata("admin")==1){
        return TRUE;
    }
if (!$CI->session->userdata("logged_in") || !$CI->session->userdata('session_id')) {
        redirect(base_url('Login'));
    }
    echo "Permission not granted to view this page";
    die();
}