<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <style>
            .icheckbox_minimal-grey{
                margin-top: 12px;
            }
        </style>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">                     <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("audit_trail") ?></h2>
                                </div>
                                <div class="panel-content">
                                    <div class="text-center">
                                        <a class="btn btn-primary btn-square" href="<?= base_url() ?>Reports/audit_trail"><?= $this->lang->line('reload_logs') ?></a>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">


                                            <table class="table table-hover audit_trail">
                                                <thead>
                                                    <tr>
                                                        <th><?= $this->lang->line('action') ?></th>
                                                        <th><?= $this->lang->line('user') ?></th>
                                                        <th><?= $this->lang->line('time') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php //foreach ($changes as $change) { ?>
<!--                                                        <tr>
                                                            <td><?//= $change->action ?></td>
                                                            <td><?//= $change->first_name . ' ' . $change->last_name ?></td>
                                                            <td><?//= $change->timestamp ?></td>
                                                        </tr>-->
                                                    <?php //} ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>

        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>

        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script>
        <script>
            $(function () {
                
                $(".audit_trail").DataTable({
                    // Processing indicator
                    "processing": true,
                    // DataTables server-side processing mode
                    "serverSide": true,
                    // Initial no order.
                    "order": [],
                    "ordering": false,
                    "pageLength": 10,
                    // Load data from an Ajax source
                    "ajax": {
                        "url": "<?php echo base_url('reports/audit_trail_ajax/'); ?>",
                        "type": "POST",
                        //"data": {"<?//= $this->security->get_csrf_token_name() ?>": get_cookie('ci_csrf_token')}
                    },
                    //Set column definition initialisation properties
                    "columnDefs": [{
                            "targets": [0],
                            "orderable": false
                        }]
                });
            });
        </script>
    </body>
</html>