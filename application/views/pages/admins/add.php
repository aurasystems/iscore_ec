<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $this->lang->line("add_admin") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form method="POST" action="" class="form-horizontal" role="form">
                                        <?= csrf() ?>
                                        <?= form_group_get("user") ?>
                                        <?= form_group_get("clientName") ?>
                                        <?= form_group_get("address") ?>
                                        <?= form_group_get("phoneNumber") ?>
                                        <?= form_group_get("email") ?>
                                        <?php
                                        $field = "password";
                                        $value = "";
                                        $dir = $this->session->userdata('lang') == "arabic" ? 'right' : 'left';
                                        $form_group = " <div class='form-group'>
                                                            <label class='col-sm-4 control-label'>" . $this->lang->line($field) . "</label>
                                                            <div class='col-sm-7 inside-tooltip'>
                                                                <input type='password' name='$field' id='$field' autocomplete='off' class='form-control' value='" . $value . "'>
                                                                    <i class='fa fa-question-circle c-blue' rel='popover' data-container='body' data-html='true' data-toggle='popover' data-placement='" . $dir . "' data-content='" . get_password_complex_hint() . "' data-original-title='" . $this->lang->line('password_complex_req') . "'></i>
                                                                <span class='c-red'>" . form_error($field) . "</span>
                                                            </div>
                                                        </div>";
                                        echo $form_group;
                                        ?>
                                        <div class="row">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-7 progress" style="display: none;">
                                                <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                                                     aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $field = "confirm_password";
                                        $value = "";
                                        $form_group = " <div class='form-group'>
                                                            <label class='col-sm-4 control-label'>" . $this->lang->line($field) . "</label>
                                                            <div class='col-sm-7'>
                                                                <input type='password' name='$field' id='$field' autocomplete='off' class='form-control' value='" . $value . "'>
                                                                <span class='c-red'>" . form_error($field) . "</span>
                                                            </div>
                                                        </div>";
                                        echo $form_group;
                                        ?>
                                        <div class="col-sm-12 text-center">
                                            <input type="submit" id="submit" class="btn btn-primary" value="<?= $this->lang->line("save") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                //            BEGIN PASSWORD STRENGTH
<?php if (!empty($password_management) && $password_management->complex) { ?>
                    $('#password').keyup(function () {
                        checkStrength($('#password').val());
                    });
                    function checkStrength(password) {

                        var strength = 0;
    <?php
    if (!empty($password_management) && $password_management->min_length) {
        ?>
                            var min_length = "<?= $password_management->min_length ?>";
                            if (password.length < parseInt(min_length)) {
                                progress_bar(25, "progress-bar-danger", "<?= $this->lang->line("too_short") ?>");
                                return;
                            } else {
                                strength += 1;
                            }
        <?php
    } else {
        ?>
                            strength += 1;
        <?php
    }
    ?>
                        // If password contains both lower and uppercase characters, increase strength value.
                        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                            strength += 1;
                        // If it has numbers and characters, increase strength value.
                        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                            strength += 1;
                        // If it has one special character, increase strength value.
                        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // If it has two special characters, increase strength value.
                        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // Calculated strength value, we can return messages
                        // If value is less than 2
                        console.log(strength);
                        if (strength < 2) {
                            progress_bar(50, "progress-bar-danger", "<?= $this->lang->line("weak") ?>");
                        } else if (strength <= 3) {
                            progress_bar(75, "progress-bar-warning", "<?= $this->lang->line("medium") ?>");
                        } else {
                            progress_bar(100, "progress-bar-success", "<?= $this->lang->line("strong") ?>");
                        }
                    }


                    function progress_bar(per, c, text) {
                        $("#result").css("width", per + "%");
                        $("#result").attr("aria-valuenow", per);
                        $("#result").removeClass("progress-bar-danger");
                        $("#result").removeClass("progress-bar-warning");
                        $("#result").removeClass("progress-bar-success");
                        $("#result").addClass(c);
                        $("#result").html(text);
                        $(".progress").show();
                        if (per == 100) {
                            $("#submit").removeAttr('disabled');
                        } else {
                            $("#submit").attr("disabled", "disabled");
                        }
                    }
<?php } ?>
//            END PASSWORD STRENGTH
            });
        </script>
    </body>
</html>