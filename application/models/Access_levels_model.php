<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Access_levels_model extends CI_Model
{
    function check_level_users($level_id){
        $this->db->where("level_id",$level_id);
        $this->db->limit(2);
        $query= $this->db->get("users");
        if($query->num_rows()>0){
            return TRUE;
        }
        return FALSE;
    }
    
    function get_level_permissions($level_id){
        $this->db->select("level_functions.*, access_levels.name as level_name");
        $this->db->where("level_functions.level_id",$level_id);
        $this->db->from("level_functions");
        $this->db->join("access_levels","access_levels.id = level_functions.level_id");
        $query= $this->db->get();
        return $query->row();
    }
    function get_fields($table) {
        return $this->db->list_fields($table);
    }
    function update_permissions($level_id,$data){
        $this->db->where("level_id",$level_id);
        $this->db->update("level_functions",$data);
    }
    
    function delete_functions($level_id){
        $this->db->where("level_id",$level_id);
        $this->db->delete("level_functions");
    }
    
    function get_last_id($name){
        $this->db->where("name",$name);
        $this->db->order_by("id","desc");
        return $this->db->get("access_levels")->row()->id;
    }
    
}