<?php

$lang['lang_dashboard'] = "Dashboard";
$lang['lang_settings'] = "Settings";
$lang['lang_back_settings'] = "Back Settings";
$lang['lang_front_settings'] = "Front Settings";
$lang['lang_email_settings'] = "Email Settings";
$lang['lang_success'] = "Success";
$lang['lang_system_name'] = "System Name";
$lang['lang_system_logo'] = "System Logo";
$lang['lang_front_logo'] = "Front Logo";
$lang['lang_footer_text'] = "Footer Text";
$lang['lang_save'] = "Save";
$lang['lang_cancel'] = "Cancel";
$lang['lang_front_title'] = "Title";
$lang['lang_smtp_host'] = "SMTP Host";
$lang['lang_host_mail'] = "SMTP MAIL";
$lang['lang_smtp_port'] = "SMTP PORT";
$lang['lang_smtp_user'] = "SMTP USER";
$lang['lang_smtp_pass'] = "SMTP PASS";
$lang['lang_basic_info'] = "Basic Information";
$lang['lang_first_name'] = "First Name";
$lang['lang_last_name'] = "Last Name";
$lang['lang_display_name'] = "Display Name";
$lang['lang_phone'] = "Phone";
$lang['lang_email_address'] = "Email";
$lang['lang_address'] = "Address";
$lang['lang_password'] = "Password";
$lang['lang_repeat_password'] = "Repeat your password";
$lang['lang_home'] = "Home";
$lang['lang_login'] = "Login";
$lang['lang_register'] = "Register";
$lang['lang_logout'] = "Logout";
$lang['lang_en'] = "EN";
$lang['lang_ar'] = "AR";
$lang['lang_edit_profile'] = "Edit Profile";
$lang['lang_client_profile'] = "Client Profile";
$lang['lang_user_profile'] = "User Profile";
$lang['lang_facebook'] = "Facebook";
$lang['lang_twitter'] = "Twitter";
$lang['lang_google_plus'] = "Google Plus";
$lang['lang_pinterest'] = "Pinterest";
$lang['lang_flickr'] = "Flickr";
$lang['lang_linkedin'] = "Linkedin";
$lang['lang_social_media'] = "Social Media";
$lang['lang_company_info'] = "Company Info";
$lang['lang_contact_info'] = "Contact Info";
$lang['lang_phone'] = "Phone";
$lang['lang_opening_time'] = "Opening Time";
$lang['lang_user_img'] = "User Image";
$lang['lang_upload_user_img'] = "Upload User Image";
$lang['lang_contact_us'] = "Contact Us";
$lang['lang_name'] = "Name";
$lang['lang_pass_reset_link'] = "Send Password Reset Link";
$lang['lang_forget_password'] = "Forgot password?";
$lang['lang_sign_up'] = "New here? Sign up";
$lang['lang_sign_up_now'] = "Sign up";
$lang['lang_sign_in'] = "Sign In";
$lang['lang_back_to_login'] = "Back to login";
$lang['lang_have_account'] = "Already have an account? Sign In";
$lang['lang_send_me_pass'] = "Send me Password";
$lang['lang_terms_conditions'] = "I agree with terms and conditions.";
$lang['lang_users'] = "Users";
$lang['lang_clients'] = "Clients";
$lang['lang_leave_msg'] = "Leave a message";
$lang['lang_send_msg'] = "Send Message";
$lang['lang_toggle_navi'] = "Toggle navigation";
$lang['lang_menu'] = "Menu";
$lang['lang_language'] = "Language";
$lang['lang_english'] = "English";
$lang['lang_espanol'] = "Español";
$lang['lang_francais'] = "Français";
$lang['lang_see_all_msgs'] = "See all messages";
$lang['lang_see_all_notif'] = "See all notifications";
$lang['lang_account_settings'] = "Account Settings";
$lang['lang_system_users'] = "System Users";
$lang['lang_add_user'] = "Add New User";
$lang['lang_actions'] = "Actions";
$lang['lang_view_users'] = "All Users";
$lang['lang_edit_user'] = "Edit User";
$lang['lang_delete_user'] = "Delete User";
$lang['lang_confirm_delete_user'] = "Are you sure to delete this user?";
$lang['lang_system_clients'] = "Systems Clients";
$lang['lang_active_clients'] = "Active Clients";
$lang['lang_waiting_clients'] = "Waiting Clients";
$lang['lang_banned_clients'] = "Banned Clients";
$lang['lang_ban'] = "Ban";
$lang['lang_active'] = "Active";
$lang['lang_#'] = "#";
$lang['lang_first'] = "First";
$lang['lang_last'] = "Last";
$lang['lang_display'] = "Display";
$lang['lang_action'] = "Action";
$lang['lang_receive_msg'] = "To receive a new password, enter your email address below.";
$lang['lang_thank_msg'] = "Thank You. A password reset link was sent to you. Please, check your inbox.";
$lang['lang_email_sent'] = "Thank You. Your message has been sent successfully.";
$lang['lang_go_to_login'] = "Go To Login";
$lang['lang_register_success'] = "You have been registered successfully. We will activate your account soon. Thank you.";
$lang['lang_edit_client'] = "Edit Client";
$lang['lang_arabic'] = "العربية";

//////////////////////////////
$lang['save'] = "Save";
$lang['search'] = "Search";
$lang['select'] = "Select";
$lang['PublicationSearch'] = "Publication Search";
$lang['partnerType'] = "Partner Type";
$lang['publicationNumber'] = "Publication Number";
$lang['nationalID'] = "National ID";
$lang['passportNumber'] = "Passport Number";
$lang['issuanceCountry'] = "Issuance Country";
$lang['registrationNumber'] = "Registration Number";


$lang['CreateOrUpdateUser'] = "Register";
$lang['user'] = "Login Name";
$lang['email'] = "Email";
$lang['address'] = "Address";
$lang['clientName'] = "Name";
$lang['phoneNumber'] = "Phone Number";
$lang['password'] = "Password";
$lang['confirm_password'] = "Confirm Password";
$lang['subscribe_annually'] = "Would you like to subscribe annually?";
$lang['subscription'] = "Subscription";
$lang['download_document'] = "Download Document";
$lang['publicationNumber'] = "Publication Number";
$lang['publicationDate'] = "Publication Date";
$lang['guaranteeAmount'] = "Guarantee Amount";
$lang['expirationDate'] = "Expiration Date";
$lang['bankruptDeptor'] = "Bankrupt Deptor";
$lang['creditorName'] = "Creditor Name";
$lang['pdf'] = "PDF";
$lang['SearchHitList'] = "Search History List";
$lang['SearchHistory'] = "Search History";
$lang['searchID'] = "Search ID";
$lang['startingDate'] = "Starting Date";
$lang['endDate'] = "End Date";
$lang['dateOfSearch'] = "Date Of Search";
$lang['nationalId'] = "National ID";
$lang['numberOfHits'] = "Number Of Hits";
$lang['action'] = "Action";
$lang['add_user'] = "Add User";
$lang['edit_user'] = "Edit User";
$lang['users'] = "Users";

//BEGIN PASSWORD MANAGEMENT
$lang['password_management'] = "Password Management";
$lang['enforce_history'] = "Enforce Password History";
$lang['max_age'] = "Maximum Password Age (Days)";
$lang['min_length'] = "Minimum Password Length";
$lang['complex_password_lang'] = "Complex Password";
$lang['lockout_duration'] = "Account Lockout Duration (Mins)";
$lang['lockout_threshold'] = "Account Lockout Threshold";
$lang['reset_counter'] = "Reset Account Lockout Counter After (Mins)";
$lang['save'] = "Save";
//END PASSWORD MANAGEMENT

$lang['account_lockedout'] = "Account Locked Out";
$lang['account_lockedout_msg'] = "You have reached the maximum number of trials. Your account is locked out. Please, try to login again in";
$lang['minutes'] = "minutes";
$lang['old_password'] = "Old Password";
$lang['new_password'] = "New Password";
$lang['change_password'] = "Change Password";
$lang["too_short"] = "Too Short";
$lang["weak"] = "Weak";
$lang["medium"] = "Medium";
$lang["strong"] = "Strong";

$lang['enforce_password_history_hint'] = "This security setting determines the number of unique new passwords that have to be associated with a user account, If disabled an old password can be reused.";
$lang['complex_password_hint'] = "This security setting determines whether passwords must meet complexity requirements, if this policy is enabled, passwords must meet the following minimum requirements, contain characters from three of the following four categories: English uppercase characters (A through Z), English lowercase characters (a through z), Base 10 digits (0 through 9), Non-alphabetic characters (for example: !, $, #, %). Complexity requirements are enforced when passwords are changed or created.";
$lang['max_age_hint'] = 'This security setting determines the period of time (in days) that a password can be used before the system requires the user to change it. You can set passwords to expire after a number of days between 1 and 999, or you can specify that passwords never expire by setting the number of days to 0.';
$lang['min_length_hint'] = 'This security setting determines the minimum number of characters required to be in the user password';
$lang['lockout_duration_hint'] = 'This security setting determines the number of minutes a locked-out account remains locked out before automatically becoming unlocked. The available range is from 0 minutes through 99,999 minutes. If you set the account lockout duration to 0, the account will be locked out until an administrator explicitly unlocks it.';
$lang['lockout_threshold_hint'] = 'This security setting determines the number of failed logon attempts that causes a user account to be locked out. A locked-out account cannot be used until it is reset by an administrator or until the lockout duration for the account has expired. You can set a value between 0 and 999 failed logon attempts. If you set the value to 0, the account will never be locked out.';
$lang['reset_counter_hint'] = 'This security setting determines the number of minutes that must elapse after a failed logon attempt before the failed logon attempt counter is reset to 0 bad logon attempts. The available range is 1 minute to 99,999 minutes.';

$lang['name'] = 'Name';
$lang['admins'] = 'Admins';
$lang['add_admin'] = 'Add Admin';
$lang['edit_admin'] = 'Edit Admin';
$lang['r_u_sure'] = 'Are you sure you want to delete this?';
$lang['user_payments'] = 'User Payments';
$lang['amount_paid'] = 'Amount Paid';
$lang['service'] = 'Service';
$lang['payment_date'] = 'Payment Date';
$lang['active_directory'] = 'Active Directory';
$lang['hostname'] = 'Hostname';
$lang['dc'] = 'DC';
$lang['ou'] = 'OU';
$lang['admin_dn'] = 'Admin Username';
$lang['admin_password'] = 'Admin Password';
$lang['second_hostname'] = 'Hostname';
$lang['second_dc'] = 'DC';
$lang['second_ou'] = 'OU';
$lang['second_admin_dn'] = 'Admin Username';
$lang['second_admin_password'] = 'Admin Password';
$lang['second_ad'] = 'Second Active Directory';
$lang['main_ad'] = 'Main Active Directory';
$lang['active_directory_settings'] = 'Active Directory Settings';
$lang['invalid_credentials'] = 'Invalid Credentials';
$lang['username'] = 'Username';


$lang['access_levels'] = 'Access Levels';
$lang['add_level'] = 'Add Level';
$lang['edit_level'] = 'Edit Level';
$lang['level_name'] = 'Level Name';
$lang['view'] = 'View';
$lang['create'] = 'Create';
$lang['update'] = 'Update';
$lang['delete'] = 'Delete';
$lang['settings'] = 'Settings';
$lang['payments'] = 'Payments';
$lang['permissions'] = 'Permissions';
$lang['force_reset'] = 'Force User To Reset Password on Login';
$lang['confirm_password'] = 'Confirm Password';
$lang['old_password_not_match'] = 'Old Password Did not Match';

$lang['subscribe'] = "Subscribe";
$lang['choose_subscription'] = "Choose Subscription";
$lang['subscribe_question'] = 'Your have to subscribe to activate your Search user, please select  either Annual Search  Subscription or One Time Search Subscription';
$lang['annual_subscription'] = "Annual Subscription";
$lang['one_time_subscription'] = "One Time Subscription";
$lang['PublicationSearch_report'] = "Publication Search Report";
$lang['SearchHistory_report'] = "Search History Report";
$lang['date_searched'] = "Date Searched";
$lang['filter'] = "Filter";
$lang['date_from'] = "Date From";
$lang['date_to'] = "Date To";
$lang['payments_report'] = "Payments Report";
$lang['user_id'] = "User";
$lang['reports'] = "Reports";
$lang['PublicationSearchResults'] = "Publication Search Results";
$lang['payment_settings'] = "Payment Settings";
$lang['secure_secret'] = "Secure Secret";
$lang['access_code'] = "Access Code";
$lang['merchant_id'] = "Merchant ID";
$lang['merchant_name'] = "Merchant Name";
$lang['api_username'] = "API Username";
$lang['api_password'] = "API Password";
$lang['inst_secure_secret'] = "Inst Secure Secret";
$lang['inst_access_code'] = "Inst Access Code";
$lang['inst_merchant_id'] = "Inst Merchant ID";
$lang['sms_settings'] = "SMS Settings";
$lang['url'] = "URL";
$lang['you_are_already_subscribed'] = "You Are Already Subscribed!";
$lang['subscription_expires_on'] = "Your subscription expires on:";
$lang['not_subscribed'] = "You are not subscribed in any plan";
$lang['renew_subscription'] = "Renew Subscription";
$lang['dashboard'] = "Dashboard";
$lang['total_users'] = "Total No Of Users";
$lang['total_admins'] = "Total No Of Admins";
$lang['total_searches'] = "Total No Of Searches";
$lang['month_searches'] = "Searches This Months";
$lang['month_payments'] = "Amount of Payments This Months";
$lang['search_results'] = "Search Results";
$lang['error'] = "ERROR! ";
$lang['success'] = "Success";
$lang['payment_successful'] = "Your Payment process has been completed successfully";
$lang['you_are_subscribed_one_time'] = "You are subscribed by one time search";

$lang['egy_n_person'] = "Egyptian Natural Person";
$lang['legal_entity'] = "Legal Entity operating in Egypt";
$lang['foreign_n_person'] = "Foreign Natural Person";
$lang['foreign_legal_entity'] = "Foreign legal entity with branch office in Egypt";
$lang['email_lang'] = "Required language of the e-mail";
$lang['creditor_name'] = "Creditor name";
$lang['mobile_num'] = "Mobile Number";
$lang['add_info'] = "Additional Info";
$lang['date_of_birth'] = "Date of birth";
$lang['gender'] = "Gender";
$lang['passport_num'] = "Passport Number";
$lang['issuance_country'] = "Issuance Country";
$lang['nationality'] = "Nationality";
$lang['institution_name'] = "Institution Name";
$lang['comm_reg_num'] = "Commercial registry number or Identification number";
$lang['establishment_date'] = "Establishment Date";
$lang['sole_proprietorship'] = "Sole Proprietorship";
$lang['private_company_limited_shares'] = "Private Company Limited Shares";
$lang['partner'] = "Partner";
$lang['others'] = "Corporation";
$lang['limited_partnership'] = "Limited Partnership";
$lang['limited_liability_company'] = "Limited Liability Company";
$lang['joint_stock_company'] = "Joint Stock Company";
$lang['general_partnership'] = "General Partnership";
$lang['corporation'] = "Others";
$lang['select_creditor_type'] = "Select Creditor Type";
$lang['creditor_type'] = "Creditor Type";
$lang['select_legal_form'] = "Select Legal Form";
$lang['male'] = "Male";
$lang['female'] = "Female";
$lang['arabic'] = "Arabic";
$lang['SENDER_ID'] = "Sender ID";
$lang['lang_next'] = "Next";
$lang['mobile_confirmation'] = "Mobile Confirmation";
$lang['confirm_num'] = "Confirmation Number";
$lamg['enter_confirm_num'] = "Enter the code you receive via SMS";
$lang['mobile_verify'] = "Verify your number";
$lang['lang_verify'] = "Verify";
$lang['success_register'] = "You are successfully registered";
$lang['missed_sms_sett'] = "Missed SMS Settings .. Please contact the system admin.";
$lang['sms_not_sent'] = "Can't send the verification SMS <br/> please click resend link.";
$lang['lang_resend_sms'] = "Didn't receive SMS? Resend the code.";
$lang['wrong_code'] = "You entered a wrong code plase try again.";
$lang['mobile_code'] = "Country mob. num. Code";
$lang['creditors'] = "Creditors";
$lang['ad_error'] = "Failed to add to active directory -- Try to change your login name";
$lang['ad_error_edit'] = "Failed to update the active directory";
$lang['password_chanhged_success'] = 'Password Changed Successfully';
$lang['cannot_create_user'] = "Can't create user";
$lang['sms_code_expiry'] = "SMS code expiry (min.)";
$lang['creditor_currency'] = "Creditor currency";
$lang['creditor_amount'] = "Creditor fees";
$lang['select_currency'] = "Select currency";
$lang['expired_code'] = "Your code has expired please click the resend code link.";
$lang['reg_date'] = "Registration date";
$lang['mobile_confirmed'] = "Mobile confirmed";
$lang['go_to_login'] = "Go to login";
$lang['refresh_captcha'] = "Reload Captcha";

// System Language
$lang['system_lang'] = "en-US";
$lang['lang_code'] = "en";
$lang['system_dir'] = "ltr";
$lang['lang_name'] = "english";
$lang['data_submitted_successfully'] = "Data submitted successfully";
$lang['data_deleted_successfully'] = "Data deleted successfully";
$lang['audit_trail_settings'] = "Audit Trail Settings";
$lang['days'] = "Days";
$lang['audits_expiry'] = "Purge Interval ";
$lang['audits_file_path'] = "Audits file path";
$lang['audit_trail'] = "Event Log";
$lang['reload_logs'] = "Reload Logs";
$lang['time'] = "Time";
$lang['user_not_found'] = "User Not Found";
$lang['email_not_set_ad'] = "Email not set at active directory";
$lang['captcha'] = "Captcha";
$lang['data_not_found'] = "No results found based on the search data used";
$lang['pdf_eng'] = "PDF(ENG)";
$lang['pdf_ar'] = "PDF(AR)";
$lang['error_occured'] = "Error occured. Please try again.";
$lang['link_expired'] = "Link expired. Please try to reset the password again.";
$lang['reset_password'] = "Reset password";
$lang['mobile_not_set_ad'] = "Mobile not set at active directory";
$lang['payment_error'] = "There is an error in your payment process. Please try again later.";
$lang['first_time_login'] = "Its your first login. you have to change your password.";
$lang['pay_error'] = "Payment error try to pay again by clicking the button below";
$lang['pay_again'] = "Pay Now";
$lang['payment_error'] = "Payment error";
$lang['payment_method'] = "Payment Method";
$lang['payment_code'] = "Payment Code";
$lang['payment_settings_error'] = "There is an error in payment settings. Please contact the system administrator.";
$lang['login_name_not_found'] = "Login name not found";
$lang['email_not_required'] = "The email  entered is already exist";
$lang['terms'] = "<p style='text-decoration:underline'>Please read below terms carefully, and click “Agree” regarding the membership and registration with respect the full acknowledgement and agreement and consent with the following terms.</p>"
        . "<ul><li>The Egyptian Credit bureau, manages and operates the Egyptian Collateral register as per the Financial Regulatory Authority board decision.</li>"
        . "<li>The registration of Rights of collaterals are publicized on the registry through the member only (creditor) </li>"
        . "<li>The Movable Collateral on which the member may create a pledge in the Registry comprises Tangible Movables, Fungible Movables, Intangible Movables and/or Future collaterals.</li>"
        . "<li>The member can not create pledges over the following movables.<ul>"
        . "<li>State owned movables, whether this property was public, private or owned by authorities, endowment entities, or foreign embassies and authorities that have immunity.</li>"
        . "<li>Privileges and licenses granted by the State, public authorities or one of the public legal persons.</li>"
        . "<li>Movables owned by banks, except for the movables required for their work which are pledged for the purpose of financing their purchase.</li>"
        . "<li>Movables assigned to personal or domestic purposes, unless they are pledged for the purpose of financing their purchase.</li>"
        . "<li>Communally owned movables, unless all owners agree on creating a guarantee right on them.</li>"
        . "<li>Egyptian or foreign securities or corresponding international certificates of deposit, vessels and aircrafts, where the registration requirements for such pledges are contained in specific legislation applicable to them.</li>"
        . "<li>Rights on Future Movables in case of inheritance, will, pension, life insurance policies, compensation resulting from a judgement or agreement, alimony or wages and salaries.</li>"
        . "</ul></li>"
        . "<li>The member shall be entitled to register in the Registry any pledge over Movable Collateral any judgments and decisions relating to Movable Collateral issued in its favor, the judicial decisions issued for precautionary seizure or execution of the Movable Collateral in accordance with the judgments issued in its favor to register any amendments, or cancellation of such pledges or judgments.</li>"
        . "<li>It is Permissible to any natural or legal person to conduct searches on any of the data existing on the Registry’s database and obtain a certified copy of such report stamped with official Government stamp from the Financial Regulatory Authority upon paying the required fees.</li>"
        . "<li>The registration service (including any registration amendment and cancelation) shall be available electronically 24/7, except for interruptions for maintenance pertaining to the electronic system, the member shall be responsible for the registration, amendment and cancellation of the electronic registrations.</li>"
        . "<li>Every concerned party has the right to dispute the registration of a pledge by submitting a written dispute using the standard form prepared for this purpose, stating the full name of the objector,  the registration number of the disputed registration and the details of the disputed information. The member can submit an dispute electronically using the standard form prepared for this purpose.</li>"
        . "<li>The member shall immediately cancel any registration from the Registry as soon as the pledge agreement to which the relevant registration relates has been terminated.</li>"
        . "<li>The member is obliged to notify the relevant debtor / guarantee provider of the registration in the Registry over Movable Collateral belonging to that debtor no later than seven Business Days after the date of registration of the pledge in the Registry. The member shall attach to such notification a copy of the registration report.</li>"
        . "<li>The member is entitled to create an search account, for the search purposes only for any of the data published over collaterals without having a certified copy after paying the required fees to create such account.</li>"
        . "<li>If the member wants to obtain certified copy of the audit logs related to results of the inquiries performed, shall submit an application along with a court permission.to the company along with any other requested documents. </li>"
        . "<li>The member agrees that the registry shall not be liable for any actions, complaints, expenses, or damage resulting from the data and information published by the member on the register and bears all the legal liabilities in such case.</li>"
        . "<li>The member shall be solely liable for the validity, accuracy and completeness of the Information and data entered upon the system by him.</li>"
        . "<li>The member acknowledges noticing the provisions of the law no. 115 for the year 2015 regarding the promulgation of a law regulating the movable collaterals and its executive regulations issued by the Minister of Investment’s Decree No. 108 of the year 2016.</li>"
        . "</ul>"
        . "<p>The company is entitled to the following remuneration and fees against the services provided to the Member <span style='text-decoration:underline'><strong>(all fees below in addition to 14% Witholding taxes)</strong></span><br/>
        EGP 1000 (one thousand Egyptian pounds) annually for opening an account.<br/>
       An annual subscription fee of EGP 500 (five hundred Egyptian pounds) for opening an account for search purposes.
       </p>"
        . "<ul>"
        . "<li>Single registration fees: EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, provided that the registration fee shall not be less than EGP 50 and not more than EGP 500 (five hundred Egyptian pounds) per obligation.</li>"
        . "<li>In case of amendments of an existing registered pledge without specification of its value, the registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Single registration amendment fees: EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, if the amendment registration fee shall not be less than EGP 50 and not more than EGP 500 (five hundred Egyptian pounds) per obligation.<br/>In case of amendments of an existing registered pledge without specification of its value, the registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Fees for amending a single registration by increasing the pledge period: EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, provided that the amendment registration fee shall not be less than EGP 50 and not more than EGP 500 (five hundred Egyptian pounds) per obligation.<br/>In case of amendments of an existing registered pledge without specification of its value, the registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Fees for amending a single registration by decreasing the pledge period: EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, provided that the amendment registration fee shall not be less than EGP 50 and not more than EGP 500 (five hundred Egyptian pounds) per obligation.<br/>In case of amendments of an existing registered pledge without specification of its value, the registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Fees for amending a single registration by addition of a movable pledger, creditor(s), debtor(s) or pledge provider(s): EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, provided that the amendment registration fee shall not be less than EGP 50 and no more than EGP 500 (five hundred Egyptian pounds) per obligation.<br/>In case of amendments of an existing registered pledge without specification of its value, the recording or registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Fees for amending a single registration by removal of a movable pledge, a creditor(s), a debtor(s) or a pledge provider(s): EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, provided that the amendment registration fee shall be not less than EGP 50 and not more than EGP 500 (five hundred Egyptian pounds) per obligation.<br/>In case of amendments of an existing registered pledge without specification of its value, the recording or registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Fees for amending the registration by waiver of a pledge or order of priority: EGP 15 (fifteen Egyptian pounds) for each one thousand pounds of the value of the contractual liability in respect of which the pledge is created, provided that the amendment registration fee shall be not less than EGP 50 and not more than EGP 500 (five hundred Egyptian pounds) per obligation.<br/>In case of amendments of an existing registered pledge without specification of its value, the recording or registration fees shall be EGP 500 (five hundred Egyptian pounds).</li>"
        . "<li>Fees for obtaining a certified copy of the data listed in the Register by the Member: EGP 100 for each certified copy of the data.</li>"
        . "</ul>";

$lang['agree_terms'] = "I agree to the terms and conditions";
$lang['terms_condition'] = "Terms & Conditions";
$lang['redirect_to_home'] = "Back to home page";
$lang['data_not_found_only'] = "No data found";
$lang['captcha_code_is_wrong'] = "Captcha code is wrong";
$lang['password_complex_req'] = "Password complexity requirement";
$lang['complex_password'] = "&bull; Must contain characters from three of the following four categories: :<br/>1. Uppercase characters (A-Z)<br/>2. Lowercase characters (a-z)"
        . "<br/>3. Digits (0-9)<br/>4. Special characters (~!@#$%^&*_-+=|\(){}[]:;<>,.?/)";
$lang['password_length'] = "&bull; password length must be at least %s characters";
$lang['expired_link'] = 'This reset link is invalid or has expired';
$lang['set_0_to_disable'] = "If set to 0 purge will be disabled";
$lang['support_title'] = "Support";
$lang['creditorShare'] = "Creditor Share";
$lang['back_to_website'] = "Back to Website";
$lang['password_black_list'] = "Password blacklist";
$lang['new_line_seperator'] = "Separate passwords by new line";
$lang['passwords'] = "Passwords";
$lang['password_in_blocked_list'] = "Enter a different password. This password is blocked.";
$lang['add'] = "Add";
$lang['delete'] = "Delete";
$lang['unique_username'] = "This Username exists before.";
$lang['paid'] = "Paid";
                