<!DOCTYPE html>
<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
        <style>
            .captcha_error p{
                color: #fff;
            }
            .captcha img{
                width: 100%; 
            }

        </style>
    </head>
    <body class="account <?= $this->lang->line('system_dir') ?>" data-page="login">
        <?= login_change_lang() ?>
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <div class="text-center" style="margin-bottom: 20px">
                            <img src="<?= base_url() ?>uploads/content/Untitled-1-1.png" style="width:60%" />
                        </div>
                        <div class="row editErrorMsg">
                            <?= flash_msg() ?>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                        </div>
                        <?php echo form_open('Login/do_login'); ?>
                        <div class="append-icon">
                            <?php echo form_input(array('type' => 'text', 'id' => 'username', 'name' => 'username', 'class' => 'form-control form-white email', 'placeholder' => $this->lang->line("username"), 'required' => 'required')); ?>
                            <i class="icon-user"></i>
                            <?php echo form_error('username'); ?>
                        </div>
                        <div class="append-icon m-b-20">
                            <?php echo form_input(array('type' => 'password', 'name' => 'password', 'class' => 'form-control form-white password', 'placeholder' => $this->lang->line('password'), "autocomplete" => "off", 'required' => 'required')); ?>
                            <i class="icon-lock"></i>
                            <?php echo form_error('password'); ?>
                        </div>
                        <div class="text-center captcha">
                            <img id="captImg" src="<?= $captchaImg ?>" width="100%" />
                            <button title="<?= $this->lang->line('refresh_captcha') ?>" type="button" id="refreshCaptcha" style="margin-right:0px;margin-left:0px;<?= $this->lang->line('system_dir') == 'rtl' ? 'float:left' : 'float:right' ?>" class="btn btn-default btn-square "><i class="fa fa-refresh" style="font-size: 15px;" ></i></button>
                        </div>
                        <div class="append-icon m-b-20 m-t-20">
                            <div class="row">
                                <div class="col-sm-8">
                                    <?php echo form_input(array('type' => 'text', 'name' => 'captcha', 'class' => 'form-control form-white', 'placeholder' => $this->lang->line('captcha'), 'required' => 'required')); ?>
                                    <span class="captcha_error"><strong><?php echo form_error('captcha'); ?></strong></span>
                                </div>
                                <!--                                <div class="col-sm-4">
                                                                    <button type="button" class="btn btn-danger btn-block" id="refreshCaptcha">Refresh</button> 
                                                                </div>-->
                            </div>
                        </div> 
                        <?php echo form_submit(array('class' => 'btn btn-lg btn-danger btn-block ladda-button', 'data-style' => 'expand-left', 'value' => $this->lang->line('lang_sign_in'), 'style' => 'background-color: #1D4B8E;text-align: center')); ?>
                        <div class="clearfix">
                            <p class="pull-left m-t-20"><a href="<?= base_url() ?>User/reset_password"><?= $this->lang->line('lang_forget_password') ?></a></p>
                        </div>
                        <div class="clearfix">
                            <p class="pull-left"><a href="<?= base_url() ?>Users/add"><?= $this->lang->line('lang_sign_up') ?></a></p>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <?php $this->load->view('private/scripts/login_js'); ?>
        <script>
            $(document).ready(function () {
                $('#refreshCaptcha').on('click', function () {
                    $.get('<?= base_url('Login/refresh_captcha'); ?>', function (data) {
                        $('#captImg').attr("src", data);
                    });
                });
            });

        </script>
    </body>
</html>