<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
//        construct_init();
        $this->load->model("Settings_model");
    }
    
    public function password_management() {
        $settings = $this->Settings_model->get_password_management();
        $this->form_validation->set_rules("enforce_history", $this->lang->line("enforce_history"), "xss_clean|trim");
        $this->form_validation->set_rules("max_age", $this->lang->line("max_age"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("min_length", $this->lang->line("min_length"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("complex", $this->lang->line("complex"), "xss_clean|trim");
        $this->form_validation->set_rules("lockout_duration", $this->lang->line("lockout_duration"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("lockout_threshold", $this->lang->line("lockout_threshold"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("reset_counter", $this->lang->line("reset_counter"), "xss_clean|trim|numeric");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("password_management");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->load->view("pages/settings/password_management", $data);
        } else {
            $data = array(
                'enforce_history' => $this->input->post("enforce_history"),
                'max_age' => $this->input->post("max_age"),
                'min_length' => $this->input->post("min_length"),
                'complex' => $this->input->post("complex"),
                'lockout_duration' => $this->input->post("lockout_duration"),
                'lockout_threshold' => $this->input->post("lockout_threshold"),
                'reset_counter' => $this->input->post("reset_counter")
            );
            if (!empty($settings)) {
                    /**/ $this->Global_model->global_update("password_management", $settings->id, $data);
            } else {
                    /**/ $this->Global_model->global_insert("password_management", $data);
            }

            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Settings/password_management"));
        }
    }
    
    public function active_directory() {
        $settings = $this->Settings_model->get_active_directory();
        $this->form_validation->set_rules("hostname", $this->lang->line("hostname"), "xss_clean|trim|required");
        $this->form_validation->set_rules("dc", $this->lang->line("dc"), "xss_clean|trim|required");
        $this->form_validation->set_rules("ou", $this->lang->line("ou"), "xss_clean|trim|required");
        $this->form_validation->set_rules("admin_dn", $this->lang->line("admin_dn"), "xss_clean|trim|required");
        $this->form_validation->set_rules("admin_password", $this->lang->line("admin_password"), "xss_clean|trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("active_directory_settings");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->load->view("pages/settings/active_directory", $data);
        } else {
            $data = array(
                'hostname' => $this->input->post("hostname"),
                'dc' => $this->input->post("dc"),
                'ou' => $this->input->post("ou"),
                'admin_dn' => $this->input->post("admin_dn"),
                'admin_password' => $this->input->post("admin_password"),
                
                'second_hostname' => $this->input->post("second_hostname"),
                'second_dc' => $this->input->post("second_dc"),
                'second_ou' => $this->input->post("second_ou"),
                'second_admin_dn' => $this->input->post("second_admin_dn"),
                'second_admin_password' => $this->input->post("second_admin_password"),
            );
            if (!empty($settings)) {
                    /**/ $this->Global_model->global_update("active_directory", $settings->id, $data);
            } else {
                    /**/ $this->Global_model->global_insert("active_directory", $data);
            }

            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Settings/active_directory"));
        }
    }
}