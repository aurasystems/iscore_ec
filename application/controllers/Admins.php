<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admins extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Admins_model');
        construct_init();
        $this->load->library('Ad_lib');
        $this->session->set_userdata('active', '2');
    }

    function index() {
        check_p("admins", "v");
        $admins = $this->Admins_model->get_admins();
        $data['title'] = $this->lang->line('admins');
        $data['admins'] = $admins;
        $this->load->view("pages/admins/index", $data);
    }

    function construct_ad_data($data_arr) {
        $ldaprecord['cn'] = $data_arr['user'];
        $ldaprecord['givenname'] = $data_arr['user'];
        $ldaprecord['sn'] = $data_arr['user'];
        $ldaprecord['mail'] = $data_arr['email'];
        $ldaprecord['mobile'] = $data_arr['phoneNumber'];
        //$ldaprecord['uid'] = '';
        $ldaprecord['displayname'] = $data_arr['clientName'];
        $ldaprecord['samaccountname'] = $data_arr['user'];
        if (isset($data_arr['password'])) {  // Edit case
            $ldaprecord['password'] = $data_arr['password'];
        }
        return $ldaprecord;
    }

    public function check_blocked($pass) {
        if (check_password_blacklist(md5($pass))) {
            $this->form_validation->set_message('check_blocked', $this->lang->line('password_in_blocked_list'));
            return false;
        }
        return TRUE;
    }

    public function add() {
//        set_active("add_user");
        check_p("admins", "c");
        $this->load->model("Settings_model");
        $password_management = $this->Settings_model->get_password_management();
        $this->form_validation->set_rules("user", $this->lang->line("user"), "trim|required|is_unique[users.user]");
        $this->form_validation->set_rules("clientName", $this->lang->line("clientName"), "trim|required");
        $this->form_validation->set_rules("address", $this->lang->line("address"), "trim|required");
        $this->form_validation->set_rules("phoneNumber", $this->lang->line("phoneNumber"), "trim|required");
        $this->form_validation->set_rules("email", $this->lang->line("email"), "trim|required|valid_email");
        $this->form_validation->set_rules("password", $this->lang->line("password"), "trim|required|callback_check_blocked");
        $this->form_validation->set_rules("confirm_password", $this->lang->line("confirm_password"), "trim|required|matches[password]");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line('add_admin');
            $data['password_management'] = $password_management;
            $this->load->view('pages/admins/add', $data);
        } else {

            $data = array(
                "user" => strtolower($this->input->post("user")),
                "password" => $this->input->post("password"),
                "clientName" => $this->input->post("clientName"),
                "address" => $this->input->post("address"),
                "phoneNumber" => $this->input->post("phoneNumber"),
                "email" => $this->input->post("email"),
                "subscription" => 0,
                "admin" => 1
            );

            $error_msg = $this->ad_lib->add_user($this->construct_ad_data($data));

            $data['password'] = md5($this->input->post("password"));

            if ($error_msg == 1) {
//print_R($data);die;
                $this->Global_model->global_insert("users", $data);

                //get user id
                $user_id = $this->Admins_model->get_user_id($this->input->post("user"));
                //insert password
                $pass_data = [
                    "user_id" => $user_id,
                    "password" => md5($this->input->post("password")),
                    "timestamp" => date("Y-m-d h:i:s")
                ];
                $this->Global_model->global_insert("user_passwords", $pass_data);

                save_audit('Add Admin with username: ' . $this->input->post("user"));

                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                redirect(base_url("Admins"));
            } else {
                save_audit('Failed to add Admin with username: ' . $this->input->post("user") . ' due to ' . $error_msg);
                $this->session->set_flashdata("error", $error_msg);
                redirect(base_url("Admins"));
            }
        }
    }

//    
    public function edit($id = NULL) {
        check_param($id);
//        set_active("users");
        check_p("admins", "u");
        // $this->form_validation->set_rules("user",$this->lang->line("user"),"trim|required");
        $this->form_validation->set_rules("clientName", $this->lang->line("clientName"), "trim|required");
        $this->form_validation->set_rules("address", $this->lang->line("address"), "trim|required");
        $this->form_validation->set_rules("phoneNumber", $this->lang->line("phoneNumber"), "trim|required");
        $this->form_validation->set_rules("email", $this->lang->line("email"), "trim|required|valid_email");
        $one = $this->Global_model->get_data_by_id("users", $id);
        check_param($one, "resource");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line('edit_admin');
            $data['one'] = $one;
            $this->load->view('pages/admins/edit', $data);
        } else {
            $data = array(
                "user" => $one->user,
                "clientName" => $this->input->post("clientName"),
                "address" => $this->input->post("address"),
                "phoneNumber" => $this->input->post("phoneNumber"),
                "email" => $this->input->post("email"),
                "subscription" => 0,
                "admin" => 1
            );
            $error_msg = $this->ad_lib->edit_user($one->user, $this->construct_ad_data($data));
            if ($error_msg == 1) {

                $this->Global_model->global_update("users", $id, $data);
                save_audit('Admin data updated with username: ' . $one->user);
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
                redirect(base_url("Admins"));
            } else {
                save_audit('Failed to update Admin data with username: ' . $one->user . ' due to ' . $error_msg);
                $this->session->set_flashdata("error", $error_msg);
                redirect(base_url("Admins"));
            }
        }
    }

    public function delete($id = NULL) {
        check_param($id);
        check_p("admins", "d");
        $user = $this->Global_model->get_data_by_id("users", $id);
        $this->Global_model->global_delete("users", $id);
        save_audit('Delete Admin with username: ' . $user->user);
        $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
        redirect(base_url("Admins"));
    }

}
