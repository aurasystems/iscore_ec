<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "ResponseParser.php";

class Search extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        if ($this->input->post()) {
            $this->session->set_userdata($this->input->post());
        }
        $this->load->model("Search_model");
        // $this->load->library("Api_lib");
        if ($this->session->userdata('admin') == 1) {
            redirect('Dashboard');
        }
    }

    public function PublicationSearch() {
        $list = [];
        $subscribed = $this->Search_model->get_subscription($this->session->userdata("user"));
        if (!$subscribed) {
            redirect(base_url("Subscription/choose_subscription"));
        }
        $this->session->set_userdata('active', '7');
        if ($this->input->get()) {
            $this->form_validation->set_data($this->input->get());
            $this->form_validation->set_rules("partnerType", $this->lang->line("partnerType"), "trim");
            if ($this->input->get("partnerType") == "EGYPTIAN_NATURAL_PERSON") {
                $this->form_validation->set_rules("nationalID", $this->lang->line("nationalID"), "trim|required");
            } elseif ($this->input->get("partnerType") == "FOREIGN_NATURAL_PERSON") {
                $this->form_validation->set_rules("passportNumber", $this->lang->line("passportNumber"), "trim|required");
                $this->form_validation->set_rules("issuanceCountry", $this->lang->line("issuanceCountry"), "trim|required");
            } elseif ($this->input->get("partnerType") == "LEGAL_ENTITY_OPERATING_IN_EGYPT" || $this->input->get("partnerType") == "FOREIGN_LEGAL_ENTITY_WITH_BRANCHOFFICE_IN_EGYPT") {
                $this->form_validation->set_rules("registrationNumber", $this->lang->line("registrationNumber"), "trim|required");
            } else {
                $this->form_validation->set_rules("publicationNumber", $this->lang->line("publicationNumber"), "trim|required");
            }
//            $this->form_validation->set_rules("paidFeeAmount",$this->lang->line("paidFeeAmount"),"trim|required");
            if (!$this->form_validation->run()) {
                $data['title'] = $this->lang->line("PublicationSearch");
                $data['subscribed'] = $subscribed;
                $data['list'] = $list;
                $this->load->view("pages/search/PublicationSearch", $data);
            } else {
                if (!$subscribed) {
//                    $params=array(
//                    "partnerType"=>$this->input->get("partnerType"),
//                    "publicationNumber"=>$this->input->get("publicationNumber"),
//                    "nationalID"=>$this->input->get("nationalID"),
//                    "passportNumber"=>$this->input->get("passportNumber"),
//                    "issuanceCountry"=>$this->input->get("issuanceCountry"),
//                    "registrationNumber"=>$this->input->get("registrationNumber"),
////                    "paidFeeAmount"=>$this->input->get("paidFeeAmount"),
//                );
//
//               $charge_params=array(
//                    "user"=>  $this->session->userdata("user"),
//                    "feeType"=>  $this->input->post("feeType"),
//                );
//
//                $response=$this->api_lib->ChargedFeeAmount($charge_params);
//                if($response['FaultString']){
//                    //set flash
//                    $this->session->set_flashdata("error",$response['FaultString']);
//                    redirect(current_url());
//                }
//                $params['paidFeeAmount']=$response['serviceFee'];
//                //TODO redirect to online payment gatweway
//                //$response['serviceFee']
//                //$response['currency']
                } else {

                    // Load Rest Api Model
                    $this->load->model('Rest_Api_model');
                    // Assign call params
                    $cp = [
                        'auth' => $this->config->item('api_key'),
                        'url' => $this->config->item('search_server_ip') . '/Rest_Api',
                        'dt' => []
                    ];
                    $user_info = $this->db->where('id', $this->session->userdata("user_id"))->get('users')->row();
                    if ($user_info) {
                        $feeType = $user_info->ONE_TIME_SEARCH ? 'SEARCH' : 'SUBSCRIPTION';
                        // Start Assign Charged Fee Params
                        $charge_params = array(
                            "user" => $this->session->userdata("user"),
                            "feeType" => 'SEARCH',
                        );
                        // End Assign Charged Fee Params
                        // Check for charged fee via Api
                        $cp['action'] = 'ChargedFeeAmount';
                        $cp['params'] = $charge_params;
                        $response_fee = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

                        if (!empty($response_fee['faultstring'])) {
                            //set flash
                            save_audit('Publication search: ERROR in ChargedFeeAmount API (' . $response_fee['faultstring'] . ') for user: ' . $this->session->userdata('user'));
                            $this->session->set_flashdata("error", $response_fee['faultstring']);
                            redirect(base_url("Search/PublicationSearch"));
                        }
                        if (empty($response_fee['response'])) {
                            //set flash
                            save_audit('Publication search: ChargedFeeAmount API return an empty response for user: ' . $this->session->userdata('user'));
                            $this->session->set_flashdata("error", "Something went wrong");
                            redirect(base_url("Search/PublicationSearch"));
                        }
                        $response_fee = $response_fee['response'];
                        $params = array(
                            "user" => $this->session->userdata("user"),
                            "partnerType" => $this->input->get("partnerType"),
                            "publicationNumber" => $this->input->get("publicationNumber"),
                            "nationalId" => $this->input->get("nationalID"),
                            "passportNumber" => $this->input->get("passportNumber"),
                            "issuanceCountry" => $this->input->get("issuanceCountry"),
                            "registrationNumber" => $this->input->get("registrationNumber"),
                            "paidFeeAmount" => $response_fee['serviceFee'],
                        );

                        if ($params["partnerType"] == 'EGYPTIAN_NATURAL_PERSON') {
                            unset($params['registrationNumber']);
                            unset($params['passportNumber']);
                            unset($params['issuanceCountry']);
                        }

                        if ($params["partnerType"] == 'FOREIGN_LEGAL_ENTITY_WITH_BRANCHOFFICE_IN_EGYPT') {
                            unset($params['nationalId']);
                            unset($params['passportNumber']);
                            unset($params['issuanceCountry']);
                        }

                        if ($params["partnerType"] == 'FOREIGN_NATURAL_PERSON') {
                            unset($params['nationalId']);
                            unset($params['registrationNumber']);
                        }

                        if ($params["partnerType"] == 'LEGAL_ENTITY_OPERATING_IN_EGYPT') {
                            unset($params['nationalId']);
                            unset($params['passportNumber']);
                            unset($params['issuanceCountry']);
                        }

                        // Do Api Call
                        $cp['action'] = 'PublicationSearch';
                        $cp['params'] = $params;

                        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

                        if (!empty($response['faultstring'])) {
                            //set flash
                            save_audit('Publication search:ERROR in PublicationSearch API (' . $response['faultstring'] . ') for user: ' . $this->session->userdata('user'));
                            $this->session->set_flashdata("error", $response['faultstring']);
                            redirect(base_url("Search/PublicationSearch"));
                        }
                        if (!isset($response['response'])) {
                            //set flash
                            save_audit('Publication search: PublicationSearch API return an empty response for user: ' . $this->session->userdata('user'));
                            $this->session->set_flashdata("error", "Something went wrong");
                            redirect(base_url("Search/PublicationSearch"));
                        }
                        $response = $response['response'];

                        $list = [];
                        if (!empty($response["respList"])) {
                            $list = $response["respList"];
                        }

                        //insert search params in db for reports
                        unset($params['paidFeeAmount']);
                        $params['PAIDFEEAMOUNT'] = $response_fee['serviceFee'];
                        $params['date_searched'] = date("Y-m-d H:i:s");
                        $params['search_results'] = json_encode($response);
                        save_audit('Publication search performed for user: ' . $this->session->userdata('user'));
                        $this->Global_model->global_insert("publication_search", $params);

                        $this->db->update('users', array('ONE_TIME_SEARCH' => 0), array('id' => $this->session->userdata('user_id')));
                        //insert response
                        $publication_search_id = $this->db->order_by("id", "desc")->get("publication_search")->row()->id;
                        $one_rec = FALSE;
                        foreach ($list as $one) {
                            if (is_array($one)) {
                                $insert_response = [
                                    "publication_search_id" => $publication_search_id,
                                    "publicationNumber" => $one['publicationNumber'],
                                    "publicationDate" => to_date($one['publicationDate'], 'Y-m-d'),
                                    "guaranteeAmount" => $one['guaranteeAmount'],
                                    "expirationDate" => to_date($one['expirationDate'], 'Y-m-d'),
                                    "bankruptDeptor" => $one['bankruptDebtor'],
                                    "creditorName" => $one['creditorName'],
                                    "documentID" => $one['documentIdEn'],
                                    "DOCUMENTIDAR" => $one['documentIdAr']
                                ];
                                $this->Global_model->global_insert("publication_search_response", $insert_response);
                            } else {
                                $one_rec = TRUE;
                                break;
                            }
                        }
                        if ($one_rec) {
                            $insert_response = [
                                "publication_search_id" => $publication_search_id,
                                "publicationNumber" => $list['publicationNumber'],
                                "publicationDate" => $list['publicationDate'] ? to_date($list['publicationDate'], 'Y-m-d') : '',
                                "guaranteeAmount" => $list['guaranteeAmount'],
                                "expirationDate" => $list['expirationDate'] ? to_date($list['expirationDate'], 'Y-m-d') : '',
                                "bankruptDeptor" => $list['bankruptDebtor'],
                                "creditorName" => $list['creditorName'],
                                "documentID" => $list['documentIdEn'],
                                "DOCUMENTIDAR" => $list['documentIdAr']
                            ];
                            $this->Global_model->global_insert("publication_search_response", $insert_response);
                        }
                        ////////////////////////////////////////////////////
                        if (empty($response)) {
                            $data['no_found'] = 1;
                        }
                        $data['title'] = $this->lang->line("PublicationSearch");
                        $data['subscribed'] = $subscribed;
                        $data['list'] = $list;

                        $this->load->view("pages/search/PublicationSearch", $data);
                    } else {
                        //set flash
                        $this->session->set_flashdata("error", $this->lang->line('user_not_found'));
                        redirect(current_url());
                    }
                }
            }
        } else {
            $data['title'] = $this->lang->line("PublicationSearch");
            $data['subscribed'] = $subscribed;
            $data['list'] = $list;
            $this->load->view("pages/search/PublicationSearch", $data);
        }
    }

    function PublicationSearchCallback() {
//        $params = $this->input->get();
//        $this->session->set_userdata('active', '7');
//        $response = $this->api_lib->PublicationSearch($params);
//        $this->load->view("pages/search/PublicationSearch", $response);
    }

    public function DocumentDownload($documentID = NULL) {
        check_param($documentID);
        $params = array(
            "documentId" => $documentID
        );
        // Assign call params
        $cp = [
            'auth' => $this->config->item('api_key'),
            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
            'action' => 'DocumentDownload',
            'dt' => [],
            'params' => $params,
        ];
        // Do Api Call
        $this->load->model('Rest_Api_model');
        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
        if (!empty($response['faultstring'])) {
            save_audit('ERROR in DocumentDownload API:' . $response['faultstring'] . ' documentID=' . $documentID . ' for user:' . $this->session->userdata('user'));
            echo $response['faultstring'];
        }

        if (!empty($response['response']['pdfFile'])) {
            header('Content-type: application/pdf');
            header('Content-Disposition: attachment; filename="service.pdf"');
            echo base64_decode($response['response']['pdfFile']);
        }
        save_audit('Document Downloaded SUCCESS documentID=' . $documentID . ' for user:' . $this->session->userdata('user'));
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function SearchHistory() {
        $this->session->set_userdata('active', '8');
        $list = [];
        if ($this->input->get("submit")) {
            $this->form_validation->set_data($this->input->get());
            // $this->form_validation->set_rules("user",$this->lang->line("user"),"trim");
            $this->form_validation->set_rules("endDate", $this->lang->line("endDate"), "trim");
            $this->form_validation->set_rules("startingDate", $this->lang->line("startingDate"), "trim");

            if (!$this->form_validation->run()) {
                $data['title'] = $this->lang->line("SearchHistory");
                $data['list'] = $list;
                $this->load->view("pages/search/SearchHistory", $data);
            } else {

                // $startingDate = $this->input->get("startingDate");
                // $endDate = $this->input->get("endDate");
                $params = array(
                    //"user"=>  $this->input->get("user"),
                    "user" => $this->session->userdata("user"),
                        //  "startingDate" => api_date($startingDate),
                        //  "endDate" => api_date($endDate)
                );
                if ($this->input->get("startingDate")) {
                    $params['startingDate'] = api_date(urldecode($this->input->get("startingDate")));
                }
                if ($this->input->get("endDate")) {
                    $params['endDate'] = api_date(urldecode($this->input->get("endDate")));
                }


                // Assign call params
                $cp = [
                    'auth' => $this->config->item('api_key'),
                    'url' => $this->config->item('search_server_ip') . '/Rest_Api',
                    'action' => 'SearchHistory',
                    'dt' => [],
                    'params' => $params,
                ];
                // Do Api Call
                $this->load->model('Rest_Api_model');
                $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);

                if (!empty($response['faultstring'])) {
                    //set flash
                    save_audit('SearchHistory API ERROR:' . $response['faultstring'] . 'for user:' . $this->session->userdata('user'));
                    $this->session->set_flashdata("error", $response['faultstring']);
                    redirect(current_url());
                }

                if (!isset($response['response'])) {
                    //set flash
                    save_audit('SearchHistory API return an empty response for user: ' . $this->session->userdata('user'));
                    $this->session->set_flashdata("error", "Something went wrong");
                    redirect(current_url());
                }

                $response = $response['response'];

                if ($response == '') {
                    $data['not_found'] = true;
                }

                if (isset($response["respList"]) && !empty($response["respList"])) {
                    $list = $response["respList"];
                }
                //insert search params in db for reports
                if ($this->input->get("startingDate")) {
                    $params["startingDate"] = to_date($this->input->get("startingDate"));
                }
                if ($this->input->get("endDate")) {
                    $params["endDate"] = to_date($this->input->get("endDate"));
                }
                $params['date_searched'] = date("Y-m-d H:i:s");
                //$params['search_results'] = json_encode($response);
                save_audit('SearchHistory API performed successfully for user: ' . $this->session->userdata('user'));
                $this->Global_model->global_insert("search_history", $params);
                //insert response
                $search_history_id = $this->db->order_by("id", "desc")->get("search_history")->row()->id;
                $one_rec = FALSE;
                foreach ($list as $one) {
                    if (!is_array($one)) {
                        $one_rec = TRUE;
                        $one = $list;
                    }
                    //print_R($one);die;
                    $insert_response = [
                        "search_history_id" => $search_history_id,
                        "dateOfSearch" => $one['dateOfSearch'],
                        "numberOfHits" => $one['numberOfHits'],
                            // "passportNumber" => $one['passportNumber'],
                            //  "publicationNumber" => $one['publicationNumber'],
                            // "registrationNumber" => $one['registrationNumber']
                    ];
                    if (isset($one['issuanceCountry'])) {
                        $insert_response['issuanceCountry'] = $one['issuanceCountry'];
                    }
                    if (isset($one['nationalId'])) {
                        $insert_response['nationalId'] = $one['nationalId'];
                    }
                    if (isset($one['passportNumber'])) {
                        $insert_response['passportNumber'] = $one['passportNumber'];
                    }
                    if (isset($one['registrationNumber'])) {
                        $insert_response['registrationNumber'] = $one['registrationNumber'];
                    }
                    if (isset($one['publicationNumber'])) {
                        $insert_response['publicationNumber'] = $one['publicationNumber'];
                    }
                    if (isset($one['searchId'])) {
                        $insert_response['SEARCHID'] = $one['searchId'];
                    }
                    $this->Global_model->global_insert("search_history_response", $insert_response);
                    if ($one_rec) {
                        $list = array($list);
                        break;
                    }
                }
                ////////////////////////////////////////////////////
                $data['title'] = $this->lang->line("SearchHistory");
                $data['list'] = $list;

                $this->load->view("pages/search/SearchHistory", $data);
            }
        } else {
            $data['title'] = $this->lang->line("SearchHistory");
            $data['list'] = $list;
            $this->load->view("pages/search/SearchHistory", $data);
        }
    }

    public function SearchHitList($searchID = NULL) {
        check_param($searchID);
        $this->session->set_userdata('active', '8');
        $params = ['searchId' => $searchID];
        // Assign call params
        $cp = [
            'auth' => $this->config->item('api_key'),
            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
            'action' => 'SearchHitList',
            'dt' => [],
            'params' => $params,
        ];
        // Do Api Call
        $this->load->model('Rest_Api_model');
        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
        if (!empty($response['faultstring'])) {
            //set flash
            save_audit('SearchHitList API ERROR:' . $response['faultstring'] . ' searchID ' . $searchID . ' for user:' . $this->session->userdata('user'));
            $this->session->set_flashdata("error", $response['faultstring']);
            redirect(current_url());
        }

        if (!isset($response['response'])) {
            //set flash
            save_audit('SearchHitList API return an empty response searchID:' . $searchID . ' for user: ' . $this->session->userdata('user'));
            $this->session->set_flashdata("error", "Something went wrong");
            redirect(current_url());
        }
        save_audit('SearchHitList API performed successfully searchID:' . $searchID . ' for user: ' . $this->session->userdata('user'));
        $response = $response['response'];
        $list = [];
        if (!empty($response["respList"])) {
            $list = $response["respList"];
        }
        $response['list'] = $list;
        if (!is_array($list[0])) {
            $response['list'] = array($list);
        }
        $response['title'] = $this->lang->line('SearchHitList');

        $this->load->view("pages/search/SearchHitList", $response);
    }

//    public function SearchHitList(){
//        $list=[];
//        if($this->input->get("submit")){
//            $this->form_validation->set_data($this->input->get());
//            $this->form_validation->set_rules("searchID",$this->lang->line("searchID"),"trim|required");
//            if(!$this->form_validation->run()){
//                $data['title']=  $this->lang->line("SearchHitList");
//                $data['list']=  $list;
//                $this->load->view("pages/search/SearchHitList",$data);
//            }
//            else{
//                $params=array(
//                    "searchID"=>  $this->input->get("searchID")
//                );
//
//                $response=$this->api_lib->SearchHitList($params);
//                 if($response['FaultString']){
//                    //set flash
//                    $this->session->set_flashdata("error",$response['FaultString']);
//                    redirect(current_url());
//                }
//                $list=[];
//                if(!empty($response["respList"])){
//                    $list=$response["respList"];
//                }
//                $response['list']=$list;
//                $this->load->view("pages/search/SearchHitList",$response);
//            }
//        }
//        else{
//            $data['title']=  $this->lang->line("SearchHitList");
//            $data['list']=  $list;
//            $this->load->view("pages/search/SearchHitList",$data);
//        }
//    }
    //Array ( [response] => Array ( [respList] => Array ( [bankruptDebtor] => true
    //[creditorName] => Bank Test
    //[creditorShare] => 1.0E8
    //[documentIdAr] => 23521 
//[documentIdEn] => 23520
// [expirationDate] => 2039-08-09T00:00:00+02:00
// [guaranteeAmount] => 1.0E8
// [publicationDate] => 2019-08-30T17:11:38.437+02:00
// [publicationNumber] => 0000009223 ) ) )
}
