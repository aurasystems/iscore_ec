<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <!-- END PAGE CSS -->
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $level_name ?>&nbsp;<?= $this->lang->line("permissions") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                    $disabled = '';
                                    if ($level_permissions->level_id == 1) {
                                        $disabled = 'disabled';
                                    }
                                    ?>
                                    <form method="POST" action="">
                                        <?= csrf() ?>
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <th><input type="checkbox" name="all" id="all" <?= $disabled ?> ><span id="all_label"><?= $this->lang->line('check_all') ?></span></th>
                                                <th class="text-center"><?= $this->lang->line('view') ?></th>
                                                <th class="text-center"><?= $this->lang->line('create') ?></th>
                                                <th class="text-center"><?= $this->lang->line('update') ?></th>
                                                <th class="text-center"><?= $this->lang->line('delete') ?></th>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($permissions as $module => $perms) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $this->lang->line("$module"); ?></td>
                                                            <td class="text-center"><input type="checkbox" name="<?= "{$module}_v" ?>" value="1" <?= checked($perms["v"]) ?> <?= $disabled ?>  /></td>
                                                            <td class="text-center"><input type="checkbox" name="<?= "{$module}_c" ?>" value="1" <?= checked($perms["c"]) ?> <?= $disabled ?>  /></td>
                                                            <td class="text-center"><input type="checkbox" name="<?= "{$module}_u" ?>" value="1" <?= checked($perms["u"]) ?> <?= $disabled ?>  /></td>
                                                            <td class="text-center"><input type="checkbox" name="<?= "{$module}_d" ?>" value="1" <?= checked($perms["d"]) ?> <?= $disabled ?>  /></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <button <?= $disabled ?> type="submit" class="btn btn-primary m-b-40"><?= $this->lang->line('save') ?></button>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
                $('#all').on('ifChanged', function (event) {
                    if ($(this).prop('checked'))
                    {
                        $('input').iCheck('check');
                        var label = "<?= $this->lang->line('uncheck_all') ?>";
                        $('#all_label').html(label);
                    } else {
                        $('input').iCheck('uncheck');
                        var label = "<?= $this->lang->line('check_all') ?>";
                        $('#all_label').html(label);
                    }
                });
            });
        </script>
    </body>
</html>