<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rest_Api_model extends CI_Model {

    function chkAuth($auth) {
        $system_auth = $this->config->item('api_key');
        return $system_auth && $auth && $auth == $system_auth;
    }

    function Curl($auth = '', $url = '', $action = '', $data = [], $params = []) {
        if (!$auth || !$url || !$action) {
            return FALSE;
        }
        $data_set = ["auth" => $auth, "dt" => $data, "params" => $params];
        $data_string = json_encode($data_set);
        $ch = curl_init($url . '/' . $action);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Content-Length: ' . strlen($data_string)]);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return FALSE;
        }
        $dt = json_decode($result, TRUE);
        return $dt;
    }

    function write_code_to_csv($param) {

        $list = array(
            array($param['code'])
        );

        $file = fopen("creditors_codes.csv", "a");
        if (!$file) {
            return array('error', 'Something went wrong. Please try again later.');
        }

        foreach ($list as $line) {
            if (!fputcsv($file, $line)) {
                return array('error', 'Something went wrong. Please try again later.');
            }
        }
        fclose($file);

        return array('success');
    }

}
