<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        construct_init();
		
        if(!$this->session->userdata("admin")){
            redirect(base_url("Search/PublicationSearch"));
        }
        $this->load->model("Dashboard_model");
        $this->session->set_userdata('active', '1');
    }
    public function index(){
        $total_users= $this->Dashboard_model->count_total_users();
        $month_payments= $this->Dashboard_model->get_month_payments();
        $total_admins=$this->Dashboard_model->count_total_admins();
        $month_searches=$this->Dashboard_model->count_month_searches();
        $total_searches=$this->Dashboard_model->count_searches();
        $data['title']=  $this->lang->line("dashboard");
        $data['total_users']=$total_users;
        $data['total_admins']=$total_admins;
        $data['month_payments']=$month_payments;
        $data['month_searches']=$month_searches;
        $data['total_searches']=$total_searches;
        $this->load->view("pages/dashboard/dashboard",$data);
    }
}