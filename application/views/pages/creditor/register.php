<!DOCTYPE html>
<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/icheck/skins/all.css" rel="stylesheet"/>
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/rateit/rateit.css" rel="stylesheet">

        <style>
            .col-sm-6,.col-sm-12{
                margin-bottom: 10px;
            }
            .account .btn.btn-lg{
                padding-top: 6px;
            }
            .rtl .dropdown-menu{
                right: auto;
            }
        </style>
    </head>
    <body class="account separate-inputs boxed <?= $this->lang->line('system_dir') ?>" data-page="signup">
        <?= login_change_lang() ?>
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3">
                    <div class="account-wall">
                        <div class="text-center" style="margin-bottom: 20px">
                            <img src="<?= base_url() ?>uploads/content/Untitled-1-1.png" style="width:40%" />
                        </div>
                        <br/>
                        <?= flash_msg() ?>
                        <?= flash_success() ?>
                        <?= flash_error() ?>
                        <?php $attributes = array('id' => 'submit_form', 'class' => 'form-signup', 'role' => 'form'); ?>
                        <form method="POST" action="" class="form-horizontal" role="form">
                            <div class="row">
                                <?= csrf() ?>
                                <div class='col-sm-6' style="margin-bottom: 0px">
                                    <div class='append-icon'>
                                        <select class="form-control" id="creditor_type" name="creditor_type">
                                            <option value=""><?= $this->lang->line('select_creditor_type') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('creditor_type') == 'egy_n_person' ? 'selected' : '' ?> value="egy_n_person"><?= $this->lang->line('egy_n_person') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('creditor_type') == 'legal_entity' ? 'selected' : '' ?> value="legal_entity"><?= $this->lang->line('legal_entity') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('creditor_type') == 'foreign_n_person' ? 'selected' : '' ?> value="foreign_n_person"><?= $this->lang->line('foreign_n_person') ?></option>
                                        </select>
                                        <span class="text-danger"><?= form_error('creditor_type') ?></span>
                                    </div>
                                </div>


                                <?php
                                $dir = $this->session->userdata('lang') == "arabic" ? 'left' : 'right';
                                $fields = ["username" => "user", "email" => "", "email_lang" => "", "password" => "", "confirm_password" => ""];
                                foreach ($fields as $field => $field_label) {
                                    $name = $field;
                                    $label = $name;
                                    $type = "text";
                                    if ($field_label) {
                                        $label = $field_label;
                                    }
                                    if ($field == "password" || $field == "confirm_password") {
                                        $type = "password";
                                    }
                                    echo " <div class='col-sm-6'>
                                            <div class='append-icon'>";
                                    if ($field == 'email_lang') {
                                        $selected1 = $selected2 = '';
                                        if ($this->input->post() && $this->input->post('email_lang') == 0) {
                                            $selected1 = 'checked';
                                        } else if ($this->input->post() && $this->input->post('email_lang') == 1) {
                                            $selected2 = 'checked';
                                        }
                                        echo "<label class='label-control'>" . $this->lang->line('email_lang') . "</label><br/>"
                                        . "<input $selected1 type='radio' name='$name' value='0'/>" . $this->lang->line('lang_english')
                                        . "&nbsp;&nbsp;&nbsp;&nbsp;<input $selected2 type='radio' name='$name' value='1'/>" . $this->lang->line('arabic');
                                    } else {
                                        echo "<input type='$type' id='$name' name='$name' " . ($field == "password" || $field == "confirm_password" ? 'autocomplete="off"' : '') . " placeholder='{$this->lang->line($label)}' class='form-control form-white' value='{$this->input->post($name)}'/>";
                                        if ($field == "password") {
                                            echo "<i class = 'fa fa-question-circle c-blue' rel = 'popover' data-container = 'body' data-html = 'true' data-toggle = 'popover' data-placement = '" . $dir . "' data-content = '" . get_password_complex_hint() . "' data-original-title = '" . $this->lang->line('password_complex_req') . "'></i>";
                                        }
                                    }
                                    echo "<span class='text-danger'>" . form_error($name) . " </span>
                                            </div>
                                        </div>";
                                    if ($field == "confirm_password") {
                                        echo ' <div class="row">
                                        <div class="col-sm-6 progress" style="display: none;width: 42%;margin: 10px 31px;">
                                            <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                                                 aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                            </div>
                                        </div>
                                    </div>';
                                    }
                                }
                                ?>
                                <div class='col-sm-12'>
                                    <div class='append-icon'>
                                        <textarea class="form-control form-white" rows="3" name="address" id="address" placeholder="<?= $this->lang->line('lang_address') ?>"><?= $this->input->post() && $this->input->post('address') ? $this->input->post('address') : '' ?></textarea>
                                        <span class="text-danger"><?= form_error('address') ?></span>
                                    </div>
                                </div>

                                <!-- =====================================  Egyptian Natural Person  ===================== -->

                                <?php
                                $fields = ["creditor_name" => "", "nationalID" => "", "date_of_birth" => "", "gender" => "", "mobile_num" => ""];
                                foreach ($fields as $field => $field_label) {
                                    $name = $field;
                                    $label = $name;
                                    $type = "text";
                                    if ($field_label) {
                                        $label = $field_label;
                                    }
                                    if ($name == 'mobile_num') {
                                        echo '<div class="clearfix"></div><div class="col-sm-6 egy_n_person custom_fields">'
                                        . '<select name="egy_mobile_code" class="form-control egy_mobile_code" data-search="true" >';
                                        $codes = mobile_countries_code();
                                        foreach ($codes as $key => $code) {
                                            $selected = '';
                                            if ($code['name'] == 'EGYPT') {
                                                $selected = 'selected';
                                            }
                                            echo '<option ' . $selected . ' value="' . $code['code'] . '">' . $code['name'] . '(+' . $code['code'] . ')' . '</option>';
                                        }
                                        echo '</select></div>';
                                    }

                                    $datepicker_class = $field == 'date_of_birth' ? 'b-datepicker' : '';
                                    $date_format = $field == 'date_of_birth' ? 'data-format="yyyy-mm-dd"' : '';
                                    echo " <div class='col-sm-6 egy_n_person custom_fields'>
                                            <div class='append-icon'>";
                                    if ($field == 'gender') {
                                        $selected1 = $selected2 = '';
                                        if ($this->input->post() && $this->input->post('egy_gender') == 0) {
                                            $selected1 = 'checked';
                                        } else if ($this->input->post() && $this->input->post('egy_gender') == 1) {
                                            $selected2 = 'checked';
                                        }
                                        echo "<input $selected1 type='radio' name='egy_gender' value='0'/>" . $this->lang->line('male')
                                        . "&nbsp;&nbsp;&nbsp;&nbsp;<input $selected2 type='radio' name='egy_gender' value='1'/>" . $this->lang->line('female');
                                    } else {
                                        echo "<input type='$type' id='egy_$name' name='egy_$name' placeholder='{$this->lang->line($label)}' class='form-control form-white $datepicker_class' $date_format value='{$this->input->post('egy_' . $name)}'/>";
                                    }

                                    echo "<span class='text-danger'>" . form_error('egy_' . $name) . "</span>
                                            </div>
                                        </div>";
                                }
                                ?>

                                <div class='col-sm-12 egy_n_person custom_fields'>
                                    <div class='append-icon'>
                                        <textarea class="form-control form-white" rows="3" name="egy_add_info" id="add_info" placeholder="<?= $this->lang->line('add_info') ?>"><?= $this->input->post() && $this->input->post('egy_add_info') ? $this->input->post('egy_add_info') : '' ?></textarea>
                                        <span class="text-danger"><?= form_error('egy_add_info') ?></span>
                                    </div>
                                </div>

                                <!-- =====================================  Foreign natural person  ===================== -->

                                <?php
                                $fields = ["creditor_name" => "", "passport_num" => "", "issuance_country" => "", "nationality" => "", "date_of_birth" => ""
                                    , "gender" => "", "mobile_num" => ""];
                                foreach ($fields as $field => $field_label) {
                                    $name = $field;
                                    $label = $name;
                                    $type = "text";
                                    if ($field_label) {
                                        $label = $field_label;
                                    }
                                    if ($name == 'mobile_num') {
                                        echo '<div class="clearfix"></div><div class="col-sm-6 foreign_n_person custom_fields">'
                                        . '<select name="foreign_mobile_code" class="form-control foreign_mobile_code" data-search="true" >';
                                        $codes = mobile_countries_code();
                                        foreach ($codes as $key => $code) {
                                            $selected = '';
                                            if ($code['name'] == 'EGYPT') {
                                                $selected = 'selected';
                                            }
                                            echo '<option ' . $selected . ' value="' . $code['code'] . '">' . $code['name'] . '(+' . $code['code'] . ')' . '</option>';
                                        }
                                        echo '</select></div>';
                                    }
                                    $datepicker_class = $field == 'date_of_birth' ? 'b-datepicker' : '';
                                    $date_format = $field == 'date_of_birth' ? 'data-format="yyyy-mm-dd"' : '';
                                    echo " <div class='col-sm-6 foreign_n_person custom_fields'>
                                            <div class='append-icon'>";
                                    if ($field == 'gender') {
                                        $selected1 = $selected2 = '';
                                        if ($this->input->post() && $this->input->post('foreign_gender') == 0) {
                                            $selected1 = 'checked';
                                        } else if ($this->input->post() && $this->input->post('foreign_gender') == 1) {
                                            $selected2 = 'checked';
                                        }
                                        echo "<input $selected1 type='radio' name='foreign_gender' value='0'/>" . $this->lang->line('male')
                                        . "&nbsp;&nbsp;&nbsp;&nbsp;<input $selected2 type='radio' name='foreign_gender' value='1'/>" . $this->lang->line('female');
                                    } else if ($field == 'issuance_country') {
                                        echo "<select data-search='true' name='foreign_$name' id='foreign_$name' class='form-control' >"
                                        . "<option value=''>Select Issuance country</option>";
                                        $countries = get_all_countries();

                                        foreach ($countries as $key => $value) {
                                            $selected = '';
                                            if ($this->input->post() && $this->input->post("foreign_$name") == $key) {
                                                $selected = 'selected';
                                            }
                                            echo "<option $selected value='$key'>" . $value . "</option>";
                                        }
                                        echo "</select>";
                                    } else if ($field == 'nationality') {
                                        echo "<select data-search='true' name='foreign_$name' id='foreign_$name' class='form-control' >"
                                        . "<option value=''>Select Nationality</option>";
                                        $nationals = get_all_nationals();
                                        foreach ($nationals as $value) {
                                            $selected = '';
                                            if ($this->input->post() && $this->input->post("foreign_$name") == $value) {
                                                $selected = 'selected';
                                            }
                                            echo "<option $selected value='$value'>" . $value . "</option>";
                                        }
                                        echo "</select>";
                                    } else {
                                        echo "<input type='$type' id='foreign_$name' name='foreign_$name' placeholder='{$this->lang->line($label)}' class='form-control form-white $datepicker_class' $date_format value='{$this->input->post('foreign_' . $name)}'/>";
                                    }

                                    echo "<span class='text-danger'>" . form_error('foreign_' . $name) . " </span>
                                            </div>
                                        </div>";
                                }
                                ?>

                                <div class='col-sm-12 foreign_n_person custom_fields'>
                                    <div class='append-icon'>
                                        <textarea class="form-control form-white" rows="3" name="foreign_add_info" id="add_info" placeholder="<?= $this->lang->line('add_info') ?>"><?= $this->input->post() && $this->input->post('foreign_add_info') ? $this->input->post('foreign_add_info') : '' ?></textarea>
                                        <span class="text-danger"> <?= form_error('foreign_add_info') ?> </span>
                                    </div>
                                </div>

                                <!-- =====================================  Legal Entity ===================== -->
                                <?php
                                $fields = ["institution_name" => "", "comm_reg_num" => "", "establishment_date" => "", "mobile_num" => ""];
                                foreach ($fields as $field => $field_label) {
                                    $name = $field;
                                    $label = $name;
                                    $type = "text";
                                    if ($field_label) {
                                        $label = $field_label;
                                    }
                                    if ($name == 'mobile_num') {
                                        echo '<div class="clearfix"></div><div class="col-sm-6 legal_entity custom_fields">'
                                        . '<select name="legal_mobile_code" class="form-control legal_mobile_code" data-search="true" >';
                                        $codes = mobile_countries_code();
                                        foreach ($codes as $key => $code) {
                                            $selected = '';
                                            if ($code['name'] == 'EGYPT') {
                                                $selected = 'selected';
                                            }
                                            echo '<option ' . $selected . ' value="' . $code['code'] . '">' . $code['name'] . '(+' . $code['code'] . ')' . '</option>';
                                        }
                                        echo '</select></div>';
                                    }
                                    $datepicker_class = $field == 'establishment_date' ? 'b-datepicker' : '';
                                    $date_format = $field == 'establishment_date' ? 'data-format="yyyy-mm-dd"' : '';
                                    $class = $field == 'comm_reg_num' ? 'col-sm-12' : 'col-sm-6';
                                    echo " <div class='$class legal_entity custom_fields'>
                                            <div class='append-icon'>
                                                <input type='$type' id='legal_$name' name='legal_$name' placeholder='{$this->lang->line($label)}' class='form-control form-white $datepicker_class' $date_format value='{$this->input->post('legal_' . $name)}'/>
                                                 <span class='text-danger'>" . form_error('legal_' . $name) . "</span>
                                            </div>
                                        </div>";
                                }
                                ?>

                                <div class='col-sm-6 legal_entity custom_fields' style="margin-bottom: 0px">
                                    <div class='append-icon'>
                                        <select class="form-control" id="legal_form" name="legal_form">
                                            <option value><?= $this->lang->line('select_legal_form') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'sole_proprietorship' ? 'selected' : '' ?> value="sole_proprietorship"><?= $this->lang->line('sole_proprietorship') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'private_company_limited_shares' ? 'selected' : '' ?> value="private_company_limited_shares"><?= $this->lang->line('private_company_limited_shares') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'partner' ? 'selected' : '' ?> value="partner"><?= $this->lang->line('partner') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'others' ? 'selected' : '' ?> value="others"><?= $this->lang->line('others') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'limited_partnership' ? 'selected' : '' ?> value="limited_partnership"><?= $this->lang->line('limited_partnership') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'limited_liability_company' ? 'selected' : '' ?> value="limited_liability_company"><?= $this->lang->line('limited_liability_company') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'joint_stock_company' ? 'selected' : '' ?> value="joint_stock_company"><?= $this->lang->line('joint_stock_company') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'general_partnership' ? 'selected' : '' ?> value="general_partnership"><?= $this->lang->line('general_partnership') ?></option>
                                            <option <?= $this->input->post() && $this->input->post('legal_form') == 'corporation' ? 'selected' : '' ?> value="corporation"><?= $this->lang->line('corporation') ?></option>
                                        </select>
                                        <span class="text-danger"><?= form_error('legal_form') ?></span>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class='col-sm-12 legal_entity custom_fields'>
                                    <div class='append-icon'>
                                        <textarea class="form-control form-white m-t-10" rows="3" name="legal_add_info" id="legal_add_info" placeholder="<?= $this->lang->line('add_info') ?>"><?= $this->input->post() && $this->input->post('legal_add_info') ? $this->input->post('legal_add_info') : '' ?></textarea>
                                        <span class="text-danger"><?= form_error('legal_add_info') ?></span>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <label class="control-label"><?= $this->lang->line('terms_condition') ?></label>
                            <div class="terms">
                                <?= $this->lang->line('terms') ?>
                            </div>
                            <br/>
                            <input type="checkbox" value="1" name="agree_terms" id="agree_terms" /><?= $this->lang->line('agree_terms') ?>

                            <div class="text-center">
                                <?php echo form_submit(array('name' => 'submit', 'id' => 'submit', 'class' => 'btn btn-lg btn-dark m-t-20', 'data-style' => 'expand-left', 'value' => $this->lang->line('lang_register'))); ?>
                            </div>
                            <div class="text-center">
                                <a href="<?= base_url() ?>creditor/login"><?= $this->lang->line('lang_have_account') ?></a>
                            </div>
                            <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END LOCKSCREEN BOX -->
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
        <script src="<?= base_url() ?>assets/js/plugins.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script> <!-- >Bootstrap Date Picker in Spanish (can be removed if not use) -->
        <?php $this->load->view('private/scripts/login_js'); ?>
        <script>
            $(document).ready(function () {
                strong_pass = false;
                $("#submit").attr("disabled", "disabled");
                $("#submit").removeClass('btn-dark');
                $("#submit").addClass('btn-gray');

                $('#agree_terms').on('ifChecked', function () {
                    if (strong_pass) {
                        $("#submit").removeAttr('disabled');
                        $("#submit").addClass('btn-dark');
                        $("#submit").removeClass('btn-gray');
                    }
                });

                $('#agree_terms').on('ifUnchecked', function () {
                    $("#submit").attr("disabled", "disabled");
                    $("#submit").removeClass('btn-dark');
                    $("#submit").addClass('btn-gray');
                });

                //            BEGIN PASSWORD STRENGTH
<?php if (!empty($password_management) && $password_management->complex) { ?>
                    $('#password').keyup(function () {
                        checkStrength($('#password').val());
                    });
                    function checkStrength(password) {

                        var strength = 0;
    <?php
    if (!empty($password_management) && $password_management->min_length) {
        ?>
                            var min_length = "<?= $password_management->min_length ?>";
                            if (password.length < min_length) {
                                progress_bar(25, "progress-bar-danger", "<?= $this->lang->line("too_short") ?>");
                            } else {
                                strength += 1;
                            }
        <?php
    } else {
        ?>
                            strength += 1;
        <?php
    }
    ?>
                        // If password contains both lower and uppercase characters, increase strength value.
                        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                            strength += 1;
                        // If it has numbers and characters, increase strength value.
                        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                            strength += 1;
                        // If it has one special character, increase strength value.
                        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // If it has two special characters, increase strength value.
                        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // Calculated strength value, we can return messages
                        // If value is less than 2
                        console.log(strength);
                        if (strength < 2) {
                            progress_bar(50, "progress-bar-danger", "<?= $this->lang->line("weak") ?>");
                        } else if (strength <= 3) {
                            progress_bar(75, "progress-bar-warning", "<?= $this->lang->line("medium") ?>");
                        } else {
                            progress_bar(100, "progress-bar-success", "<?= $this->lang->line("strong") ?>");
                        }
                    }


                    function progress_bar(per, c, text) {
                        $("#result").css("width", per + "%");
                        $("#result").attr("aria-valuenow", per);
                        $("#result").removeClass("progress-bar-danger");
                        $("#result").removeClass("progress-bar-warning");
                        $("#result").removeClass("progress-bar-success");
                        $("#result").addClass(c);
                        $("#result").html(text);
                        $(".progress").show();
                        if (per == 100) {
                            strong_pass = true;
                            if ($('#agree_terms').attr('checked')) {
                                $("#submit").removeAttr('disabled');
                                $("#submit").addClass('btn-dark');
                                $("#submit").removeClass('btn-gray');
                            }
                        } else {
                            strong_pass = false;
                            $("#submit").attr("disabled", "disabled");
                            $("#submit").removeClass('btn-dark');
                            $("#submit").addClass('btn-gray');
                        }
                    }
<?php } ?>
//            END PASSWORD STRENGTH

                $('.custom_fields').hide();

                $("#creditor_type").change(function () {
                    var selected = $(this).val();
                    $('.custom_fields').hide();
                    $('.' + selected).show();
                });

<?php if ($this->input->post()) { ?>
                    $("#creditor_type").change();
<?php } ?>

                $(".foreign_mobile_code").change(function () {
                    if ($(this).val() == '20') {
                        $("#foreign_mobile_num").attr('placeholder', '1xxxxxxxxx');
                    } else {
                        $("#foreign_mobile_num").attr('placeholder', 'Mobile Num.');
                    }
                });
                $(".legal_mobile_code").change(function () {
                    if ($(this).val() == '20') {
                        $("#legal_mobile_num").attr('placeholder', '1xxxxxxxxx');
                    } else {
                        $("#legal_mobile_num").attr('placeholder', 'Mobile Num.');
                    }
                });
                $(".egy_mobile_code").change(function () {
                    if ($(this).val() == '20') {
                        $("#egy_mobile_num").attr('placeholder', '1xxxxxxxxx');
                    } else {
                        $("#egy_mobile_num").attr('placeholder', 'Mobile Num.');
                    }
                });
                $(".foreign_mobile_code").change();
                $(".legal_mobile_code").change();
                $(".egy_mobile_code").change();
            });
        </script>
    </body>
</html>