<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sms
{
    public $CI;
    public $username;
    public $password;
    public $url;
//    public $response;


    public function __construct() {
        $this->CI=& get_instance();
        $this->set_settings();
    }
    
    protected function set_settings(){
        $this->CI->load->model("Settings_model");
        $settings= $this->Settings_model->get_settings("sms_settings");
        if(!$settings){
            echo "SMS Gateway Settings Are Not Set";die;
        }
        $this->username= $settings->username;
        $this->password= $settings->password;
        $this->url= $settings->url;
    }
    
    public function send_sms($to,$message){
        //TODO
        $request["to"]=$to;
        $request["message"]=$message;
        $this->call_api($request);
    }
    
    //TODO
    protected function call_api($request){
        
    }
}