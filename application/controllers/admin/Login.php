<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        construct_init(1);
        $this->load->model('Login_model');
        $this->load->library("Ad_lib");
    }
    public function index()
    {
        if(logged_in()){
            redirect(base_url("Search/PublicationSearch"));
        }
        else
        {
            $data['title']= 'Sign in';
            $this->load->view("pages/login", $data);
        }
    }
    
    public function do_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $username = $this->input->post('username');
            $password =md5($this->input->post('password'));
            $user = $this->Login_model->check_login($username,$password);
            if($user){
                //active directory authentication
                $username="cn=read-only-admin";
                $password="password";
                $bind=  $this->ad_lib->authenticate($username,$password);
                if($bind['error'])
                {
                    $this->session->set_flashdata('msg', $bind['error']);
                    redirect(current_url());
                }
                $this->check_password_lock($user);
                $this->check_password_expiry($user);
                $this->set_session($user);
                redirect('Search/PublicationSearch');
                
            }
            else{
                $this->session->set_flashdata('msg',  $this->lang->line("invalid_credentials"));
                 redirect(current_url());
            }
        }
    }
    function Logout()
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $new_data = array(
            'user_id' => '',
            'user_email' => '',
            'user_display_name' => '',
            'logged_in' => FALSE
            );
            $this->session->unset_userdata($new_data);
            $this->session->sess_destroy();
            redirect('Dashboard');
        }
        else
        {
            redirect('Dashboard');
        }
    }
    
    function authenticate(){
        $username="admin";
        $password="12";
        $username="read-only-admin";
        $username="uid=tesla,dc=example,dc=com";
        $password="password";
        $bind=  $this->ad_lib->authenticate($username,$password);
        var_dump($bind);
    }
    
    function check_password_lock($user){
        $password_management= $this->Login_model->get_password_management();
        if($password_management){
        
            if (!empty($user->lockout_counter) && $user->lockout_counter >= $password_management->lockout_threshold) {
                //calculate lockout duration
                $mins = floor((strtotime("now") - strtotime($user->lockout_timestamp)) / 60);
                if ($mins < $password_management->lockout_duration) {
                    //destroy session and redirect to lockout page
                    $this->session->sess_destroy();
                    redirect(base_url("admin/Login/lockout/$password_management->lockout_duration"));
                }
                //if eligibile for counter reset
                if ($mins >= $password_management->reset_counter) {
                    $reset = array(
                        "lockout_counter" => 0,
                        "lockout_timestamp" => NULL
                    );
                    $this->Global_model->global_update("users", $user->id, $reset);
                }
            }     
        
        }
    }
    
    function check_password_expiry($check_login_result){
        $password_management= $this->Login_model->get_password_management();
        if($password_management){
            //check if force reset password is applied
            if ($check_login_result->force_reset) {
                $this->session->set_userdata(array("temp_id" => $check_login_result->id));
                //redirect to change password
                redirect('admin/Login/change_password');
            }

            //check password settings
            $user_id = $check_login_result->id;
            $last_password = $this->Login_model->get_last_password($user_id);
            $days = floor((strtotime("now") - strtotime($last_password->timestamp)) / 86400);
            // check if password age expired
            if ($days >= $password_management->max_age) {
                $this->session->set_userdata(array("temp_id" => $user_id));
                //redirect to change password
                redirect('admin/Login/change_password');
            }
            //end check password settings
        }
    }
    
    function set_session($user){
        //add user data to session
        $new_data = array(
            'user' => $user->user,
            'user_id' => $user->id,
            'user_email' => $user->email,
            'user_display_name' => $user->clientName,
            'logged_in' => TRUE
        );
        $this->session->set_userdata($new_data);
    }
}