<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        construct_init();
    }
    
    public function subscription(){
        $subscription= $this->Global_model->get_data_by_id("users",  $this->session->userdata("user_id"));
        $data['title']=  $this->lang->line("subscription");
        $data['subscription']=$subscription;
        $this->load->view("pages/profile/subscription",$data);
    }
    
    public function test_payment() {
        $this->load->view('new_payment');
    }
}