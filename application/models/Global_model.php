<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Global_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->db->query("alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS'", "");
        $this->db->query("alter session set nls_timestamp_format='YYYY-MM-DD HH24:MI:SS.FF6'", "");
    }

    function global_insert($table_name, $data) {
       return $this->db->insert($table_name, $data);
        // return $this->db->insert_id();
    }

    function global_update($table_name, $id, $data) {
        $this->db->where('id', $id);
        $this->db->update($table_name, $data);
    }

    function get_data_by_id($table_name, $id) {
        $this->db->where('id', $id);
        return $this->db->get($table_name)->row();
    }

    function get_table_count($table_name) {
        $this->db->from($table_name);
        return $this->db->count_all_results();
    }

    function get_table_data($table_name) {
        return $this->db->get($table_name)->result();
    }

    function get_all($table_name) {
        return $this->db->get($table_name)->result();
    }

    function get_all_active($table_name) {
        $this->db->where('deleted !=', 1);
        return $this->db->get($table_name)->result();
    }

    function check_email($table_name, $email) {
        $this->db->where("email", $email);
        $get_data = $this->db->get($table_name);
        if ($get_data->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    function reset_password($table_name, $email, $new_password) {
        $this->db->where('email', $email);
        $this->db->update($table_name, array('password' => md5($new_password)));
        $this->send_new_password($table_name, $email, $new_password);
    }

    function send_reset_link($user_id) {
        $msg_content = '';
        //get Client info
        $this->db->where('id', $user_id);
        $info = $this->db->get("users")->row();
        $last_password = $this->get_last_password($info->id);
        if (!empty($last_password->password)) {
            $last_password = $last_password->password;
        }
        $token = $info->id . "_" . substr($last_password, 0, 5);
        //echo $token;die;
        $msg_content .= "Dear $info->clientName <br/>";
        $reset_link = base_url("User/reset_my_password/$token");
        $msg_content .= "You are receiving this email because you requested a password reset <br/> Please use this "
                . "<a href='$reset_link'>Link</a> to reset your password <br/>"
                . "If it wasn't you, please, ignore this email. ";      // config email smtp
        $settings = $this->db->get('email_settings')->row();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user == '' ? $settings->host_mail : $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($settings->host_mail);
        $this->email->to($info->email);
        $this->email->subject('Password Reset Link');
        $this->email->message($msg_content);
        $res = $this->email->send();
        $r = $this->email->print_debugger();
        // print_R($r);
        // die;
    }

    function send_reset_link_n_user($email, $user) {
        $msg_content = '';
        //get Client info
        //echo $token;die;
        $number = mt_rand(100000, 999999);

        $this->write_to_csv($number, $user);

        $msg_content .= "Dear $user <br/>";
        $reset_link = base_url("N_user/reset_my_password/$number");
        $msg_content .= "You are receiving this email because you requested a password reset <br/> Please use this "
                . "<a href='$reset_link'>Link</a> to reset your password <br/>"
                . "If it wasn't you, please, ignore this email. ";      // config email smtp
        $settings = $this->db->get('email_settings')->row();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user == '' ? $settings->host_mail : $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($settings->host_mail);
        $this->email->to($email);
        $this->email->subject('Password Reset Link');
        $this->email->message($msg_content);
        $res = $this->email->send();
        $r = $this->email->print_debugger();
        // print_R($r);
        // die;
    }

    function write_to_csv($num, $user) {
        $list = array(
            array($num, $user)
        );

        $file = fopen("n_users_forget_pass.csv", "a");

        foreach ($list as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }

    function get_last_password($user_id) {
        $this->db->where("user_id", $user_id);
        $this->db->order_by("id", "desc");
        $query = $this->db->get("user_passwords");
        return $query->row();
    }

    function send_new_password($table_name, $email, $new_password) {
        $msg_content = '';
        //get Client info
        $this->db->where('email', $email);
        $info = $this->db->get($table_name)->row();
        $msg_content .= "Dear $info->first_name $info->last_name <br>";
        $msg_content .= "Your new password is $new_password <br>";
        $msg_content .= "You can change it after login to your account";
        // config email smtp
        $settings = $this->db->get('email_settings')->row();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user == '' ? $settings->host_mail : $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($settings->host_mail);
        $this->email->to($email);
        $this->email->subject('Your New Password');
        $this->email->message($msg_content);
        $this->email->send();
    }

    function send_mail($email, $msg, $subject) {
        // config email smtp
        $settings = $this->db->get('email_settings')->row();
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => $settings->smtp_host, //ssl://smtp.googlemail.com
            'smtp_port' => $settings->smtp_port, // 465 
            'smtp_user' => $settings->smtp_user == '' ? $settings->host_mail : $settings->smtp_user,
            'smtp_pass' => $settings->smtp_pass,
            'mailtype' => 'html',
            'charset' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n"
        );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($settings->host_mail);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($msg);
        $this->email->send();
    }

    function get_count_clients_by_status($table_name, $num) {
        $this->db->where('status', $num);
        $this->db->from($table_name);
        return $this->db->count_all_results();
    }

    function get_clients_by_status($table_name, $num) {
        $this->db->where('status', $num);
        $this->db->from($table_name);
        return $this->db->get()->result();
    }

    function update_client($table_name, $client_id, $data) {
        $this->db->where('id', $client_id);
        $this->db->update($table_name, $data);
        if ($this->db->affected_rows() > 0) {
            $new_data = array(
                'client_first_name' => $data['first_name'],
                'client_last_name' => $data['last_name'],
                'client_display_name' => $data['display_name']
            );
            if ($this->session->userdata('client_id') == $client_id) {
                $this->session->set_userdata($new_data);
            }
        }
    }

    function active_waiting_ban_client($table_name, $num, $client_id) {
        $this->db->where('id', $client_id);
        $data = array('status' => $num);
        $this->db->update($table_name, $data);
    }

    function global_delete($table_name, $id) {
        $this->db->where('id', $id);
        $this->db->delete($table_name);
    }

}
