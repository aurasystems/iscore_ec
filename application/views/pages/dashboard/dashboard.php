<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- END PAGE CSS -->
    </head>
    <style>
        .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $this->lang->line("dashboard") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <!--BEGIN COUNTERS-->
                                    <div class="row m-t-10">
                                        <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="panel">
                                                <div class="panel-content widget-info">
                                                    <div class="row">
                                                        <div class="left">
                                                            <i class="fa fa-users bg-green"></i>
                                                        </div>
                                                        <div class="right">
                                                            <p class="number" data-from="0" data-to="<?= $total_users ?>"><?= $total_users ?></p>
                                                            <p class="text"><?= $this->lang->line("total_users") ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="panel">
                                                <div class="panel-content widget-info">
                                                    <div class="row">
                                                        <div class="left">
                                                            <i class="fa fa-user-plus bg-blue"></i>
                                                        </div>
                                                        <div class="right">
                                                            <p class="number" data-from="0" data-to="<?= $total_admins ?>"><?= $total_admins ?></p>
                                                            <p class="text"><?= $this->lang->line("total_admins") ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row m-t-10">
                                        <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="panel">
                                                <div class="panel-content widget-info">
                                                    <div class="row">
                                                        <div class="left">
                                                            <i class="fa fa-clipboard bg-orange"></i>
                                                        </div>
                                                        <div class="right">
                                                            <p class="number" data-from="0" data-to="<?= $total_searches ?>"><?= $total_searches ?></p>
                                                            <p class="text"><?= $this->lang->line("total_searches") ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="panel">
                                                <div class="panel-content widget-info">
                                                    <div class="row">
                                                        <div class="left">
                                                            <i class="fa fa-calendar bg-yellow"></i>
                                                        </div>
                                                        <div class="right">
                                                            <p class="number" data-from="0" data-to="<?= $month_searches ?>"><?= $month_searches ?></p>
                                                            <p class="text"><?= $this->lang->line("month_searches") ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xlg-2 col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="panel">
                                                <div class="panel-content widget-info">
                                                    <div class="row">
                                                        <div class="left">
                                                            <i class="fa fa-money bg-red"></i>
                                                        </div>
                                                        <div class="right">
                                                            <p class="number" data-from="0" data-to="<?= $month_payments ?>"><?= $month_payments ?></p>
                                                            <p class="text"><?= $this->lang->line("month_payments") ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END COUNTERS-->
                                </div>
                            </div>

                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>

            <div id="dialog" title="<?= $this->lang->line("subscription") ?>">
                <p><?= $this->lang->line("subscribe_annually") ?></p>
            </div>

        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
    </body>
</html>