<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Language extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        construct_init(1);
    }
    
    function index($lang="english"){
        $this->load->library('user_agent');
        $ref=  $this->agent->referrer();
        $this->session->set_userdata("lang",$lang);
        redirect($ref);
    }
}