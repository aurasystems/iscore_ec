<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reports_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_publication_search($filter = NULL) {
        if (!empty($filter["date_from"])) {
            $this->db->where("date_searched >=", $filter["date_from"]);
        }
        if (!empty($filter["date_to"])) {
            $this->db->where("date_searched <", $filter["date_to"]);
        }
        $this->db->order_by("date_searched", "desc");
        return $this->db->get("publication_search")->result();
    }

    function get_search_history($filter = NULL) {
        if (!empty($filter["date_from"])) {
            $this->db->where("date_searched >=", $filter["date_from"]);
        }
        if (!empty($filter["date_to"])) {
            $this->db->where("date_searched <", $filter["date_to"]);
        }
        $this->db->order_by("date_searched", "desc");
        return $this->db->get("search_history")->result();
    }

    function get_payments($filter = NULL) {
        if (!empty($filter["user_id"])) {
            $this->db->where("user_id", $filter["user_id"]);
        }
        if (!empty($filter["date_from"])) {
            $this->db->where("payment_date >=", $filter["date_from"]);
        }
        if (!empty($filter["date_to"])) {
            $this->db->where("payment_date <", $filter["date_to"]);
        }
        $this->db->select('user_payments.*,users.user');
        $this->db->join('users', 'user_payments.user_id=users.id');
        $this->db->order_by("payment_date", "desc");
        return $this->db->get("user_payments")->result();
    }

    function get_publication_search_response($publication_search_id) {
        $this->db->where("publication_search_id", $publication_search_id);
        $query = $this->db->get("publication_search_response");
        return $query->result();
    }

    function get_search_history_response($publication_search_id) {
        $this->db->where("search_history_id", $publication_search_id);
        $query = $this->db->get("search_history_response");
        return $query->result();
    }

    function get_logs($length = -1, $start = 0, $search_value = '') {
        $this->db->select('AUDITS.*,users.clientName');
        //->db->select('audits.action,users.first_name,audits.timestamp');
        // $this->db->order_by('audits.timestamp','DESC');
        $this->db->order_by('AUDITS.TIMESTAMP', 'DESC');
        $this->db->join('users', 'users.id=AUDITS.USER_ID', 'left');
        if ($search_value) {
            $this->db->like("users.clientName", $search_value);
            $this->db->or_like("users.user", $search_value);
            $this->db->or_like("AUDITS.ACTION", $search_value);
        }
        if ($length != -1) {
            $this->db->limit($length, $start);
        }
        return $this->db->get('AUDITS')->result();
    }

    function get_logs_filter($search_value) {
        $this->db->select('AUDITS.*,users.clientName');
        $this->db->order_by('AUDITS.TIMESTAMP', 'DESC');
        $this->db->join('users', 'users.id=AUDITS.USER_ID', 'left');
        $this->db->from('AUDITS');
        if ($search_value) {
            $this->db->like("users.clientName", $search_value);
            $this->db->or_like("users.user", $search_value);
            $this->db->or_like("AUDITS.ACTION", $search_value);
        }
        $query = $this->db->get();
        return $query->num_rows();
    }

}
