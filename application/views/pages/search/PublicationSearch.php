<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- END PAGE CSS -->
    </head>
    <style>
        .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $this->lang->line("PublicationSearch") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form id="form" method="GET" action="" class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label class='col-sm-4 control-label'><?= $this->lang->line("partnerType") ?></label>
                                            <?php
                                            $partnerType = [
                                                "EGYPTIAN_NATURAL_PERSON" => $this->lang->line("egy_n_person"),
                                                "FOREIGN_LEGAL_ENTITY_WITH_BRANCHOFFICE_IN_EGYPT" => $this->lang->line("foreign_legal_entity"),
                                                "FOREIGN_NATURAL_PERSON" => $this->lang->line("foreign_n_person"),
                                                "LEGAL_ENTITY_OPERATING_IN_EGYPT" => $this->lang->line("legal_entity"),
                                            ];
                                            ?>
                                            <div class='col-sm-7'>
                                                <select name="partnerType" id="partnerType" class="form-control">
                                                    <option value=""><?= $this->lang->line("select") ?></option>
                                                    <?php
                                                    foreach ($partnerType as $k => $v) {
                                                        ?>
                                                        <option <?= selected($k, $this->input->get("partnerType")) ?> value="<?= $k ?>"><?= $v ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class='c-red'><?= form_error("partnerType") ?></span>
                                            </div>
                                        </div>
                                        <?= form_group("publicationNumber") ?>
                                        <div id="VnationalID">
                                            <?= form_group("nationalID") ?>
                                        </div>
                                        <div id="VpassportNumber">
                                            <?= form_group("passportNumber") ?>
                                        </div>
                                        <div id="VissuanceCountry" class="form-group">
                                            <label class='col-sm-4 control-label'><?= $this->lang->line("issuanceCountry") ?></label>
                                            <div class='col-sm-7'>
                                                <select data-search="true" name="issuanceCountry" id="issuanceCountry" class="form-control">
                                                    <option value=""><?= $this->lang->line("select") ?></option>
                                                    <?php
                                                    foreach (get_issuance_country() as $one) {
                                                        ?>
                                                        <option <?= selected($one, $this->input->get("issuanceCountry")) ?> value="<?= $one ?>"><?= str_replace("_", " ", $one); ?></option>
                                                    <?php } ?>
                                                </select>
                                                <span class='c-red'><?= form_error("issuanceCountry") ?></span>
                                            </div>
                                        </div>
                                        <div id="VregistrationNumber">
                                            <?= form_group("registrationNumber") ?>
                                        </div>
                                        <input type="hidden" name="feeType" id="feeType" value="SEARCH"/>
                                        <div class="col-sm-12 text-center">
                                            <input id="search" type="submit" class="btn btn-primary" value="<?= $this->lang->line("search") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table class="table table-hover">
                                        <thead>
                                        <th><?= $this->lang->line("publicationNumber") ?></th>
                                        <th><?= $this->lang->line("publicationDate") ?></th>
                                        <th><?= $this->lang->line("guaranteeAmount") ?></th>
                                        <th><?= $this->lang->line("expirationDate") ?></th>
                                        <th><?= $this->lang->line("bankruptDeptor") ?></th>
                                        <th><?= $this->lang->line("creditorName") ?></th>
                                        <th><?= $this->lang->line("pdf_eng") ?></th>
                                        <th><?= $this->lang->line("pdf_ar") ?></th>
                                        </thead>
                                        <tbody>
                                            <?php if (isset($no_found)) { ?>
                                                <tr><td colspan="7"><?= $this->lang->line('data_not_found') ?></td></tr>
                                                <?php
                                            } else {
                                                $one_rec = FALSE;
                                                foreach ($list as $one) {
                                                    if (is_array($one)) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $one['publicationNumber'] ?></td>
                                                            <td><?= $one['publicationDate'] ? to_date($one['publicationDate'], 'Y-m-d') : '' ?></td>
                                                            <td><?= str_replace(".00", "", (string)number_format($one['guaranteeAmount'], 2, '.', ',')) ?></td>
                                                            <td><?= $one['expirationDate'] ? to_date($one['expirationDate'], 'Y-m-d') : '' ?></td>
                                                            <td><?= $one['bankruptDebtor'] ?></td>
                                                            <td><?= $one['creditorName'] ?></td>
                                                            <td><a href="<?= base_url() ?>Search/DocumentDownload/<?= $one['documentIdEn'] ?>" ><?= $this->lang->line("download_document") ?></a></td>
                                                            <td><a href="<?= base_url() ?>Search/DocumentDownload/<?= $one['documentIdAr'] ?>" ><?= $this->lang->line("download_document") ?></a></td>
                                                        </tr>
                                                        <?php
                                                    } else {
                                                        $one_rec = TRUE;
                                                        break;
                                                    }
                                                }
                                                if ($one_rec) {
                                                    ?>
                                                    <tr>
                                                        <td><?= $list['publicationNumber'] ?></td>
                                                        <td><?= $list['publicationDate'] ? to_date($list['publicationDate'], 'Y-m-d') : '' ?></td>
                                                        <td><?= str_replace(".00", "", (string)number_format($list['guaranteeAmount'], 2, '.', ',')) ?></td>
                                                        <td><?= $list['expirationDate'] ? to_date($list['expirationDate'], 'Y-m-d') : '' ?></td>
                                                        <td><?= $list['bankruptDebtor'] ?></td>
                                                        <td><?= $list['creditorName'] ?></td>
                                                        <td><a href="<?= base_url() ?>Search/DocumentDownload/<?= $list['documentIdEn'] ?>" ><?= $this->lang->line("download_document") ?></a></td>
                                                        <td><a href="<?= base_url() ?>Search/DocumentDownload/<?= $list['documentIdAr'] ?>" ><?= $this->lang->line("download_document") ?></a></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>



                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>

            <div id="dialog" title="<?= $this->lang->line("subscription") ?>">
                <p><?= $this->lang->line("subscribe_annually") ?></p>
            </div>

        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>


            $(document).ready(function () {
                $('#VnationalID').hide();
                $('#VpassportNumber').hide();
                $('#VissuanceCountry').hide();
                $('#VregistrationNumber').hide();

                $('#partnerType').on("change", function (e) {
                    var sel = $('#partnerType').val();
                    $('#VnationalID').hide();
                    $('#VpassportNumber').hide();
                    $('#VissuanceCountry').hide();
                    $('#VregistrationNumber').hide();
                    switch (sel) {
                        case "EGYPTIAN_NATURAL_PERSON":
                            $('#VnationalID').show();
                            break;
                        case "FOREIGN_LEGAL_ENTITY_WITH_BRANCHOFFICE_IN_EGYPT":
                        case "LEGAL_ENTITY_OPERATING_IN_EGYPT":
                            $('#VregistrationNumber').show();
                            break;
                        case "FOREIGN_NATURAL_PERSON":
                            $('#VpassportNumber').show();
                            $('#VissuanceCountry').show();
                            break;
                    }
                });
<?php if ($this->input->get("partnerType")) { ?>
                    $("#partnerType").change();
<?php } ?>
<?php
if (!$subscribed) {
    ?>
                    $("#search").click(function (e) {
                        e.preventDefault();
                        open_dialogue();
                    });

                    function open_dialogue() {
                        $("#dialog").dialog({
                            modal: true,
                            buttons: {
                                Yes: function () {
                                    $("#feeType").val("SUBSCRIPTION");
                                    $("#form").submit();
                                },
                                No: function () {
                                    $("#form").submit();
                                }
                            }
                        });
                    }
    <?php
}
?>


            });
        </script>
    </body>
</html>