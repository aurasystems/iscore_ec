<HTML>
    <!-- BUTTON -->
    <button class="payBtn btn btn-primary" onclick="callLightbox()">PAY LightBox</button>
    <!-- CALL SCRIPT -->
    <script src="https://upgstaging.egyptianbanks.com:3006/js/Lightbox.js"></script>

    <script type="text/javascript">
        function callLightbox() {
            var orderId = '123';
            var paymentMethodFromLightBox = 2;
            var amount = 100;
            var mID = 40357;
            var tID = 80050602;
            var secureHash = "64373939653761352D343730352D343666632D623264312D34363235323463616166336";
            var trxDateTime = "20190807032800";
            var returnUrl = "";
            Lightbox.Checkout.configure = {
                OrderId: orderId,
                paymentMethodFromLightBox: paymentMethodFromLightBox,
                MID: mID,
                TID: tID,
                SecureHash: secureHash,
                TrxDateTime: trxDateTime,
                AmountTrxn: amount,
                MerchantReference: "",
                ReturnUrl: returnUrl,
                completeCallback: function (data) {
                    //your code here         
                }, errorCallback: function () {
                    //your code here         
                }, cancelCallback: function () {
                    //your code here         
                }
            };
            Lightbox.Checkout.showLightbox();
        }</script> 

</HTML> 
