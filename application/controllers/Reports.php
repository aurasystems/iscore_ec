<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "ResponseParser.php";

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model("Reports_model");
        $this->session->set_userdata('active', '5');
    }

    function PublicationSearch() {
        $filter = [
            "date_from" => $this->input->get("date_from") ? to_date($this->input->get("date_from")) : '',
            "date_to" => $this->input->get("date_to") ? to_date($this->input->get("date_to")) : ''
        ];
        $publicationSearch = $this->Reports_model->get_publication_search($filter);
        $data['title'] = $this->lang->line("PublicationSearch_report");
        $data['publicationSearch'] = $publicationSearch;
        $this->session->set_userdata('sub_active', '11');
        $this->load->view("pages/reports/PublicationSearch", $data);
    }

    function PublicationSearchResults($id = NULL) {
        check_param($id);
//        $search= $this->Global_model->get_data_by_id("publication_search",$id);
//        check_param($search,"resource");
//        $parser= new ResponseParser(json_decode($search->search_results));
//        $list=$parser->ParsePublicationSearch();
        $list = $this->Reports_model->get_publication_search_response($id);
        $data['title'] = $this->lang->line("PublicationSearchResults");
        $data['list'] = $list;
        $this->session->set_userdata('sub_active', '11');
        $this->load->view("pages/reports/PublicationSearchResults", $data);
    }

    function SearchHistory() {
        $filter = [
            "date_from" => $this->input->get("date_from") ? to_date($this->input->get("date_from")) : '',
            "date_to" => $this->input->get("date_to") ? to_date($this->input->get("date_to")) : ''
        ];
        $searchHistory = $this->Reports_model->get_search_history($filter);
        $data['title'] = $this->lang->line("SearchHistory_report");
        $data['searchHistory'] = $searchHistory;
        $this->session->set_userdata('sub_active', '12');
        $this->load->view("pages/reports/SearchHistory", $data);
    }

    function SearchHistoryResults($id = NULL) {
        check_param($id);
//        $search= $this->Global_model->get_data_by_id("search_history",$id);
//        check_param($search,"resource");
//        $parser= new ResponseParser(json_decode($search->search_results));
//        $list=$parser->ParseSearchHistory();
        $list = $this->Reports_model->get_search_history_response($id);
        $data['title'] = $this->lang->line("SearchHistoryResults");
        $data['list'] = $list;
        $this->session->set_userdata('sub_active', '12');
        $this->load->view("pages/reports/SearchHistoryResults", $data);
    }

    function payments() {
        $filter = [
            "user_id" => $this->input->get("user_id"),
            "date_from" => $this->input->get("date_from") ? to_date($this->input->get("date_from")) : '',
            "date_to" => $this->input->get("date_to") ? to_date($this->input->get("date_to")) : ''
        ];
        $users = $this->Global_model->get_table_data("users");
        $payments = $this->Reports_model->get_payments($filter);
        $data['title'] = $this->lang->line("payments_report");
        $data['payments'] = $payments;
        $data['users'] = $users;
        $this->session->set_userdata('sub_active', '13');
        $this->load->view("pages/reports/payments", $data);
    }

    function audit_trail() {
        $data['title'] = $this->lang->line("audit_trail");

        //$data['changes'] = $this->Reports_model->get_logs($this->input->post('length'), $this->input->post('start'));
        $data['label_att'] = 'col-sm-3 control-label';
        $this->session->set_userdata('sub_active', '14');
        $this->load->view("pages/reports/audit_trail", $data);
    }

    function audit_trail_ajax() {
        $search_val = isset($this->input->post('search')["value"]) ? $this->input->post('search')["value"] : 0;

        $db_data = $this->Reports_model->get_logs($this->input->post('length'), $this->input->post('start'), $search_val);

        $arr = array();
        foreach ($db_data as $one) {
            $sub_array = array();
            $sub_array[] = $one->ACTION;
            $sub_array[] = $one->clientName;
            $sub_array[] = $one->TIMESTAMP;
            $arr[] = $sub_array;
        }



        $output = array(
            "draw" => intval($this->input->post("draw")),
            "recordsTotal" => $this->db->get('AUDITS')->num_rows(),
            "recordsFiltered" => $this->Reports_model->get_logs_filter($search_val),
            "data" => $arr
        );
        echo json_encode($output);
    }

}
