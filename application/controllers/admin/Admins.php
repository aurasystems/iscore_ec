<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admins extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Admins_model');
        construct_init();
    }
    function index() {
//        check_p("users", "v");
        $admins=  $this->Admins_model->get_admins();
        $data['title'] = $this->lang->line('admins');
        $data['admins'] = $admins;
        $this->load->view("pages/admins/index", $data);
    }
    
    public function add() {
//        set_active("add_user");
        check_p("admins", "c");
        $this->form_validation->set_rules("user",$this->lang->line("user"),"trim|required");
        $this->form_validation->set_rules("clientName",$this->lang->line("clientName"),"trim|required");
        $this->form_validation->set_rules("address",$this->lang->line("address"),"trim|required");
        $this->form_validation->set_rules("phoneNumber",$this->lang->line("phoneNumber"),"trim|required");
        $this->form_validation->set_rules("email",$this->lang->line("email"),"trim|required|valid_email");
        $this->form_validation->set_rules("password",$this->lang->line("password"),"trim|required");
        $this->form_validation->set_rules("confirm_password",$this->lang->line("confirm_password"),"trim|required|matches[password]");
        if(!$this->form_validation->run()){
            $data['title'] = $this->lang->line('add_admin');
            $this->load->view('pages/admins/add', $data);
        }
        else{
            $data=array(
                    "user"=>$this->input->post("user"),
                    "password"=>md5($this->input->post("password")),
                    "clientName"=>$this->input->post("clientName"),
                    "address"=>$this->input->post("address"),
                    "phoneNumber"=>$this->input->post("phoneNumber"),
                    "email"=>$this->input->post("email"),
                    "subscription"=>0,
                    "admin"=>1
            );
            $this->Global_model->global_insert("users",$data);
            
            //get user id
            $user=$this->Admins_model->get_user_id($this->input->post("user"));
            //insert password
            $pass_data=[
                "user_id"=>$user->id,
                "password"=>md5($this->input->post("password")),
                "timestamp"=>date("Y-m-d h:i:s")
            ];
            $this->Global_model->global_insert("user_passwords",$pass_data);
            
            $this->session->set_flashdata("success",  $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Admins"));
        }
    }
//    
    public function edit($id=NULL) {
        check_param($id);
//        set_active("users");
        check_p("admins", "u");
        $this->form_validation->set_rules("user",$this->lang->line("user"),"trim|required");
        $this->form_validation->set_rules("clientName",$this->lang->line("clientName"),"trim|required");
        $this->form_validation->set_rules("address",$this->lang->line("address"),"trim|required");
        $this->form_validation->set_rules("phoneNumber",$this->lang->line("phoneNumber"),"trim|required");
        $this->form_validation->set_rules("email",$this->lang->line("email"),"trim|required|valid_email");
        $one= $this->Global_model->get_data_by_id("users",$id);
        check_param($one,"resource");
        if(!$this->form_validation->run()){
            $data['title'] = $this->lang->line('edit_admin');
            $data['one']= $one;
            $this->load->view('pages/admins/edit', $data);
        }
        else{
            $data=array(
                    "user"=>$this->input->post("user"),
                    "clientName"=>$this->input->post("clientName"),
                    "address"=>$this->input->post("address"),
                    "phoneNumber"=>$this->input->post("phoneNumber"),
                    "email"=>$this->input->post("email"),
                    "subscription"=>0,
                    "admin"=>1
            );
            $this->Global_model->global_update("users",$id,$data);
            $this->session->set_flashdata("success",  $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Admins"));
        }
    }
    
    public function delete($id=NULL){
        check_param($id);
        check_p("users", "d");
        $this->Global_model->global_delete("users",$id);
        $this->session->set_flashdata("success",  $this->lang->line("data_deleted_successfully"));
        redirect(base_url("Admins"));
    }
}