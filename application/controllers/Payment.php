<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
//        $this->load->model("Subscription_model");
    }

//    TODO
    function callback() {
        //set payment
        //then on success update user subscription
        $this->update_subscription();
    }

    function update_subscription() {
        $user_id = $this->session->userdata("user_id");
        $user = $this->Global_model->get_data_by_id("users", $user_id);
        $params = array(
            "user" => $user->user,
            "clientName" => $user->clientName,
            "nameOfTheUser" => $user->clientName,
            "address" => $user->address,
            "phoneNumber" => $user->phoneNumber,
            "email" => $user->email,
            "subscription" => 1
        );
        $response = $this->api_lib->CreateOrUpdateUser($params);
        if ($response['faultstring']) {
            //set flash
            $this->session->set_flashdata("error", $response['faultstring']);
            redirect(base_url("Subscription/choose_subscription"));
        }
        $response = $response['response'];
        if ($response['userCreateOrUpdateStatus'] == "USER_CREATE_SUCCESFUL" || $response['userCreateOrUpdateStatus'] == "USER_UPDATE_SUCCESFUL") {
            $params['startDateOfSubscription'] = $response['startDateOfSubscription'];
            $params['endDateOfSubscription'] = $response['endDateOfSubscription'];
        }
        $params['admin'] = 0;
        unset($params["nameOfTheUser"]);
        $params["clientName"] = $user->clientName;
        $this->Global_model->global_update("users", $user_id, $params);
        redirect(base_url("Search/PublicationSearch"));
    }

    //TODO
    function insert_payment() {
        $data = [
            "user_id" => $this->session->userdata("user_id"),
            "payment_date" => date("Y-m-d H:i:s")
        ];
        $this->Global_model->global_insert("user_payments", $data);
    }

}
