<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_add_user') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="box-title editBoxTitle">
                        <h4><i class="fa fa-globe"></i> <?= $this->lang->line('lang_add_user'); ?></h4>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($status == "success") { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                    <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                </div>
                            <?php } ?>
                            <div class="box-body big col-md-6">
                                <div class="row editErrorMsg editThisMsg">
                                    <?php if ($this->session->flashdata('msg')) { ?>
                                        <?php echo $this->session->flashdata('msg');
                                    }
                                    ?>
                                </div>
                                    <?php echo form_open_multipart('User/add_user', $attributes); ?>
                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_upload_user_img') . ':', 'lang_upload_user_img', $label_att); ?> 
                                    <div class="col-sm-7">
                                        <?php echo form_input(array('type' => 'file', 'id' => 'userfile', 'name' => 'userfile', 'class' => 'form-control', 'size' => 20)); ?>
<?php echo form_error('userfile'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_first_name') . ':', 'lang_first_name', $label_att); ?>
                                    <div class="col-sm-7">
                                        <?php echo form_input(array('id' => 'first_name', 'name' => 'first_name', 'class' => 'form-control', 'value' => set_value('first_name'), 'placeholder' => 'FIRST NAME', 'required' => 'required')); ?>
<?php echo form_error('first_name'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_last_name') . ':', 'lang_last_name', $label_att); ?>
                                    <div class="col-sm-7">
                                        <?php echo form_input(array('id' => 'last_name', 'name' => 'last_name', 'class' => 'form-control', 'value' => set_value('last_name'), 'placeholder' => 'LAST NAME', 'required' => 'required')); ?>
<?php echo form_error('last_name'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_display_name') . ':', 'lang_display_name', $label_att); ?>
                                    <div class="col-sm-7">
                                        <?php echo form_input(array('id' => 'display_name', 'name' => 'display_name', 'class' => 'form-control', 'placeholder' => 'DISPLAY NAME', 'value' => set_value('display_name'))); ?>
<?php echo form_error('display_name'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_email_address') . ':', 'lang_email_address', $label_att); ?>
                                    <div class="col-sm-7">
                                        <?php echo form_input(array('type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control', 'value' => set_value('email'), 'placeholder' => 'EMAIL', 'required' => 'required')); ?>
<?php echo form_error('email'); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_password') . ':', 'lang_password', $label_att); ?>
                                    <div class="col-sm-7 inside-tooltip">
                                        <?php echo form_input(array('type' => 'password', 'id' => 'password', 'name' => 'password', 'class' => 'form-control', 'placeholder' => 'PASSWORD', "autocomplete" => "off", 'required' => 'required')); ?>
                                        <i class="fa fa-question-circle c-blue" rel="popover" data-container="body" data-toggle="popover" data-placement="left" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus." data-original-title="Support Title"></i>
<?php echo form_error('password'); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-7 progress" style="display: none;">
                                        <div id="result" class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="25"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <?php echo form_label($this->lang->line('lang_repeat_password') . ':', 'lang_repeat_password', $label_att); ?>
                                    <div class="col-sm-7">
                                        <?php echo form_input(array('type' => 'password', 'id' => 'password2', 'name' => 'password2', 'class' => 'form-control', 'placeholder' => 'CONFIRM PASSWORD', 'required' => 'required')); ?>
<?php echo form_error('password2'); ?>
                                    </div>
                                </div>
                                    <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-success')); ?>
                                <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>User/get_system_users"><?= $this->lang->line('lang_cancel') ?></a></span>
<?php echo form_close(); ?>
                            </div>
                        </div>
                    </div>
<?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
<?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script>
            $(document).ready(function () {
                //            BEGIN PASSWORD STRENGTH
<?php if (!empty($password_management) && $password_management->complex) { ?>
                    $('#password').keyup(function () {
                        checkStrength($('#password').val());
                    });
                    function checkStrength(password) {

                        var strength = 0;
    <?php
    if (!empty($password_management) && $password_management->min_length) {
        ?>
                            var min_length = "<?= $password_management->min_length ?>";
                            if (password.length < min_length) {
                                progress_bar(25, "progress-bar-danger", "<?= $this->lang->line("too_short") ?>");
                            } else {
                                strength += 1;
                            }
        <?php
    } else {
        ?>
                            strength += 1;
        <?php
    }
    ?>
                        // If password contains both lower and uppercase characters, increase strength value.
                        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
                            strength += 1;
                        // If it has numbers and characters, increase strength value.
                        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
                            strength += 1;
                        // If it has one special character, increase strength value.
                        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // If it has two special characters, increase strength value.
                        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
                            strength += 1;
                        // Calculated strength value, we can return messages
                        // If value is less than 2
                        console.log(strength);
                        if (strength < 2) {
                            progress_bar(50, "progress-bar-danger", "<?= $this->lang->line("weak") ?>");
                        } else if (strength <= 3) {
                            progress_bar(75, "progress-bar-warning", "<?= $this->lang->line("medium") ?>");
                        } else {
                            progress_bar(100, "progress-bar-success", "<?= $this->lang->line("strong") ?>");
                        }
                    }


                    function progress_bar(per, c, text) {
                        $("#result").css("width", per + "%");
                        $("#result").attr("aria-valuenow", per);
                        $("#result").removeClass("progress-bar-danger");
                        $("#result").removeClass("progress-bar-warning");
                        $("#result").removeClass("progress-bar-success");
                        $("#result").addClass(c);
                        $("#result").html(text);
                        $(".progress").show();
                        if (per == 100) {
                            $("#submit").removeAttr('disabled');
                        } else {
                            $("#submit").attr("disabled", "disabled");
                        }
                    }
<?php } ?>
//            END PASSWORD STRENGTH
            });
        </script>
    </body>
</html>