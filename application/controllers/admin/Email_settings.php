<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Email_settings extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Email_settings_model');
        if($this->session->userdata('language') != "" )
        {
            $this->lang->load($this->session->userdata('language'), $this->session->userdata('language'));
        }
        else
        {
            $this->lang->load('default');
        }
    }
    public function index($status = NULL)
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $data = array('title' => 'Email Settings');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role'=>'form');
            $data['label_att'] = array ('class' => 'col-sm-4 control-label');
            $data['email_settings'] = $this->Email_settings_model->get_email_settings();
            $this->load->view("pages/settings/email_settings", $data);
        }
        else
        {
            redirect('Dashboard');
        }
    }
    public function update()
    {
        if(($this->session->userdata('user_id') != ""))
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('smtp_host', 'SMTP HOST', 'trim|required');
            $this->form_validation->set_rules('host_mail', 'HOST MAIL', 'trim|required');
            $this->form_validation->set_rules('smtp_port', 'SMTP PORT', 'trim|required');
            //$this->form_validation->set_rules('smtp_user', 'SMTP USER', 'trim|required');
            //$this->form_validation->set_rules('smtp_pass', 'SMTP PASS', 'trim|required');
            if($this->form_validation->run() == FALSE)
            {
                $this->index();
            }
            else
            { 
                $data = array(
                    'smtp_host' => $this->input->post('smtp_host'),
                    'host_mail' => $this->input->post('host_mail'),
                    'smtp_port' => $this->input->post('smtp_port'),
                    'smtp_user' => $this->input->post('smtp_user'),
                    'smtp_pass' => $this->input->post('smtp_pass')
                );
                $this->Email_settings_model->update_settings($data);
                save_audit('Update E-mail settings');
            }
        }
        else
        {
            redirect('Dashboard');
        } 
    }
}