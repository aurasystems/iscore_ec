<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard_model extends CI_Model
{
    function count_total_users(){
        $this->db->where("admin !=",1);
        $query=$this->db->get("users");
        return $query->num_rows();
    }
    function count_total_admins(){
        $this->db->where("admin",1);
        $query=$this->db->get("users");
        return $query->num_rows();
    }
    
    function get_month_payments(){
        $this->db->select('SUM("amount_paid") as "sum"');
        $this->db->where('EXTRACT(month FROM "payment_date") =',date("m"));
        $this->db->where('EXTRACT(year FROM "payment_date") =',date("Y"));
        $query=$this->db->get("user_payments");
        $sum=$query->row()->sum;
        $sum=$sum?$sum:0;
        return $sum;
    }
    
    function count_searches(){
        $query=$this->db->get("publication_search");
        return $query->num_rows();
    }
    
    function count_month_searches(){
        $this->db->where('EXTRACT(month FROM "date_searched") =',date("m"));
        $this->db->where('EXTRACT(year FROM "date_searched") =',date("Y"));
        $query=$this->db->get("publication_search");
        return $query->num_rows();
    }
}