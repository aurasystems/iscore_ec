<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function check_login($username, $password) {
        $this->db->select('*');
//        $this->db->where("user_passwords.password", $password);
        $this->db->where('LOWER("user")', strtolower($username));
//        $this->db->join('user_passwords', 'user_passwords.user_id=users.id');
        $user = $this->db->get("users")->row();
        if ($user) {
            $user_password = $this->get_last_password($user->id);
            if (!$user_password) {
                $this->db->insert("user_passwords", array('user_id' => $user->id, 'timestamp' => date('Y-m-d H:i:s'),
                    'password' => $password));
            }
            if ($user_password->password != $password) {
                $this->db->update("user_passwords", array('password' => $password), array('id' => $user_password->id));
            }
            return $user;
//            $user_password = $this->get_last_password($user->id);
//            if (!empty($user_password->password)) {
//                $user_password = $user_password->password;
//            }
//            //else{
//            //	if($user->admin==0){
//            //		$user_not_admin=$this->db->get_where('users',array('id'=>$user->id,'password'=>$user->password))->row();
//            //		if($user_not_admin){
////return $user_not_admin;
////}
//            //	}
//            //}
//            if ($password == $user_password) {
//                return $user;
//            }
        }
        return FALSE;
    }

    function get_password_management() {
        $query = $this->db->get("password_management");
        return $query->row();
    }

    function get_last_password($user_id) {
        $this->db->where("user_id", $user_id);
        $this->db->order_by("id", "desc");
        $query = $this->db->get("user_passwords");
        return $query->row();
    }

    function check_password($user_id, $password) {
        $user_pass_row = $this->db->get_where('user_passwords', array('user_id' => $user_id, 'password' => $password))->row();
        if ($user_pass_row) {
            return TRUE;
        }
        return FALSE;
    }

}
