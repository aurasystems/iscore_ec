<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    function update_user($user_id, $data)
    {
        if(isset($data['image']))
        {
            $this->db->where('id', $user_id);
            $get_user_img = $this->db->get('users')->row()->image;
        }
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
        if($this->db->affected_rows() > 0)
        {
            $new_data = array(
                'user_first_name' => $data['first_name'],
                'user_last_name' => $data['last_name'],
                'user_display_name' => $data['display_name']
            );
            if(isset($data['image']))
            {
                unlink('./uploads/users/'.$get_user_img.'');
                $new_data['user_image'] = $data['image'];
            }
            if($this->session->userdata('user_id') == $user_id)
            {
                $this->session->set_userdata($new_data);
            }
        }
    }
    function do_system_user_delete($system_user_id)
    {
        $this->db->where('id', $system_user_id);
        $get_user_img = $this->db->get('users')->row()->image;
        unlink('./uploads/users/'.$get_user_img.'');
        $this->db->where('id', $system_user_id);
        $this->db->delete('users');
    }
}