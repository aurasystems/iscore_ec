<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model');
        construct_init();
    }
    function index() {
//        check_p("users", "v");
        $users=  $this->Users_model->get_users();
        $data['title'] = $this->lang->line('users');
        $data['users'] = $users;
        $this->load->view("pages/users/index", $data);
    }
    
//    public function add() {
////        set_active("add_user");
//        check_p("users", "c");
//        $this->form_validation->set_rules("user",$this->lang->line("user"),"trim|required");
//        $this->form_validation->set_rules("clientName",$this->lang->line("clientName"),"trim|required");
//        $this->form_validation->set_rules("address",$this->lang->line("address"),"trim|required");
//        $this->form_validation->set_rules("phoneNumber",$this->lang->line("phoneNumber"),"trim|required");
//        $this->form_validation->set_rules("email",$this->lang->line("email"),"trim|required");
//        $this->form_validation->set_rules("password",$this->lang->line("password"),"trim|required");
//        $this->form_validation->set_rules("confirm_password",$this->lang->line("confirm_password"),"trim|required|matches[password]");
//        if(!$this->form_validation->run()){
//            $data['title'] = lang('add_user');
//            $this->load->view('pages/users/add', $data);
//        }
//        else{
//            $params=array(
//                    "user"=>"",  $this->input->get("user"),
//                    "clientName"=>$this->input->get("clientName"),
//                    "address"=>$this->input->get("address"),
//                    "phoneNumber"=>$this->input->get("phoneNumber"),
//                    "email"=>$this->input->get("email"),
//                    "subscription"=>0
//            );
//            $this->Global_model->global_insert("users",$data);
//            $this->session->set_flashdata("success",  lang("data_submitted_successfully"));
//            redirect(base_url("Categories"));
//        }
//    }
//    
//    public function edit($id=NULL) {
//        check_param($id);
////        set_active("users");
////        check_p("users", "u");
//        $this->form_validation->set_rules("name_en", lang("name_in") . lang("lang_en"), "trim|required");
//        $this->form_validation->set_rules("name_ar", lang("name_in") . lang("lang_ar"), "trim|required");
//        $one= $this->Global_model->get_data_by_id("users",$id);
//        check_param($one,"resource");
//        if(!$this->form_validation->run()){
//            $data['title'] = lang('edit_user');
//            $data['one']= $one;
//            $this->load->view('pages/users/edit', $data);
//        }
//        else{
//            $data=array(
//                "name_en"=>  $this->input->post("name_en"),
//                "name_ar"=>  $this->input->post("name_ar")
//            );
//            $this->Global_model->global_update("users",$id,$data);
//            $this->session->set_flashdata("success",  lang("data_submitted_successfully"));
//            redirect(base_url("Categories"));
//        }
//    }
    
    public function delete($id=NULL){
        check_param($id);
//        check_p("users", "d");
        //check existing users with access level
       // $exists=$this->Categories_model->check_user_customers($id);
     //   if($exists){
       //     $this->session->set_flashdata("error",  lang("customers_assigned_user"));
        //    redirect(base_url("Categories"));
       // }
        $this->Global_model->global_delete("users",$id);
        $this->session->set_flashdata("success",  $this->lang->line("data_deleted_successfully"));
        redirect(base_url("admin/Users"));
    }
    
    function user_payments($user_id=NULL){
        check_param($user_id);
        $payments= $this->Users_model->get_user_payments($user_id);
        $data['title'] = $this->lang->line('user_payments');
        $data['payments'] = $payments;
        $this->load->view("pages/users/user_payments", $data);
    }
    
     function change_password($user_id=NULL) {
         check_param($user_id);
 
        $this->load->model("Login_model");
        $password_management = $this->Login_model->get_password_management();
        //check minimum length
        $length = "";
        if (!empty($password_management) && $password_management->min_length) {
            $length = "min_length[$password_management->min_length]";
        }
        $this->form_validation->set_rules('old_password', $this->lang->line('old_password'), 'xss_clean|trim|required|callback_valid_old_password');
        $this->form_validation->set_rules('password', $this->lang->line('lang_password'), "xss_clean|trim|required|matches[password2]|callback_password_validation|$length");
        $this->form_validation->set_rules('password2', $this->lang->line('pass_confirmation'), 'xss_clean|trim|required|matches[password]');
        if (!$this->form_validation->run()) {
            $data = array('title' => $this->lang->line("change_password"));
            $data['system_user_id'] = $user_id;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $data['user_data'] = $this->Global_model->get_data_by_id('users', $user_id);
            $this->load->model("admin/Login_model");
            $password_management = $this->Login_model->get_password_management();
            $data['password_management'] = $password_management;
            $this->load->view('admin/pages/user/reset_password', $data);
        } else {

            //insert password in database
            $data = array(
                'user_id' => $user_id,
                'password' => md5($this->input->post("password")),
                'timestamp' => date("Y-m-d H:i:s")
            );
            $data['id'] = incremented('user_passwords');
            /**/ $this->Crud_model->insert("user_passwords", $data);
            //end insert password in database
            if ($this->input->post("force_reset")) {
                $update = array('force_reset' => $this->input->post("force_reset"));
                $this->Global_model->global_update("users", $user_id, $update);
            }
            $user = $this->Global_model->get_data_by_id('users', $user_id);
            redirect(current_url());
        }

       
    }
    
    function valid_old_password($value){
        $this->form_validation->set_message("valid_old_password",  $this->lang->line("old_password_not_match"));
        $user_id=$this->session->userdata('user_id');
        $last= $this->Users_model->get_last_password($user_id);
        if(!empty($last)){
            if($last->password==md5($value)){
                return TRUE;
            }
        }
        return FALSE;
        
    }
    
    function password_validation() {
        //check password matches any histrory
        $this->load->model("Login_model");
        $this->form_validation->set_message('password_validation', 'The {field} field shouldn\'t match any of your previous passwords');
        $new_password = $this->input->post("password");
        $password_management = $this->Login_model->get_password_management();
        if ($password_management->enforce_history) {
            $exists = $this->Login_model->check_password($this->session->userdata("user_id"), md5($new_password));
            if (!empty($exists)) {
                return FALSE;
            }
        }
        return TRUE;
    }
}