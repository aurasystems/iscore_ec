<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_lib {

    public $CI;
    public $hostname;
    public $conn;
    public $dc;
    public $response;
    public $admin_dn;       // admin_username
    public $admin_password;
    public $ou;

    public function __construct($admin = NULL) {
        $this->CI = & get_instance();
        $this->set_settings($admin);
        $this->response["error"] = FALSE;
        $this->conn = ldap_connect($this->hostname);
        $this->admin;
//dn distinguished name: This refers to the name that uniquely identifies an entry in the directory.
//dc domain component: This refers to each component of the domain. For example www.mydomain.com would be written as DC=www,DC=mydomain,DC=com
//ou organizational unit: This refers to the organizational unit (or sometimes the user group) that the user is part of. If the user is part of more than one group, you may specify as such, e.g., OU= Lawyer,OU= Judge.
//cn common name: This refers to the individual object (person's name; meeting room; recipe name; job title; etc.) for whom/which you are querying.
//        $this->dc="uid=gauss,ou=mathematicians,dc=example,dc=com";
        //   $this->dc="DC=Colreguat ,DC=dc";
    }

    protected function set_settings($admin = NULL) {
        $this->CI->load->model("Settings_model");
        $ad = $this->CI->Settings_model->get_settings("active_directory");
        if (!$ad) {
            echo "Active Directory Settings Are Not Set";
            die;
        }
        $this->admin = $admin;
        if ($admin) {

            $this->hostname = $ad->second_hostname;
            $this->dc = $ad->second_dc;
            $this->ou = $ad->second_ou;
            $this->admin_dn = $ad->second_admin_dn;
            $this->admin_password = $ad->second_admin_password;
        } else {

            $this->hostname = $ad->hostname;
            $this->dc = $ad->dc;
            $this->ou = $ad->ou;
            $this->admin_dn = $ad->admin_dn;
            $this->admin_password = $ad->admin_password;
        }
    }

    public function authenticate($username, $password) {
        $ci=&get_instance();
        if ($this->check_username_get_pwdlastset($username)) {  // for creditors only
            return -1;
        }

        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($this->conn, $username, $password);
        $error = ldap_error($this->conn);

        if (strtolower($error) != "success") {
            $domain = $this->get_domain_name();
            $bind = ldap_bind($this->conn, $username . '@' . $domain, $password);
//echo $user_name . '@colreguat.dc';DIE;
            $error = ldap_error($this->conn);
            if (strtolower($error) != "success") {
                if ($error == "LDAP: can't find user") {
                    $error = $ci->lang->line('login_name_not_found');
                }
                return $error;
            }
        }


        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .
        //$du="DC=colreguat,DC=dc";
        //echo $du;die;
        $attr = array("telephonenumber");
        // $encoded_newPassword = "{SHA}" . base64_encode(pack("H*", sha1($password)));
        //	echo $encoded_newPassword;die;

        $user_search = ldap_search($this->conn, $du, "(|(cn=$username))", $attr);

        $user_get = ldap_get_entries($this->conn, $user_search);
//print_R($user_get);die;
        ldap_close($this->conn);
        if (empty($user_get) || !isset($user_get[0]['telephonenumber'][0])) {
            return 'NO mobile set for this account';
        }
        $phone_num = $this->adjust_phone_num(str_replace('"', '', $user_get[0]['telephonenumber'][0]));

        return array('success', $phone_num);

        //$attr = "userpassword";
        // $r = ldap_compare($e, $du, $attr, $password);
        //  if ($r === -1) {
        //    return "Error";
        //  } elseif ($r === true) {
        //    $datauser = $this->check_username($user);
        //$password=md5($password);
        //var_dump($bind,$username,$password);
        //return false;
        //search login name first and get user
//        $filter = '(sAMAccountName='.$username.')';
//    $attributes = array("name", "telephonenumber", "mail", "samaccountname");
//    $result = ldap_search($ldap_conn, $baseDN, $filter, $attributes);
//
//    $entries = ldap_get_entries($ldap_conn, $result);  
//    $userDN = $entries[0]["name"][0];  
//    echo ('<p style="color:green;">I have the user DN: '.$userDN.'</p>');
//
//    //Okay, we're in! But now we need bind the user now that we have the user's DN
//    $ldapBindUser = ldap_bind($ldap_conn, $userDN, $userpass);
    }

    public function adjust_phone_num($phone_num) {
        if (substr($phone_num, 0, 2) == '01') {
            return '2' . $phone_num;
        }
        return $phone_num;
    }

    public function get_domain_name() {
        $host_name_arr = explode('//', $this->hostname);
        if (isset($host_name_arr[1])) {
            return $host_name_arr[1];
        }
        return $this->hostname;
    }

    public function add_user($data = NULL) {
        //  $auth = $this->authenticate("auraadmin", "");
        // if (!empty($auth["data"])) {
        //  $ci = & get_instance();
        $e = $this->conn;
        ldap_set_option($e, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($e, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($e, $this->admin_dn, $this->admin_password);
        $error = ldap_error($e);
        if (strtolower($error) != "success") {
            return ldap_error($e);
        }

        $ldaprecord['objectclass'][0] = "top";
        $ldaprecord['objectclass'][1] = "person";
        $ldaprecord['objectclass'][2] = "organizationalPerson";
        $ldaprecord['objectclass'][3] = "user";
        $ldaprecord['cn'] = $data['cn'];
        $ldaprecord['givenname'] = $data['givenname'];
        $ldaprecord['sn'] = $data['sn'];
        $ldaprecord['mail'] = $data['mail'];
        $ldaprecord['telephonenumber'] = $data['mobile'];
        $ldaprecord['samaccountname'] = $data['samaccountname'];
        // $ldaprecord['uid'] = $data['uid'];
        $ldaprecord['displayname'] = $data['displayname'];

        $domain = $this->get_domain_name();
        $ldaprecord['userprincipalname'] = $data['cn'] . '@' . $domain;
        //	$ldaprecord['pwdlastset']='132272009648231314';
        //badpasswordtime,pwdlastset,userprincipalname = domain(cn@colreguat.dc),badpwdcount=1,
        $ldaprecord['useraccountcontrol'] = '512';
        if (isset($data['password'])) {
            $pwdtxt = $data['password'];
            $newPassword = '"' . $pwdtxt . '"';
            $newPass = iconv('UTF-8', 'UTF-16LE', $newPassword);
            // $ldaprecord["userPassword"] = $pwdtxt;
            // $pwdtxt1=  base64_encode($newPass);
            $ldaprecord['unicodePwd'] = $newPass;
        }


        $bs_u = 'cn=' . $data['cn'] . ',' . $this->ou . ',' . $this->dc;

        // try {
        $p = ldap_add($e, $bs_u, $ldaprecord);
        //	echo ldap_error($e);die;
        // } catch (Exception $error) {
        //     echo $error;
        //     die;
        //     return ldap_error($e);
        // }

        $error = ldap_error($this->conn);

        if (strtolower($error) != "success") {
            return ldap_error($e);
        } else {
            ldap_close($e);
            return 1;
        }

        //  } else {
        //     echo "Authentication Error";
        //     die;
        // }
    }

    public function add_user_info($user = NULL, $data = NULL) {
        $ci = $this->CI;
        $e = $this->conn;

        $bs_u = 'cn=' . $user . ',' . $this->dc;


        $p = $this->bind();
        $p = ldap_mod_add($e, $bs_u, $data);
        if ($p == 1) {
            return "sucess";
        } else {
            return "error";
        }
        ldap_close($e);
    }

    public function edit_user($user = NULL, $data = NULL) {
        $ci = & get_instance();
        $e = $this->conn;

        $bs_u = $this->check_username_get_dn($user);

        //$bs_u = 'cn=' . $user . ',' . $this->ou . ',' . $this->dc;

        ldap_set_option($e, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($e, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($e, $this->admin_dn, $this->admin_password);
        $error = ldap_error($e);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }
//	$p = $this->bind();
//echo $bs_u;die;
        //$cn=$data['cn'];



        unset($data['cn']);
        unset($data['password']);
        $data['telephonenumber'] = $data['mobile'];
        //echo $bs_u;die;
        $p = ldap_modify($e, $bs_u, $data);
        //echo ldap_error($e);die;
        //ldap_rename($e, $cn, $bs_u, null, true);
        //echo ldap_error($e);die;
        //var_dump(ldap_error($this->conn));

        if ($p == 1) {
            ldap_close($e);
            return 1;
        } else {
            return ldap_error($e);
        }
    }

    public function edit_user_password($user, $newpassword) {
        $ci = & get_instance();
        $e = $this->conn;

        $bs_u = $this->check_username_get_dn($user);

        // $bs_u = 'cn=' . $user . ',' . $this->ou . ',' . $this->dc;

        ldap_set_option($e, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($e, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($e, $this->admin_dn, $this->admin_password);
        $error = ldap_error($e);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }

        // create the unicode password
        $pwdtxt = $newpassword;
        $newPassword = '"' . $pwdtxt . '"';
        $newPass = iconv('UTF-8', 'UTF-16LE', $newPassword);
        // $ldaprecord["userPassword"] = $pwdtxt;
        // $pwdtxt1=  base64_encode($newPass);
        $entry['unicodePwd'] = $newPass;
        //$entry["unicodePwd"] = $newpass;
        // Modify the password
        if (ldap_mod_replace($e, $bs_u, $entry)) {
            ldap_close($e);
            return 1;
        }
        return ldap_error($e);
    }

    public function delete_user($user = NULL) {
        $ci = & get_instance();
        $e = $this->conn;

        $bs_u = 'cn=' . $user . ',' . $this->dc;


//	$p = $this->bind();
        $p = ldap_delete($e, $bs_u);
        var_dump($p);
        die;
        if ($p == 1) {
            return "sucess";
        } else {
            return "error";
        }
        ldap_close($e);
    }

    public function list_user() {

        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            return $error;
        }

        $filter = "(|(objectclass=*))";
        //$filter="(|(cn=aurapassword))";
        $result = ldap_search($this->conn, $this->dc, $filter);
        $entries = ldap_get_entries($this->conn, $result);
        // echo "<pre>";
        //  Print_r($entries);
        // echo "Search: " . ldap_error($this->conn) . "\n";
        // echo "</pre>";

        return $entries;
        ldap_close($e);
    }

    public function check_username($user = NULL) {

        $ci = & get_instance();
        $e = $this->conn;
        $justthese = array("ou");

        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }
        $du = "cn=" . $user . "," . $this->ou . "," . $this->dc;
        $filter = "(|(cn=$user))";
        $justthese = array("ou", "sn", "givenname", "mail", "employeetype");

        try {
            $sr = ldap_search($e, $du, $filter, $justthese);
            return ldap_get_entries($e, $sr);
        } catch (Exception $error) {
            return 0;
        }
        ldap_close($e);
    }
    
    public function get_email($user = NULL) {
        $this->conn = ldap_connect($this->hostname);
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            return 0;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .
        
        $attr = array("mail");
        
        $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);
        
        $user_get = ldap_get_entries($this->conn, $user_search);
        ldap_close($this->conn);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return 0;
        }
        if (isset($user_get[0]['mail'][0])) {
            return str_replace('"', '', $user_get[0]['mail'][0]);
        }
        
        return 0;
    }

    public function get_bahagian($user = NULL) {

        $ci = & get_instance();
        $e = $this->connect();
        $justthese = array("ou");

        $p = $this->bind();
        $du = "cn=" . $user . "," . $ci->config->item('ldap_user');
        $filter = "(|(cn=$user))";
        $justthese = array("gidNumber");
        $sr = ldap_search($e, $du, $filter, $justthese);

        if ($sr === FALSE) {
            return 0;
        } else {
            //return ldap_get_entries($e, $sr);
            $getgroup = ldap_get_entries($e, $sr);
            $gid = $getgroup[0]['gidnumber'][0];
            $gn = "dc=dosh,dc=gov,dc=my";
            $fgn = "(|(gidNumber=$gid))";
            $jget = array("cn");
            $sgn = ldap_search($e, $gn, $fgn, $jget);
            $gname = ldap_get_entries($e, $sgn);
            return $gname[0]['cn'][0];
        }
        ldap_close($e);
    }

    public function check_username_only($user) {
        $ci= &get_instance();

        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .

        $attr = array("mail");

        $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);

        $user_get = ldap_get_entries($this->conn, $user_search);
        ldap_close($this->conn);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return $ci->lang->line('login_name_not_found');
        }
        if (!isset($user_get[0]['mail'][0])) {
            $user_mail = '';
        } else {
            $user_mail = $user_get[0]['mail'][0];
        }
        $user_mail = str_replace('"', '', $user_mail);

        return array('success', $user_mail);

        //}
    }
    
    public function check_if_username_exist($user) {
        $ci= &get_instance();
        
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .
        
        $attr = array("mail");
        
        $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);
        
        $user_get = ldap_get_entries($this->conn, $user_search);
        ldap_close($this->conn);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return true;
        }
        return false;
    }
    
    function check_email_get_mobile($email){
        $this->conn = ldap_connect($this->hostname);
        $ci = &get_instance();
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .
        
        $attr = array("telephonenumber");
        
        $user_search = ldap_search($this->conn, $du, "(|(mail=$email))", $attr);
        
        $user_get = ldap_get_entries($this->conn, $user_search);
        ldap_close($this->conn);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return $ci->lang->line('login_name_not_found');
        }
        if (!isset($user_get[0]['telephonenumber'][0])) {
            $user_mob = '';
        } else {
            $user_mob = $this->adjust_phone_num(str_replace('"', '', $user_get[0]['telephonenumber'][0]));
        }
        
        return array('success', $user_mob);
    }

    function check_username_get_mobile($user) {
        $this->conn = ldap_connect($this->hostname);
        $ci = &get_instance();
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            if ($error == "LDAP: can't find user") {
                $error = $ci->lang->line('login_name_not_found');
            }
            return $error;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .

        $attr = array("telephonenumber");

        $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);

        $user_get = ldap_get_entries($this->conn, $user_search);
        ldap_close($this->conn);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return $ci->lang->line('login_name_not_found');
        }
        if (!isset($user_get[0]['telephonenumber'][0])) {
            $user_mob = '';
        } else {
            $user_mob = $this->adjust_phone_num(str_replace('"', '', $user_get[0]['telephonenumber'][0]));
        }

        return array('success', $user_mob);
    }

    function check_username_get_pwdlastset($user) {
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            // ldap_close($this->conn);
            return FALSE;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .

        $attr = array("pwdlastset");

        $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);

        $user_get = ldap_get_entries($this->conn, $user_search);
        //ldap_close($this->conn);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return FALSE;
        }

        if (!isset($user_get[0]['pwdlastset'][0])) {
            return FALSE;
        } else if ($user_get[0]['pwdlastset'][0] == 0) {
            return true;
        }

        return FALSE;
    }

    function check_username_get_dn($user) {
        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            return $error;
        }
        $du = $this->dc; //"CN=" . $username . "," . $this->ou . "," .

        $attr = array("distinguishedname");

        $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);

        $user_get = ldap_get_entries($this->conn, $user_search);
        if (empty($user_get) || (isset($user_get['count']) && $user_get['count'] == 0)) {
            return FALSE;
        }
        if (!isset($user_get[0]['distinguishedname'][0])) {
            return FALSE;
        } else {
            return $user_get[0]['distinguishedname'][0];
        }
    }

    public function check_login($user_name, $password) {
        $ci=&get_instance();

        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $bind = ldap_bind($this->conn, $user_name, $password);
        $error = ldap_error($this->conn);

        if (strtolower($error) != "success") {
            $domain = $this->get_domain_name();
            $bind = ldap_bind($this->conn, $user_name . '@' . $domain, $password);
//echo $user_name . '@colreguat.dc';DIE;
            $error = ldap_error($this->conn);
            if (strtolower($error) != "success") {
                if ($error == "LDAP: can't find user") {
                    $error = $ci->lang->line('login_name_not_found');
                }
                return $error;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    public function check_login_old($user = NULL, $password = NULL) {

        $ci = & get_instance();
        $e = $this->conn;
        $justthese = array("ou");

        ldap_set_option($this->conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($this->conn, LDAP_OPT_REFERRALS, 0);
        $p = ldap_bind($this->conn, $this->admin_dn, $this->admin_password);
        $error = ldap_error($this->conn);
        if (strtolower($error) != "success") {
            return $error;
        }

        if ($datauser = $this->check_username($user) !== 0) {

            $du = "CN=" . $user . "," . $this->ou . "," . $this->dc;

            $attr = array("userpassword");
            // $encoded_newPassword = "{SHA}" . base64_encode(pack("H*", sha1($password)));
            //	echo $encoded_newPassword;die;

            $user_search = ldap_search($this->conn, $du, "(|(cn=$user))", $attr);
            $user_get = ldap_get_entries($this->conn, $user_search);

            if (empty($user_get)) {
                echo 'Email or Password is wrong...';
                return false;
            }
            $ad_password = str_replace('"', '', $user_get[0]['userpassword'][0]);

            //$attr = "userpassword";
            // $r = ldap_compare($e, $du, $attr, $password);
            //  if ($r === -1) {
            //    return "Error";
            //  } elseif ($r === true) {
            //    $datauser = $this->check_username($user);
            //$password=md5($password);
            ldap_close($e);
            if (strip_tags($ad_password) == strip_tags($password)) {
                echo 'Success';
                return true;
            } else {
                echo 'Email or Password is wrong...';
                return false;
            }
            //    } else {
            //        echo "apa ko buat";
            //  }
            //   } else {
            //      echo "no user";
        }
    }

//     $info["cn"]="John Jones";
//    $info["sn"]="Jones";
//    $info["mail"]="jonj@example.com";
//    $info["objectclass"]="person";
//
//    // Add data to directory
//    $r = ldap_add($ldapconn, "cn=John Jones,dc=test,dc=com", $info);
}
