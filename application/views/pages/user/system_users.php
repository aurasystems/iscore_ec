<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_view_users') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="box-title editBoxTitle">
                        <h4><i class="fa fa-globe"></i> <?= $this->lang->line('lang_view_users'); ?></h4>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?php if ($status == "success") { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                    <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                </div>
                            <?php } ?>
                            <div class="panel">
                                <div class="panel-content">
                                    <div class="filter-left">
                                        <table class="table table-dynamic table-tools">
                                            <thead>
                                                <tr>
                                                    <th><?= $this->lang->line('lang_first_name') ?></th>
                                                    <th><?= $this->lang->line('lang_last_name') ?></th>
                                                    <th><?= $this->lang->line('lang_display_name') ?></th>
                                                    <th><?= $this->lang->line('lang_email_address') ?></th>
                                                    <th><?= $this->lang->line('lang_actions') ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($users_data) {
                                                    foreach ($users_data as $user) {
                                                        ?>
                                                        <tr>
                                                            <td><?= $user->first_name ?></td>
                                                            <td><?= $user->last_name ?></td>
                                                            <td><?= $user->display_name ?></td>
                                                            <td><?= $user->email ?></td>
                                                            <td>
                                                                <a href="<?= base_url() ?>User/edit_user_view/<?= $user->id ?>" class="edit btn btn-sm btn-default"><i class="icon-note"></i></a>
                                                                <a href="<?= base_url() ?>User/delete_user_view/<?= $user->id ?>" class="delete btn btn-sm btn-danger"><i class="icons-office-52"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
    </body>
</html>