<!DOCTYPE html>
<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
    </head>
    <body class="account <?= $this->lang->line('system_dir') ?>" data-page="login">
        <?= login_change_lang() ?>
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <div class="account-wall">
                        <div class="text-center" style="margin-bottom: 20px">
                            <img src="<?= base_url() ?>uploads/content/Untitled-1-1.png" style="width:60%" />
                        </div>
                        <div class="row editErrorMsg">
                            <?= flash_msg() ?>
                        </div>
                        <?php echo form_open('n_user/do_reset_password'); ?>
                        <div class="append-icon m-b-20">
                            <?php echo form_input(array('type' => 'text', 'id' => 'user', 'name' => 'user', 'class' => 'form-control form-white user', 'placeholder' => $this->lang->line("user"), 'required' => 'required')); ?>
                            <i class="icon-lock"></i>
                            <?php echo form_error('user'); ?>
                        </div>
                        <!--                        <div class="append-icon m-b-20">
                        <?php //echo form_input(array('type' => 'text', 'id' => 'ou', 'name' => 'ou', 'class' => 'form-control form-white ou', 'placeholder' => $this->lang->line("ou"), 'required' => 'required')); ?>
                                                    <i class="icon-lock"></i>
                        <?php //echo form_error('ou'); ?>
                                                </div>-->
                        <!--<div class="append-icon m-b-20">
                        <?php //echo form_input(array('type' => 'text', 'id' => 'phoneNumber', 'name' => 'phoneNumber', 'class' => 'form-control form-white phoneNumber', 'placeholder' => $this->lang->line("phoneNumber"), 'required' => 'required')); ?>
                            <i class="icon-lock"></i>
                        <?php //echo form_error('phoneNumber'); ?>
                        </div>
                        <div class="append-icon m-b-20">
                        <?php //echo form_input(array('type' => 'email', 'id' => 'email', 'name' => 'email', 'class' => 'form-control form-white email', 'placeholder' => $this->lang->line("email"), 'required' => 'required')); ?>
                            <i class="icon-lock"></i>
                        <?php //echo form_error('email'); ?>
                        </div>-->
                        <div class="text-center captcha">
                            <img id="captImg" src="<?= $captchaImg ?>" width="100%" />
                            <button title="<?= $this->lang->line('refresh_captcha') ?>" type="button" id="refreshCaptcha" style="margin-right:0px;margin-left: 0px;<?= $this->lang->line('system_dir') == 'rtl' ? 'float:left' : 'float:right' ?>" class="btn btn-default btn-square"><i class="fa fa-refresh" style="font-size: 15px;" ></i></button>
                        </div>
                        <div class="append-icon m-b-20 m-t-20">
                            <div class="row">
                                <div class="col-sm-8">
                                    <?php echo form_input(array('type' => 'text', 'name' => 'captcha', 'class' => 'form-control form-white', 'placeholder' => $this->lang->line('captcha'), 'required' => 'required')); ?>
                                    <?php echo form_error('captcha'); ?>                
                                </div>
                                <!--                                <div class="col-sm-4">
                                                                    <button type="button" class="btn btn-danger btn-block" id="refreshCaptcha">Refresh</button> 
                                                                </div>-->
                            </div>
                        </div>
                        <?php echo form_submit(array('class' => 'btn btn-lg btn-danger btn-block ladda-button', 'data-style' => 'expand-left', 'value' => $this->lang->line('lang_pass_reset_link'),'style' => 'background-color: #1D4B8E;text-align: center')); ?>
                        <div class="text-center">
                            <a href="" onclick = "window.top.location.href = 'https://login.ecr.gov.eg/Creditor/load_login'"><?= $this->lang->line('redirect_to_home') ?></a>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <?php $this->load->view('private/scripts/login_js'); ?>
        <script>
            $(document).ready(function () {
                $('#refreshCaptcha').on('click', function () {
                    $.get('<?= base_url('User/refresh_captcha'); ?>', function (data) {
                        $('#captImg').attr("src", data);
                    });
                });
            });

        </script>
    </body>
</html>