<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(1);
        $this->load->model('User_model');
    }

    public function index() {
        redirect('Dashboard');
    }

    public function reset_password() {
        if (($this->session->userdata('user_id') != "")) {
            redirect('Dashboard');
        } else {
            $data = array('title' => 'Reset Password');
            $this->load->helper('captcha');
            $config = array(
                'img_url' => base_url() . 'uploads/',
                'img_path' => 'uploads/',
                'colors' => array(
                    'background' => array(255, 255, 255),
                    'border' => array(255, 255, 255),
                    'text' => array(0, 0, 0),
//                'grid' => array(155, 164, 171)
                    'grid' => array(199, 87, 87)
                )
            );
            $captcha = create_captcha($config);
            $this->session->unset_userdata('valuecaptchaCode');
            $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
            $data['captchaImg'] = base_url() . 'uploads/' . $captcha['filename'];
            $this->load->view("pages/forget_password", $data);
        }
    }

    public function validate_captcha() {
        //return true;
        if ($this->input->post('captcha') != $this->session->userdata['valuecaptchaCode']) {
            $this->form_validation->set_message('validate_captcha', $this->lang->line('captcha_code_is_wrong'));
            return false;
        } else {
            return true;
        }
    }

    public function refresh_captcha() {
        // Captcha configuration
        $config = array(
            'img_url' => base_url() . 'uploads/',
            'img_path' => 'uploads/',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                //'grid' => array(155, 164, 171),
                'grid' => array(199, 87, 87)
            )
        );
        $captcha = create_captcha($config);

        // Unset previous captcha and set new captcha word
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        // Display captcha image link
        echo base_url('uploads/' . $captcha['filename']);
    }

    public function do_reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user', $this->lang->line("user"), 'trim|required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha');
        //  $this->form_validation->set_rules('ou', $this->lang->line("ou"), 'trim|required');
        //  $this->form_validation->set_rules('phoneNumber', $this->lang->line("phoneNumber"), 'trim|required');
        //  $this->form_validation->set_rules('email', $this->lang->line("email"), 'trim|required|valid_email');
        if ($this->form_validation->run() == FALSE) {
            $this->reset_password();
        } else {
//            TODO remove the below and validate against active directory
            // $email = $this->input->post('email');
            // $check_user = $this->Global_model->check_email('users',$email);
            $this->load->library('Ad_lib');
            $error = $this->ad_lib->check_username_only($this->input->post('user'));
            if (is_array($error)) {
                $ad_email = $error[1];

                $user_row = $this->db->get_where('users', array('user' => strtolower($this->input->post('user'))))->row();
                if (!$user_row && $ad_email == '') {
                    $this->session->set_flashdata('msg', $this->lang->line('email_not_set_ad'));
                    redirect(base_url('User/reset_password'));
                    // $this->reset_password();
                    //  return;
                }
                if (!$user_row) {
                    $data = array(
                        "user" => strtolower($this->input->post("user")),
                        "clientName" => $this->input->post("user"),
                        "email" => $ad_email,
                        "subscription" => 0
                    );
                    $user_id = $this->add_user_forget_pass($data);
                } else {
                    if ($ad_email == '') {
                        $this->db->update('users', array('email' => $ad_email), array('id' => $user_row->id));
                    }
                    $user_id = $user_row->id;
                }
                if ($ad_email == '') { // if ad_email =='' then for sure there is a user w2la kan hy5rg mn awel if.
                    $ad_email = $user_row->email;
                }

//                    $this->session->set_flashdata('msg', 'This login name is not found');
//                    $this->reset_password();
                $this->Global_model->send_reset_link($user_id);
                save_audit('forget password: reset password mail send to user:' . $this->input->post('user'));
                redirect('User/password_reset_success');
            } else {
                // user not exists
                save_audit('ERROR: forget password: login name (' . $this->input->post('user') . ') : ' . $error);
                $this->session->set_flashdata('msg', $error);
                $this->reset_password();
            }
        }
    }

    function add_user_forget_pass($data) {

        $data['admin'] = 0;
        $this->Global_model->global_insert("users", $data);

        $this->load->model('Users_model');
        $user_id = $this->Users_model->get_user_id($data['user']);


//        //insert password
        $pass_data = [
            "user_id" => $user_id,
            "password" => md5('123456S$s'),
            "timestamp" => date("Y-m-d h:i:s")
        ];
        save_audit('New search user added while forget password with username: ' . $data['user']);
        $this->Global_model->global_insert("user_passwords", $pass_data);
        return $user_id;
    }

    function password_reset_success() {
        if (($this->session->userdata('user_id') != "")) {
            redirect('Dashboard');
        } else {
            $data['title'] = 'Password Recover Success';
            $this->load->view("pages/password_recover_success", $data);
        }
    }

    function get_system_users($status = NULL) {
        if (($this->session->userdata('user_id') != "")) {
            $data = array('title' => 'System Users');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $data['users_data'] = $this->Global_model->get_table_data('users');
            $this->load->view("pages/user/system_users", $data);
        } else {
            redirect('Dashboard');
        }
    }

    function add_user_view($status = NULL) {
        if (($this->session->userdata('user_id') != "")) {
            $data = array('title' => 'Add New User');
            $data['status'] = $status;
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $this->load->view("pages/user/add_user", $data);
        } else {
            redirect('Dashboard');
        }
    }

    function add_user($status = NULL) {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('display_name', 'display name', 'trim|required|min_length[4]');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('password', 'Password', 'trim|matches[password2]|min_length[4]|max_length[32]');
        $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|matches[password]');
        if ($this->form_validation->run() == FALSE) {
            $this->add_user_view($status);
        } else {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'display_name' => $this->input->post('display_name'),
                'email' => $this->input->post('email'),
                'password' => do_hash($this->input->post('password'), 'md5'),
            );
            if ($_FILES['userfile']['name'] != '') {
                $config['upload_path'] = './uploads/users';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '10000';
                $config['max_width'] = '5000';
                $config['max_height'] = '5000';
                $this->lang->load('upload');
                $this->load->library('upload', $config);
                if ($this->upload->do_upload()) {
                    $upload_data = $this->upload->data();
                    $data['image'] = $upload_data['file_name'];
                }
            }
            $this->Global_model->global_insert('users', $data);
            redirect('User/add_user/success');
        }
    }

    function edit_user_view($user_id, $status = NULL) {
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {
                $data = array('title' => 'Edit System User');
                $data['system_user_id'] = $user_id;
                $data['status'] = $status;
                $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                $data['label_att'] = array('class' => 'col-sm-4 control-label');
                $data['user_data'] = $this->Global_model->get_data_by_id('users', $user_id);
                $this->load->view('pages/user/edit_user', $data);
            } else {
                redirect('Dashboard');
            }
        } else {
            redirect('Dashboard');
        }
    }

    function update_user($user_id) {
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[3]');
                $this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[4]');
                $this->form_validation->set_rules('display_name', 'display name', 'trim|required|min_length[4]');
                $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
                $this->form_validation->set_rules('password', 'Password', 'trim|matches[password2]|min_length[4]|max_length[32]');
                $this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|matches[password]');
                if ($this->form_validation->run() == FALSE) {
                    $this->edit_user_view($user_id);
                } else {
                    if ($this->input->post('password') != "" && $this->input->post('password2') != "") {
                        $data = array(
                            'first_name' => $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'display_name' => $this->input->post('display_name'),
                            'email' => $this->input->post('email'),
                            'password' => do_hash($this->input->post('password'), 'md5'),
                        );
                    } else {
                        $data = array(
                            'first_name' => $this->input->post('first_name'),
                            'last_name' => $this->input->post('last_name'),
                            'display_name' => $this->input->post('display_name'),
                            'email' => $this->input->post('email')
                        );
                    }
                    if ($_FILES['userfile']['name'] != '') {
                        $config['upload_path'] = './uploads/users';
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';
                        $config['max_size'] = '10000';
                        $config['max_width'] = '5000';
                        $config['max_height'] = '5000';
                        $this->lang->load('upload');
                        $this->load->library('upload', $config);
                        if ($this->upload->do_upload()) {
                            $upload_data = $this->upload->data();
                            $data['image'] = $upload_data['file_name'];
                        }
                    }
                    $this->User_model->update_user($user_id, $data);
                    redirect('User/edit_user_view/' . $user_id . '/success');
                }
            } else {
                redirect('Dashboard');
            }
        } else {
            redirect('Dashboard');
        }
    }

    function delete_user_view($user_id, $status = NULL) {
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {
                $data = array('title' => 'Delete System User');
                $data['system_user_id'] = $user_id;
                $data['status'] = $status;
                $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
                $data['label_att'] = array('class' => 'col-sm-4 control-label');
                $this->load->view('pages/user/delete_user', $data);
            } else {
                redirect('Dashboard');
            }
        } else {
            redirect('Dashboard');
        }
    }

    function delete_user($user_id = NULL) {
        if ($user_id != '' && $user_id != NULL) {
            if (($this->session->userdata('user_id') != "")) {

                $this->Global_model->global_delete('users', $user_id);
                redirect('User/get_system_users/success');
            } else {
                redirect('dashboard');
            }
        } else {
            redirect('dashboard');
        }
    }

    function construct_ad_data($data_arr) {
        $ldaprecord['cn'] = $data_arr['user'];
        $ldaprecord['givenname'] = $data_arr['user'];
        $ldaprecord['sn'] = $data_arr['user'];
        $ldaprecord['mail'] = $data_arr['email'];
        $ldaprecord['mobile'] = $data_arr['phoneNumber'];
        //$ldaprecord['uid'] = '';
        $ldaprecord['displayname'] = $data_arr['clientName'];
        //$ldaprecord['samaccountname'] = $data_arr['user'];
        if (isset($data_arr['password'])) {  // Edit case
            $ldaprecord['password'] = $data_arr['password'];
        }
        return $ldaprecord;
    }

    public function reset_my_password($token) {
        $token_temp = explode("_", $token);
        $id = $token_temp[0];
        $user = $this->Global_model->get_data_by_id("users", $id);
        $last_password = $this->Global_model->get_last_password($id);
        if (!empty($last_password->password)) {
            $last_password = $last_password->password;
        }
        if (!($user && substr($last_password, 0, 5) == $token_temp[1])) {
            $this->session->set_flashdata('msg', $this->lang->line('expired_link'));
            redirect(base_url("login"));
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_blocked');
        if (!$this->form_validation->run()) {
            $data = array('title' => 'Reset Password', "token" => $token);
            $this->load->model("Settings_model");
            $data['password_management'] = $this->Settings_model->get_password_management();
            $this->load->view("pages/reset_my_password", $data);
        } else {
            //verify token
            $token = explode("_", $token);
            $id = $token[0];
            $user = $this->Global_model->get_data_by_id("users", $id);
            $last_password = $this->Global_model->get_last_password($id);
            if (!empty($last_password->password)) {
                $last_password = $last_password->password;
            }
            if ($user && substr($last_password, 0, 5) == $token[1]) {
                
                $this->load->library('Ad_lib');
                // $ad_data['password'] = $this->input->post("password");
                $error_msg = $this->ad_lib->edit_user_password($user->user, $this->input->post("password"));
                //$error_msg = $this->ad_lib->edit_user($user->user, $ad_data);
                if ($error_msg == 1) {
                    $data = ["password" => md5($this->input->post("password"))];
                    $this->Global_model->global_update("users", $id, $data);
                    $user_password = [
                        "user_id" => $id,
                        "password" => md5($this->input->post("password")),
                        "timestamp" => date("Y-m-d H:i:s")
                    ];
                    $this->Global_model->global_insert("user_passwords", $user_password);
                    if ($this->session->userdata('first_login')) {
                        $this->session->unset_userdata('first_login');
                    }
                    $this->session->set_flashdata('msg', $this->lang->line('password_chanhged_success'));
                    redirect('Login');
                } else {
                    $this->session->set_flashdata('msg', $error_msg);
                    redirect(base_url("user/reset_password"));
                }
            } else {
                // user not exists
                $this->session->set_flashdata('msg', $this->lang->line('expired_link'));
                redirect(base_url("user/reset_password"));
            }
        }
    }
    
    public function check_blocked($pass) {
        if (check_password_blacklist(md5($pass))) {
            $this->form_validation->set_message('check_blocked', $this->lang->line('password_in_blocked_list'));
            return false;
        }
        return TRUE;
    }

}
