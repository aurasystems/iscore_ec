﻿<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2015, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (http://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']		= 'خانة {field} مطلوبة';
$lang['form_validation_isset']			= 'خانة {field} يجب أن تحتوي على قيمة';
$lang['form_validation_valid_email']		= 'خانة {field} يجب أن يحتوي على عنوان بريد إلكتروني صالح.';
$lang['form_validation_valid_emails']		= 'خانة {field} يجب أن يحتوي على جميع عناوين البريد الإلكتروني الصالحة.';
$lang['form_validation_valid_url']		= 'خانة {field} يجب أن يحتوي على عنوان URL صالح.';
$lang['form_validation_valid_ip']		= 'خانة {field} يجب أن يحتوي على IP صالح.';
$lang['form_validation_min_length']		= 'خانة {field} لا بد أن يكون على الأقل{param} حرفا';
$lang['form_validation_max_length']		= 'خانة {field} لا يمكن تجاوز {param} حرفا';
$lang['form_validation_exact_length']		= 'خانة {field} يجب أن يكون بالضبط{param} حرفا';
$lang['form_validation_alpha']			= 'خانة {field} قد يحتوي فقط على أحرف أبجدية';
$lang['form_validation_alpha_numeric']		= 'خانة {field} قد يحتوي فقط على أحرف أبجدية رقمية';
$lang['form_validation_alpha_numeric_spaces']	= 'خانة {field} قد يحتوي فقط على أحرف أبجدية رقمية ومسافات';
$lang['form_validation_alpha_dash']		= 'خانة {field} قد يحتوي فقط على أحرف أبجدية رقمية و underscore وشرطات';
$lang['form_validation_numeric']		= 'خانة {field} يجب أن يحتوي على أرقام فقط';
$lang['form_validation_is_numeric']		= 'خانة {field} يجب أن يحتوي على أحرف رقمية فقط.';
$lang['form_validation_integer']		= 'خانة {field} يجب أن يحتوي على عدد صحيح';
$lang['form_validation_regex_match']		= 'خانة {field} ليس بالتنسيق الصحيح';
$lang['form_validation_matches']		= 'خانة {field} لا يطابق{param} خانة';
$lang['form_validation_differs']		= 'خانة {field} يجب أن تختلف عن{param} خانة';
$lang['form_validation_is_unique'] 		= 'خانة {field} يجب أن يحتوي على قيمة فريدة';
$lang['form_validation_is_natural']		= 'خانة {field} يجب أن يحتوي فقط على أرقام.';
$lang['form_validation_is_natural_no_zero']	= 'خانة {field} يجب أن يحتوي فقط على أرقام ويجب أن يكون أكبر من الصفر';
$lang['form_validation_decimal']		= 'خانة {field} يجب أن يحتوي على رقم عشري';
$lang['form_validation_less_than']		= 'خانة {field} يجب أن يحتوي على رقم أقل من{param}.';
$lang['form_validation_less_than_equal_to']	= 'خانة {field} يجب أن يحتوي على رقم أقل من أو يساوي{param}.';
$lang['form_validation_greater_than']		= 'خانة {field} يجب أن يحتوي على عدد أكبر من{param}.';
$lang['form_validation_greater_than_equal_to']	= 'خانة {field} يجب أن يحتوي على عدد أكبر من أو يساوي{param}.';
$lang['form_validation_error_message_not_set']	= 'غير قادر على الوصول إلى رسالة خطأ المقابلة لاسم خانة الخاصة بك{field}.';
$lang['form_validation_in_list']		= 'خانة {field}يجب أن يكون واحدًا من:{param}.';
