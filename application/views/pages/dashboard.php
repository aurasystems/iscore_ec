<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li class="active"><?= $this->lang->line('lang_dashboard') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="box-title editBoxTitle">
                        <h4><i class="fa fa-globe"></i> <?= $this->lang->line('lang_dashboard'); ?></h4>
                    </div>
                    <div class="row">
                        <div class="col-xlg-12 col-lg-4 col-md-4 col-sm-12">
                            <div class="panel">
                                <div class="panel-content widget-small bg-green">
                                    <div class="title text-center">
                                        <h1><?= $this->lang->line('lang_active_clients') ?></h1>
                                        <span><?= $active_clients ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xlg-12 col-lg-4 col-md-4 col-sm-12">
                            <div class="panel">
                                <div class="panel-content widget-small bg-yellow">
                                    <div class="title text-center">
                                        <h1><?= $this->lang->line('lang_waiting_clients') ?></h1>
                                        <span><?= $waiting_clients ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xlg-12 col-lg-4 col-md-4 col-sm-12">
                            <div class="panel">
                                <div class="panel-content widget-small bg-red">
                                    <div class="title text-center">
                                        <h1><?= $this->lang->line('lang_banned_clients') ?></h1>
                                        <span><?= $banned_clients ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xlg-12 col-lg-4 col-md-4 col-sm-12">
                            <div class="panel">
                                <div class="panel-content widget-small bg-blue-light">
                                    <div class="title text-center">
                                        <h1><?= $this->lang->line('lang_system_users') ?></h1>
                                        <span><?= $users_count ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>