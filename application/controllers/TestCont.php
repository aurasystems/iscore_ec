<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . "ResponseParser.php";

class TestCont extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function session() {
        pd($this->session->userdata());
    }


    function parser() {
        $parser = new ResponseParser();
        $parser->parse();
    }

    function extract() {
        $s = 'AFGHANISTAN"/>
            ALBANIA"/>
            ALGERIA"/>
            AMERICAN_SAMOA"/>
            ANDORRA"/>
            ANGOLA"/>
            ANGUILLA"/>
            ANTARCTICA"/>
            ANTIGUA_AND_BARBUDA"/>
            ARGENTINA"/>
            ARMENIA"/>
            ARUBA"/>
            AUSTRALIA"/>
            AUSTRIA"/>
            AZERBAIJAN"/>
            BAHAMAS_THE"/>
            BAHRAIN"/>
            BANGLADESH"/>
            BARBADOS"/>
            BELARUS"/>
            BELGIUM"/>
            BELIZE"/>
            BENIN"/>
            BERMUDA"/>
            BHUTAN"/>
            BOLIVIA"/>
            BOSNIA_AND_HERZEGOVINA"/>
            BOTSWANA"/>
            BOUVET_ISLAND"/>
            BRAZIL"/>
            BRITISH_INDIAN_OCEAN_TERRITORY"/>
            BRUNEI_DARUSSALAM"/>
            BULGARIA"/>
            BURKINA_FASO"/>
            BURUNDI"/>
            CAMBODIA"/>
            CAMEROON"/>
            CANADA"/>
            CAPE_VERDE"/>
            CAYMAN_ISLANDS"/>
            CENTRAL_AFRICAN_REPUBLIC"/>
            CHAD"/>
            CHANNEL_ISLANDS"/>
            CHILE"/>
            CHINA"/>
            CHRISTMAS_ISLAND"/>
            COCOS_KEELING_ISLANDS"/>
            COLOMBIA"/>
            COMOROS"/>
            CONGO_DEM_REP"/>
            CONGO_REP"/>
            COOK_ISLANDS"/>
            COSTA_RICA"/>
            COTE_D_IVOIRE"/>
            CROATIA"/>
            CUBA"/>
            CYPRUS"/>
            CZECH_REPUBLIC"/>
            DENMARK"/>
            DJIBOUTI"/>
            DOMINICA"/>
            DOMINICAN_REPUBLIC"/>
            ECUADOR"/>
            EGYPT_ARAB_REP"/>
            EL_SALVADOR"/>
            EQUATORIAL_GUINEA"/>
            MALDIVES"/>
            MALI"/>
            MALTA"/>
            MARSHALL_ISLANDS"/>
            MARTINIQUE"/>
            MAURITANIA"/>
            MAURITIUS"/>
            MAYOTTE"/>
            MEXICO"/>
            MICRONESIA_FED_STS"/>
            MOLDOVA"/>
            MONACO"/>
            MONGOLIA"/>
            MONTENEGRO"/>
            MONTSERRAT"/>
            MOROCCO"/>
            MOZAMBIQUE"/>
            MYANMAR"/>
            NAMIBIA"/>
            NAURU"/>
            NEPAL"/>
            NETHERLANDS"/>
            NETHERLANDS_ANTILLES"/>
            NEW_CALEDONIA"/>
            NEW_ZEALAND"/>
            NICARAGUA"/>
            NIGER"/>
            NIGERIA"/>
            NIUE"/>
            NORFOLK_ISLAND"/>
            NORTHERN_MARIANA_ISLANDS"/>
            NORWAY"/>
            OMAN"/>
            PAKISTAN"/>
            PALAU"/>
            PANAMA"/>
            PAPUA_NEW_GUINEA"/>
            PARAGUAY"/>
            PERU"/>
            PHILIPPINES"/>
            PITCAIRN"/>
            POLAND"/>
            PORTUGAL"/>
            PUERTO_RICO"/>
            QATAR"/>
            REUNION"/>
            ROMANIA"/>
            RUSSIAN_FEDERATION"/>
            RWANDA"/>
            SAMOA"/>
            SAN_MARINO"/>
            SAO_TOME_AND_PRINCIPE"/>
            SAUDI_ARABIA"/>
            SENEGAL"/>
            SERBIA"/>
            SEYCHELLES"/>
            SIERRA_LEONE"/>
            SINGAPORE"/>
            SLOVAK_REPUBLIC"/>
            SLOVENIA"/>
            SOLOMON_ISLANDS"/>
            SOMALIA"/>
            SOUTH_AFRICA"/>
            SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS"/>
            SPAIN"/>
            SRI_LANKA"/>
            ST_HELENA"/>
            ST_KITTS_AND_NEVIS"/>
            ST_LUCIA"/>
            ST_PIERRE_AND_MIQUELON"/>
            ST_VINCENT_AND_THE_GRENADINES"/>
            SUDAN"/>
            SURINAME"/>
            SVALBARD_AND_JAN_MAYEN_ISLANDS"/>
            SWAZILAND"/>
            SWEDEN"/>
            SWITZERLAND"/>
            SYRIAN_ARAB_REPUBLIC"/>
            TAIWAN_PROVINCE_OF_CHINA"/>
            TAJIKISTAN"/>
            TANZANIA"/>
            THAILAND"/>
            TIMORLESTE"/>
            TOGO"/>
            TOKELAU"/>
            TONGA"/>
            TRINIDAD_AND_TOBAGO"/>
            TUNISIA"/>
            TURKEY"/>
            TURKMENISTAN"/>
            TURKS_AND_CAICOS_ISLANDS"/>
            TUVALU"/>
            UGANDA"/>
            UKRAINE"/>
            UNITED_ARAB_EMIRATES"/>
            UNITED_KINGDOM"/>
            UNITED_STATES"/>
            UNITED_STATES_MINOR_OUTLYING_ISLANDS"/>
            URUGUAY"/>
            UZBEKISTAN"/>
            VANUATU"/>
            VATICAN_CITY_STATE"/>
            VENEZUELA_RB"/>
            VIETNAM"/>
            VIRGIN_ISLANDS_BRITISH"/>
            VIRGIN_ISLANDS_US"/>
            WALLIS_AND_FUTUNA_ISLANDS"/>
            WEST_BANK_AND_GAZA"/>
            WESTERN_SAHARA"/>
            YEMEN_REP"/>
            ZAMBIA"/>
            ZIMBABWE"/>
            ERITREA"/>
            ESTONIA"/>
            ETHIOPIA"/>
            FAEROE_ISLANDS"/>
            FALKLAND_ISLANDS_MALVINAS"/>
            FIJI"/>
            FINLAND"/>
            FRANCE"/>
            FRANCE_METROPOLITAN"/>
            FRENCH_GUIANA"/>
            FRENCH_POLYNESIA"/>
            FRENCH_SOUTHERN_TERRITORIES"/>
            GABON"/>
            GAMBIA_THE"/>
            GEORGIA"/>
            GERMANY"/>
            GHANA"/>
            GIBRALTAR"/>
            GREECE"/>
            GREENLAND"/>
            GRENADA"/>
            GUADELOUPE"/>
            GUAM"/>
            GUATEMALA"/>
            GUINEA"/>
            GUINEABISSAU"/>
            GUYANA"/>
            HAITI"/>
            HEARD_AND_MCDONALD_ISLANDS"/>
            HONDURAS"/>
            HONG_KONG_CHINA"/>
            HUNGARY"/>
            ICELAND"/>
            INDIA"/>
            INDONESIA"/>
            IRAN_ISLAMIC_REP"/>
            IRAQ"/>
            IRELAND"/>
            ISLE_OF_MAN"/>
            ISRAEL"/>
            ITALY"/>
            JAMAICA"/>
            JAPAN"/>
            JORDAN"/>
            KAZAKHSTAN"/>
            KENYA"/>
            KIRIBATI"/>
            KOREA_DEM_REP"/>
            KOREA_REP"/>
            KUWAIT"/>
            KYRGYZ_REPUBLIC"/>
            LAO_PDR"/>
            LATVIA"/>
            LEBANON"/>
            LESOTHO"/>
            LIBERIA"/>
            LIBYA"/>
            LIECHTENSTEIN"/>
            LITHUANIA"/>
            LUXEMBOURG"/>
            MACAO_CHINA"/>
            MACEDONIA_FYR"/>
            MADAGASCAR"/>
            MALAWI"/>
            MALAYSIA"/>';
        $s = explode(" ", $s);
        $p = "";
        foreach ($s as $one) {
            if ($one) {
//                $p[trim(str_replace('"/>','',$one))]=str_replace('"/>','',$one);
                $p.= '"' . trim(str_replace('"/>', '', $one)) . '",' . PHP_EOL;
            }
        }
        echo $p;
        pd($p);
    }

    function test() {
        $this->load->view('test_pay');
        //   echo $this->CallAPI('https://banquemisr.gateway.mastercard.com/api/page/version/55/pay');
        //$this->postman();
//        $fields = array('order.amount' => '50',
//            'order.currency' => 'EGP',
//            'browserPayment.operation' => 'PAY',
//            'browserPayment.returnUrl' => 'https://www.google.com',
//            'sourceOfFunds.type' => 'ALIPAY'
//        );
//        echo $this->CallAPI2('PUT', 'https://banquemisr.gateway.mastercard.com/api/rest/version/55/merchant/TESTI_SCORE/order/123/transaction/12', $fields);
    }

    function create_api_session() {
        $fields = array('order' => array('amount' => 50, 'currency' => 'EGP', 'id' => '1'),
            'apiOperation' => 'CREATE_CHECKOUT_SESSION',
            'interaction' => array('operation' => 'PURCHASE')
        );
        echo $this->CallAPI2('POST', 'https://banquemisr.gateway.mastercard.com/api/rest/version/55/merchant/TESTI_SCORE/session', $fields);
    }

    function CallAPI($url) {
        $headers = array(
            "cache-control: no-cache",
            "content-type: application/xml",
        );
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $fields = array('interaction.merchant.name' => 'I SCORE',
            'interaction.operation' => 'PURCHASE',
            'merchant' => 'TESTI_SCORE',
            'order.amount' => 50,
            'order.currency' => 'EGP',
            'interaction.cancelUrl' => 'https://www.google.com/',
            'order.description' => 'search payment');
        $postvars = '';
        foreach ($fields as $key => $value) {
            $postvars .= $key . "=" . $value . "&";
        }

        curl_setopt($curl, CURLOPT_POSTFIELDS, rtrim($postvars, '&'));


        curl_setopt($curl, CURLOPT_VERBOSE, true);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);


        $response = curl_exec($curl);

        //$errors = curl_error($curl);
        //$info = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        // echo $errors.'<br/>'.$info;die;

        if ($error = curl_errno($curl)) {
            return curl_error($curl);
        }
        curl_close($curl);
        return $response;
    }

    function CallAPI2($method, $url, $data = false) {
        $curl = curl_init();

        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        $headers = array(
            // "authorization: Basic TWVyY2hhbnQuVEVTVElfU0NPUkU6ZGI2MDMzMmMxNTEyMzlmODcwZDU2NGU0NWU0MjVkNWU=",
            "cache-control: no-cache",
            "content-type: application/xml",
        );

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "Merchant.TESTI_SCORE:db60332c151239f870d564e45e425d5e");
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        //curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }

    function test_s() {
        $this->load->library('Ad_lib');
        print_R($this->ad_lib->check_username_only('iscore_123'));
    }

    function import_users() {
        $users = array(
            array('user' => 'Mostafa', 'clientName' => 'Mostafa', 'subscription' => 1, 'startDateOfSubscription' => '2019-05-23', 'endDateOfSubscription' => '2020-05-23', 'admin' => 0),
            array('user' => 'Dentons2020', 'clientName' => 'Dentons2020', 'subscription' => 1, 'startDateOfSubscription' => '2019-07-04', 'endDateOfSubscription' => '2020-07-04', 'admin' => 0),
            array('user' => 'RiadLawFirm', 'clientName' => 'RiadLawFirm', 'subscription' => 1, 'startDateOfSubscription' => '2019-08-21', 'endDateOfSubscription' => '2020-08-21', 'admin' => 0),
            array('user' => 'matoukBassiouny123', 'clientName' => 'matoukBassiouny123', 'subscription' => 1, 'startDateOfSubscription' => '2019-07-21', 'endDateOfSubscription' => '2020-07-21', 'admin' => 0),
            array('user' => 'loxon', 'clientName' => 'loxon', 'subscription' => 1, 'startDateOfSubscription' => '2020-01-02', 'endDateOfSubscription' => '2021-01-02', 'admin' => 0),
        );

        $payment = array('price' => 500, 'amount_paid' => 500, 'service_type' => 'SearchSubscriptionAnnual');
        foreach ($users as $user) {
            $this->db->insert('users', $user);

            $this->db->order_by('id', 'DESC');
            $inserted_id = $this->db->get('users')->row()->id;

            $payment['payment_date'] = $user['startDateOfSubscription'];
            $payment['user_id'] = $inserted_id;

            $this->db->insert('user_payments', $payment);
        }
    }
function test_prevvvvvv(){
$this->db->query("ALTER TABLE CREDITOR 
MODIFY INSTITUTION_NAME VARCHAR2( 500 ) ");
$params = array(
                        "USERNAME" => 'mlf_mgr',
                        "EMAIL" => 'ahmed.abdlfattah@mlf-finance-eg.com',
                        "EMAIL_LANG" => 1,
                        "ADDRESS" => 'shekh zayed - giza - el shabab',
                        "PASSWORD" => 'a2f07222c5fb3fd5f97431922e42ed44',
                        "CREDITOR_TYPE" => "legal_entity",
                        "CREDITOR_NAME" => '',
                        "NATIONAL_ID" => '',
                        "DATE_OF_BIRTH" => '',
                        "GENDER" => '',
                        "MOBILE_NUM" => '201223881694',
                        "ADDITIONAL_INFO" => "",
                        "PASSPORT_NUM" => "",
                        "ISSUANCE_COUNTRY" => "",
                        "NATIONALITY" => "",
                        "INSTITUTION_NAME" => "?? ?? ?? ??????? ??????? ???????? ???????? ????????",
                        "LEGAL_FORM" => "joint_stock_company",
                        "COMM_REGISTRY_NUM" => "9387",
                        "ESTABLISHMENT_DATE" => "28-10-2019",
                        "OUR_REG" => 1,
                        "TIMESTAMP" => "2020-12-03 11:15:39"                    );

$this->db->insert('CREDITOR',$params);

}
function test_mm(){
echo '<pre>';
//$this->db->order_by('TIMESTAMP','DESC');
//$this->db->like('ACTION','login send msg with error');
//print_r($this->db->get('AUDITS')->result());

//print_r($this->db->get_where('users',array('admin'=>1))->result());

//print_r($this->db->get_where('CREDITOR',array('REG_CODE_CREATED>='=>'2020-10-12 00:00:00','REG_CODE_CREATED<='=>'2020-10-12 23:00:00'))->result());
//$this->db->like('MOBILE_NUM','01009177091');
echo '===================================<br/>';
//$this->db->delete('CREDITOR',array('ID'=>6481));
//$this->db->order_by('ID','DESC');
//print_r($this->db->get_where('CREDITOR')->result());
//$this->db->update('CREDITOR',array('OUR_REG'=>1),array('ID'=>3721));
//$this->db->update('CREDITOR',array('OUR_REG'=>1),array('ID'=>6061));

//$this->db->update('CREDITOR',array('OUR_REG'=>1),array('ID'=>5581));

print_r($this->db->get_where('CREDITOR',array('USERNAME'=>'t_mekkawey'))->result());

print_r($this->db->get_where('CREDITOR',array('USERNAME'=>'y_delta'))->result());
echo '</pre>';
print_r($this->db->get_where('active_directory')->result());

}

function test_mail(){
print_R($this->db->get('email_settings')->result());
}

}
