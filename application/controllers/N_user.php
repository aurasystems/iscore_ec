<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class N_user extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(1);
        //$this->load->model('N_user_model');
        $this->load->model('Email_settings_model');
        //$this->load->library("Ad_lib");
        $this->load->library('Ad_lib');
    }

    function index() {
        $data['title'] = $this->lang->line("lang_login");
        $this->load->view("pages/n_user/login", $data);
    }

    function login() {   // now is not used
        if ($this->session->userdata('new_user_username')) {
            redirect('N_user/change_my_password');
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = $this->lang->line("lang_login");
            $this->load->view("pages/n_user/login", $data);
        } else {
            // $mobile = $this->input->post('mobile_code') . '' . $this->input->post('mobile');
            //$user = $this->Creditor_model->check_login($mobile);
            //$mob_num = '2017827785';
            $error_msg = $this->ad_lib->authenticate($this->input->post('username'), $this->input->post('password'));
            //$error_msg=array('1','201004707161');
            if (is_array($error_msg)) {
                $this->session->unset_userdata('user_display_name');
                $mob_num = $error_msg[1];
                //$user = $this->Creditor_model->check_login($this->input->post('username'), md5($this->input->post('password')));
                $this->session->set_userdata(array('new_user_username' => $this->input->post('username')));
                //save_audit('Creditor logged in with username: ' . $this->input->post('username'));


                redirect('N_user/change_my_password');

                //}
            } else {
                //save_audit('Creditor failed to logged in with username: ' . $this->input->post('username') . ' due to ' . $error_msg);
                $this->session->set_flashdata('msg', $error_msg);
                redirect(current_url());
            }
        }
    }
    public function check_blocked($pass) {
        if (check_password_blacklist(md5($pass))) {
            $this->form_validation->set_message('check_blocked', $this->lang->line('password_in_blocked_list'));
            return false;
        }
        return TRUE;
    }

    function change_my_password() {
//        if (!$this->session->userdata('new_user_username')) {
//            redirect('N_user/login');
//        }
        $this->load->model("Login_model");
        $password_management = $this->Login_model->get_password_management();
        //check minimum length
        $length = "";
        if (!empty($password_management) && $password_management->min_length) {
            $length = "min_length[$password_management->min_length]";
        }
        $this->form_validation->set_rules('username', $this->lang->line('username'), 'xss_clean|trim|required');
        $this->form_validation->set_rules('old_password', $this->lang->line('old_password'), 'xss_clean|trim|required');
        $this->form_validation->set_rules('password', $this->lang->line('lang_password'), "xss_clean|trim|required|matches[password2]|callback_check_blocked|$length");
        $this->form_validation->set_rules('password2', $this->lang->line('confirm_password'), 'xss_clean|trim|required|matches[password]');
        if (!$this->form_validation->run()) {
            $data = array('title' => $this->lang->line("change_password"));
            $data['attributes'] = array('class' => 'form-horizontal', 'role' => 'form');
            $data['label_att'] = array('class' => 'col-sm-4 control-label');
            $this->load->model("admin/Login_model");
            $password_management = $this->Login_model->get_password_management();
            $data['password_management'] = $password_management;
            $this->load->view('pages/n_user/change_password', $data);
        } else {
            //$error_msg = $this->ad_lib->edit_user($user->user, $ad_data);
            $error = $this->ad_lib->check_login($this->input->post('username'), $this->input->post('old_password'));
            if ($error == 1) {
                
                $error_msg = $this->ad_lib->edit_user_password($this->input->post('username'), $this->input->post("password"));
                if ($error_msg == 1) {
                    $this->session->set_flashdata('success', $this->lang->line('password_chanhged_success'));
                    redirect(current_url());
                } else {
                    $this->session->set_flashdata('error', $error_msg);
                    redirect(current_url());
                }
            } else {
                $this->session->set_flashdata('error', $error);
                redirect(current_url());
            }
        }
    }

    function logout() {
        $website_link = $this->config->item('website_link');

        $this->session->sess_destroy();

        // save_audit('logged out');
        redirect(base_url('N_user/login'));
    }

    // resend link ...

    public function reset_password() {
        $data = array('title' => 'Reset Password');
        $this->load->helper('captcha');
        $config = array(
            'img_url' => base_url() . 'uploads/',
            'img_path' => 'uploads/',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
//                'grid' => array(155, 164, 171)
                'grid' => array(199, 87, 87)
            )
        );
        $captcha = create_captcha($config);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        $data['captchaImg'] = base_url() . 'uploads/' . $captcha['filename'];
        $this->load->view("pages/n_user/forget_password", $data);
    }

    public function do_reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user', $this->lang->line("user"), 'trim|required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha');

        if ($this->form_validation->run() == FALSE) {
            $this->reset_password();
        } else {
            $error = $this->ad_lib->check_username_only($this->input->post('user'));
            if (is_array($error)) {
                $ad_email = $error[1];

                $this->Global_model->send_reset_link_n_user($ad_email, $this->input->post('user'));

                redirect('N_user/password_reset_success');
            } else {
                // user not exists
                $this->session->set_flashdata('msg', $error);
                redirect('N_user/reset_password');
            }
        }
    }

    function password_reset_success() {
        $data['title'] = 'Password Recover Success';
        $this->load->view("pages/n_user/password_recover_success", $data);
    }

    function reset_my_password($code) {
        if (!$this->check_csv($code)) {
            $this->session->set_flashdata('msg', $this->lang->line('link_expired'));
            redirect(base_url("N_user/reset_password"));
        }

        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_blocked');
        if (!$this->form_validation->run()) {
            $data = array('title' => 'Reset Password', 'token' => $code);
            $this->load->model("Settings_model");
            $data['password_management'] = $this->Settings_model->get_password_management();
            $this->load->view("pages/n_user/reset_my_password", $data);
        } else {
            $user_name = $this->check_csv($code);
            //verify token

            if ($user_name) {
                
                // $ad_data['password'] = $this->input->post("password");
                $error_msg = $this->ad_lib->edit_user_password($user_name, $this->input->post("password"));
                //$error_msg = $this->ad_lib->edit_user($user->user, $ad_data);
                if ($error_msg == 1) {
                    $this->remove_code_csv($code);
                    $this->session->set_flashdata('msg', $this->lang->line('password_chanhged_success'));
                    redirect('N_user/login');
                } else {
                    $this->session->set_flashdata('msg', $error_msg);
                    redirect(base_url("N_user/reset_password"));
                }
            } else {
                // user not exists
                $this->session->set_flashdata('msg', $this->lang->line('link_expired'));
                redirect(base_url("N_user/reset_password"));
            }
        }
    }

    function test_csv() {
        $list = array(
            array("Peter", "Griffin", "Oslo", "Norway"),
            array("Glenn", "Quagmire", "Oslo", "Norway")
        );

        $file = fopen("n_users_forget_pass.csv", "a");

        foreach ($list as $line) {
            fputcsv($file, $line);
        }

        fclose($file);
    }

    function check_csv($code) {

        if (($handle = fopen("n_users_forget_pass.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                if ($data[0] == $code) {
                    return $data[1];
                }
//                for ($c = 0; $c < $num; $c++) {
//                    echo $data[$c] . "<br />\n";
//                }
            }
            fclose($handle);
        }
        return FALSE;
    }

    function remove_code_csv($code) {
        $file_handle = fopen("n_users_forget_pass.csv", "w+");
        while (($line_of_text = fgetcsv($file_handle, 1000, ",")) !== FALSE) {
            if ($line_of_text[0] != $code) {
                fputcsv($file_handle, $line_of_text);
            }
        }
        fclose($file_handle);
    }

    public function validate_captcha() {
        //return true;
        if ($this->input->post('captcha') != $this->session->userdata['valuecaptchaCode']) {
            $this->form_validation->set_message('validate_captcha', $this->lang->line('captcha_code_is_wrong'));
            return false;
        } else {
            return true;
        }
    }

}
