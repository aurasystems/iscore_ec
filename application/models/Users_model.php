<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Users_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function get_users() {
        $this->db->where("admin !=", 1);
        $query = $this->db->get("users");
        return $query->result();
    }

    function get_user_payments($user_id, $user_creditor) {
        $this->db->where('USER_CREDITOR', $user_creditor);
        $this->db->where("user_id", $user_id);
        $query = $this->db->get("user_payments");
        return $query->result();
    }

    function get_last_password($user_id) {
        $this->db->where("user_id", $user_id);
        $this->db->order_by("id", "desc");
        $query = $this->db->get("user_passwords");
        return $query->row();
    }

    function get_user_id($user) {
        $this->db->order_by('id', 'DESC');
        $user_row = $this->db->get_where('users', array('user' => $user, 'admin' => 0))->row();
        if ($user_row) {
            return $user_row->id;
        }
        return false;
    }

}
