<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Subscription_model extends CI_Model
{
    function check_subscription($user_id){
        $this->db->where("id", $user_id);
        //$this->db->where("subscription", 1);
        $query = $this->db->get("users")->row();
        if ($query) {
            $today = date("Y-m-d");
            if (($today >= $query->startDateOfSubscription && $today <= $query->endDateOfSubscription) || $query->ONE_TIME_SEARCH) {
                return TRUE;
            }
        }
        return FALSE;
    }
}