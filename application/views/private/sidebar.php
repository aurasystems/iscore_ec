<!-- BEGIN SIDEBAR -->
<div class="sidebar">
    <div class="logopanel" style="background-color: #2b2e33;">
        <h1 style="background-color: #fff;">
            <a href="<?= ($this->session->userdata('admin') != 1) ? base_url('Search/PublicationSearch') : base_url('Dashboard') ?>" style="background: none;color: #fff;text-align: center;">
                <img class="img-responsive sys_logo" src="<?= base_url() ?>uploads/content/Untitled-1-1.svg" style="margin: 0 auto;width:40%">
               <!-- <span style="width:100%;word-wrap:break-word;" class="bank_title">ECR</span> -->
            </a>
        </h1>
    </div>
    <div class="sidebar-inner" style="padding-top:55px;">
        <ul class="nav nav-sidebar">
            <?php if ($this->session->userdata("admin")) { ?>
                <!-- For Dashoboard -->
                <li class=" nav<?= is_active(1) ?>"><a href="<?= base_url() ?>Dashboard"><i class="icon-home"></i><span><?= $this->lang->line('dashboard') ?></span></a></li>
                <?php
            } else {
                ?>
                <!-- For Search -->
                <li class=" nav<?= is_active(7) ?>"><a href="<?= base_url() ?>Search/PublicationSearch"><i class="icon-clipboard"></i><span><?= $this->lang->line('PublicationSearch') ?></span></a></li>
            <?php } ?>
            <?php if (!$this->session->userdata("admin")) { ?>
                <!-- For Search History -->
                <li class="nav<?= is_active(8) ?>"><a href="<?= base_url() ?>Search/SearchHistory"><i class="icon-calendar"></i><span><?= $this->lang->line('SearchHistory') ?></span></a></li>
            <?php } ?> 
            <!-- For Admins -->
            <?php if (get_p("admins", "v")) {
                ?>
                <li class="nav<?= is_active(2) ?>"><a href="<?= base_url() ?>Admins"><i class="icon-users"></i><span><?= $this->lang->line('admins') ?></span></a></li>
                <?php
            }
            ?>
            <!-- For Users -->
            <?php if (get_p("users", "v")) {
                ?>
                <li class="nav<?= is_active(3) ?>"><a href="<?= base_url() ?>Users"><i class="icon-users"></i><span><?= $this->lang->line('users') ?></span></a></li>
                <?php
            }
            ?>
            <!-- For Creditors -->
            <?php if (get_p("creditors", "v")) {
                ?>
                <li class="nav<?= is_active(4) ?>"><a href="<?= base_url('Creditor/view_all') ?>"><i class="icon-users"></i><span><?= $this->lang->line('creditors') ?></span></a></li>
                <?php
            }
            ?>
            <!-- For Reports -->
            <?php if (get_p("reports", "v")) {
                ?>
                <li class="nav-parent nav<?= is_active(5) ?>">
                    <a href="#"><i class="fa fa-calculator"></i><span><?= $this->lang->line('reports') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li class="nav<?= is_active(11, 1) ?>"><a href="<?= base_url() ?>Reports/PublicationSearch"><?= $this->lang->line('PublicationSearch_report') ?></a></li>
                        <li class="nav<?= is_active(12, 1) ?>"><a href="<?= base_url() ?>Reports/SearchHistory"><?= $this->lang->line('SearchHistory_report') ?></a></li>
                        <li class="nav<?= is_active(13, 1) ?>"><a href="<?= base_url() ?>Reports/payments"><?= $this->lang->line('payments_report') ?></a></li>
                        <li class="nav<?= is_active(14, 1) ?>"><a href="<?= base_url() ?>Reports/audit_trail"><?= $this->lang->line('audit_trail') ?></a></li>
                    </ul>
                </li>
                <?php
            }
            ?>
            <!-- For Settings -->
            <?php if (get_p("settings", "v")) {
                ?>
                <li class="nav-parent nav<?= is_active(6) ?>">
                    <a href="#"><i class="fa fa-cog"></i><span><?= $this->lang->line('lang_settings') ?></span> <span class="fa arrow"></span></a>
                    <ul class="children collapse">
                        <li class="nav<?= is_active(21, 1) ?>"><a href="<?= base_url() ?>Back_settings"><?= $this->lang->line('lang_back_settings') ?></a></li>
                        <li class="nav<?= is_active(22, 1) ?>"><a href="<?= base_url() ?>Email_settings"><?= $this->lang->line('lang_email_settings') ?></a></li>
                        <li class="nav<?= is_active(23, 1) ?>"><a href="<?= base_url() ?>Settings/password_management"><?= $this->lang->line('password_management') ?></a></li>
                        <li class="nav<?= is_active(28, 1) ?>"><a href="<?= base_url() ?>Settings/password_black_list"><?= $this->lang->line('password_black_list') ?></a></li>
                        <li class="nav<?= is_active(24, 1) ?>"><a href="<?= base_url() ?>Settings/audit_trail"><?= $this->lang->line('audit_trail_settings') ?></a></li>
                        <li class="nav<?= is_active(25, 1) ?>"><a href="<?= base_url() ?>Settings/active_directory"><?= $this->lang->line('active_directory_settings') ?></a></li>
                        <li class="nav<?= is_active(26, 1) ?>"><a href="<?= base_url() ?>Settings/payment"><?= $this->lang->line('payment_settings') ?></a></li>
                        <li class="nav<?= is_active(27, 1) ?>"><a href="<?= base_url() ?>Settings/sms"><?= $this->lang->line('sms_settings') ?></a></li>
                    </ul>
                </li>
                <?php
            }
            ?>
        </ul>
    </div>
</div>
<!-- END SIDEBAR -->