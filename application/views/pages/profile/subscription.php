<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- END PAGE CSS -->
    </head>
    <style>
        .ui-dialog-titlebar-close {
            display: none;
        }
    </style>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $this->lang->line("subscription") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php if (!empty($subscription) && $subscription->subscription == 1 && date("Y-m-d H:i:s") <= $subscription->endDateOfSubscription) { ?>
                                        <h4>
                                            <?= $this->lang->line("you_are_already_subscribed") ?>
                                        </h4>
                                        <p><?= $this->lang->line("subscription_expires_on") . " " . to_date($subscription->endDateOfSubscription, "Y-m-d") ?></p>
                                        <?php
                                    } else if (!empty($subscription) && $subscription->ONE_TIME_SEARCH) {
                                        ?>
                                        <h4>
                                            <?= $this->lang->line("you_are_subscribed_one_time") ?>
                                        </h4>
                                    <?php } else { ?>
                                        <h4>
                                            <?= $this->lang->line("not_subscribed") ?>
                                        </h4>
                                        <a class="btn btn-success" href="<?= base_url() ?>Subscription/choose_subscription"><?= $this->lang->line("renew_subscription") ?></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>

            <div id="dialog" title="<?= $this->lang->line("subscription") ?>">
                <p><?= $this->lang->line("subscribe_annually") ?></p>
            </div>

        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {
<?php
if (!$subscribed) {
    ?>
                    $("#search").click(function (e) {
                        e.preventDefault();
                        open_dialogue();
                    });

                    function open_dialogue() {
                        $("#dialog").dialog({
                            modal: true,
                            buttons: {
                                Yes: function () {
                                    $("#feeType").val("SUBSCRIPTION");
                                    $("#form").submit();
                                },
                                No: function () {
                                    $("#form").submit();
                                }
                            }
                        });
                    }
    <?php
}
?>


            });
        </script>
    </body>
</html>