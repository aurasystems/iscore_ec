<?php
//$this->load->helper('cookie');
//if (!get_cookie('allow_redirect')) {
//    $this->session->set_flashdata('msg', 'Not Allowed');
//    redirect('creditor/login');
//}
?>

<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | Creditors</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.png">
    </head>
    <body style="margin:0px">
        <iframe src="<?= $this->config->item('creditor_sub_redirect') ?>" height="100%" width='100%' style="border:none"></iframe>
    </body>
</html>
