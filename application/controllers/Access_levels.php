<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class Access_levels extends CI_Controller {
    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model('Access_levels_model');
    }
    function index() {
        check_p("access_levels", "v");
        $functions= get_level_functions();
        $access_levels=  $this->Global_model->get_all("access_levels");
        $data['title'] = $this->lang->line('access_levels');
        $data['access_levels'] = $access_levels;
        $data['functions'] = $functions;
        $this->load->view("pages/access_levels/index", $data);
    }
    function add() {
        check_p("access_levels", "c");
        $this->form_validation->set_rules("name",  $this->lang->line("level_name"),"trim|required");
        if(!$this->form_validation->run()){
            $data['title'] = $this->lang->line('add_level');
            $this->load->view('pages/access_levels/add', $data);
        }
        else{
            $data=array(
                "name"=>  $this->input->post("name"),
            );
            $this->Global_model->global_insert("access_levels",$data);
            $level_id=$this->Access_levels_model->get_last_id($this->input->post("name"));
            $this->Global_model->global_insert("level_functions",array("level_id"=>$level_id));
            $this->session->set_flashdata("success",  $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Access_levels"));
        }
    }
    function edit($id=NULL) {
        check_param($id);
        check_p("access_levels", "u");
        $this->form_validation->set_rules("name",  $this->lang->line("level_name"),"trim|required");
        $level= $this->Global_model->get_data_by_id("access_levels",$id);
        check_param($level,"resource");
        if(!$this->form_validation->run()){
            $data['title'] = $this->lang->line('edit_level');
            $data['level']= $level;
            $this->load->view('pages/access_levels/edit', $data);
        }
        else{
            $data=array(
                "name"=>  $this->input->post("name")
            );
            $this->Global_model->global_update("access_levels",$id,$data);
            $this->session->set_flashdata("success",  $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Access_levels"));
        }
    }
    function delete($id=NULL){
        check_param($id);
        check_p("access_levels", "d");
//        $exists=$this->Access_levels_model->check_level_users($id);
//        if($exists){
//            $this->session->set_flashdata("error",  $this->lang->line("users_assigned_level"));
//            redirect(base_url("Access_levels"));
//        }
        $this->Access_levels_model->delete_functions($id);
        $this->Global_model->global_delete("access_levels",$id);
        $this->session->set_flashdata("success",  $this->lang->line("data_deleted_successfully"));
        redirect(base_url("Access_levels"));
    }
    function permissions($level_id=NULL){
        check_param($level_id);
        check_p("access_levels", "u");
        $perms= $this->Access_levels_model->get_level_permissions($level_id);
        check_param($perms,"resource");
        $permissions=[];
        $functions=  get_level_functions();
        foreach ($functions as $function){
            $permissions[$function]['v']=substr($perms->$function, 0, 1);
            $permissions[$function]['c']=substr($perms->$function, 1, 1);
            $permissions[$function]['u']=substr($perms->$function, 2, 1);
            $permissions[$function]['d']=substr($perms->$function, 3, 1);
        }
        if($_SERVER["REQUEST_METHOD"]=="POST"){
            $updates=[];
            foreach ($permissions as $module=>$arr){
                $updates[$module]="";
                foreach ($arr as $p=>$value){
                    if($this->input->post($module."_".$p)){
                        $updates[$module].="1";
                    }
                    else{
                        $updates[$module].="0";
                    }
                }
            }
            $this->Access_levels_model->update_permissions($level_id,$updates);
            $this->session->set_flashdata("success",  $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Access_levels"));
        }
        else{
            $data['title']=  $this->lang->line("permissions");
            $data['level_permissions']=$perms;
            $data['level_name']=$perms->level_name;
            $data['permissions']=$permissions;
            $this->load->view('pages/access_levels/permissions', $data);
        }
    }
}