<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="header">
                        <div class="breadcrumb-wrapper editWrapper">
                            <ol class="breadcrumb">
                                <li><a href="<?= base_url() ?>Dashboard"><?= $this->lang->line('lang_dashboard') ?></a></li>
                                <li class="active"><?= $this->lang->line('lang_back_settings') ?></li>
                            </ol>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($status == "success") { ?>
                                <div class="alert alert-block alert-success fade in">
                                    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">×</a>
                                    <p></p><h4><i class="fa fa-check"></i> <?= $this->lang->line('lang_success') ?></h4> <?= $this->lang->line('atumsl_alertsettingssuccess') ?><p></p>
                                </div>
                            <?php } ?>

                            <div class="panel panel-default">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><i class="fa fa-gear"></i> <?= $this->lang->line("lang_back_settings") ?></h2>
                                </div>

                                <div class="panel-body">
                                    <div class="box-body big col-md-6">
                                        <?php
                                        echo form_open_multipart('Back_settings/update', $attributes);
                                        ?>
<!--                                        <div class="form-group">
                                            <?php //echo form_label($this->lang->line('lang_system_logo') . ':', 'lang_system_logo', $label_att); ?>
                                            <div class="col-sm-7 controls">
                                                <?php //if ($back_settings) { ?>
                                                    <img src="<?//= base_url() ?>uploads/settings/back/<?//= $back_settings->system_logo ?>" alt="<?= $back_settings->system_name ?>" class="img-responsive thumbnail" width="100" >
                                                <?php //} ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php //echo form_label($this->lang->line('lang_system_name') . ':', 'lang_system_name', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
//                                                if ($back_settings) {
//                                                    $system_name = $back_settings->system_name;
//                                                } else {
//                                                    $system_name = '';
//                                                }
                                                ?>
                                                <?php //echo form_input(array('id' => 'system_name', 'name' => 'system_name', 'class' => 'form-control', 'placeholder' => 'System Name', 'value' => $system_name)); ?>
                                                <?php //echo form_error('system_name'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php //echo form_label($this->lang->line('lang_system_logo') . ':', 'lang_system_logo', $label_att); ?> 
                                            <div class="col-sm-7">
                                                <input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
                                                <?php //echo form_error('userfile'); ?>
                                            </div>
                                        </div>-->
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('creditor_currency') . ':', 'creditor_currency', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($back_settings) {
                                                    $creditor_currency = $back_settings->CREDITOR_CURRENCY;
                                                } else {
                                                    $creditor_currency = '';
                                                }
                                                $currencies_list = array(
                                                    'ALL' => 'Albania Lek',
                                                    'AFN' => 'Afghanistan Afghani',
                                                    'ARS' => 'Argentina Peso',
                                                    'AWG' => 'Aruba Guilder',
                                                    'AUD' => 'Australia Dollar',
                                                    'AZN' => 'Azerbaijan New Manat',
                                                    'BSD' => 'Bahamas Dollar',
                                                    'BBD' => 'Barbados Dollar',
                                                    'BDT' => 'Bangladeshi taka',
                                                    'BYR' => 'Belarus Ruble',
                                                    'BZD' => 'Belize Dollar',
                                                    'BMD' => 'Bermuda Dollar',
                                                    'BOB' => 'Bolivia Boliviano',
                                                    'BAM' => 'Bosnia and Herzegovina Convertible Marka',
                                                    'BWP' => 'Botswana Pula',
                                                    'BGN' => 'Bulgaria Lev',
                                                    'BRL' => 'Brazil Real',
                                                    'BND' => 'Brunei Darussalam Dollar',
                                                    'KHR' => 'Cambodia Riel',
                                                    'CAD' => 'Canada Dollar',
                                                    'KYD' => 'Cayman Islands Dollar',
                                                    'CLP' => 'Chile Peso',
                                                    'CNY' => 'China Yuan Renminbi',
                                                    'COP' => 'Colombia Peso',
                                                    'CRC' => 'Costa Rica Colon',
                                                    'HRK' => 'Croatia Kuna',
                                                    'CUP' => 'Cuba Peso',
                                                    'CZK' => 'Czech Republic Koruna',
                                                    'DKK' => 'Denmark Krone',
                                                    'DOP' => 'Dominican Republic Peso',
                                                    'XCD' => 'East Caribbean Dollar',
                                                    'EGP' => 'Egypt Pound',
                                                    'SVC' => 'El Salvador Colon',
                                                    'EEK' => 'Estonia Kroon',
                                                    'EUR' => 'Euro Member Countries',
                                                    'FKP' => 'Falkland Islands (Malvinas) Pound',
                                                    'FJD' => 'Fiji Dollar',
                                                    'GHC' => 'Ghana Cedis',
                                                    'GIP' => 'Gibraltar Pound',
                                                    'GTQ' => 'Guatemala Quetzal',
                                                    'GGP' => 'Guernsey Pound',
                                                    'GYD' => 'Guyana Dollar',
                                                    'HNL' => 'Honduras Lempira',
                                                    'HKD' => 'Hong Kong Dollar',
                                                    'HUF' => 'Hungary Forint',
                                                    'ISK' => 'Iceland Krona',
                                                    'INR' => 'India Rupee',
                                                    'IDR' => 'Indonesia Rupiah',
                                                    'IRR' => 'Iran Rial',
                                                    'IMP' => 'Isle of Man Pound',
                                                    'ILS' => 'Israel Shekel',
                                                    'JMD' => 'Jamaica Dollar',
                                                    'JPY' => 'Japan Yen',
                                                    'JEP' => 'Jersey Pound',
                                                    'KZT' => 'Kazakhstan Tenge',
                                                    'KPW' => 'Korea (North) Won',
                                                    'KRW' => 'Korea (South) Won',
                                                    'KGS' => 'Kyrgyzstan Som',
                                                    'LAK' => 'Laos Kip',
                                                    'LVL' => 'Latvia Lat',
                                                    'LBP' => 'Lebanon Pound',
                                                    'LRD' => 'Liberia Dollar',
                                                    'LTL' => 'Lithuania Litas',
                                                    'MKD' => 'Macedonia Denar',
                                                    'MYR' => 'Malaysia Ringgit',
                                                    'MUR' => 'Mauritius Rupee',
                                                    'MXN' => 'Mexico Peso',
                                                    'MNT' => 'Mongolia Tughrik',
                                                    'MZN' => 'Mozambique Metical',
                                                    'NAD' => 'Namibia Dollar',
                                                    'NPR' => 'Nepal Rupee',
                                                    'ANG' => 'Netherlands Antilles Guilder',
                                                    'NZD' => 'New Zealand Dollar',
                                                    'NIO' => 'Nicaragua Cordoba',
                                                    'NGN' => 'Nigeria Naira',
                                                    'NOK' => 'Norway Krone',
                                                    'OMR' => 'Oman Rial',
                                                    'PKR' => 'Pakistan Rupee',
                                                    'PAB' => 'Panama Balboa',
                                                    'PYG' => 'Paraguay Guarani',
                                                    'PEN' => 'Peru Nuevo Sol',
                                                    'PHP' => 'Philippines Peso',
                                                    'PLN' => 'Poland Zloty',
                                                    'QAR' => 'Qatar Riyal',
                                                    'RON' => 'Romania New Leu',
                                                    'RUB' => 'Russia Ruble',
                                                    'SHP' => 'Saint Helena Pound',
                                                    'SAR' => 'Saudi Arabia Riyal',
                                                    'RSD' => 'Serbia Dinar',
                                                    'SCR' => 'Seychelles Rupee',
                                                    'SGD' => 'Singapore Dollar',
                                                    'SBD' => 'Solomon Islands Dollar',
                                                    'SOS' => 'Somalia Shilling',
                                                    'ZAR' => 'South Africa Rand',
                                                    'LKR' => 'Sri Lanka Rupee',
                                                    'SEK' => 'Sweden Krona',
                                                    'CHF' => 'Switzerland Franc',
                                                    'SRD' => 'Suriname Dollar',
                                                    'SYP' => 'Syria Pound',
                                                    'TWD' => 'Taiwan New Dollar',
                                                    'THB' => 'Thailand Baht',
                                                    'TTD' => 'Trinidad and Tobago Dollar',
                                                    'TRY' => 'Turkey Lira',
                                                    'TRL' => 'Turkey Lira',
                                                    'TVD' => 'Tuvalu Dollar',
                                                    'UAH' => 'Ukraine Hryvna',
                                                    'GBP' => 'United Kingdom Pound',
                                                    'USD' => 'United States Dollar',
                                                    'UYU' => 'Uruguay Peso',
                                                    'UZS' => 'Uzbekistan Som',
                                                    'VEF' => 'Venezuela Bolivar',
                                                    'VND' => 'Viet Nam Dong',
                                                    'YER' => 'Yemen Rial',
                                                    'ZWD' => 'Zimbabwe Dollar'
                                                );
                                                ?>
                                                <select name="creditor_currency" id="creditor_currency" data-search="true" class="form-control">
                                                    <option value=""><?= $this->lang->line('select_currency ') ?></option>
                                                    <?php
                                                    foreach ($currencies_list as $c => $n):
                                                        echo "<option " . ($c == $creditor_currency ? 'selected' : '') .
                                                        " value='" . $c . "'>" . $n . "</option>";
                                                    endforeach;
                                                    ?>
                                                </select>
                                                <?php echo form_error('creditor_currency'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('creditor_amount') . ':', 'creditor_amount', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($back_settings) {
                                                    $creditor_amount = $back_settings->CREDITOR_AMOUNT;
                                                } else {
                                                    $creditor_amount = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'creditor_amount', 'name' => 'creditor_amount', 'class' => 'form-control', 'placeholder' => $this->lang->line('creditor_amount'), 'value' => $creditor_amount)); ?>
                                                <?php echo form_error('creditor_amount'); ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <?php echo form_label($this->lang->line('sms_code_expiry') . ':', 'sms_code_expiry', $label_att); ?>
                                            <div class="col-sm-7">
                                                <?php
                                                if ($back_settings) {
                                                    $sms_code_expiry = $back_settings->SMS_CODE_EXPIRY;
                                                } else {
                                                    $sms_code_expiry = '';
                                                }
                                                ?>
                                                <?php echo form_input(array('id' => 'sms_code_expiry', 'name' => 'sms_code_expiry', 'class' => 'form-control', 'placeholder' => $this->lang->line('sms_code_expiry'), 'value' => $sms_code_expiry)); ?>
                                                <?php echo form_error('sms_code_expiry'); ?>
                                            </div>
                                        </div>
                                        <?php echo form_submit(array('id' => 'submit', 'value' => $this->lang->line('lang_save'), 'class' => 'btn btn-primary')); ?>
                                        <span class="btn btn-default btn-mini"><a style="text-decoration: none;" href="<?= base_url() ?>Dashboard"><?= $this->lang->line('lang_cancel') ?></a></span>
                                            <?php echo form_close(); ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
    </body>
</html>