<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include('VPCPaymentConnection.php');

class Online_payment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
    }
    
    function migs($amount, $currency, $subscription = NULL, $start_date = NULL) {
///////////////////////////////////
        $migs_settings = $this->db->get('payment_settings')->row();
        if (!empty($migs_settings)) {
            $amount = ceil($amount);
            $accessCode = $migs_settings->access_code; //value from migs payment gateway
            $merchantId = $migs_settings->merchant_id; //value from migs payment gateway
            $_POST = array(
                "vpc_AccessCode" => $accessCode,
                "vpc_Amount" => $amount * 100,
                "vpc_Command" => 'pay',
                "vpc_Locale" => 'en',
                "vpc_MerchTxnRef" => "ODID" . rand(),
                "vpc_Merchant" => $merchantId,
                "vpc_OrderInfo" => rand(),
                "vpc_ReturnURL" => base_url() . "Online_payment/migs_callback/" . $subscription . "/" . $start_date,
                "vpc_Version" => '1',
                "vpc_Currency" => $currency,
                "vpc_MrchTrxRef" => rand(), //$this->session->userdata('user'),
                "Title" => 'IScore ECR',
                "SubButL" => 'Pay Now!'
            );

////////////////////////////////
            $conn = new VPCPaymentConnection();


// This is secret for encoding the SHA256 hash
// This secret will vary from merchant to merchant

            $secureSecret = $migs_settings->secure_secret;

// Set the Secure Hash Secret used by the VPC connection object
            $conn->setSecureSecret($secureSecret);


// *******************************************
// START OF MAIN PROGRAM
// *******************************************
// Sort the POST data - it's important to get the ordering right
            ksort($_POST);

// add the start of the vpcURL querystring parameters
//$vpcURL = $_POST["virtualPaymentClientURL"];
            $vpcURL = "https://banquemisr.gateway.mastercard.com/api/rest/version/55";

// This is the title for display
            $title = $_POST["Title"];


// Remove the Virtual Payment Client URL from the parameter hash as we 
// do not want to send these fields to the Virtual Payment Client.
            unset($_POST["virtualPaymentClientURL"]);
            unset($_POST["SubButL"]);
            unset($_POST["Title"]);

// Add VPC post data to the Digital Order
            foreach ($_POST as $key => $value) {
                if (strlen($value) > 0) {
                    $conn->addDigitalOrderField($key, $value);
                }
            }

// Add original order HTML so that another transaction can be attempted.
//$conn->addDigitalOrderField("AgainLink", $againLink);
// Obtain a one-way hash of the Digital Order data and add this to the Digital Order
            $secureHash = $conn->hashAllFields();
            $conn->addDigitalOrderField("Title", $title);
            $conn->addDigitalOrderField("vpc_SecureHash", $secureHash);
            $conn->addDigitalOrderField("vpc_SecureHashType", "SHA256");

// Obtain the redirection URL and redirect the web browser
            $vpcURL = $conn->getDigitalOrder($vpcURL);

            header("Location: " . $vpcURL);
//echo "<a href=$vpcURL>$vpcURL</a>";
        } else {
            $this->session->set_flashdata('error', 'Missing Payment Settings');
            redirect(base_url('Dashboard'));
        }
    }

    function migs_old($amount, $currency, $subscription = NULL, $start_date = NULL) {
///////////////////////////////////
        $migs_settings = $this->db->get('payment_settings')->row();
        if (!empty($migs_settings)) {
            $amount = ceil($amount);
            $accessCode = $migs_settings->access_code; //value from migs payment gateway
            $merchantId = $migs_settings->merchant_id; //value from migs payment gateway
            $_POST = array(
                "vpc_AccessCode" => $accessCode,
                "vpc_Amount" => $amount * 100,
                "vpc_Command" => 'pay',
                "vpc_Locale" => 'en',
                "vpc_MerchTxnRef" => "ODID" . rand(),
                "vpc_Merchant" => $merchantId,
                "vpc_OrderInfo" => rand(),
                "vpc_ReturnURL" => base_url() . "Online_payment/migs_callback/" . $subscription . "/" . $start_date,
                "vpc_Version" => '1',
                "vpc_Currency" => $currency,
                "vpc_MrchTrxRef" => rand(), //$this->session->userdata('user'),
                "Title" => 'IScore ECR',
                "SubButL" => 'Pay Now!'
            );

////////////////////////////////
            $conn = new VPCPaymentConnection();


// This is secret for encoding the SHA256 hash
// This secret will vary from merchant to merchant

            $secureSecret = $migs_settings->secure_secret;

// Set the Secure Hash Secret used by the VPC connection object
            $conn->setSecureSecret($secureSecret);


// *******************************************
// START OF MAIN PROGRAM
// *******************************************
// Sort the POST data - it's important to get the ordering right
            ksort($_POST);

// add the start of the vpcURL querystring parameters
//$vpcURL = $_POST["virtualPaymentClientURL"];
            $vpcURL = "https://migs.mastercard.com.au/vpcpay";

// This is the title for display
            $title = $_POST["Title"];


// Remove the Virtual Payment Client URL from the parameter hash as we 
// do not want to send these fields to the Virtual Payment Client.
            unset($_POST["virtualPaymentClientURL"]);
            unset($_POST["SubButL"]);
            unset($_POST["Title"]);

// Add VPC post data to the Digital Order
            foreach ($_POST as $key => $value) {
                if (strlen($value) > 0) {
                    $conn->addDigitalOrderField($key, $value);
                }
            }

// Add original order HTML so that another transaction can be attempted.
//$conn->addDigitalOrderField("AgainLink", $againLink);
// Obtain a one-way hash of the Digital Order data and add this to the Digital Order
            $secureHash = $conn->hashAllFields();
            $conn->addDigitalOrderField("Title", $title);
            $conn->addDigitalOrderField("vpc_SecureHash", $secureHash);
            $conn->addDigitalOrderField("vpc_SecureHashType", "SHA256");

// Obtain the redirection URL and redirect the web browser
            $vpcURL = $conn->getDigitalOrder($vpcURL);

            header("Location: " . $vpcURL);
//echo "<a href=$vpcURL>$vpcURL</a>";
        } else {
            $this->session->set_flashdata('error', 'Missing Payment Settings');
            redirect(base_url('Dashboard'));
        }
    }

    function migs_callback($subscription, $start_date = NULL) {
        $this->load->library("Api_lib");
        $responseCode = $this->input->get("vpc_TxnResponseCode");
        $amount = $this->input->get("vpc_Amount");
        $card_type = $this->input->get("vpc_Card");
        $currency = $this->input->get("vpc_Currency");
        $msg = $this->getResultDescription($responseCode);

        $user_id = $this->session->userdata('user_id');
        $user_row = $this->db->get_where('users', array('id' => $user_id))->row();
        $pay_type = $subscription == 'SUBSCRIPTION' ? 'a subscription' : 'a one time search';
        save_audit($msg . ' for Search User payment with amount of ' . ($amount / 100) . ' for ' . $pay_type . ' to username: ' . $user_row->user);
        if ($responseCode == '0') {  // || $responseCode === 'B'  || $responseCode == '2'
            // BEGIN INSERT PAYMENT
            $amount = $amount / 100;


            if ($subscription == 'SUBSCRIPTION') {


                $data = array(
                    "user" => $user_row->user,
                    "clientName" => $user_row->clientName,
                    //"NameOfTheUser" => $this->input->post("clientName"),
                    "address" => $user_row->address,
                    "phoneNumber" => $user_row->phoneNumber,
                    "email" => $user_row->email,
                    "nameOfTheUser" => "",
                    "subscription" => 1,
                    "startDateOfSubscription" => api_date(date("Y-m-d H:i:s"))
                );

                $response = $this->api_lib->CreateOrUpdateUser($data);
                if (!empty($response['faultstring'])) {
                    //set flash
                    $this->session->set_flashdata("error", $response['faultstring']);
                    redirect(base_url());
                }
                if ($start_date == NULL) {
                    $start_date = date('Y-m-d');
                }
                $data = array(
                    "subscription" => 1,
                    "startDateOfSubscription" => to_date($response['response']['startDateOfSubscription']),
                    "endDateOfSubscription" => to_date($response['response']['endDateOfSubscription'])
                );
            } else {  // one time search 
                $data = array(
                    "ONE_TIME_SEARCH" => 1,
                );
            }
            $this->Global_model->global_update("users", $user_id, $data);
            // SEND EMAIL
            if (!empty($amount)) {
                $subscription_type = 'search';
                if ($subscription == 'SUBSCRIPTION') {
                    $subscription_type = 'subscription';
                }
                $this->db->insert('user_payments', array('user_id' => intval($user_id), 'price' => $amount,
                    'amount_paid' => $amount, 'payment_method' => $card_type, 'payment_date' => date('Y-m-d'),
                    'service_type' => $subscription_type));  //
                // Get all data


                $msg = "Dear " . $this->session->userdata('user_display_name') . ",<br><br><br>"
                        . "Kindly note that your payment with the amount of (EGP $amount) has been successfully paid.<br>"
                        . "<ul>"
                        . "<li>Phone : " . $this->session->userdata('phone_number') . "</li>"
                        . "<li>Fee Type : " . $subscription_type . "</li>"
                        . "<li>Amount :  $amount  ($currency) </li>"
                        . "<li>Payment Date : " . date('Y-m-d') . "</li>";
                if ($subscription == 'SUBSCRIPTION') {
                    $msg .= "<li>Subscription Start Date: " . $start_date . "</li>"
                            . "<li>Subscription End Date : " . date('Y-m-d', strtotime('+1 years', strtotime($start_date))) . "</li>";
                }

                $msg .= "</ul> <br><br><br>"
                        //. "<br/>You can now use our search  "
                        . "Best regards";
                $this->Global_model->send_mail($this->session->userdata('user_email'), $msg, 'IScore ECR: (Online Payment Successful)');
            }
            // END SEND EMAIL
            $this->session->set_flashdata('success', $this->lang->line("payment_successful"));
            redirect(base_url());
//            END INSERT PAYMENT
        } else {
            $this->session->set_flashdata('error', $msg);
            redirect(base_url('Dashboard'));
        }
    }

    function getResultDescription($responseCode) {

        switch ($responseCode) {
            case "0" : $result = "Transaction Successful";
                break;
            case "?" : $result = "Transaction status is unknown";
                break;
            case "E" : $result = "Referred";
                break;
            case "1" : $result = "Transaction Declined";
                break;
            case "2" : $result = "Bank Declined Transaction";
                break;
            case "3" : $result = "No Reply from Bank";
                break;
            case "4" : $result = "Expired Card";
                break;
            case "5" : $result = "Insufficient funds";
                break;
            case "6" : $result = "Error Communicating with Bank";
                break;
            case "7" : $result = "Payment Server detected an error";
                break;
            case "8" : $result = "Transaction Type Not Supported";
                break;
            case "9" : $result = "Bank declined transaction (Do not contact Bank)";
                break;
            case "A" : $result = "Transaction Aborted";
                break;
            case "B" : $result = "Fraud Risk Blocked";
                break;
            case "C" : $result = "Transaction Cancelled";
                break;
            case "D" : $result = "Deferred transaction has been received and is awaiting processing";
                break;
            case "E" : $result = "Transaction Declined - Refer to card issuer";
                break;
            case "F" : $result = "3D Secure Authentication failed";
                break;
            case "I" : $result = "Card Security Code verification failed";
                break;
            case "L" : $result = "Shopping Transaction Locked (Please try the transaction again later)";
                break;
            case "M" : $result = "Transaction Submitted (No response from acquirer)";
                break;
            case "N" : $result = "Cardholder is not enrolled in Authentication scheme";
                break;
            case "P" : $result = "Transaction has been received by the Payment Adaptor and is being processed";
                break;
            case "R" : $result = "Transaction was not processed - Reached limit of retry attempts allowed";
                break;
            case "S" : $result = "Duplicate SessionID (Amex Only)";
                break;
            case "T" : $result = "Address Verification Failed";
                break;
            case "U" : $result = "Card Security Code Failed";
                break;
            case "V" : $result = "Address Verification and Card Security Code Failed";
                break;
            default : $result = "Unable to be determined";
        }
        return $result;
    }

}
