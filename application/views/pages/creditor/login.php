<!DOCTYPE html>
<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <meta charset="utf-8">
        <title><?= MY_APP_NAME ?> | <?= $title ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta content="" name="description" />
        <meta content="themes-lab" name="author" />
        <link rel="shortcut icon" href="<?= base_url() ?>assets/img/favicon.png">
        <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/ui.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/css/custom.css" rel="stylesheet">
        <link href="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.css" rel="stylesheet">
        <style>
            .edit_login_btn {
                width: 30% !important;
                margin: 0 auto;
                margin-top: 5%;
            }
        </style>
    </head>
    <body class="account <?= $this->lang->line('system_dir') ?>" data-page="login">
        <?= login_change_lang() ?>
        <!-- BEGIN LOGIN BOX -->
        <div class="container" id="login-block">
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3">
                    <div class="account-wall">
                        <div class="text-center" style="margin-bottom: 20px">
                            <img src="<?= base_url() ?>uploads/content/Untitled-1-1.png" style="width:40%" />
                        </div>
                        <br/>
                        <!--<i class="user-img icons-faces-users-03"></i>-->
                        <div class="row editErrorMsg">
                            <?php //if ($this->session->flashdata('msg')) { ?>
                            <?php
                            //echo $this->session->flashdata('msg');
                            //}
                            ?>
                            <?= flash_msg() ?>
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                        </div>
                        <?php //echo form_open('Creditor/login');    ?>
                        <!--<div class="row">
                            <div class="col-sm-6">
                                <div class="append-icon">
                                    <select name="mobile_code" class="form-control foreign_mobile_code" data-search="true">
                        <?php
                        /* $codes = mobile_countries_code();
                          foreach ($codes as $key => $code) {
                          echo '<option value="' . $code['code'] . '">' . $code['name'] . '(+' . $code['code'] . ')' . '</option>';
                          } */
                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="append-icon">
                                    <input type="text" id="mobile" name="mobile" placeholder="1xxxxxxxxx" class="form-control form-white" required>
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                        </div>
                        <?php //echo form_submit(array('class'=>'btn btn-lg btn-dark btn-block ladda-button edit_login_btn','data-style' => 'expand-left','value' => $this->lang->line('lang_sign_in')));    ?>
                        <div class="clearfix">
                            <p class="pull-left m-t-20"><a href="<?= base_url() ?>User/reset_password"><?= $this->lang->line('lang_forget_password') ?></a></p>
                        </div> -->
                        <?php //echo form_close();    ?>

                        <?php echo form_open('Creditor/login'); ?>
                        <div class="append-icon">
                            <?php echo form_input(array('type' => 'text', 'id' => 'username', 'name' => 'username', 'class' => 'form-control form-white email', 'placeholder' => $this->lang->line("username"), 'required' => 'required')); ?>
                            <i class="icon-user"></i>
                            <?php echo form_error('username'); ?>
                        </div>
                        <div class="append-icon m-b-20">
                            <?php echo form_input(array('type' => 'password', 'name' => 'password', 'class' => 'form-control form-white password', 'placeholder' => $this->lang->line('password'), "autocomplete" => "off", 'required' => 'required')); ?>
                            <i class="icon-lock"></i>
                            <?php echo form_error('password'); ?>
                        </div>


                        <?php echo form_submit(array('class' => 'btn btn-lg btn-danger btn-block ladda-button', 'data-style' => 'expand-left', 'value' => $this->lang->line('lang_sign_in'), 'style' => 'background-color: #1D4B8E;text-align: center')); ?>

                        <div class="clearfix">
                            <p class="pull-left"><a href="<?= base_url() ?>creditor/register" ><?= $this->lang->line('lang_sign_up') ?></a></p>
                        </div>
                        <div class="clearfix">
                            <p class="pull-left"><a href="<?= base_url() ?>creditor/reset_password"><?= $this->lang->line('lang_forget_password') ?></a></p>
                        </div>

                        <?php echo form_close(); ?>

                    </div>
                </div>
            </div>
        </div>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-1.11.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/gsap/main-gsap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/icheck/icheck.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/backstretch/backstretch.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-loading/lada.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
        <script src="<?= base_url() ?>assets/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
        <script src="<?= base_url() ?>assets/js/plugins.js"></script>
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script> <!-- >Bootstrap Date Picker in Spanish (can be removed if not use) -->
        <?php $this->load->view('private/scripts/login_js'); ?>
    </body>
</html>