<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
        <!-- BEGIN PAGE CSS -->
        <link href="<?= base_url() ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- END PAGE CSS -->
    </head>
    <style>
        .ui-dialog-titlebar-close {
            display: none;
        }
        .rtl .dropdown-menu{
            right: auto;
        }
    </style>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <?= flash_success() ?>
                    <?= flash_error() ?>
                    <!--/////////////////-->
                    <div class="panel panel-default no-bd">
                        <div class="panel-header bg-primary">
                            <h2 class="panel-title"><?= $this->lang->line("payments_report") ?></h2>
                        </div>
                        <div class="panel-body bg-white">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <form id="form" method="GET" action="" class="form-horizontal" role="form">
                                        <?= form_group_select("user_id", $users, "user", $this->input->get("user_id")) ?>
                                        <?= form_group("date_from", $this->input->get("date_from"), "b-datepicker") ?>
                                        <?= form_group("date_to", $this->input->get("date_to"), "b-datepicker") ?>
                                        <div class="col-sm-12 text-center">
                                            <input id="search" type="submit" class="btn btn-primary" value="<?= $this->lang->line("filter") ?>">
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <table class="table table-hover">
                                        <thead>
                                        <th><?= $this->lang->line("payment_date") ?></th>
                                        <th><?= $this->lang->line("user") ?></th>
                                        <th><?= $this->lang->line("amount_paid") ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($payments as $one) { ?>
                                                <tr>
                                                    <td><?= $one->payment_date ?></td>
                                                    <td><?= $one->user ?></td>
                                                    <td><?= $one->amount_paid ?></td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>



                        </div>
                    </div>
                    <!--//////////////////-->
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>

            <div id="dialog" title="<?= $this->lang->line("subscription") ?>">
                <p><?= $this->lang->line("subscribe_annually") ?></p>
            </div>

        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script> <!-- Time Picker -->
        <script src="<?= base_url() ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> <!-- >Bootstrap Date Picker -->
        <script>
            $(document).ready(function () {

            });
        </script>
    </body>
</html>