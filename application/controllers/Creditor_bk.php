<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Creditor extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init(1);
        $this->load->model('Creditor_model');
        $this->load->model('Email_settings_model');
        //$this->load->library("Ad_lib");
        $this->load->library('Ad_lib', array('admin' => 1));
    }

    public function view_all() {
        check_p("creditor", "v");
        $this->session->set_userdata('active', '4');
        $data['title'] = $this->lang->line('creditors');
        $data['creditors'] = $this->db->get('CREDITOR')->result();
        $this->load->view("pages/creditor/index", $data);
    }

    public function check_blocked($pass) {
        if (check_password_blacklist(md5($pass))) {
            $this->form_validation->set_message('check_blocked', $this->lang->line('password_in_blocked_list'));
            return false;
        }
        return TRUE;
    }

    public function is_unique_username($username) {
        $exist = $this->db->get_where('CREDITOR', array('USERNAME' => strtolower($username)))->row();
        if ($exist) {
            $this->form_validation->set_message('is_unique_username', $this->lang->line('unique_username'));
            return FALSE;
        }
        return TRUE;
    }

    public function register() {
        $this->load->model("Settings_model");
        $password_management = $this->Settings_model->get_password_management();
        if ($this->input->post()) {
            $this->form_validation->set_rules("creditor_type", $this->lang->line("creditor_type"), "trim|required");
            $this->form_validation->set_rules("username", $this->lang->line("username"), "trim|required|callback_is_unique_username");
            $this->form_validation->set_rules("email", $this->lang->line("email"), "trim|required|valid_email|is_unique[CREDITOR.EMAIL]", array('is_unique' => $this->lang->line('email_not_required')));
            $this->form_validation->set_rules("password", $this->lang->line("password"), "trim|required|callback_check_blocked");
            $this->form_validation->set_rules("confirm_password", $this->lang->line("confirm_password"), "trim|required|matches[password]");
            $this->form_validation->set_rules("email_lang", $this->lang->line("email_lang"), "trim|required");
            if ($this->input->post('creditor_type') == 'egy_n_person') {
                $this->form_validation->set_rules("egy_creditor_name", $this->lang->line("creditor_name"), "trim|required");
                $this->form_validation->set_rules("egy_nationalID", $this->lang->line("nationalID"), "trim|required|numeric|exact_length[14]");
                $this->form_validation->set_rules("egy_date_of_birth", $this->lang->line("date_of_birth"), "trim|required");
                $this->form_validation->set_rules("egy_gender", $this->lang->line("gender"), "trim|required");
                $this->form_validation->set_rules("egy_mobile_code", $this->lang->line("mobile_code"), "trim|required");
                $this->form_validation->set_rules("egy_mobile_num", $this->lang->line("mobile_num"), "trim|required");
            } else if ($this->input->post('creditor_type') == 'foreign_n_person') {
                $this->form_validation->set_rules("foreign_creditor_name", $this->lang->line("creditor_name"), "trim|required");
                $this->form_validation->set_rules("foreign_passport_num", $this->lang->line("passport_num"), "trim|required");
                $this->form_validation->set_rules("foreign_issuance_country", $this->lang->line("issuance_country"), "trim|required");
                $this->form_validation->set_rules("foreign_nationality", $this->lang->line("nationality"), "trim|required");
                $this->form_validation->set_rules("foreign_date_of_birth", $this->lang->line("date_of_birth"), "trim|required");
                $this->form_validation->set_rules("foreign_mobile_code", $this->lang->line("mobile_code"), "trim|required");
                $this->form_validation->set_rules("foreign_mobile_num", $this->lang->line("mobile_num"), "trim|required");
            } else if ($this->input->post('creditor_type') == 'legal_entity') {
                $this->form_validation->set_rules("legal_institution_name", $this->lang->line("institution_name"), "trim|required");
                $this->form_validation->set_rules("legal_comm_reg_num", $this->lang->line("comm_reg_num"), "trim|required");
                $this->form_validation->set_rules("legal_establishment_date", $this->lang->line("establishment_date"), "trim|required");
                $this->form_validation->set_rules("legal_form", $this->lang->line("legal_form"), "trim|required");
                $this->form_validation->set_rules("legal_mobile_code", $this->lang->line("mobile_code"), "trim|required");
                $this->form_validation->set_rules("legal_mobile_num", $this->lang->line("mobile_num"), "trim|required");
            }

            if (!$this->form_validation->run()) {
                $data['title'] = $this->lang->line("CreateOrUpdateUser");
                $data['password_management'] = $password_management;
                $this->load->view("pages/creditor/register", $data);
            } else {

                $tab_pre = '';
                if ($this->input->post("creditor_type") == 'egy_n_person') {
                    $tab_pre = 'egy_';
                } else if ($this->input->post("creditor_type") == 'foreign_n_person') {
                    $tab_pre = 'foreign_';
                } else if ($this->input->post("creditor_type") == 'legal_entity') {
                    $tab_pre = 'legal_';
                }
                $mobile_num = $this->input->post($tab_pre . "mobile_num");
                if ($this->input->post($tab_pre . "mobile_code") == '20' && strlen($mobile_num) > 10) {
//                    if ($this->input->post($tab_pre . "mobile_code") == '02' && substr($mobile_num, 0, 1) == 0) {
                    $mobile_num = substr($mobile_num, strlen($mobile_num) - 10);
//                    }
                }
                $mobile_num = $this->input->post($tab_pre . "mobile_code") . $mobile_num;
                $params = array(
                    "USERNAME" => strtolower($this->input->post("username")),
                    "EMAIL" => $this->input->post("email"),
                    "EMAIL_LANG" => $this->input->post("email_lang"),
                    "ADDRESS" => $this->input->post("address"),
                    "PASSWORD" => md5($this->input->post("password")),
                    "CREDITOR_TYPE" => $this->input->post("creditor_type"),
                    "CREDITOR_NAME" => $this->input->post("creditor_type") != 'legal_entity' ? $this->input->post($tab_pre . "creditor_name") : '',
                    "NATIONAL_ID" => $this->input->post($tab_pre . "nationalID"),
                    "DATE_OF_BIRTH" => $this->input->post("creditor_type") != 'legal_entity' ? $this->input->post($tab_pre . "date_of_birth") : '',
                    "GENDER" => $this->input->post("creditor_type") != 'legal_entity' ? $this->input->post($tab_pre . "gender") : '',
                    "MOBILE_NUM" => $mobile_num,
                    "ADDITIONAL_INFO" => $this->input->post($tab_pre . "add_info"),
                    "PASSPORT_NUM" => $this->input->post("foreign_passport_num"),
                    "ISSUANCE_COUNTRY" => $this->input->post("foreign_issuance_country"),
                    "NATIONALITY" => $this->input->post("foreign_nationality"),
                    "INSTITUTION_NAME" => $this->input->post("legal_institution_name"),
                    "LEGAL_FORM" => $this->input->post("legal_form"),
                    "COMM_REGISTRY_NUM" => $this->input->post("legal_comm_reg_num"),
                    "ESTABLISHMENT_DATE" => $this->input->post("legal_establishment_date"),
                    "OUR_REG" => 1,
                    "TIMESTAMP" => date("Y-m-d H:i:s")
                );
//                if ($this->input->post("creditor_type") == 'legal_entity') {
//                    $params['MOBILE_CONFIRMED'] = 1;
//                }
                //insert in our data base
                //TODO get user id
                if($this->Global_model->global_insert("CREDITOR", $params)){
                    save_audit('new creditor has been registered with username: ' . $this->input->post("username"));
    
                    $this->db->order_by('ID', 'DESC');
                    $inserted_id = $this->db->get('CREDITOR')->row();
    
    
    //                if ($this->input->post('creditor_type') == 'egy_n_person' || $this->input->post('creditor_type') == 'foreign_n_person') {
                    $this->confirm_mobile_num($inserted_id->ID);
                    return;
                }else{
                    $data['title'] = $this->lang->line("CreateOrUpdateUser");
                    $data['password_management'] = $password_management;
                    $this->session->set_flashdata('error','Something went wrong !!');
                    $this->load->view("pages/creditor/register", $data);
                }
//                }
//                $this->session->set_flashdata('success', $this->lang->line('success_register'));
//                redirect(current_url());
            }
        } else {
            $sms_sett = $this->db->get('sms_settings')->row();
            if (!$sms_sett || $sms_sett->username == '' || $sms_sett->password == '' || $sms_sett->url == '' || $sms_sett->SENDER_ID == '') {
                echo $this->lang->line('missed_sms_sett');
                die;
            }
            $data['title'] = $this->lang->line("CreateOrUpdateUser");
            $data['password_management'] = $password_management;
            $this->load->view("pages/creditor/register", $data);
        }
    }

    function confirm_mobile_num($reg_id, $resend = 0) {
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        save_audit('confirm_mobile_num from '.$_SERVER['HTTP_REFERER'].': user ' . $reg_row->USERNAME);
        $data['title'] = $this->lang->line("mobile_verify");

        $first_time_send = $this->Creditor_model->generate_confrmation_code($reg_id, $resend);
        if (!$first_time_send) {
            $this->session->set_flashdata('error', $this->lang->line('sms_not_sent'));
        }
        $data['reg_id'] = $reg_id;
        $this->load->view("pages/creditor/mobile_confirmation", $data);
    }

    function check_code($reg_id) {
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        save_audit('check_code from '.$_SERVER['HTTP_REFERER'].': user ' . $reg_row->USERNAME);
        $this->form_validation->set_rules("confirm_num", $this->lang->line("confirm_num"), "trim|required|numeric");

        if (!$this->form_validation->run()) {
            $this->confirm_mobile_num($reg_id);
            return;
        }
        $this->load->model('Back_settings_model');
        $settings = $this->Back_settings_model->get_back_settings();
        check_param($settings, 'resource');
        $expiry = (int) $settings->SMS_CODE_EXPIRY;
        $confirm_code = $this->Creditor_model->check_code($this->input->post('confirm_num'), $reg_id, $expiry);

        if ($confirm_code['st']) {
            // send email to admin
            $this->email_admin($reg_id);
            save_audit('Mobile number confirmed on registration for creditor with username: ' . $reg_row->USERNAME);
            $this->session->set_flashdata('success', $this->lang->line('success_register'));
            redirect(base_url('Creditor/register'));
        } else {
            $this->session->set_flashdata('error', $this->lang->line(isset($confirm_code['expired']) ? 'expired_code' : 'wrong_code'));
            redirect(base_url('Creditor/confirm_mobile_num/' . $reg_id));
        }
    }

    function login() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() == FALSE) {
            $data['title'] = $this->lang->line("creditors");
            $this->load->view("pages/creditor/login", $data);
        } else {
            // $mobile = $this->input->post('mobile_code') . '' . $this->input->post('mobile');
            //$user = $this->Creditor_model->check_login($mobile);
            //$mob_num = '2017827785';
            $error_msg = $this->ad_lib->authenticate($this->input->post('username'), $this->input->post('password'));
            //$error_msg = array('1', '201004707161');

            if (is_array($error_msg)) {
                $mob_num = $error_msg[1];
                $user = $this->Creditor_model->check_login($this->input->post('username'), md5($this->input->post('password')));

                save_audit('Creditor logged in with username: ' . $this->input->post('username'));
                if ($user) {
                    $this->set_session($user);
                    $this->login_confirm_mobile_num($user->ID);
                } else {
                    if ($this->db->insert('CREDITOR', array('USERNAME' => strtolower($this->input->post('username')), 'PASSWORD' => md5($this->input->post('password')), 'MOBILE_NUM' => $mob_num))) {
                        $this->db->order_by('ID', 'DESC');
                        $last_id = $this->db->get('CREDITOR')->row();
                        $this->set_session($last_id);
                        $this->login_confirm_mobile_num($last_id->ID);
                    } else {
                        $this->session->set_flashdata('msg', $this->lang->line("cannot_create_user"));
                        redirect(current_url());
                    }
                }


                //active directory authentication
                //    $this->ad_authenticate($user->admin);
                //                $this->check_password_lock($user);
                //                $this->check_password_expiry($user);
                // $this->check_subscription($user->id);
                //redirect('Creditor/payment');
                //} else {
                //$this->session->set_flashdata('msg', $this->lang->line("invalid_credentials"));
                //redirect(current_url());
                //}
            } else if ($error_msg == -1) {  // Must reset user password
                $user = $this->Creditor_model->check_login($this->input->post('username'));
                if (!$user) {
                    if ($this->db->insert('CREDITOR', array('USERNAME' => strtolower($this->input->post('username')), 'PASSWORD' => md5($this->input->post('password'))))) {
                        $this->db->order_by('ID', 'DESC');
                        $last_id = $this->db->get('CREDITOR')->row();
                    } else {
                        $this->session->set_flashdata('msg', $this->lang->line("cannot_create_user"));
                        redirect(current_url());
                    }
                }
                $this->session->set_flashdata('msg', $this->lang->line('first_time_login'));
                redirect('creditor/reset_password');
            } else {
                save_audit('Creditor failed to logged in with username: ' . $this->input->post('username') . ' due to ' . $error_msg);
                $this->session->set_flashdata('msg', $error_msg);
                redirect(current_url());
            }
        }
    }

    function login_confirm_mobile_num($reg_id, $resend = 0, $error = FALSE) {
        $data['title'] = $this->lang->line("mobile_verify");

        $first_time_send = !$error ? $this->Creditor_model->login_generate_confrmation_code($reg_id, $resend) : TRUE;
        if (!$first_time_send) {
            $this->session->set_flashdata('error', $this->lang->line('sms_not_sent'));
        }
        $data['reg_id'] = $reg_id;
        $this->load->view("pages/creditor/login_mobile_confirmation", $data);
    }

//    function pre_login_check_code($reg_id) {
//        $this->session->set_flashdata('from_our_system', 1);
//        redirect(base_url("Creditor/login_check_code/$reg_id"));
//    }

    function post_pay() {
        if (!$this->session->flashdata('from_our_system')) {
            redirect(base_url('New_payment/pay_callback_creditor'));
        }

        $payment_sett = $this->db->get('payment_settings')->row();

        if (!$payment_sett) {
            $this->session->set_flashdata('error', $this->lang->line('payment_settings_error'));
            redirect(base_url("Creditor/login"));
        }

        $amount = $this->session->userdata('cre_amount');
        $currency = $this->session->userdata('cre_currency');

        $arr = array('amount' => $amount,
            'currency' => $currency,
            'type' => 'creditor',
            'payment_type' => 'Creditor Subscription',
            'payment_sett' => $payment_sett);
        $this->load->view('payment', $arr);
    }

    function login_check_code($reg_id) {

        $this->load->library('form_validation');
        $this->form_validation->set_rules("confirm_num", $this->lang->line("confirm_num"), "trim|required|numeric");

        if (!$this->form_validation->run()) {
            $this->login_confirm_mobile_num($reg_id, 0, TRUE);
            return;
        }

        $this->load->model('Back_settings_model');
        $settings = $this->Back_settings_model->get_back_settings();
        check_param($settings, 'resource');
        $expiry = (int) $settings->SMS_CODE_EXPIRY;
        $confirm_code = $this->Creditor_model->login_check_code($this->input->post('confirm_num'), $reg_id, $expiry);
        if ($confirm_code['st']) {
            //$this->session->set_flashdata('success', 'Login Success');
            $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
            save_audit('Mobile num. confirmed on login for a creditor with username: ' . $reg_row->USERNAME);
            if ($reg_row) {
//                $this->load->model('Rest_Api_model');
//                // Assign call params
//                $cp = [
//                    'auth' => $this->config->item('api_key'),
//                    'url' => $this->config->item('search_server_ip') . '/Rest_Api',
//                    'dt' => []
//                ];
//                $code = mt_rand(100000, 999999);
//
//                $cp['action'] = 'put_code';
//                $cp['params'] = array('code' => $code);
//                $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
//                if (!isset($response['success'])) {
//                    $this->session->set_flashdata('msg', $this->lang->line('error_occured'));
//                    redirect(base_url('creditor/login'));
//                }
//                $sub_from = $reg_row->PAYMENT_DATE ? strtotime($reg_row->PAYMENT_DATE) : NULL;
//                $sub_to = $sub_from ? strtotime("+1 year", $sub_from) : NULL;
//                if ($sub_from && $sub_to && $sub_to > time()) {
                if (!$reg_row->OUR_REG || ($reg_row->OUR_REG && $reg_row->PAYMENT_DATE)) {
                    // redirect($this->config->item('creditor_sub_redirect'));
                    $this->load->helper('cookie');
                    set_cookie('allow_redirect', '1');
                    $this->load->view('pages/private_page');
                    return;
                    // redirect to 27 , 28
                    //redirect($this->config->item('search_server_ip') . '/Creditor_redirect/redirect_to/' . $code);
                } else {
                    $pay = FALSE;
                    $currency = $settings->CREDITOR_CURRENCY;
                    $amount = $settings->CREDITOR_AMOUNT;
                    if ($currency && $amount) {
                        $this->session->set_userdata('cre_amount', $amount);
                        $this->session->set_userdata('cre_currency', $currency);
                        $this->session->set_flashdata('from_our_system', 1);
                        redirect(base_url('Creditor/post_pay'));
                        //$this->post_pay($amount, $currency);
                        //redirect(base_url('Creditor_payment/migs/' . $amount . '/' . $currency . '/' . $reg_id));
                    }
                    if (!$pay) {
                        redirect(base_url('Creditor/login'));
                    }
                }
            }
        } else {
            $error_msg = $this->lang->line(isset($confirm_code['expired']) ? 'expired_code' : 'wrong_code');
            save_audit('Failed to verify Mobile num. on login for a creditor with username: ' . $reg_row->USERNAME . ' due to ' . $error_msg);
            $this->session->set_flashdata('error', $error_msg);
            redirect(base_url('Creditor/login_confirm_mobile_num/' . $reg_id));
        }
    }

    function payment_error_page() {
        $arr = array('title' => $this->lang->line('payment_error'));
        $this->load->view('pages/creditor/pay_error', $arr);
    }

    function try_pay() {
        if ($this->session->userdata('cre_amount') && $this->session->userdata('cre_currency')) {
            $this->session->set_flashdata('from_our_system', 1);
            redirect(base_url('Creditor/post_pay'));
            return;
        } else {
            redirect(base_url('Creditor/login'));
        }
    }

    function set_session($user) {

        //add user data to session
        $new_data = array(
            'cre_user' => $user->USERNAME,
            'cre_user_id' => $user->ID,
            'cre_user_email' => $user->EMAIL,
            'cre_logged_in' => TRUE
        );
        $this->session->set_userdata($new_data);
    }

    function email_admin($reg_id) {
        $msg_content = '';
        //get Client info
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        if ($reg_row) {
            $creditor_type = '';
            if ($reg_row->CREDITOR_TYPE == 'egy_n_person') {
                $creditor_type = 'Egyptian Natural Person';
            } else if ($reg_row->CREDITOR_TYPE == 'legal_entity') {
                $creditor_type = 'Legal Entity operating in Egypt';
            } else if ($reg_row->CREDITOR_TYPE == 'foreign_n_person') {
                $creditor_type = 'Foreign Natural Person';
            }
            $email_lang = $reg_row->EMAIL_LANG == 0 ? 'English' : 'Arabic';
            $gender = $reg_row->GENDER == 0 ? 'Male' : 'Female';
            $msg_content .= "Dear Admin, <br>";
            $msg_content .= "New creditor has been registered.<br> Name: " . $reg_row->USERNAME . "<br> Email: " . $reg_row->EMAIL . "<br> CREDITOR TYPE: " . $creditor_type . "<br>"
                    . "Required language of the e-mail: $email_lang <br/>"
                    . "Address: " . $reg_row->ADDRESS . "<br/>";
            if ($reg_row->CREDITOR_TYPE == 'egy_n_person') {
                $msg_content .="Creditor name: " . $reg_row->CREDITOR_NAME . "<br/>"
                        . "National ID: " . $reg_row->NATIONAL_ID . "<br/>"
                        . "Date of Birth: " . date('Y-m-d', strtotime($reg_row->DATE_OF_BIRTH)) . "<br/>"
                        . "Gender: $gender <br/>"
                        . "Mobile num: " . $reg_row->MOBILE_NUM . "<br/>"
                        . "Additional Information: " . $reg_row->ADDITIONAL_INFO . "<br/>";
            } else if ($reg_row->CREDITOR_TYPE == 'legal_entity') {
                $msg_content .="Institution name: " . $reg_row->INSTITUTION_NAME . "<br/>"
                        . "Mobile num: " . $reg_row->MOBILE_NUM . "<br/>"
                        . "Commercial registry number or Identification number: " . $reg_row->COMM_REGISTRY_NUM . "<br/>"
                        . "Establishment Date: " . date('Y-m-d', strtotime($reg_row->ESTABLISHMENT_DATE)) . "<br/>"
                        . "Legal form: " . $reg_row->LEGAL_FORM . "<br/>"
                        . "Additional Information: " . $reg_row->ADDITIONAL_INFO . "<br/>";
            } else if ($reg_row->CREDITOR_TYPE == 'foreign_n_person') {
                $msg_content .="Creditor name: " . $reg_row->CREDITOR_NAME . "<br/>"
                        . "Passport num: " . $reg_row->PASSPORT_NUM . "<br/>"
                        . "Issuance country: " . $reg_row->ISSUANCE_COUNTRY . "<br/>"
                        . "Nationality: " . $reg_row->NATIONALITY . "<br/>"
                        . "Date of Birth: " . date('Y-m-d', strtotime($reg_row->DATE_OF_BIRTH)) . "<br/>"
                        . "Gender: $gender <br/>"
                        . "Mobile num: " . $reg_row->MOBILE_NUM . "<br/>"
                        . "Additional Information: " . $reg_row->ADDITIONAL_INFO . "<br/>";
            }
            $this->Global_model->send_mail($this->config->item('system_admin_email'), $msg_content, 'New Registration');
        }
    }

    // resend link ...

    public function reset_password() {
        $data = array('title' => 'Reset Password');
        $this->load->helper('captcha');
        $config = array(
            'img_url' => base_url() . 'uploads/',
            'img_path' => 'uploads/',
            'colors' => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
//                'grid' => array(155, 164, 171)
                'grid' => array(199, 87, 87)
            )
        );
        $captcha = create_captcha($config);
        $this->session->unset_userdata('valuecaptchaCode');
        $this->session->set_userdata('valuecaptchaCode', $captcha['word']);
        $data['captchaImg'] = base_url() . 'uploads/' . $captcha['filename'];
        $this->load->view("pages/creditor/forget_password", $data);
    }

    public function do_reset_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user', $this->lang->line("user"), 'trim|required');
        $this->form_validation->set_rules('captcha', 'Captcha', 'callback_validate_captcha');

        if ($this->form_validation->run() == FALSE) {
            $this->reset_password();
        } else {
            $error = $this->ad_lib->check_username_get_mobile($this->input->post('user'));
            if (is_array($error)) {
                $mobile = $error[1];

                $user_row = $this->db->get_where('CREDITOR', array('USERNAME' => strtolower($this->input->post('user'))))->row();
                if (!$user_row && $mobile == '') {
                    $this->session->set_flashdata('msg', $this->lang->line('mobile_not_set_ad'));
                    redirect(base_url('creditor/reset_password'));
                    // $this->reset_password();
                    //  return;
                }
                if ($mobile == '') {
                    $mobile = $user_row->MOBILE_NUM;
                }
                if (!$user_row) {
                    $data = array(
                        "USERNAME" => strtolower($this->input->post("user")),
                        "MOBILE_NUM" => $mobile,
                        "MOBILE_CONFIRMED" => 1
                    );
                    $this->db->insert('CREDITOR', $data);

                    $user_id = $this->db->order_by('ID', 'DESC')->get('CREDITOR')->row()->ID;
                } else {
                    if ($mobile != '') {
                        $this->db->update('CREDITOR', array('MOBILE_NUM' => $mobile), array('ID' => $user_row->ID));
                    }
                    $user_id = $user_row->ID;
                }

                save_audit('forget password: reset password MSG send to creditor:' . $this->input->post('user'));
                $this->confirm_mobile_num_forget_pass($user_id, 1);

//                    $this->session->set_flashdata('msg', 'This login name is not found');
//                    $this->reset_password();
                //redirect('Creditor/password_reset_success');
            } else {
                // user not exists
                save_audit('ERROR (Creditor): forget password: login name (' . $this->input->post('user') . ') : ' . $error);
                $this->session->set_flashdata('msg', $error);
                $this->reset_password();
            }
        }
    }

    function confirm_mobile_num_forget_pass($reg_id, $resend = 0) {
        
        $data['title'] = $this->lang->line("mobile_verify");

        $first_time_send = $this->Creditor_model->generate_confrmation_code_forget_password($reg_id, $resend);
        if (!$first_time_send) {
            $this->session->set_flashdata('error', $this->lang->line('sms_not_sent'));
        }
        $data['reg_id'] = $reg_id;
        $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
        save_audit('Creditor ' . $reg_row->USERNAME . ' make a forget password request');
        
        $this->load->view("pages/creditor/forget_mobile_confirmation", $data);
    }

    function check_code_forget_password($reg_id) {
        $this->form_validation->set_rules("confirm_num", $this->lang->line("confirm_num"), "trim|required|numeric");

        if (!$this->form_validation->run()) {
            $this->confirm_mobile_num_forget_pass($reg_id);
            return;
        }
        $this->load->model('Back_settings_model');
        $settings = $this->Back_settings_model->get_back_settings();
        check_param($settings, 'resource');
        $expiry = (int) $settings->SMS_CODE_EXPIRY;
        $confirm_code = $this->Creditor_model->check_code_forget_pass($this->input->post('confirm_num'), $reg_id, $expiry);

        if ($confirm_code['st']) {
            $reg_row = $this->db->get_where('CREDITOR', array('ID' => $reg_id))->row();
            save_audit('Mobile number confirmed on forget password for creditor with username: ' . $reg_row->USERNAME);
            $this->session->set_userdata('creditor_reset_pass_code', $this->input->post('confirm_num'));
            redirect(base_url('Creditor/reset_my_password'));
        } else {
            $error = $this->lang->line(isset($confirm_code['expired']) ? 'expired_code' : 'wrong_code');
            save_audit('Creditor Error username: ' . $reg_row->USERNAME . ' on confirming the Mobile number(forget password request): ' . $error);
            $this->session->set_flashdata('error', $error);
            redirect(base_url('Creditor/confirm_mobile_num_forget_pass/' . $reg_id));
        }
    }

    public function reset_my_password() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|callback_check_blocked');
        if (!$this->form_validation->run()) {
            $data = array('title' => 'Reset Password');
            $this->load->model("Settings_model");
            $data['password_management'] = $this->Settings_model->get_password_management();
            $this->load->view("pages/creditor/reset_my_password", $data);
        } else {
            //verify token
            if (!$this->session->userdata('creditor_reset_pass_code')) {
                $this->session->flashdata('error', $this->lang->line('error_occured'));
                redirect(base_url('creditor/login'));
            }

            $user = $this->db->get_where("CREDITOR", array('FORGET_PASSWORD_NUM' => $this->session->userdata('creditor_reset_pass_code')))->row();

            if ($user) {
                // $ad_data['password'] = $this->input->post("password");
                $error_msg = $this->ad_lib->edit_user_password($user->USERNAME, $this->input->post("password"));
                //$error_msg = $this->ad_lib->edit_user($user->user, $ad_data);
                if ($error_msg == 1) {
                    $data = ["PASSWORD" => md5($this->input->post("password"))];
                    $this->db->update('CREDITOR', $data, array('ID' => $user->ID));
                    $this->session->unset_userdata('creditor_reset_pass_code');
                    $this->session->set_flashdata('msg', $this->lang->line('password_chanhged_success'));
                    save_audit('Creditor with username: ' . $user->USERNAME . ' set new password with forget password request');
                    redirect(base_url('Creditor/Login'));
                } else {
                    save_audit('Creditor with username: ' . $user->USERNAME . ' failed to set new password with forget password request due to : ' . $error_msg);
                    $this->session->set_flashdata('msg', $error_msg);
                    redirect(base_url("creditor/reset_password"));
                }
            } else {
                // user not exists
                $this->session->set_flashdata('msg', $this->lang->line('link_expired'));
                redirect(base_url("creditor/reset_password"));
            }
        }
    }

    public function validate_captcha() {
        //return true;
        if ($this->input->post('captcha') != $this->session->userdata['valuecaptchaCode']) {
            $this->form_validation->set_message('validate_captcha', $this->lang->line('captcha_code_is_wrong'));
            return false;
        } else {
            return true;
        }
    }

    // to be moved on // 29 aw 28
//    function test_api() {
//        $this->load->model('Rest_Api_model');
//        // Assign call params
//        $cp = [
//            'auth' => $this->config->item('api_key'),
//            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
//            'dt' => []
//        ];
//
//        $cp['action'] = 'put_code';
//        $cp['params'] = array('code' => mt_rand(100000, 999999));
//        $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
//    }

    function test_re() {
        $this->load->model('Rest_Api_model');
        // Assign call params
        $cp = [
            'auth' => $this->config->item('api_key'),
            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
            'dt' => []
        ];
        $code = mt_rand(100000, 999999);

        $cp['action'] = 'put_code';
        $cp['params'] = array('code' => $code);
        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
        if (!isset($response['success'])) {
            $this->session->set_flashdata('msg', 'Login Error. Please try to login again');
            redirect(base_url('creditor/login'));
        }
//                $sub_from = $reg_row->PAYMENT_DATE ? strtotime($reg_row->PAYMENT_DATE) : NULL;
//                $sub_to = $sub_from ? strtotime("+1 year", $sub_from) : NULL;
//                if ($sub_from && $sub_to && $sub_to > time()) {
        //  redirect($this->config->item('creditor_sub_redirect'));
        // redirect to 27 , 28
        //  redirect($this->config->item('search_server_ip') . '/Creditor_redirect/redirect_to/' . $code.'123');
        $cp = [
            'auth' => $this->config->item('api_key'),
            'url' => $this->config->item('search_server_ip') . '/Rest_Api',
            'dt' => []
        ];
        $code = mt_rand(100000, 999999);

        $cp['action'] = 'put_code';
        $cp['params'] = array('code' => $code);
        $response = $this->Rest_Api_model->Curl($cp['auth'], $cp['url'], $cp['action'], $cp['dt'], $cp['params']);
        if (!isset($response['success'])) {
            $this->session->set_flashdata('msg', 'Login Error. Please try to login again');
            redirect(base_url('creditor/login'));
        }
    }

}
