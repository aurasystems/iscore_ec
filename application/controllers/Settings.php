<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        construct_init();
        $this->load->model("Settings_model");
        check_p("settings", "c");
        $this->session->set_userdata('active', '6');
    }

    public function password_management() {
        $settings = $this->Settings_model->get_password_management();
        $this->form_validation->set_rules("enforce_history", $this->lang->line("enforce_history"), "xss_clean|trim");
        $this->form_validation->set_rules("max_age", $this->lang->line("max_age"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("min_length", $this->lang->line("min_length"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("complex", $this->lang->line("complex"), "xss_clean|trim");
        $this->form_validation->set_rules("lockout_duration", $this->lang->line("lockout_duration"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("lockout_threshold", $this->lang->line("lockout_threshold"), "xss_clean|trim|numeric");
        $this->form_validation->set_rules("reset_counter", $this->lang->line("reset_counter"), "xss_clean|trim|numeric");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("password_management");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->session->set_userdata('sub_active', '23');
            $this->load->view("pages/settings/password_management", $data);
        } else {
            $data = array(
                'enforce_history' => $this->input->post("enforce_history"),
                'max_age' => $this->input->post("max_age"),
                'min_length' => $this->input->post("min_length"),
                'complex' => $this->input->post("complex"),
                'lockout_duration' => $this->input->post("lockout_duration"),
                'lockout_threshold' => $this->input->post("lockout_threshold"),
                'reset_counter' => $this->input->post("reset_counter")
            );
            if (!empty($settings)) {
                /**/ $this->Global_model->global_update("password_management", $settings->id, $data);
            } else {
                /**/ $this->Global_model->global_insert("password_management", $data);
            }
            save_audit('update password managment settings');
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Settings/password_management"));
        }
    }

    public function active_directory() {
        $settings = $this->Settings_model->get_active_directory();
        $this->form_validation->set_rules("hostname", $this->lang->line("hostname"), "xss_clean|trim|required");
        $this->form_validation->set_rules("dc", $this->lang->line("dc"), "xss_clean|trim|required");
        $this->form_validation->set_rules("ou", $this->lang->line("ou"), "xss_clean|trim|required");
        $this->form_validation->set_rules("admin_dn", $this->lang->line("admin_dn"), "xss_clean|trim|required");
        $this->form_validation->set_rules("admin_password", $this->lang->line("admin_password"), "xss_clean|trim|required");
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("active_directory_settings");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->session->set_userdata('sub_active', '25');
            $this->load->view("pages/settings/active_directory", $data);
        } else {
            $data = array(
                'hostname' => $this->input->post("hostname"),
                'dc' => $this->input->post("dc"),
                'ou' => $this->input->post("ou"),
                'admin_dn' => $this->input->post("admin_dn"),
                'admin_password' => $this->input->post("admin_password"),
                'second_hostname' => $this->input->post("second_hostname"),
                'second_dc' => $this->input->post("second_dc"),
                'second_ou' => $this->input->post("second_ou"),
                'second_admin_dn' => $this->input->post("second_admin_dn"),
                'second_admin_password' => $this->input->post("second_admin_password"),
            );
            if (!empty($settings)) {
                /**/ $this->Global_model->global_update("active_directory", $settings->id, $data);
            } else {
                /**/ $this->Global_model->global_insert("active_directory", $data);
            }
            save_audit('update active directory settings');
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Settings/active_directory"));
        }
    }

    public function payment() {
        $settings = $this->Settings_model->get_settings("payment_settings");
        $this->form_validation->set_rules("merchant_id", $this->lang->line("merchant_id"), 'trim|required');
        $this->form_validation->set_rules("merchant_name", $this->lang->line("merchant_name"), 'trim|required');
        $this->form_validation->set_rules("api_username", $this->lang->line("api_username"), 'trim|required');
        $this->form_validation->set_rules("api_password", $this->lang->line("api_password"), 'trim|required');

//        $this->form_validation->set_rules("inst_secure_secret",$this->lang->line("inst_secure_secret"),'trim|required');
//        $this->form_validation->set_rules("inst_access_code",$this->lang->line("inst_access_code"),'trim|required');
//        $this->form_validation->set_rules("inst_merchant_id",$this->lang->line("inst_merchant_id"),'trim|required');
        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("payment_settings");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->session->set_userdata('sub_active', '26');
            $this->load->view("pages/settings/payment", $data);
        } else {
            $data = array(
                'API_PASSWORD' => $this->input->post("api_password"),
                'API_USERNAME' => $this->input->post("api_username"),
                'MERCHANT_NAME' => $this->input->post("merchant_name"),
                'MERCHANT_ID' => $this->input->post("merchant_id"),
//                'inst_secure_secret' => $this->input->post("inst_secure_secret"),
//                'inst_access_code' => $this->input->post("inst_access_code"),
//                'inst_merchant_id' => $this->input->post("inst_merchant_id")
            );
            if (!empty($settings)) {
                /**/ $this->Global_model->global_update("payment_settings", $settings->id, $data);
            } else {
                /**/ $this->Global_model->global_insert("payment_settings", $data);
            }

            save_audit('update online payment gateway settings');
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(current_url());
        }
    }

    public function sms() {
        $settings = $this->Settings_model->get_settings("sms_settings");
        // $this->form_validation->set_rules("url",$this->lang->line("url"),'trim|required');
        $this->form_validation->set_rules("gateway_type", $this->lang->line("sender_id"), 'trim|required');
        $v = '';
        if ($this->input->post('gateway_type') == 1) {
            $v = 'v_';
        }
        $this->form_validation->set_rules($v . "username", $this->lang->line("username"), 'trim|required');
        $this->form_validation->set_rules($v . "password", $this->lang->line("password"), 'trim|required');
        $this->form_validation->set_rules($v . "SENDER_ID", $this->lang->line("sender_id"), 'trim|required');


        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("sms_settings");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['row'] = $settings;
            $this->session->set_userdata('sub_active', '27');
            $this->load->view("pages/settings/sms", $data);
        } else {
            $data = array(
                // 'url' => $this->input->post("url"),
                'username' => $this->input->post("username"),
                'password' => $this->input->post("password"),
                'SENDER_ID' => $this->input->post("SENDER_ID"),
                'gateway_type' => $this->input->post('gateway_type'),
                'v_username' => $this->input->post("v_username"),
                'v_password' => $this->input->post("v_password"),
                'v_SENDER_ID' => $this->input->post("v_SENDER_ID"),
            );
            if (!empty($settings)) {
                /**/ $this->Global_model->global_update("sms_settings", $settings->id, $data);
            } else {
                /**/ $this->Global_model->global_insert("sms_settings", $data);
            }

            save_audit('update SMS gateway settings');
            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(current_url());
        }
    }

    function audit_trail() {
        $settings = $this->Settings_model->get_audit_trail_settings();
        $this->form_validation->set_rules("audits_expiry", $this->lang->line("audits_expiry"), "xss_clean|trim|required|numeric");
        //$this->form_validation->set_rules("audits_file_path", $this->lang->line("audits_file_path"), "xss_clean|trim|required");

        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("audit_trail_settings");
            $data['label_att'] = 'col-sm-6 control-label';
            $data['settings'] = $settings;
            $this->session->set_userdata('sub_active', '24');
            $this->load->view("pages/settings/audit_trail", $data);
        } else {

            if (!empty($settings)) {
                $data = array(
                    "audits_expiry" => $this->input->post("audits_expiry"),
                        //"audits_file_path" => $this->input->post("audits_file_path"),
                );
                $this->Global_model->global_update("back_settings", $settings->id, $data);
            } else {
                $this->Global_model->global_insert("back_settings", $data);
            }
            // audit
            save_audit("update audit trail settings");
            /////////

            $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            redirect(base_url("Settings/audit_trail"));
        }
    }

    function password_black_list() {
        $settings = $this->Settings_model->get_password_management();
        $this->form_validation->set_rules("passwords", $this->lang->line("passwords"), "xss_clean|trim|required");

        if (!$this->form_validation->run()) {
            $data['title'] = $this->lang->line("password_black_list");
            $data['label_att'] = 'col-sm-2 control-label';
            $data['row'] = $settings;
            $this->session->set_userdata('sub_active', '28');
            $this->load->view("pages/settings/password_black_list", $data);
        } else {
            $passwords = explode(PHP_EOL, $this->input->post('passwords'));
            $inserted_pass = array();
            foreach ($passwords as $password) {
                if ($this->input->post('action') == 'add') {
                    $pass_exist = $this->db->get_where('PASSWORD_BLACKLIST', array('PASSWORD' => md5($password)))->row();
                    if (!$pass_exist) {
                        $inserted_pass[] = array('PASSWORD' => md5($password));
                    }
                } else {
                    $this->db->delete('PASSWORD_BLACKLIST', array('PASSWORD' => md5($password)));
                }
            }
            if ($this->input->post('action') == 'add') {
                $this->db->insert_batch('PASSWORD_BLACKLIST', $inserted_pass);
                save_audit('add new passwords to password blacklist');
                $this->session->set_flashdata("success", $this->lang->line("data_submitted_successfully"));
            } else {
                save_audit('delete passwords from password blacklist');
                $this->session->set_flashdata("success", $this->lang->line("data_deleted_successfully"));
            }

            redirect(base_url("Settings/password_black_list"));
        }
    }

}
