<html lang="<?= $this->lang->line('system_lang') ?>" dir="<?= $this->lang->line('system_dir') ?>">
    <head>
        <?php $this->load->view('private/head'); ?>
        <link href="<?= base_url() ?>assets/plugins/charts-nvd3/src/nv.d3.css" rel="stylesheet" type="text/css"/>
    </head>
    <body class="fixed-topbar fixed-sidebar theme-sdtl color-default <?= $this->lang->line('system_dir') ?>">        
        <section>
            <?php $this->load->view('private/sidebar'); ?>
            <div class="main-content">
                <?php $this->load->view('private/topbar'); ?>
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-thin">
                    <div class="row">
                        <div class="col-lg-12 portlets">
                            <?= flash_success() ?>
                            <?= flash_error() ?>
                            <div class="panel">
                                <div class="panel-header bg-primary">
                                    <h2 class="panel-title"><?= $this->lang->line("users") ?></h2>
                                </div>
                                <div class="panel-body">
                                    <?php if (get_p("users", "c")) { ?>
                                        <div class="text-center">
                                            <a class="btn btn-primary" href="<?= base_url() ?>Users/add"><?= $this->lang->line("add_user") ?></a>
                                        </div>
                                    <?php } ?>
                                    <table class="table table-dynamic">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th><?= $this->lang->line('clientName') ?></th>
                                                <th><?= $this->lang->line('user') ?></th>
                                                <th><?= $this->lang->line('address') ?></th>
                                                <th><?= $this->lang->line('phoneNumber') ?></th>
                                                <th><?= $this->lang->line('email') ?></th>
                                                <th><?= $this->lang->line('subscription') ?></th>
                                                <th><?= $this->lang->line('action') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 1;
                                            foreach ($users as $one) {
                                                ?>
                                                <tr>
                                                    <td><?= $i++ ?></td>
                                                    <td><?= $one->clientName ?></td>
                                                    <td><?= $one->user ?></td>
                                                    <td><?= $one->address ?></td>
                                                    <td><?= $one->phoneNumber ?></td>
                                                    <td><?= $one->email ?></td>
                                                    <td><?= $one->subscription ? "<span><i class='fa fa-check c-green f-24'></i><span>" : "<span><i class='fa fa-times c-red f-24'></i><span>" ?></td>
                                                    <td>
                                                        <div class="btn-group">
                                                            <?php if (get_p("user_payments", "v")) { ?>
                                                                <a title="<?= $this->lang->line("user_payments") ?>" href="<?= base_url() ?>Users/user_payments/<?= $one->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-money"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p("users", "u")) { ?>
                                                                <a href="<?= base_url() ?>Users/edit/<?= $one->id ?>" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>
                                                                <a href="<?= base_url() ?>Users/change_password/<?= $one->id ?>" class="btn btn-sm btn-success"><i class="fa fa-lock"></i></a>
                                                            <?php } ?>
                                                            <?php if (get_p("users", "d")) { ?>
                                                                <a href="#" data-id="<?= $one->id ?>" class="delete btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                                                <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $this->load->view('private/copyright'); ?>
                </div>
                <!-- END PAGE CONTENT -->
            </div>
        </section>
        <?php $this->load->view('private/quick_sidebar'); ?>
        <?php $this->load->view('private/search'); ?>
        <?php $this->load->view('private/preloader'); ?>
        <?php $this->load->view('private/footer'); ?>
        <script src="<?= base_url() ?>assets/plugins/countup/countUp.min.js"></script> <!-- Animated Counter Number -->
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/lib/d3.v3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/nv.d3.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/legend.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pie.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/models/pieChart.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/charts-nvd3/src/utils.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script> <!-- Tables Filtering, Sorting & Editing -->
        <script src="<?= base_url() ?>assets/js/pages/table_dynamic.js"></script>
        <script>
            $(document).ready(function () {

                $(document).on("click", ".delete", function (e) {
                    e.preventDefault();
                    var id = $(this).attr("data-id");
                    var conf = confirm("<?= $this->lang->line("r_u_sure") ?>");
                    if (conf) {
                        window.location = "<?= base_url() ?>Users/delete/" + id;
                    }
                });
              
            });
        </script>
    </body>
</html>