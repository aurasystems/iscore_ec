<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_model extends CI_Model {

    function get_subscription($user) {
        $this->db->where("user", $user);
        //$this->db->where("subscription", 1);
        $query = $this->db->get("users")->row();
        if ($query) {
            $today = date("Y-m-d");
            if (($today >= $query->startDateOfSubscription && $today <= $query->endDateOfSubscription) || $query->ONE_TIME_SEARCH) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
